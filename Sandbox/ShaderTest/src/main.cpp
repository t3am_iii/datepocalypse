#if !defined(NDEBUG)
  #pragma comment(lib, "sfml-main-d.lib")
  #pragma comment(lib, "sfml-window-d.lib")
  #pragma comment(lib, "sfml-system-d.lib")
  #pragma comment(lib, "sfml-graphics-d.lib")
  #pragma comment(lib, "sfml-audio-d.lib")
#else
  #pragma comment(lib, "sfml-main.lib")
  #pragma comment(lib, "sfml-window.lib")
  #pragma comment(lib, "sfml-system.lib")
  #pragma comment(lib, "sfml-graphics.lib")
  #pragma comment(lib, "sfml-audio.lib")
#endif

#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "Engine.lib")

#include <ShaderTest\ShaderScene.h>

#include <Engine\Engine.h>


int main(int argc, char** argv)
{
	dpEngine::Engine engine;
	engine.startup(1920, 1080, "Datepocalypse - Shader Test");

	engine.addScene(new ShaderScene, true);
	engine.run();
	engine.shutdown();
}
