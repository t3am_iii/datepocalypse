#include <ShaderTest\ShaderScene.h>

#include <Engine\SystemInfo.h>
#include <Engine\InputSystem\InputManager.h>

#include <SFML\Graphics\RenderWindow.hpp>
#include <SFML\OpenGL.hpp>

void ShaderScene::onEnter(const SystemInfo&)
{
	GLenum err;
	if (!m_pixelateShader.loadFromFile("../assets/shaders/pixelate.frag", sf::Shader::Fragment))
		err = glGetError();

	m_testVertShader.loadFromFile("../assets/shaders/test.vert", sf::Shader::Vertex);
	m_testFragShader.loadFromFile("../assets/shaders/test.frag", sf::Shader::Vertex);
	m_testCombShader.loadFromFile("../assets/shaders/test.vert", "../assets/shaders/test.frag");

	if (!m_texture.loadFromFile("../assets/textures/female_character.png"))
		err = glGetError();

	m_pixelateShader.setParameter("texture", sf::Shader::CurrentTexture);
	m_pixelateShader.setParameter("pixel_threshold", 8.f / 512.f);

	const int WIDTH = 512;
	const int HEIGHT = 512;

	m_testFragShader.setParameter("texture", sf::Shader::CurrentTexture);

	m_testCombShader.setParameter("u_viewportInverse", 1.f / WIDTH, 1.f / HEIGHT);
	m_testCombShader.setParameter("u_offset", 1.f);
	m_testCombShader.setParameter("u_step", std::min(1.f, WIDTH / 70.f));
	m_testCombShader.setParameter("u_color", 0.f, 0.f, 1.f);

	m_sprite.setTexture(m_texture);
	m_sprite.setPosition(200.f, 200.f);



	// BlendMode Test
	// Players & Shadows
	m_playerTexture.loadFromFile("../assets/textures/female_spritesheet.png", sf::IntRect(0, 0, 512, 512));
	m_shadowTexture.loadFromFile("../assets/textures/shadow.png");

	m_playerSprites[0].setTexture(m_playerTexture);
	m_playerSprites[1].setTexture(m_playerTexture);
	m_shadowSprites[0].setTexture(m_shadowTexture);
	m_shadowSprites[1].setTexture(m_shadowTexture);


	m_playerSprites[0].setPosition(0.f, 0.f);
	m_playerSprites[1].setPosition(128.f - 60.f, 512.f - 100.f);
	m_shadowSprites[0].setPosition(256.f - 80.f, 512.f - 120.f);
	m_shadowSprites[1].setPosition(0.f, 0.f);
}

void ShaderScene::onUpdate(float, const SystemInfo& sysInfo)
{
	//
}

void ShaderScene::onRender(sf::RenderWindow& window)
{
	glClearColor(0.f, 0.f, 0.f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	auto state = sf::RenderStates();
	state.shader = &m_pixelateShader;

	//window.draw(m_sprite, state);

	
	sf::BlendMode blendMode = sf::BlendNone;
	window.draw(m_playerSprites[1]);

	blendMode = sf::BlendAdd;
	blendMode.colorSrcFactor = sf::BlendMode::Factor::One;
	blendMode.colorDstFactor = sf::BlendMode::Factor::OneMinusSrcAlpha;
	window.draw(m_shadowSprites[0], blendMode);

	blendMode = sf::BlendNone;
	window.draw(m_playerSprites[0]);
}

void ShaderScene::onExit(const SystemInfo&)
{
	//
}
