#pragma once

#include <ShaderTest\ShaderScene.h>

#include <Engine\SceneSystem\BaseScene.h>

#include <SFML\Graphics\Shader.hpp>
#include <SFML\Graphics\Texture.hpp>
#include <SFML\Graphics\Sprite.hpp>

using namespace dpEngine;

class ShaderScene : public SceneSystem::BaseScene
{
private:
	sf::Shader m_pixelateShader;

	sf::Shader m_testVertShader;

	sf::Shader m_testFragShader;

	sf::Shader m_testCombShader;

	sf::Texture m_texture;

	sf::Sprite m_sprite;

	sf::Texture m_playerTexture;

	sf::Texture m_shadowTexture;

	sf::Sprite m_playerSprites[2];

	sf::Sprite m_shadowSprites[2];

public:
	ShaderScene() { }

	~ShaderScene() { }

	void onEnter(const SystemInfo&);

	void onFadeIn() { }

	void onUpdate(float, const SystemInfo&);

	bool nextScene(int&) const { return false; }

	bool shouldClose() const { return false; }

	void onRender(sf::RenderWindow&);

	void onFadeOut() { }

	void onExit(const SystemInfo&);

};
