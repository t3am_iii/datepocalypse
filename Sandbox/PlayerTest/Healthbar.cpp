#include "Healthbar.h"

#include <SFML\Graphics\Texture.hpp>

Healthbar::Healthbar()
{
	
	m_pxTexture = new sf::Texture();
	m_pxTexture->loadFromFile("../assets/heart.png", sf::IntRect(0,560,320,300));
	m_pxSprite->setTexture(*m_pxTexture);
	
	for (int i = 0; i < 3; i++)
	{
		sf::RectangleShape* rect = new sf::RectangleShape();
		rect->setSize(sf::Vector2f(32, 30));
		rect->setPosition(i, i * 20);
		rect->setTexture(m_pxSprite->getTexture());
		
		m_aHP.push_back(rect);
	}

	m_iHealthPoints = 3;
}

Healthbar::~Healthbar()
{

}

void Healthbar::Update(float p_fDeltaTime)
{

}


