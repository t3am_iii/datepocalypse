#include "EnemyProjectile.h"
#include "Global.h"
#include "Enums.h"

#include <SFML\Graphics\RenderTarget.hpp>

EnemyProjectile::EnemyProjectile(sf::Texture* p_xTexture, sf::Texture* trailTexture, float p_fX, float p_fY, float p_fSpeed)
{
	m_speed = p_fSpeed;
	m_sprite.setTexture(*p_xTexture);
	m_sprite.setTextureRect(sf::IntRect(0, 213, 167, 175));
	m_sprite.setScale(.3f, .3f);
	m_sprite.setOrigin(167 * .3f, 175 * .3f);

	m_animTrail.setSpriteSheet(*trailTexture);
	m_animTrail.addFrame(sf::IntRect(0, 0, 256, 768));
	m_animTrail.addFrame(sf::IntRect(256, 0, 256, 768));
	m_animTrail.addFrame(sf::IntRect(512, 0, 256, 768));
	m_animTrail.addFrame(sf::IntRect(768, 0, 256, 768));
	m_animTrail.addFrame(sf::IntRect(1024, 0, 256, 768));
	m_animTrail.addFrame(sf::IntRect(1280, 0, 256, 768));
	m_animTrail.addFrame(sf::IntRect(1536, 0, 256, 768));

	m_animSpriteTrail.setScale(0.25f, 0.25f);
	m_animSpriteTrail.setOrigin(150, 50);
	m_animSpriteTrail.setRotation(180);
	m_animSpriteTrail.play(m_animTrail);

	pDotText.loadFromFile("../assets/Particles/dot_electric.png");
	pLineText.loadFromFile("../assets/Particles/line_electric.png");
	pLineText2.loadFromFile("../assets/Particles/line_electric2.png");
	pAmount = 0;
}

EnemyProjectile::~EnemyProjectile()
{

}

void EnemyProjectile::Update(float p_fDeltaTime)
{
	if (m_particles.size() < 10)
	{
		EProjectileParticle* p = new EProjectileParticle;
		m_particles.push_back(p);
		if (pAmount % 3 == 0)
			m_particles.at(pAmount)->sprite.setTexture(pDotText);
		else if(pAmount % 3 == 1)
			m_particles.at(pAmount)->sprite.setTexture(pLineText);
		else
			m_particles.at(pAmount)->sprite.setTexture(pLineText2);

		m_particles.at(pAmount)->startPos = sf::Vector2f(100, 100);
		m_particles.at(pAmount)->sprite.setPosition(m_particles.at(pAmount)->startPos);
		m_particles.at(pAmount)->sprite.setRotation(180);//rand() % 4 + 1);
		m_particles.at(pAmount)->ttl = .1f;
		pAmount++;
	}

	for (int i = 0; i < m_particles.size(); i++)
	{
		float r1 = rand() % 20 - 10;
		float r2 = rand() % 3 - 9;
		int r3 = rand() % 255 + 120;
		if (m_particles.at(i)->ttl < 0)
		{
			m_particles.at(i)->sprite.setPosition(getPosition().x + r1, getPosition().y);
			m_particles.at(i)->sprite.setColor(sf::Color(255, 255, 255, 255));
			m_particles.at(i)->sprite.setRotation(rand() % 10 - 5);
			m_particles.at(i)->sprite.setScale(1.2f, 1.2f);//rand() % 2 + 1, rand() % 2 + 1);
			m_particles.at(i)->ttl = .1f;
		}
		else
		{
			//m_particles.at(i)->sprite.setScale(rand() % 3 + 1, rand() % 3 + 1);
			//m_particles.at(i)->sprite.move(sf::Vector2f(r1, r2));
			//m_particles.at(i)->sprite.setColor(sf::Color(255, r3, r3, m_particles.at(i)->ttl * 510));
		}
		m_particles.at(i)->ttl -= p_fDeltaTime;
	}

	addPosition(m_dir * m_speed * p_fDeltaTime);

	if (getPosition().y > SCREEN_HEIGHT)
		SetIsActive(false);

	m_animSpriteTrail.update(p_fDeltaTime * 2000);
}

bool EnemyProjectile::IsActive() { return m_isActive; }

void EnemyProjectile::SetIsActive(bool p_bValue)
{
	body->setActive(p_bValue);
	m_isActive = p_bValue;
}

void EnemyProjectile::setDirection(sf::Vector2f dir) { m_dir = dir; }

EENTITYTYPE EnemyProjectile::GetType() { return ENTITY_ENEMY_PROJECTILE; }

void EnemyProjectile::onConstruction(const GameObjectCreationKit& kit)
{
	shape = new CircleShape(10.0f);
	BodyDef def(EBodyTypes::kKinematicBody, FILTER(EEntityCategory::kEnemyProjectile, ~EEntityCategory::kEnemyProjectile), false, shape, this);
	body = kit.collisionMngr->createBody(def, this);

	m_dir = sf::Vector2f(0.f, 1.f);

	SetIsActive(true);
}

void EnemyProjectile::onDestruction(const GameObjectCreationKit& kit)
{
	kit.collisionMngr->destroyBody(body);
	body = nullptr;

	delete shape;
	shape = nullptr;
}

void EnemyProjectile::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	states.transform *= getTransform();
	target.draw(m_sprite, states);
	//target.draw(m_animSpriteTrail, states);
	for (int i = 0; i < m_particles.size(); i++)
		target.draw(m_particles.at(i)->sprite);
}