#pragma once;

#include "AudioManager.h"
#include <SFML\Audio\SoundBuffer.hpp>
#include <SFML\Audio\Sound.hpp>
#include <SFML\Audio\Music.hpp>
#include <iostream>

AudioManager::AudioManager()
{

}

AudioManager::~AudioManager()
{
	unloadSounds();
	unloadMusic();
}


void AudioManager::loadSounds(const std::string& filePath)
{
	sf::SoundBuffer* sfxBuffer;
	auto bit = m_buffers.find(filePath);
	if (bit == m_buffers.end())
	{

		sfxBuffer = new sf::SoundBuffer;
		sfxBuffer->loadFromFile(filePath.c_str());
		if (sfxBuffer == nullptr)
		{
			std::cout << "Failed to load: " << filePath << std::endl;
		}

		m_buffers.insert(std::pair<std::string, sf::SoundBuffer*>(filePath, sfxBuffer));
	
		sf::Sound* sfxSound;
		auto sit = m_sounds.find(filePath);
		if (sit == m_sounds.end())
		{
			sfxSound = new sf::Sound;
			sfxSound->setBuffer(*sfxBuffer);

			m_sounds.insert(std::pair<std::string, std::vector< sf::Sound*>>(filePath, std::vector<sf::Sound*>({ sfxSound })));
		}
	}
}


sf::Sound* AudioManager::createSounds(const std::string& p_sFilepath)
{
	sf::SoundBuffer* sfxBuffer;
	auto bit = m_buffers.find(p_sFilepath);
	if (bit == m_buffers.end())
	{

		sfxBuffer = new sf::SoundBuffer;
		sfxBuffer->loadFromFile(p_sFilepath.c_str());
		if (sfxBuffer == nullptr)
		{
			std::cout << "Failed to load: " << p_sFilepath << std::endl;
			return nullptr;
		}

		m_buffers.insert(std::pair<std::string, sf::SoundBuffer*>(p_sFilepath, sfxBuffer));
		bit = m_buffers.find(p_sFilepath);
	}
	else
	{
		sfxBuffer = (bit)->second;
	}

	sf::Sound* sfxSound;
	auto sit = m_sounds.find(p_sFilepath);
	if (sit == m_sounds.end())
	{
		sfxSound = new sf::Sound;
		sfxSound->setBuffer(*sfxBuffer);
		sit->second.push_back(sfxSound);
		return sfxSound;
	}
	else
	{
		for (int i = 0; i < sit->second.size(); i++)
		{
			if (sit->second.at(i)->getStatus() != sf::Sound::Playing)
				return sit->second.at(i);
		}
		sfxSound = new sf::Sound;
		sfxSound->setBuffer(*sfxBuffer);
		sit->second.push_back(sfxSound);
		return sfxSound;
	}
}

void AudioManager::playSound(std::string s , bool pitch, int volume)
{
	sf::Sound* sfx = createSounds(s);

	if (pitch)
	{
		float r = rand() % 100 + 90;
		r = r / 100;
		sfx->setPitch(r);
	}

	sfx->setVolume(volume);
	sfx->play();
}

void AudioManager::playMusic(std::string s, bool loop, int volume)
{
	if (!music.openFromFile(s))
		std::cout << "Failed to load: " << s << std::endl;

	music.setLoop(loop);
	music.setVolume(volume);
	music.play();
}

void AudioManager::stopAllSounds()
{
	auto s = m_sounds.begin();
	while (s != m_sounds.end())
	{
		for (int i = 0; i < s->second.size(); i++)
		{
			s->second.at(i)->stop();
		}
	}
}

void AudioManager::unloadSounds()
{
	auto s = m_sounds.begin();
	while (s != m_sounds.end())
	{
		for (int i = 0; i < s->second.size(); i++)
		{
			delete s->second.at(i);
			s->second.at(i) = nullptr;
		}
		s->second.clear();
	}
	m_sounds.clear();

	auto b = m_buffers.begin();
	while (b != m_buffers.end())
	{
		delete b->second;
		b->second = nullptr;
	}
	m_buffers.clear();

}

void AudioManager::unloadMusic()
{
	// Unload music: success!
}