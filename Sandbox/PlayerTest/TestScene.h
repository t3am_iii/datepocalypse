#pragma once

#include <Engine\SceneSystem\BaseScene.h>

#include <SFML\Graphics\View.hpp>

#include <SFML\Graphics\RectangleShape.hpp>

#include <SFML\Graphics\Sprite.hpp>

#include <SFML\Graphics.hpp>

using namespace dpEngine;

class AudioManager;
class ProjectileManager;
class LevelManager;
class WaveManager;
class EnemyManager;
class PlayerManager;
class ScoreManager;
class HUD;
class GameLogic;
class TextureCache;

struct PauseButton
{
	sf::Text text;
	int index;
};

class TestScene : public SceneSystem::BaseScene
{
private:
	bool m_closeGame;

	bool m_restartGame;
	
	float m_wonTimer;

	float m_nextSceneTimer;

	TextureCache* m_texCache;

	LevelManager* m_levelMngr;

	GameLogic* m_gameLogic;

	ScoreManager* m_scoreMngr;

	HUD* m_hud;

	AudioManager* m_audioMngr;

	WaveManager* m_waveMngr;

	ProjectileManager* m_projectileMngr;

	EnemyManager* m_enemyMngr;

	PlayerManager* m_playerMngr;

	std::vector<PauseButton> m_pauseButtons;

	bool m_pause;

	sf::RectangleShape m_fadeRect;

	sf::Sprite m_pauseMenu;
	
	sf::Font m_font;

	sf::Text* m_pauseMenuTitle;

	int m_index;

	bool m_menuScene;

public:
	TestScene(TextureCache*, AudioManager*, ScoreManager*);

	~TestScene();

	void onEnter(const SystemInfo&);

	void onFadeIn() { }

	void onUpdate(float p_fDealtaTime, const SystemInfo& p_xSystemInfo);

	bool nextScene(int&) const;

	bool shouldClose() const;

	void onRender(sf::RenderWindow&);

	void onFadeOut() { }

	void onExit(const SystemInfo&);

};
