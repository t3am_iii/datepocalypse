#pragma once

enum EENTITYTYPE
{
	ENTITY_PLAYER,
	ENTITY_MUTATED_HUMAN,
	ENTITY_MUTATED_CROW,
	ENTITY_MUTATED_RIOT_POLICE,
	ENTITY_BOSS,
	ENTITY_PLAYER_PROJECTILE,
	ENTITY_ENEMY_PROJECTILE,
	ENTITY_PLAYER_SHIELD,
	ENTITY_ENEMY_SHIELD,
	ENTITY_BOOMERANG,
	ENTITY_BASEBALL_BALL,
	ENTITY_BASEBALL_BAT,
	ENTITY_HURRICANE,
	ENTITY_TSUNAMI,
};

class Collider;

class IEntity
{
public:
	virtual ~IEntity() { }
	//virtual void Update(float) = 0;
	//virtual sf::Sprite* GetSprite() = 0; // Can return nullptr if there is no sprite
	//virtual Collider* GetCollider() = 0;
	//virtual bool IsVisible() = 0;
	virtual EENTITYTYPE GetType() = 0;

};