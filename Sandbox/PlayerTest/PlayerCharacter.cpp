#include "PlayerCharacter.h"
#include "Boomerang.h"
#include "PlayerState.h"
#include "PlayerManager.h"
#include "Global.h"
#include "Enums.h"

#include <Engine\InputSystem\InputManager.h>

#include <SFML\Graphics\Texture.hpp>

PlayerCharacter::PlayerCharacter(sf::Texture* texture, sf::Texture* trailTexture, sf::Texture* shadowTexture, float p_fX, float p_fY, PlayerState* playerState, PlayerManager* playerMngr)
	: m_playerState(playerState)
	, m_playerMngr(playerMngr)
	, m_spriteShadow(*shadowTexture)
{
	// Lean Left
	m_animations.push_back(Animation());
	m_animations[0].setSpriteSheet(*texture);
	m_animations[0].addFrame(sf::IntRect(1536, 0, 512, 512));

	// Move Forward
	m_animations.push_back(Animation());
	m_animations[1].setSpriteSheet(*texture);
	m_animations[1].addFrame(sf::IntRect(0, 0, 512, 512));
	m_animations[1].addFrame(sf::IntRect(512, 0, 512, 512));
	m_animations[1].addFrame(sf::IntRect(1024, 0, 512, 512));

	// Lean Right
	m_animations.push_back(Animation());
	m_animations[2].setSpriteSheet(*texture);
	m_animations[2].addFrame(sf::IntRect(0, 512, 512, 512));

	// Death 
	m_animations.push_back(Animation());
	m_animations[3].setSpriteSheet(*texture);
	m_animations[3].addFrame(sf::IntRect(0, 1024, 512, 512));
	m_animations[3].addFrame(sf::IntRect(512, 1024, 512, 512));
	m_animations[3].addFrame(sf::IntRect(1024, 1024, 512, 512));
	m_animations[3].addFrame(sf::IntRect(1536, 1024, 512, 512));
	m_animations[3].addFrame(sf::IntRect(0, 1536, 512, 512));
	m_animations[3].addFrame(sf::IntRect(512, 1536, 512, 512));
	m_animations[3].addFrame(sf::IntRect(1024, 1536, 512, 512));

	// Taking Damage 
	m_animations.push_back(Animation());
	m_animations[4].setSpriteSheet(*texture);
	m_animations[4].addFrame(sf::IntRect(0, 1024, 512, 512));

	m_spriteShadow.setOrigin(100, -150);
	m_spriteShadow.setScale(0.4, 0.65);
	m_spriteShadow.setColor(sf::Color(255, 255, 255, 180));

	// Player Trail
	m_animTrail.setSpriteSheet(*trailTexture);
	m_animTrail.addFrame(sf::IntRect(0, 0, 256, 256));
	m_animTrail.addFrame(sf::IntRect(256, 0, 256, 256));
	m_animTrail.addFrame(sf::IntRect(512, 0, 256, 256));
	m_animTrail.addFrame(sf::IntRect(768, 0, 256, 256));

	//for (int i = 0; i < 5; i++)
	//{
	//	m_trailAnims.push_back(AnimatedSprite());
	//	m_trailAnims[i].setAnimation(m_animTrail);
	//}

	for (int i = 0; i < 4; i++)
	{
		m_playerTrails.push_back(PlayerTrail());
		//m_playerTrails[i].animSprite = AnimatedSprite(500, false, true);
		m_playerTrails[i].animSprite.setAnimation(m_animTrail);
		m_playerTrails[i].animSprite.setScale(.3f, .3f);
		m_playerTrails[i].ttl =  i / 5.f;
		m_playerTrails[i].animSprite.play();
	}

	//m_animSpriteTrail.setAnimation(m_animTrail);

	m_prevAnimation = -1;
	m_flyModifier = { 1.f, 1.f };
	m_hasInvincibility = false;

	m_iHealthPoints = 3;

	m_damageStrength = 1;

	m_isAlive = true;
	m_isHit = false;
	m_invincibleTimer = 0.0f;

	m_isShielding = false;
	
	m_knockBack.inState = false;
	m_knockBack.dir = { 0.f, 0.f };
	m_knockBack.curStrenght = 0.f;
	m_knockBack.maxStrength = 0.f;

	m_fSpeed = 6;
	m_fMaxSpeed = 6;
	m_ttl = 0.1;
	m_ttmove = 0;
	m_nextTrail = 0;

	pHeartText.loadFromFile("../assets/Particles/heart.png");
	pDotText.loadFromFile("../assets/Particles/dot.png");

	pAmount = 0;
}

PlayerCharacter::~PlayerCharacter() { }

void PlayerCharacter::update(float deltaTime)
{
	// Trail animation //
	if (m_particles.size() < 70)
	{
		Particle* p = new Particle;
		m_particles.push_back(p);
		if (pAmount % 2 == 0)
			m_particles.at(pAmount)->sprite.setTexture(pHeartText);
		else
			m_particles.at(pAmount)->sprite.setTexture(pDotText);

		m_particles.at(pAmount)->startPos = sf::Vector2f(100, 100);
		m_particles.at(pAmount)->sprite.setPosition(m_particles.at(pAmount)->startPos);
		m_particles.at(pAmount)->ttl = .1f;
		pAmount++;
	}

	for (int i = 0; i < m_particles.size(); i++)
	{
		float r1 = rand() % 9 - 4;
		float r2 = rand() % 8 + 3;
		int r3 = rand() % 255 + 120;
		if (m_particles.at(i)->ttl < 0 && m_iHealthPoints > 0)
		{
			m_particles.at(i)->sprite.setPosition(getPosition().x, getPosition().y + 120);
			m_particles.at(i)->sprite.setColor(sf::Color(255, r3, r3, 255));
			m_particles.at(i)->ttl = .5f;
		}
		else
		{
			m_particles.at(i)->sprite.move(sf::Vector2f(r1, r2));
			//m_particles.at(i)->sprite.setColor(sf::Color(255, r3, r3, m_particles.at(i)->ttl * 510));
		}

		m_particles[i]->ttl -= deltaTime;
	}
	// -End- Trail Animation //

	// Player Bounds
	if (getPosition().x < 470)
	{
		setPosition(470, getPosition().y);
	}
	else if (getPosition().x > SCREEN_WIDTH - 520)
	{
		setPosition(SCREEN_WIDTH - 520, getPosition().y);
	}
	
	if (getPosition().y < 0)
	{
		setPosition(getPosition().x, 0);
	}
	else if (getPosition().y > SCREEN_HEIGHT - 250)
	{
		setPosition(getPosition().x, SCREEN_HEIGHT - 250);
	}

	m_animatedSprite.update(deltaTime * 1000.f);
	

	// Invincibility Frames
	if (m_isHit && m_isAlive)
	{
		if (m_invincibleTimer < 300)
		{
			m_hasInvincibility = true;
			m_invincibleTimer += (deltaTime * 1000);
			m_animatedSprite.setAnimation(m_animations[4]);

			if (m_hitEffect < 5)
			{
				
				m_animatedSprite.setColor(sf::Color(255, 255, 255, 40));
				m_hitEffect++;
			}	
			else if (m_hitEffect < 8)
			{
				
				m_animatedSprite.setColor(sf::Color(255, 255, 255, 255));
				m_hitEffect++;
			}
			else
				m_hitEffect = 0;	
		}
		else
		{
			m_isHit = false;
			m_hasInvincibility = false;
			m_invincibleTimer = 0;
			m_animatedSprite.setColor(sf::Color(255, 255, 255, 255));
			m_animatedSprite.setAnimation(m_animations[1]);
		}
	}

	if (!m_isAlive)
	{
		if (m_isHit)
		{
			m_animatedSprite.play(m_animations[3]);
			m_isHit = false;
			body->setActive(false);
		}
		if (m_animatedSprite.getCurrentFrame() >= 6)
			m_animatedSprite.setFrame(6);
	}
	else
	{
		if (m_knockBack.inState)
		{
			setFlyModifier(0, 0);
			setInvincibility(true);


			auto deltaPos = m_knockBack.dir * m_knockBack.curStrenght;
			addPosition(deltaPos);

			m_knockBack.curStrenght -= deltaTime * m_knockBack.maxStrength;
			if (m_knockBack.curStrenght < 0.f)
			{
				setFlyModifier(1.f, 1.f);
				setInvincibility(false);
				m_knockBack.inState = false;
			}
		}
	}
}

void PlayerCharacter::setAnimation(int value) 
{ 
	if (m_prevAnimation != value + 1)
	{
		m_animatedSprite.setAnimation(m_animations[value + 1]);
		m_prevAnimation = value + 1;
	}
}

void PlayerCharacter::setFlyModifier(float x, float y) { m_flyModifier.x = x; m_flyModifier.y = y; }

void PlayerCharacter::setInvincibility(bool value) { m_hasInvincibility = value; }

const sf::Vector2f& PlayerCharacter::getFlyModifier() const { return m_flyModifier; }

bool PlayerCharacter::hasInvincibility() const { return m_hasInvincibility; }

int PlayerCharacter::GetHealthPoints() { return m_iHealthPoints; }
void PlayerCharacter::SetHealthPoints(int p_iHealthPoints) 
{ 
	m_iHealthPoints = p_iHealthPoints; 

	if( m_iHealthPoints > 0)
		m_isHit = true;
	else if(m_isAlive)
	{
		m_isHit = true;
		m_isAlive = false;
	}
}

void PlayerCharacter::SetMoveSpeed(float p_fValue) { m_fSpeed = p_fValue; }

void PlayerCharacter::setDamageStrength(int p_iValue) { m_damageStrength = p_iValue; }

bool PlayerCharacter::getIsHit() { return m_isHit; }

void PlayerCharacter::setIshit(bool p_bValue) { m_isHit = p_bValue; m_hasInvincibility = p_bValue; m_invincibleTimer = 0;}

bool PlayerCharacter::isAlive() { return m_isAlive; }

bool PlayerCharacter::isShielding() { return m_isShielding; }
void PlayerCharacter::setIsShielding(bool value) { m_isShielding = value; }

void PlayerCharacter::addKnockBack(sf::Vector2f dir, float strenght)
{
	m_knockBack.dir += dir;
	m_knockBack.curStrenght += strenght;
	m_knockBack.maxStrength += strenght;
	m_knockBack.inState = true;
}

void PlayerCharacter::onNotifyControllerChange(PlayerController* controller) { m_controller = controller; }

PlayerController* PlayerCharacter::getController() const { return m_controller; }

EENTITYTYPE PlayerCharacter::GetType() { return EENTITYTYPE::ENTITY_PLAYER; }

void PlayerCharacter::onConstruction(const GameObjectCreationKit& kit)
{
	shape = new CircleShape(30.0f);
	BodyDef def(EBodyTypes::kKinematicBody, FILTER(EEntityCategory::kPlayerCharacter, EEntityCategory::kEverything), false, shape, this);
	body = kit.collisionMngr->createBody(def, this);

	m_animatedSprite = AnimatedSprite(100, false, true);
	m_animatedSprite.setScale(.35f, .35f);
	m_animatedSprite.setOrigin(256.f * 1.0f, 0);//256.f * 1.0f);
	m_animatedSprite.play(m_animations[1]);
}

void PlayerCharacter::onDestruction(const GameObjectCreationKit& kit)
{
	kit.collisionMngr->destroyBody(body);
	body = nullptr;

	delete shape;
	shape = nullptr;
}

void PlayerCharacter::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	states.transform = getTransform();

	target.draw(m_spriteShadow, states);

	for (int i = 0; i < m_particles.size(); i++)
		target.draw(m_particles[i]->sprite);

	target.draw(m_animatedSprite, states);
}
