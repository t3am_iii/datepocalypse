#include "LoseScene.h"
#include "Global.h"

#include <Engine\SystemInfo.h>
#include <Engine\InputSystem\InputManager.h>

#include <SFML\Graphics\RenderWindow.hpp>

void LoseScene::onEnter(const SystemInfo&)
{
	m_gameOverTex.loadFromFile("../assets/textures/gameover.png");
	m_gameOverSpr.setTexture(m_gameOverTex);
	m_NextScene = 0;
	m_closeGame = false;
}

void LoseScene::onUpdate(float, const SystemInfo& sysInfo)
{
	if (sysInfo.inputMngr->wasKeyPressed(sf::Keyboard::Key::Escape))
	{
		m_closeGame = true;
	}

	if (sysInfo.inputMngr->wasKeyPressed(sf::Keyboard::Key::Return))
	{
		m_NextScene = 1;
	}
}

bool LoseScene::nextScene(int& nextScene) const
{
	if (m_NextScene)
	{
		nextScene = MENU_SCENE;
		return true;
	}

	return false;
}

bool LoseScene::shouldClose() const { return m_closeGame; }

void LoseScene::onRender(sf::RenderWindow& window)
{
	window.draw(m_gameOverSpr);
}

void LoseScene::onExit(const SystemInfo&)
{

}
