#include "TextureCache.h"
#include <iostream>

TextureCache::TextureCache()
{
	//
}

TextureCache::~TextureCache()
{
	unloadTextures();
}

// Loads a texture and filepath to map
void TextureCache::loadTexture(std::string filePath)
{
	sf::Texture* texture;
	auto t = m_textures.find(filePath);
	if (t == m_textures.end())
	{
		texture = new sf::Texture();
		texture->loadFromFile(filePath.c_str());
		if (texture == nullptr)
		{
			std::cout << "Failed to load: " << filePath << std::endl;
		}

		m_textures.insert(std::pair<std::string, sf::Texture*>(filePath, texture));
		//t = m_textures.find(filePath);
	}
}

// Checks if texture has been pro-loaded, else adds it to the list.
// Returns Texture Pointer from map.
sf::Texture* TextureCache::getTexture(std::string filePath)
{
	sf::Texture* texture;
	auto t = m_textures.find(filePath);
	if (t == m_textures.end())
	{
		return nullptr;
		texture = new sf::Texture();
		texture->loadFromFile(filePath.c_str());
		if (texture == nullptr)
		{
			std::cout << "Failed to load: " << filePath << std::endl;
			return nullptr;
		}

		m_textures.insert(std::pair<std::string, sf::Texture*>(filePath, texture));
		t = m_textures.find(filePath);
	}
	else
	{
		texture = (t)->second;
	}

	return texture;
}

void TextureCache::unloadTextures()
{
	// Clear texture map
	auto t = m_textures.begin();
	while (t != m_textures.end())
	{
		delete t->second;
		t->second = nullptr;
	}
	m_textures.clear();
}