#pragma once

#include <SFML\System\Vector2.hpp>

#include <vector>

struct Path
{
	std::vector<sf::Vector2f> points;

	bool loop;
};
