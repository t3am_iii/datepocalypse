#pragma once

#include "IEntity.h"

#include <Engine\GameObjectCreationKit.h>
#include <Engine\Objects\Pawn.h>
#include <Engine\CollisionSystem\CollisionManager.h>
#include <Engine\CollisionSystem\BodyDef.h>
#include <Engine\CollisionSystem\Body.h>
#include <Engine\CollisionSystem\Enums.h>
#include <Engine\CollisionSystem\Shapes\RectangleShape.h>
#include <Engine\CollisionSystem\Shapes\CircleShape.h>

#include <SFML\Graphics\Sprite.hpp>
#include <SFML\Graphics\RectangleShape.hpp>
#include <SFML\Graphics\CircleShape.hpp>

using namespace dpEngine;
using namespace dpEngine::CollisionSystem;


class PlayerShield : public IEntity, public dpEngine::Pawn
{
public:
	PlayerShield(sf::Texture* p_xTexture, float p_fX, float p_fY, float p_fRadius);
	
	~PlayerShield();
	
	void Update(float p_fDeltaTime);

	int GetDuration();
	void SetDuration(float p_iValue);

	float GetRadius();
	void SetRadius(float p_fValue);

	bool IsActive();
	void SetIsActive(bool p_bValue);

	EENTITYTYPE GetType();

	void onConstruction(const GameObjectCreationKit&);

	void onDestruction(const GameObjectCreationKit&);

	void draw(sf::RenderTarget&, sf::RenderStates) const;

private:
	sf::Sprite m_sprite;
	int rotation;
	float leftPos;

	float m_fRadius;
	bool m_bActive;
	float m_fDurationTime;
	bool m_bHit;
	int m_iHitEffect;
	float m_spriteRadius;

protected:
	
};