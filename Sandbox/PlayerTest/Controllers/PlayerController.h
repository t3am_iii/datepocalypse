#pragma once

#include "../States/Player/PlayerInteractiveTutorialState.h"
#include "../States/Player/PlayerFlyingState.h"
#include "../States/Player/PlayerFiringState.h"
#include "../States/Player/PlayerBoomerangingState.h"
#include "../States/Player/PlayerBaseballingState.h"
#include "../States/Player/PlayerLoveSpinState.h"
#include "../States/Player/PlayerDeadState.h"

#include <SFML\Window\Keyboard.hpp>

class AudioManager;
class PlayerCharacter;
class PlayerManager;
struct PlayerState;
enum class EActions;

namespace dpEngine {
	class InputManager;
	namespace CollisionSystem { class CollisionManager; }
}
using namespace dpEngine::CollisionSystem;

class PlayerController
{
private:
	sf::Keyboard::Key m_moveKeys[4];

	PlayerCharacter* m_character;

	PlayerManager* m_playerMngr;

	AudioManager* m_audioMngr;

	//CollisionManager* m_collisionMngr;

	int m_id;

	bool m_canMove;

	PlayerInteractiveTutorialState m_interactiveTutorialState;

	PlayerFlyingState m_flyingState;

	PlayerFiringState m_firingState;

	PlayerBoomerangingState m_boomeraningState;

	PlayerBaseballingState m_baseballingState;

	PlayerLoveSpinState m_loveSpinningState;

	PlayerDeadState m_deadState;
	
	BasePlayerState* m_previoustState;
	
	BasePlayerState* m_currentState;



public:
	PlayerController(int, PlayerManager*, AudioManager*, LevelManager*, const sf::Keyboard::Key&, const sf::Keyboard::Key&, const sf::Keyboard::Key&, const sf::Keyboard::Key&);

	~PlayerController();

	void update(dpEngine::InputManager*, float);

	void throwBoomerang();

	void throwBaseball();

	void activateLoveSpin();
	
	void setCharacter(PlayerCharacter*);

	void setCanMove(bool);

	PlayerCharacter* getCharacter();

	int getId() const;

	PlayerState* getState() const;

	AudioManager* getAudio() const;

	sf::Keyboard::Key getActionKey(const EActions&) const;

	bool canMove() const;

	bool allowBoomerangUsage() const;

	bool allowBaseballUsage() const;

};
