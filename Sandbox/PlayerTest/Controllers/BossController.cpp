#include "BossController.h"
#include "../GameObjects/Enemies/BossCharacter.h"
#include "../EnemyProjectile.h"
#include "../EnemyManager.h"
#include "../ProjectileManager.h"
#include "../Enums.h"

BossController::BossController(EnemyManager* enemyMngr, ProjectileManager* projectileMngr)
	: m_enemyMngr(enemyMngr)
	, m_projectileMngr(projectileMngr)
	, m_currentState(&m_minigunState)
{
	m_currentState->onEnter(this);
}

BossController::~BossController() { }

void BossController::update(float deltaTime)
{
	return;
	if (m_character != nullptr)
	{
		EBossStates state = m_currentState->onUpdate(this, deltaTime);
		if (state != EBossStates::kStay)
		{
			m_currentState->onExit(this);

			switch (state)
			{
			case EBossStates::kMiniguning:
				m_currentState = &m_minigunState;
				break;
			case EBossStates::kPukeing:
				m_currentState = &m_pukeState;
				break;
			}

			m_currentState->onEnter(this);
		}
	}
}

void BossController::setCharacter(BossCharacter* character) { m_character = character; }

void BossController::createProjectile(const sf::Vector2f& direction)
{
	auto pos = m_character->getPosition();
	auto proj = m_projectileMngr->CreateEnemyProjectile(pos.x, pos.y);
	proj->setDirection(direction);
}
