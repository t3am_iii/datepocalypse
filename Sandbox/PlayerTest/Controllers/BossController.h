#pragma once

#include "../States/Boss/BossMinigunAttack.h"
#include "../States/Boss/BossRadioactivePuke.h"

class BossCharacter;
class EnemyManager;
class ProjectileManager;

class BossController
{
private:
	BossCharacter* m_character;

	EnemyManager* m_enemyMngr;

	ProjectileManager* m_projectileMngr;

	BossMinigunAttack m_minigunState;

	BossRadioactivePuke m_pukeState;

	BaseBossState* m_currentState;

public:
	BossController(EnemyManager*, ProjectileManager*);

	~BossController();

	void update(float);

	void setCharacter(BossCharacter*);

	void createProjectile(const sf::Vector2f&);

};
