#include "PlayerController.h"
#include "../PlayerCharacter.h"
#include "../PlayerManager.h"
#include "../Enums.h"

#include <Engine\InputSystem\InputManager.h>

PlayerController::PlayerController(int id, PlayerManager* playerMngr, AudioManager* audioMngr, LevelManager* levelMngr, const sf::Keyboard::Key& up, const sf::Keyboard::Key& left, const sf::Keyboard::Key& down, const sf::Keyboard::Key& right)
	: m_id(id)
	, m_canMove(true)
	, m_playerMngr(playerMngr)
	, m_audioMngr(audioMngr)
	, m_interactiveTutorialState(levelMngr)
	, m_character(nullptr)
	, m_currentState(&m_interactiveTutorialState)
	, m_moveKeys{ up, left, down, right }
{
	m_currentState->onEnter(this);
}

PlayerController::~PlayerController() { /* EMPTY */ }

void PlayerController::update(dpEngine::InputManager* inputMngr, float deltaTime)
{
	if (m_character != nullptr)
	{
		EPlayerStates stateType = m_currentState->handleInput(this, inputMngr);
		if (stateType != EPlayerStates::kStay)
		{
			if (stateType != EPlayerStates::kPrev)
				m_previoustState = m_currentState;
			m_currentState->onExit(this);

			switch (stateType)
			{
			case EPlayerStates::kInteractiveTutorial:
				m_currentState = &m_interactiveTutorialState;
				break;
			case EPlayerStates::kDead:
				m_currentState = &m_deadState;
				break;
			case EPlayerStates::kFlying:
				m_currentState = &m_flyingState;
				break;
			case EPlayerStates::kFiring:
				m_currentState = &m_firingState;
				break;
			case EPlayerStates::kBoomeranging:
				m_currentState = &m_boomeraningState;
				break;
			case EPlayerStates::kBaseballing:
				m_currentState = &m_baseballingState;
				break;
			case EPlayerStates::kLoveSpinning:
				m_currentState = &m_loveSpinningState;
				break;
			case EPlayerStates::kPrev:
				m_currentState = m_previoustState;
				break;
			}

			m_currentState->onEnter(this);
		}
		else
			m_currentState->update(this, deltaTime);
	}
}

void PlayerController::throwBoomerang()
{
	auto pos = m_character->getPosition();
	auto state = m_playerMngr->getState();
	state->boomerangPath[m_id - 1].boomerang = m_playerMngr->createBoomerang(pos.x, pos.y - 50, m_id == 2);
}

void PlayerController::throwBaseball()
{
	auto state = m_playerMngr->getState();
	state->baseballKit = m_playerMngr->createBaseballKit();
	state->baseballingState = EBaseballingState::kFlying;
}

void PlayerController::activateLoveSpin()
{
	auto state = m_playerMngr->getState();
	//state->loveSpinPath.centerBody = m_collisionMngr->createBody(BodyDef(EBodyTypes::kKinematicBody, FILTER(EEntityCategory::kPlayerCharacter, EEntityCategory::kEverything), false, shape, this),this);
}

void PlayerController::setCharacter(PlayerCharacter* newCharacter)
{
	m_character = newCharacter;
	m_character->onNotifyControllerChange(this);
}

void PlayerController::setCanMove(bool newCanMove) { m_canMove = newCanMove; }

PlayerCharacter* PlayerController::getCharacter() { return m_character; }

bool PlayerController::canMove() const { return m_canMove; }

bool PlayerController::allowBoomerangUsage() const
{
	auto state = m_playerMngr->getState();
	return state->hasBoomerang
		//&& state->throwerId == m_id
		&& (state->boomerangingState == EBoomerangingState::kNotActive
			|| state->boomerangingState == EBoomerangingState::kChargingStrengthBar);
}

bool PlayerController::allowBaseballUsage() const
{
	auto state = m_playerMngr->getState();
	return state->hasBaseballKit
		&& state->baseballingState == EBaseballingState::kNotActive;
}

int PlayerController::getId() const { return m_id; }

PlayerState* PlayerController::getState() const { return m_playerMngr->getState(); }

AudioManager* PlayerController::getAudio() const { return m_audioMngr; }

sf::Keyboard::Key PlayerController::getActionKey(const EActions& key) const
{
	auto idx = (int)key - 1;
	if (idx > -1 && idx < 4)
		return m_moveKeys[idx];
	else if (key == EActions::kFire)
		return sf::Keyboard::Key::Space;
	else if (key == EActions::kShield)
		return sf::Keyboard::Key::Escape;
	else if (key == EActions::kBoomerang)
		return sf::Keyboard::Key::E;
	else if (key == EActions::kBaseball)
		return sf::Keyboard::Key::Q;

	return sf::Keyboard::Key::Escape;
}
