#include "PlayerShield.h"
#include "Enums.h"

#include <SFML\Graphics\RenderTarget.hpp>

PlayerShield::PlayerShield(sf::Texture* p_xTexture, float p_fX, float p_fY, float p_fRadius)
{
	m_sprite.setTexture(*p_xTexture);
	//m_spriteLeft.setTextureRect(sf::IntRect(0, 0, 256, 512));
	m_sprite.setScale(0.f, 0.f);
	m_sprite.setOrigin(256.f/* * 1.f*/, 160.f/* * 1.f*/);

	m_fRadius = p_fRadius;
	m_bActive = false;
	m_fDurationTime = 200.f;
	m_spriteRadius = 0;


	//leftPos = 2048;
	rotation = 0;
}

PlayerShield::~PlayerShield()
{
	//
}

void PlayerShield::Update(float p_fDeltaTime)
{
	if (m_bActive)
	{
		//m_spriteLeft.setOrigin(leftPos, 160);
		//if(leftPos > 256)
		//	leftPos -= 64;
		//if (leftPos <= 256)
		//	leftPos = 256;

		if (m_spriteRadius < 1)
		{
			m_spriteRadius += 0.03f;
			m_sprite.setScale(m_spriteRadius, m_spriteRadius);
		}
		

		if (m_fDurationTime <= 0.f)
		{
			SetIsActive(false);
			m_fDurationTime = 0.f;
		}
		else
		{
			m_fDurationTime -= p_fDeltaTime * 10.f;
		}

		if (m_bHit && m_iHitEffect < 6)
		{
			if (m_iHitEffect == 0)
				m_sprite.setColor(sf::Color(255, 50, 50, 255));
			else if (m_iHitEffect == 1)
				m_sprite.setColor(sf::Color(255, 255, 255, 255));
			else if (m_iHitEffect == 2)
				m_sprite.setColor(sf::Color(255, 50, 50, 255));
			else if (m_iHitEffect == 3)
				m_sprite.setColor(sf::Color(255, 255, 255, 255));
			else if (m_iHitEffect == 4)
				m_sprite.setColor(sf::Color(255, 50, 50, 255));
			else if (m_iHitEffect == 5)
			{
				m_sprite.setColor(sf::Color(255, 255, 255, 255));
				m_bHit = false;
				m_iHitEffect = 0;
			}

			m_iHitEffect++;
		}

	}
	else
	{
		//leftPos = 2048;
		m_spriteRadius = 0;
		body->setActive(false);
		m_fDurationTime += p_fDeltaTime * 10.f;
		if (m_fDurationTime > 200.f)
			m_fDurationTime = 200.f;
	}
}

int  PlayerShield::GetDuration() { return m_fDurationTime; }
void  PlayerShield::SetDuration(float p_iValue) { m_fDurationTime = p_iValue; m_bHit = true; }

float PlayerShield::GetRadius() { return m_fRadius; }
void PlayerShield::SetRadius(float p_fValue) { m_fRadius = p_fValue; }

bool PlayerShield::IsActive() { return m_bActive; }
void PlayerShield::SetIsActive(bool p_bValue)
{
	m_bActive = p_bValue;
	body->setActive(p_bValue);
}

EENTITYTYPE PlayerShield::GetType() { return EENTITYTYPE::ENTITY_PLAYER_SHIELD; }

void PlayerShield::onConstruction(const GameObjectCreationKit& kit)
{
	shape = new CircleShape(150.0f);
	BodyDef def(EBodyTypes::kKinematicBody, FILTER(EEntityCategory::kPlayerShield, ~EEntityCategory::kPlayerCharacter), false, shape, this);
	body = kit.collisionMngr->createBody(def, this);
}

void PlayerShield::onDestruction(const GameObjectCreationKit& kit)
{
	kit.collisionMngr->destroyBody(body);
	body = nullptr;

	delete shape;
	shape = nullptr;
}

void PlayerShield::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	states.transform *= getTransform();
	target.draw(m_sprite, states);
}