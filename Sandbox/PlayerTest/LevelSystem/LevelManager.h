#pragma once

#include <Engine\ViewSystem\IViewModifier.h>

#include <SFML\Graphics\Sprite.hpp>
#include <SFML\Graphics\RenderTarget.hpp>

#include <vector>

class WaveManager;
class EnemyManager;
class TextureCache;
class BossCharacter;

struct AdjustField
{
	float current;
	float scalar;
	float target;
};

class LevelManager// : public dpEngine::IViewModifier
{
private:
	WaveManager* m_waveMngr;

	EnemyManager* m_enemyMngr;

	TextureCache* m_texCache;

	std::vector<sf::Texture*> m_textures;

	std::vector<sf::Sprite> m_sprites;

	bool m_isScrolling;
	
	struct
	{
		AdjustField speed;
		bool shouldLoop;
		bool adjustSpeed;
		bool allowChange;
		float amountLeft;
	} m_scroll;

	AdjustField m_screenShake;
	
	struct
	{
		sf::Sprite* first;
		sf::Sprite* second;
	} m_currentPair;

	struct
	{
		int first;
		int second;
	} m_repeated;

	bool m_setEndLevel;

	BossCharacter* m_bossCharacter;

public:
	LevelManager(WaveManager*, EnemyManager*, TextureCache*);

	~LevelManager();

	void startup();

	void update(float);

	void render(sf::RenderTarget&, sf::RenderStates);

	void setScrolling(bool);

	WaveManager* getWaveMngr() const;

	bool hasWon() const;

private:
	void loadLevel(const std::string&);

};
