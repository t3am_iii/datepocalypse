#include "LevelManager.h"
#include "../WaveSystem/WaveManager.h"
#include "../EnemyManager.h"
#include "../TextureCache.h"
#include "../GameObjects/Enemies/BossCharacter.h"

#include <SFML\Graphics\Texture.hpp>

#include <rapidjson\document.h>
#include <rapidjson\writer.h>
#include <rapidjson\stringbuffer.h>
#include <rapidjson\filereadstream.h>

#include <stdio.h>
#include <iostream>
#include <fstream>
#include <sstream>

//#define LOG_PARSER
#ifdef LOG_PARSER
	#define LOG(stream, format, ...) fprintf(stream, format, __VA_ARGS__)
	#define LOG_ERROR(tag, format, ...) fprintf(stderr, std::string("[%s] (%s)", __VA_ARGS__)
	#define LOG_WARN(tag, format, ...) fprintf(stdout, f, __VA_ARGS__)
	#define LOG_DEBUG(tag, format, ...) fprintf(stdout, f, __VA_ARGS__)

	#define ASSERT_ERROR(x, f, ...) if (!(x)) { LOG_ERROR(std::string("[Level Parser] (Critical Error)   ").append(f).append("\n\n").c_str(), __VA_ARGS__); system("PAUSE"); exit(-1); }
	#define ASSERT_DEBUG(x, f, ...) if (!(x)) { LOG_ERROR(std::string("[Level Parser] (Critical Error)   ").append(f).append("\n\n").c_str(), __VA_ARGS__); }
#else
	#define ASSERT_ERROR(x, format, ...) if (!(x)) { }
	#define ASSERT_DEBUG(x, format, ...) if (!(x)) { }
#endif

using namespace rapidjson;

LevelManager::LevelManager(WaveManager* waveMngr, EnemyManager* enemyMngr, TextureCache* texCache)
	: m_waveMngr(waveMngr)
	, m_enemyMngr(enemyMngr)
	, m_texCache(texCache)
	, m_repeated({ 0, 0 })
	, m_setEndLevel(false)
{
	m_isScrolling = false;
	m_scroll.shouldLoop = true;
	m_scroll.adjustSpeed = true;
	m_scroll.allowChange = true;
	m_scroll.amountLeft = 3240.f * 16;
	m_scroll.speed.current = 0.f;
	m_scroll.speed.scalar = 300.f;
	m_scroll.speed.target = 600.f;
}

LevelManager::~LevelManager()
{
	for (auto tex : m_textures)
		delete tex;
	m_textures.clear();
	
	m_sprites.clear();
}

void LevelManager::startup()
{
	FILE* file;
	fopen_s(&file, "../assets/levels/boot.json", "rb");
	char buffer[65536];
	FileReadStream stream(file, buffer, sizeof(buffer));
	Document doc;
	doc.ParseStream<0, UTF8<>, FileReadStream>(stream);

	ASSERT_ERROR(!doc.HasMember("load"), "");
	Value& load = doc["load"];
	if (!load.IsString())
	{
		std::cerr << "Boot file does NOT contain 'load' variable!" << std::endl;
		return;
	}

	loadLevel(std::string("../assets/levels/") + load.GetString());
}

void LevelManager::loadLevel(const std::string& filepath)
{
	FILE* file;
	fopen_s(&file,filepath.c_str(), "rb");
	char buffer[65536];
	FileReadStream stream(file, buffer, sizeof(buffer));
	Document doc;
	doc.ParseStream<0, UTF8<>, FileReadStream>(stream);
	
	auto errCode = doc.GetParseError();
	if (errCode != ParseErrorCode::kParseErrorNone)
	{
		std::cout << "Error code " << errCode << " at line " << doc.GetErrorOffset() << " in level file '" << filepath << "'" << std::endl;
		return;
	}

	
	Value& name = doc["name"];
	ASSERT_ERROR(name.IsString(), "Level object is missing a 'name' attribute.")

	/*sf::Texture m_xTexture_Background1;
	sf::Texture m_xTexture_Background2;
	sf::Texture m_xTexture_Background3;
	sf::Sprite m_xSprite_Background1;
	sf::Sprite m_xSprite_Background2;
	float m_fScrollSpeed;
	float m_fBackgroundBox1_Pos;
	float m_fBackgroundBox2_Pos;*/

	/*m_xTexture_Background1.loadFromFile("../assets/textures/test_bg1.png", sf::IntRect(0, 0, 1940, 3240)); //1920 x 3240
	m_xTexture_Background2.loadFromFile("../assets/textures/test_bg5.png");
	m_xTexture_Background3.loadFromFile("../assets/textures/test_bg5.png");
	m_xSprite_Background1.setTexture(m_xTexture_Background2);
	m_xSprite_Background2.setTexture(m_xTexture_Background3);*/


	/*srand(time(NULL));
	m_iScreenShake = 0;
	m_fKnockBack = 0;
	m_fBulletSpread = 0;

	m_xTexture_lyktstople.loadFromFile("../assets/lyktstolpe.png", sf::IntRect(0, 0, 285, 340));
	m_xLyktSprite.setTexture(m_xTexture_lyktstople);
	m_fLyktstolpe_pos = 0;
	lyktstolpe.setSize(sf::Vector2f(200,280));
	lyktstolpe.setPosition(100, m_fLyktstolpe_pos);
	lyktstolpe.setTexture(m_xLyktSprite.getTexture());*/

	m_textures.push_back(m_texCache->getTexture("../assets/textures/test_bg5.png"));

	m_textures.push_back(m_texCache->getTexture("../assets/textures/test_bg5.png"));

	m_textures.push_back(m_texCache->getTexture("../assets/textures/test_bg5.png"));

	m_textures.push_back(m_texCache->getTexture("../assets/textures/end_level.png"));

	sf::Sprite spr;
	spr.setTexture(*m_textures[0]);
	m_sprites.push_back(spr);

	spr = sf::Sprite();
	spr.setTexture(*m_textures[1]);
	m_sprites.push_back(spr);

	spr = sf::Sprite();
	spr.setTexture(*m_textures[3]);
	m_sprites.push_back(spr);

	m_currentPair.first = &m_sprites[0];
	m_currentPair.second = &m_sprites[1];
	m_currentPair.first->setPosition(0.f, -2000.f); //2520
	m_currentPair.second->setPosition(0.f, -2000.f - 3240.f);

	/*m_fScrollSpeed = 300;
		
	m_fBackgroundBox1_Pos = 0;
	m_fBackgroundBox2_Pos = -1215;//-1080;*/

	std::cout << "Loaded level '" << name.GetString() << "'" << std::endl;

	ASSERT_ERROR(doc.HasMember("waves"), "Level object is missing a 'waves' attribute.")
	Value& wavesVal = doc["waves"];
	ASSERT_ERROR(wavesVal.IsArray(), "Level object has an invalid 'waves' attribute.")

	//std::cout << "Reading waves" << std::endl;

	std::vector<WaveInfo> waves;

	for (SizeType waveIdx = 0; waveIdx < wavesVal.Size(); waveIdx++)
	{
		Value& wave = wavesVal[waveIdx];

		ASSERT_ERROR(wave.HasMember("delay"), "Wave %i is missing a 'delay' attribute.", (waveIdx + 1))
		Value& delay = wave["delay"];
		ASSERT_ERROR(delay.IsNumber(), "Wave %i has an invalid 'delay' attribute.", (waveIdx + 1))

		ASSERT_ERROR(wave.HasMember("threats"), "Wave %i is missing a 'threats' attribute.", (waveIdx + 1))
		Value& threats = wave["threats"];
		ASSERT_ERROR(threats.IsArray(), "Wave %i has an invalid 'threats' attribute.", (waveIdx + 1))

		WaveInfo waveInfo;
		waveInfo.delay = delay.GetFloat();

		for (SizeType threatIdx = 0; threatIdx < threats.Size(); threatIdx++)
		{
			Value& threat = threats[threatIdx];

			ASSERT_ERROR(threat.HasMember("type"), "Threat %i in wave %i is missing a 'type' attribute.", (threatIdx + 1), (waveIdx + 1))
			Value& type = threat["type"];
			ASSERT_ERROR(type.IsString(), "Threat %i in wave %i has an invalid 'type' attribute, it should be a string, e.g: \"mutated human\"", (threatIdx + 1), (waveIdx + 1))

			ASSERT_ERROR(threat.HasMember("location"), "Threat %i in wave %i is missing a 'location' attribute.", (threatIdx + 1), (waveIdx + 1))
			Value& location = threat["location"];
			ASSERT_ERROR(location.IsArray(), "Threat %i in wave %i has an invalid 'location' attribute, it should be an array, e.g: [ 0, 0 ]", (threatIdx + 1), (waveIdx + 1))

			ThreatInfo threatInfo;
			
			std::string typeName = type.GetString();
			if (typeName == "mutated human")
				threatInfo.type = EThreatType::kMutatedHuman;
			else if (typeName == "mutated crow")
				threatInfo.type = EThreatType::kMutatedCrow;
			else if (typeName == "mutated riot police")
				threatInfo.type = EThreatType::kMutatedRiotPolice;
			else if (typeName == "hurricane")
				threatInfo.type = EThreatType::kHurricane;
			else if (typeName == "tsunami")
				threatInfo.type = EThreatType::kTsunami;
			else
				ASSERT_ERROR(false, "Threat %i in wave %i has an invalid 'type' attribute, check your spelling.", (threatIdx + 1), (waveIdx + 1))

			ASSERT_ERROR(location.Size() == 2, "Threat %i in wave %i has an invalid 'location' attribute, its array shuld have 2 elements only.", (threatIdx + 1), (waveIdx + 1))
			ASSERT_ERROR(location[0].IsNumber() && location[1].IsNumber(), "Threat %i in wave %i has an invalid 'location' attribute, only numbers are accepted.", (threatIdx + 1), (waveIdx + 1))

			threatInfo.location = {
				location[0].GetFloat(), location[1].GetFloat()
			};

			waveInfo.threats.push_back(threatInfo);

			//std::cout << "added threat '" << typeName << "' (" << (int)threatInfo.type << ") " << " to wave " << waveIdx << std::endl;
		}

		waves.push_back(waveInfo);
	}

	m_waveMngr->setWaves(waves);
}

void LevelManager::update(float deltaTime)
{
	if (m_isScrolling)
	{
		float newFirstY = m_currentPair.first->getPosition().y;
		float newSecondY = m_currentPair.second->getPosition().y;

		// 1940, 3240
		if (m_scroll.shouldLoop)
		{
			if (m_scroll.adjustSpeed)
			{
				bool reachedTarget;

				if (m_scroll.speed.current < m_scroll.speed.target)
				{
					m_scroll.speed.current += m_scroll.speed.scalar * deltaTime;
					reachedTarget = m_scroll.speed.current > m_scroll.speed.target;
				}
				else
				{
					m_scroll.speed.current -= m_scroll.speed.scalar * deltaTime;
					reachedTarget = m_scroll.speed.current < m_scroll.speed.target;
				}

				if (reachedTarget)
				{
					m_scroll.speed.current = m_scroll.speed.target;
					m_scroll.adjustSpeed = false;
				}
			}

			newFirstY += m_scroll.speed.current * deltaTime;
			newSecondY += m_scroll.speed.current * deltaTime;

			if (newFirstY > 1080.f)
			{
				newFirstY = newSecondY - 3240.f;
				m_repeated.first++;

				if (m_repeated.first > 12)
				{
					m_currentPair.first = &m_sprites[2];
					m_setEndLevel = true;
					m_waveMngr->setSpawning(false);
					m_bossCharacter = m_enemyMngr->createBoss(-10000.f, -10000.f);
				}
			}
			else if (newSecondY > 1080.f)
			{
				newSecondY = newFirstY - 3240.f;
				m_repeated.second++;
			}

			if (newFirstY > 0.f && m_setEndLevel)
			{
				newFirstY = 0.f;
				m_scroll.shouldLoop = false;
			}

			if (m_setEndLevel)
			{
				m_bossCharacter->setPosition(1920 / 2, newFirstY + 200);
			}

			m_currentPair.first->setPosition(0.f, newFirstY);
			m_currentPair.second->setPosition(0.f, newSecondY);
		}
	}

	//--- Shooting Feedback ---//

	/*if (false /*m_playerMngr->getCharacter(1)->IsShooting()*///)
	//{
		/*	m_iScreenShake = ((rand() % 3000 - 2000) / 1000) - 5;

		/*backgroundBox1.setPosition(0 - m_iScreenShake, m_fBackgroundBox1_Pos + m_iScreenShake);
		backgroundBox2.setPosition(0 - m_iScreenShake, m_fBackgroundBox2_Pos + m_iScreenShake);*/

		/*	m_fKnockBack = (rand() % 300 + 1) / 1000.0f;
		m_playerMngr->getCharacter(1)->addPosition(0.f, m_fKnockBack);
		m_playerMngr->getCharacter(2)->addPosition(0.f, m_fKnockBack);


		m_fKnockBack = (rand() % 200 - 100) / 1000.0f;
		m_playerMngr->getCharacter(1)->addPosition(m_fKnockBack, 0.f);
		m_playerMngr->getCharacter(2)->addPosition(m_fKnockBack, 0.f);

		//lyktstolpe.setPosition(60, m_fBackgroundBox2_Pos + m_iScreenShake + 500);
	}
	else
	{
		//sound.stop();
		//rectangle.setPosition(-5.0f, m_fBackgroundBox1_Pos);

		/*backgroundBox1.setPosition(0.0f, m_fBackgroundBox1_Pos);
		backgroundBox2.setPosition(0.0f, m_fBackgroundBox2_Pos);

		lyktstolpe.setPosition(60, m_fBackgroundBox2_Pos + 500);*/
	/*}*/
	//--- End Shooting Feedback ---//
}

void LevelManager::render(sf::RenderTarget& target, sf::RenderStates states)
{
	target.draw(*m_currentPair.first, states);
	target.draw(*m_currentPair.second, states);

	//window.draw(lyktstolpe, sf::RenderStates(lyktstolpe.getTransform()));
}

void LevelManager::setScrolling(bool value) { m_isScrolling = value; }

WaveManager* LevelManager::getWaveMngr() const { return m_waveMngr; }

bool LevelManager::hasWon() const
{
	return m_bossCharacter != nullptr && m_bossCharacter->getHealthPoints() < 1;
}
