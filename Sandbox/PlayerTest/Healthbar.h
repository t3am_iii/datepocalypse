#pragma once

#include <SFML\Graphics\Sprite.hpp>
#include <SFML\Graphics\RectangleShape.hpp>

#include <vector>

class Healthbar
{
public:
	Healthbar();
	~Healthbar();
	void Update(float p_fDeltaTime);
	
private:
	int m_iHealthPoints;
	sf::Texture* m_pxTexture;
	sf::Sprite* m_pxSprite;
	std::vector<sf::RectangleShape*> m_aHP;

};