#pragma once

#include "ScoreEntry.h"

#include <Engine\SceneSystem\BaseScene.h>

#include <SFML\Graphics\Texture.hpp>
#include <SFML\Graphics\Sprite.hpp>

using namespace dpEngine;

class ScoreManager;
struct ScoreButton
{
	sf::Text text;
	sf::Texture texture;
	sf::Sprite sprite;
	int Index;
};

class ScoreBoard : public SceneSystem::BaseScene
{
private:
	std::vector<ScoreButton*> m_ScoreButton;
	int index;

	std::vector<ScoreEntry> m_entries;

	sf::Texture	m_ScoreBoardTex;
	sf::Sprite m_ScoreBoardSpr;
	sf::Texture m_menuTex;
	sf::Sprite m_menuspr;
	sf::Texture m_ScoreButtonTex;
	sf::Sprite m_scoreButtonspr;
	sf::Text Text;
	sf::Font font;

	bool m_return;
	bool m_closeGame;

public:
	ScoreBoard() {}
	~ScoreBoard() {}

	void onEnter(const SystemInfo&);

	void onFadeIn() {}

	void onUpdate(float, const SystemInfo&);

	bool nextScene(int&) const;

	bool shouldClose() const;

	void onRender(sf::RenderWindow&);

	void onFadeOut() {}

	void onExit(const SystemInfo&);

};

