#include "ProjectileManager.h"
#include "PlayerProjectile.h"
#include "EnemyProjectile.h"
#include "TextureCache.h"

#include <Engine\SystemInfo.h>

#include <SFML\Graphics\RenderTarget.hpp>

ProjectileManager::ProjectileManager(const dpEngine::SystemInfo& systemInfo, TextureCache* texCache)
{
	kit.collisionMngr = systemInfo.collisionMngr;
	m_texCache = texCache;

	m_electricTrailTexture.loadFromFile("../assets/textures/vfx/electric_trail.png");
}

ProjectileManager::~ProjectileManager()
{
	for (auto e : m_apxPlayerProjectiles)
	{
		e->onDestruction(kit);
		delete e;
	}
	m_apxPlayerProjectiles.clear();
}

PlayerProjectile* ProjectileManager::CreatePlayerProjectile(float p_fX, float p_fY, float p_fSpeed, sf::Vector2f p_Dir)
{
	// Check for inactiveProjectile
	auto it = m_apxPlayerProjectiles.begin();
	for (int i = 0; i < m_apxPlayerProjectiles.size(); i++)
	{
		if (!m_apxPlayerProjectiles.at(i)->isActive())
		{
			m_apxPlayerProjectiles.at(i)->setDirection(p_Dir);
			m_apxPlayerProjectiles.at(i)->setIsActive(true);
			m_apxPlayerProjectiles.at(i)->setPosition(p_fX - 10, p_fY - 20);
			return m_apxPlayerProjectiles.at(i);
		}
	}

	// else create new projectile
	PlayerProjectile* xPlayerProjectile = new PlayerProjectile(
		m_texCache,
		p_fX,
		p_fY,
		p_fSpeed);
	xPlayerProjectile->setDirection(p_Dir);
	m_apxPlayerProjectiles.push_back(xPlayerProjectile);
	xPlayerProjectile->onConstruction(kit);
	xPlayerProjectile->setPosition(p_fX, p_fY);
	return xPlayerProjectile;
}

EnemyProjectile* ProjectileManager::CreateEnemyProjectile(float p_fX, float p_fY)
{
	// Check for inactiveProjectile
	auto it = m_apxEnemyProjectiles.begin();
	for (int i = 0; i < m_apxEnemyProjectiles.size(); i++)
	{
		if (!m_apxEnemyProjectiles.at(i)->IsActive())
		{
			m_apxEnemyProjectiles.at(i)->SetIsActive(true);
			m_apxEnemyProjectiles.at(i)->setPosition(p_fX - 10, p_fY - 20);
			return m_apxEnemyProjectiles.at(i);
		}
	}

	// else create new projectile
	EnemyProjectile* xEnemyProjectile = new EnemyProjectile(
		m_texCache->getTexture("../assets/textures/spritesheets/projectile_spritesheet.png"),
		&m_electricTrailTexture,
		p_fX,
		p_fY,
		800.0f);
	m_apxEnemyProjectiles.push_back(xEnemyProjectile);
	xEnemyProjectile->onConstruction(kit);
	xEnemyProjectile->setPosition(p_fX, p_fY);
	return xEnemyProjectile;
}

void ProjectileManager::Update(float p_fDeltaTime)
{
	for (auto pp : m_apxPlayerProjectiles)
		if (pp->isActive())
			pp->update(p_fDeltaTime);

	for (auto ep : m_apxEnemyProjectiles)
		if (ep->IsActive())
			ep->Update(p_fDeltaTime);
}

void ProjectileManager::Render(sf::RenderTarget& target)
{
	for (auto pp : m_apxPlayerProjectiles)
		if (pp->isActive())
			target.draw(*pp);

	for (auto ep : m_apxEnemyProjectiles)
		if (ep->IsActive())
			target.draw(*ep);
}
