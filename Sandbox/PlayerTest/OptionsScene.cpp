#include "OptionsScene.h"
#include "Global.h"
#include "AudioManager.h"

#include <Engine\SystemInfo.h>
#include <Engine\InputSystem\InputManager.h>
#include <Engine\ViewSystem\ViewManager.h>

OptionsScene::OptionsScene()
{
	m_sound = false;
	m_fullscreen = false;
}

void OptionsScene::onEnter(const SystemInfo& sysInfo)
{
	m_optionsTex.loadFromFile("../assets/textures/Optionas.png");
	m_optionsspr.setTexture(m_optionsTex);

	m_backgroundTex.loadFromFile("../assets/textures/Menu_text.png");
	m_backgroundspr.setTexture(m_backgroundTex);

	m_optionsButtonTex.loadFromFile("../assets/textures/Menu_Select.png");
	m_optionsButtonspr.setTexture(m_optionsButtonTex);
	m_optionsButtonspr.setPosition(sf::Vector2f(598, 202));

	font.loadFromFile("../assets/fonts/NuevaStd-Cond.otf");


	OptionsButton* b = new OptionsButton;
	m_OptionsButton.push_back(b);
	b->Index = 0;
	b->text.setString("FullScreen");
	b->text.setFont(font);
	b->text.setPosition(sf::Vector2f(415, 200));
	b->text.setCharacterSize(58);
	b->text.setColor(sf::Color(0, 0, 0, 255));

	OptionsButton* b2 = new OptionsButton;
	m_OptionsButton.push_back(b2);
	b2->Index = 1;
	b2->text.setString("Return");
	b2->text.setFont(font);
	b2->text.setPosition(sf::Vector2f(475, 340));
	b2->text.setCharacterSize(58);
	b2->text.setColor(sf::Color(0, 0, 0, 255));

	index = 0;
	m_return = false;
	m_closeGame = false;

	m_viewMngr = sysInfo.viewMngr;
}

void OptionsScene::onUpdate(float, const SystemInfo& sysInfo)
{
	if (sysInfo.inputMngr->wasKeyPressed(sf::Keyboard::Key::Up))
	{
		index--;
		m_optionsButtonspr.setPosition(sf::Vector2f(598, 202));
		if (index < 0)
		{
			index = 1;
			m_optionsButtonspr.setPosition(sf::Vector2f(598, 343));
		}
	}

	if (sysInfo.inputMngr->wasKeyPressed(sf::Keyboard::Key::Down))
	{
		index++;
		m_optionsButtonspr.setPosition(sf::Vector2f(598, 343));
		if (index > 1)
		{
			index = 0;
			m_optionsButtonspr.setPosition(sf::Vector2f(598, 202));

		}
	}

	for (int i = 0; i < m_OptionsButton.size(); i++)
	{
		if (m_OptionsButton.at(i)->Index == index)
		{
			m_OptionsButton.at(i)->text.setColor(sf::Color(255, 0, 0, 255));
		}
		else
		{
			m_OptionsButton.at(i)->text.setColor(sf::Color(0, 0, 0, 255));
		}
	}

	if (m_OptionsButton.at(0)->Index == index)
	{
		if (sysInfo.inputMngr->wasKeyPressed(sf::Keyboard::Key::Return))
		{
			m_fullscreen = !m_fullscreen;
			m_viewMngr->setFullscreen(m_fullscreen);
		}
	}
	if (m_OptionsButton.at(1)->Index == index)
	{
		if (sysInfo.inputMngr->wasKeyPressed(sf::Keyboard::Key::Return))
		{
			m_return = true;
		}
	}

	if (sysInfo.inputMngr->wasKeyPressed(sf::Keyboard::Key::Escape))
	{
		m_closeGame = true;
	}

}

bool OptionsScene::nextScene(int& nextScene) const
{
	if (m_return == true)
	{
		nextScene = MENU_SCENE;
		return true;
	}

	return false;
}

bool OptionsScene::shouldClose() const { return m_closeGame; }

void OptionsScene::onRender(sf::RenderWindow& window)
{
	window.draw(m_backgroundspr);
	window.draw(m_optionsspr);
	window.draw(m_optionsButtonspr);
	for (int i = 0; i < m_OptionsButton.size(); i++)
	{
		window.draw(m_OptionsButton.at(i)->text);
		window.draw(m_OptionsButton.at(i)->sprite);
	}
}

void OptionsScene::onExit(const SystemInfo&)
{

}