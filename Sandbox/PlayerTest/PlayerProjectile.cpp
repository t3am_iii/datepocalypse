#include "PlayerProjectile.h"
#include "Global.h"
#include "Enums.h"
#include "TextureCache.h"

#include <SFML\Graphics\RenderTarget.hpp>

PlayerProjectile::PlayerProjectile(TextureCache* texCache, float p_fX, float p_fY, float p_fSpeed)
{
	m_texCache = texCache;
	m_fSpeed = p_fSpeed;
	m_bActive = true;

	m_pxSprite = new sf::Sprite;
	m_pxSprite->setTexture(*texCache->getTexture("../assets/textures/spritesheets/projectile_spritesheet.png"));
	m_pxSprite->setTextureRect(sf::IntRect(166, 0, 167, 213));
	m_pxSprite->setScale(0.2f, 0.2f);

	//m_animTrail.setSpriteSheet(*trailTexture);
	//m_animTrail.addFrame(sf::IntRect(0, 0, 256, 512));
	//m_animTrail.addFrame(sf::IntRect(256, 0, 256, 512));
	//m_animTrail.addFrame(sf::IntRect(512, 0, 256, 512));
	//m_animTrail.addFrame(sf::IntRect(768, 0, 256, 512));
	//m_animTrail.addFrame(sf::IntRect(1024, 0, 256, 512));
	//m_animTrail.addFrame(sf::IntRect(1280, 0, 256, 512));
	//m_animTrail.addFrame(sf::IntRect(1536, 0, 256, 512));

	m_animImpact.setSpriteSheet(*m_texCache->getTexture("../assets/textures/spritesheets/projectileimpact_spritesheet.png"));
	m_animImpact.addFrame(sf::IntRect(0, 0, 159, 512));
	m_animImpact.addFrame(sf::IntRect(159, 0, 240, 512));
	m_animImpact.addFrame(sf::IntRect(399, 0, 378, 512));
	m_animImpact.addFrame(sf::IntRect(777, 0, 459, 512));

	m_animImpact.setSpriteSheet(*m_texCache->getTexture("../assets/textures/spritesheets/projectileimpact_spritesheet.png"));
	m_animNothing.addFrame(sf::IntRect(0, 0, 0, 0));

	m_animSpriteImpact.setOrigin(92, 198);
	m_animSpriteImpact.setScale(0.25, 0.25);

	//m_animSpriteTrail.setScale(0.15f, 0.30f);
	//m_animSpriteTrail.setOrigin(10, -60);
	//m_animSpriteTrail.play(m_animTrail);

	pHeartText.loadFromFile("../assets/Particles/heartpink.png");
	pLineText.loadFromFile("../assets/Particles/line.png");
	pAmount = 0;

	prevPos = { 0,0 };

	//for (int i = 0; i < 20; i++)
	//{
	//	ProjectileParticle* st = new ProjectileParticle;
	//	m_particles.push_back(st);
	//	m_particles.at(i)->sprite.setTexture(pHeartText);
	//	m_particles.at(pAmount)->sprite.setPosition(0,0);
	//	m_particles.at(pAmount)->ttl = .15f;

	//}
}

PlayerProjectile::~PlayerProjectile()
{
	//
}

void PlayerProjectile::update(float p_fDeltaTime)
{

	if (m_bImpact)
	{
		if (m_bStartImpact)
		{		
			m_animSpriteImpact.play(m_animImpact);
			m_animSpriteImpact.setFrame(0);
			m_bStartImpact = false;
		}
		//else
			//m_animSpriteImpact.update(p_fDeltaTime * 1000);
		
		else if (m_animSpriteImpact.getCurrentFrame() == 3)
		{
			m_animSpriteImpact.play(m_animNothing);
			m_bStartImpact = true;
			m_bImpact = false;
			m_bActive = false;
			return;
		}

		m_animSpriteImpact.update(p_fDeltaTime * 2000);

	}
	else
	{
		if (m_particles.size() < 5)
		{
			ProjectileParticle* p = new ProjectileParticle;
			m_particles.push_back(p);
			if (pAmount % 2 == 0)
				m_particles.at(pAmount)->sprite.setTexture(pHeartText);
			else
				m_particles.at(pAmount)->sprite.setTexture(pLineText);

			m_particles.at(pAmount)->startPos = sf::Vector2f(-100, -100);
			m_particles.at(pAmount)->sprite.setPosition(getPosition().x + 5, getPosition().y + 10);//m_particles.at(pAmount)->startPos);
			m_particles.at(pAmount)->ttl = .075f;
			pAmount++;
		}

		body->addPosition(0, -m_fSpeed * p_fDeltaTime);

		if (body->getPosition().y < -100)
		{
			m_bStartImpact = true;
			m_bImpact = false;
			m_bActive = false;
			setIsActive(false);
		}

		//m_animSpriteTrail.update(p_fDeltaTime * 2000);
	}

	// Tail
	for (int i = 0; i < m_particles.size(); i++)
	{
		float r1 = rand() % 9 - 4;
		float r2 = rand() % 3 + 1;
		int r3 = rand() % 255 + 120;
		if (m_particles.at(i)->ttl < 0)
		{
			m_particles.at(i)->sprite.setPosition(getPosition().x + 8, getPosition().y + 20);
			m_particles.at(i)->sprite.setColor(sf::Color(255, 255, 255, 255));
			m_particles.at(i)->ttl = .075f;
		}
		else
		{
			m_particles.at(i)->sprite.move(sf::Vector2f(r1, r2));
		}
		m_particles.at(i)->ttl -= p_fDeltaTime;
	}
}

void PlayerProjectile::setDirection(sf::Vector2f value) { m_Dir = value; }

bool PlayerProjectile::isActive() { return m_bActive; }

void PlayerProjectile::setIsActive(bool p_bValue)
{
	if (p_bValue == false)
	{
		pAmount = 0;
		for (int i = 0; i < m_particles.size(); i++)
			delete m_particles[i];

		m_particles.clear();
	}

	if (p_bValue)
	{
		prevPos = getPosition();
	}
	
	body->setActive(p_bValue);
	m_bActive = p_bValue;
}

void PlayerProjectile::setImpact(bool value)
{
	m_bImpact = value;
	body->setActive(false);
}

EENTITYTYPE PlayerProjectile::GetType() { return ENTITY_PLAYER_PROJECTILE; }

void PlayerProjectile::onConstruction(const GameObjectCreationKit& kit)
{
	shape = new CircleShape(20.0f);
	BodyDef def(EBodyTypes::kKinematicBody, FILTER(EEntityCategory::kPlayerProjectile, ~EEntityCategory::kPlayerProjectile), true, shape, this);
	body = kit.collisionMngr->createBody(def, this);

	m_animSpriteImpact.setLooped(true);
	m_animSpriteImpact.play(m_animNothing);
	m_bImpact = false;
	m_bStartImpact = true;
	setIsActive(true);
}

void PlayerProjectile::onDestruction(const GameObjectCreationKit& kit)
{
	kit.collisionMngr->destroyBody(body);
	body = nullptr;

	delete shape;
	shape = nullptr;
}

void PlayerProjectile::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	states.transform *= getTransform();

	for (int i = 0; i < m_particles.size(); i++)
		if (!m_bImpact)
			target.draw(m_particles.at(i)->sprite);

	for (int i = 0; i < m_starticles.size(); i++)
		target.draw(m_starticles.at(i)->sprite);

	if(!m_bImpact)
		target.draw(*m_pxSprite, states);

	if(m_bImpact)
		target.draw(m_animSpriteImpact,states);
}