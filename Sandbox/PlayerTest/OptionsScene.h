#pragma once

#include <vector>

#include <Engine\SceneSystem\BaseScene.h>

#include <SFML\Graphics\Text.hpp>
#include <SFML\Graphics\Sprite.hpp>
#include <SFML\Audio\Music.hpp>

using namespace dpEngine;

struct OptionsButton
{
	sf::Text text;
	sf::Texture texture;
	sf::Sprite sprite;
	int Index;
};

namespace dpEngine { class ViewManager; }

class OptionsScene : public SceneSystem::BaseScene
{
private:
	std::vector<OptionsButton*> m_OptionsButton;
	sf::Font font;
	int index;	

	sf::Texture m_optionsTex;
	sf::Sprite m_optionsspr;
	sf::Texture m_backgroundTex;
	sf::Sprite m_backgroundspr;
	sf::Texture m_optionsButtonTex;
	sf::Sprite m_optionsButtonspr;

	sf::Music music;

	bool m_closeGame;
	bool m_sound;
	bool m_fullscreen;
	bool m_return;

	ViewManager* m_viewMngr;

public:
	OptionsScene();

	void onEnter(const SystemInfo&);

	void onFadeIn() { }

	void onUpdate(float, const SystemInfo&);

	bool nextScene(int&) const;

	//bool nextScene(int&) const { return false; }

	bool shouldClose() const;

	void onRender(sf::RenderWindow&);

	void onFadeOut() { }

	void onExit(const SystemInfo&);

};

