#pragma once

#include <SFML/Main.hpp>
#include <SFML/Window.hpp>
#include <SFML/System.hpp>
#include <SFML/graphics.hpp>

#if !defined(NDEBUG)
#pragma comment(lib, "sfml-main-d.lib")
#pragma comment(lib, "sfml-window-d.lib")
#pragma comment(lib, "sfml-system-d.lib")
#pragma comment(lib, "sfml-graphics-d.lib")
#else
#pragma comment(lib, "sfml-main.lib")
#pragma comment(lib, "sfml-window.lib")
#pragma comment(lib, "sfml-system.lib")
#pragma comment(lib, "sfml-graphics.lib")
#endif

class Particle;

class ParticleEngine
{
public:
	
	ParticleEngine(std::vector<sf::Texture*> p_apxTextures, sf::Vector2f p_aLocation);
	~ParticleEngine();

	void Update(float p_fDeltaTime);

	Particle* GenerateNewParticle();

	sf::Vector2f GetEmitterLocatrion();
	void SetEmitterLocation(sf::Vector2f p_fVector);
	
private:
	
	//private Random random;
	sf::Vector2f m_aEmitterLocation;
	int m_iRandom;
	std::vector<Particle*> m_apxParticles;
	std::vector<sf::Texture*> m_apxTextures;

};