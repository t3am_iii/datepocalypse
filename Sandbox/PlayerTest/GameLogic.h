#pragma once

#include "IEntity.h"

#include <Engine\ViewSystem\IViewModifier.h>
#include <Engine\CollisionSystem\ContactListener.h>

#include <string>

#define __cast dynamic_cast

#define BEGIN_HANDLER_LIST() \
	void* leftData = info.leftBody->getUserData(); \
	void* rightData = info.rightBody->getUserData(); \
\
	if (leftData == nullptr || rightData == nullptr) \
		return; \
\
	IEntity* leftEntity = static_cast<IEntity*>(leftData); \
	IEntity* rightEntity = static_cast<IEntity*>(rightData); \
	if (false) { } \
\

#define ADD_CONTACT_HANDLER(lhsType, rhsType, funcName, lhsClass, rhsClass) \
	else if (leftEntity->GetType() == EENTITYTYPE::lhsType \
	&& rightEntity->GetType() == EENTITYTYPE::rhsType) \
	{ \
		funcName(__cast<lhsClass*>(leftEntity), \
			__cast<rhsClass*>(rightEntity)); \
	} \
	else if (leftEntity->GetType() == EENTITYTYPE::rhsType \
		&& rightEntity->GetType() == EENTITYTYPE::lhsType) \
	{ \
		funcName(__cast<lhsClass*>(rightEntity), \
			__cast<rhsClass*>(leftEntity)); \
	} \
\

#define ADD_CONTACT_HANDLER_SINGLE(typeName, funcName, className) \
	else if (leftEntity->GetType() == EENTITYTYPE::typeName \
	&& rightEntity->GetType() == EENTITYTYPE::typeName) \
	{ \
		funcName(__cast<className*>(leftEntity), \
			__cast<className*>(rightEntity)); \
	} \
\

#define END_HANDLER_LIST() \
	else \
	{ \
		int i = -1; \
	} \
\

/*
std::cerr << "Unhandled contact in GameLogic between: '" \
<< std::to_string(leftEntity->GetType()) \
<< "' and '" \
<< std::to_string(rightEntity->GetType()) \
<< "'" << std::endl; \
*/

class PlayerProjectile;
class PlayerCharacter;
class PlayerShield;
class Boomerang;
class Baseball;
class EnemyProjectile;
class MutatedHuman;
class MutatedCrow;
class MutatedRiotPolice;
class BossCharacter;
class EnemyShield;
class HurricaneAgent;
class Tsunami;
class ScoreManager;
class AudioManager;
class HUD;

namespace dpEngine { class ViewManager; }
using namespace dpEngine;

class GameLogic : public ContactListener, private IViewModifier
{
private:
	ScoreManager* m_scoreMngr;

	AudioManager * m_audioMngr;

	ViewManager* m_viewMngr;

	HUD* m_hud;

	int dmg;

	int m_crowValue;

	int m_humanValue;

	int m_riotPoliceValue;

	struct
	{
		bool hasReset;
		float time;
		/*struct
		{
			float dividend;
			float divisor;
		} strength;*/

		float strength;
	} m_screenShake;

public:
	GameLogic(ScoreManager*, AudioManager*, HUD*, ViewManager*);
	
	~GameLogic();
	
	void onContact(const dpEngine::ContactInfo&);


	void handlePlayerProjectile(PlayerProjectile*, MutatedHuman*);

	void handlePlayerProjectile(PlayerProjectile*, MutatedCrow*);

	void handlePlayerProjectile(PlayerProjectile*, MutatedRiotPolice*);

	void handlePlayerProjectile(PlayerProjectile*, BossCharacter*);

	void handleEnemyProjectile(EnemyProjectile*, PlayerCharacter*);

	void handleEnemyProjectile(EnemyProjectile*, PlayerShield*);


	void handleBoomerang(Boomerang*, MutatedHuman*);

	void handleBoomerang(Boomerang*, MutatedCrow*);

	void handleBoomerang(Boomerang*, MutatedRiotPolice*);

	void handleBoomerang(Boomerang*, BossCharacter*);


	void handleBaseball(Baseball*, MutatedHuman*);

	void handleBaseball(Baseball*, MutatedCrow*);

	void handleBaseball(Baseball*, MutatedRiotPolice*);

	void handleBaseball(Baseball*, BossCharacter*);


	//void handleHurricane(HurricaneAgent*, MutatedHuman*);

	void handleHurricane(HurricaneAgent*, PlayerCharacter*);
	
	
	void handlePlayers(PlayerCharacter*, PlayerCharacter*);

	void handleEnemyShield(PlayerProjectile*, EnemyShield*);

	void handleMutatedCrow(MutatedCrow*, PlayerCharacter*);

	void handleMutatedCrow(MutatedCrow*, PlayerShield*);

	void handleMutatedRiotPolice(MutatedRiotPolice*, PlayerCharacter*);

	void handleTsunami(Tsunami*, PlayerCharacter*);

	void handleTsunami(Tsunami*, MutatedHuman*);

private:
	void dealDamage(PlayerCharacter*, int);

	void dealDamage(PlayerShield*, int);

	void onProjectileHit(float=2000.f, float=.2f);
	//void onDamageDealt(PlayerProjectile*);

	void onViewUpdate(sf::View&, float);

};
