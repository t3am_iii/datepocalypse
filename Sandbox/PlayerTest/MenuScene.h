#pragma once

#include <vector>
#include <SFML/Main.hpp>
#include <SFML/Window.hpp>
#include <SFML/System.hpp>
#include <SFML/graphics.hpp>
#include <SFML/Audio.hpp>

#if !defined(NDEBUG)
#pragma comment(lib, "sfml-main-d.lib")
#pragma comment(lib, "sfml-window-d.lib")
#pragma comment(lib, "sfml-system-d.lib")
#pragma comment(lib, "sfml-graphics-d.lib")
#pragma comment(lib, "sfml-audio-d.lib")
#else
#pragma comment(lib, "sfml-main.lib")
#pragma comment(lib, "sfml-window.lib")
#pragma comment(lib, "sfml-system.lib")
#pragma comment(lib, "sfml-graphics.lib")
#endif

#include <Engine\SceneSystem\BaseScene.h>

class AudioManager;
using namespace dpEngine;

struct Button 
{
	sf::Text text;
	int Index;
};

class MenuScene : public SceneSystem::BaseScene
{
private:
	std::vector<Button*> m_Buttons;
	sf::Font font;
	int index;

	AudioManager* m_audioMngr;

	sf::Texture	m_MenuTex;
	sf::Sprite m_Menuspr;
	
	sf::String SetColor;


	bool m_playScene;
	bool m_optionsScene;
	bool m_scoreBoard;
	bool m_closeGame;

public:
	MenuScene(AudioManager*);
	~MenuScene() {}

	void draw(sf::RenderWindow &window);

	void onEnter(const SystemInfo&);

	void onFadeIn() {}

	void onUpdate(float, const SystemInfo&);

	bool nextScene(int&) const;

	bool shouldClose() const;

	void onRender(sf::RenderWindow&);

	void onFadeOut() {}

	void onExit(const SystemInfo&);

};
