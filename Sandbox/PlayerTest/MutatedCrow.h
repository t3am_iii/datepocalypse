#pragma once

#include "IEntity.h"
#include "AnimatedSprite.h"
#include "TextureCache.h"

#include <Engine\GameObjectCreationKit.h>
#include <Engine\Objects\Pawn.h>
#include <Engine\CollisionSystem\CollisionManager.h>
#include <Engine\CollisionSystem\BodyDef.h>
#include <Engine\CollisionSystem\Body.h>
#include <Engine\CollisionSystem\Enums.h>
#include <Engine\CollisionSystem\Shapes\CircleShape.h>

#include <SFML/Graphics/Sprite.hpp>
#include <SFML/System/Vector2.hpp>

enum class ECrowStates;
class PlayerManager;
class AudioManager;

using namespace dpEngine;
using namespace dpEngine::CollisionSystem;

class MutatedCrow : public IEntity, public dpEngine::Pawn
{
private:
	bool m_isDead;

	bool m_animToggle;

	bool m_isActive;

	int m_healthPoints;

	float m_speed;

	sf::Sprite m_spriteShadow;

	sf::Vector2f m_direction;

	sf::Vector2f m_targetPos;

	ECrowStates m_prevState;

	ECrowStates m_curState;

	TextureCache* m_texCache;

	PlayerManager* m_playerMngr;

	AudioManager* m_audioMngr;

	AnimatedSprite m_animSprite;

	Animation m_normalAnim;

	Animation m_chargeAnim;

	Animation m_deathAnim;

public:
	MutatedCrow(TextureCache*, PlayerManager*, AudioManager*);

	~MutatedCrow();

	void onConstruction(const GameObjectCreationKit&);

	void onDestruction(const GameObjectCreationKit&);

	void update(float);

	void setHealthPoints(int);
	
	void setDead(bool);

	void setActive(bool);

	int getHealthPoints() const;

	bool isActive() const;

	bool isDead() const;

	EENTITYTYPE GetType();

	void preDraw(sf::RenderTarget&, sf::RenderStates) const;

protected:
	void draw(sf::RenderTarget&, sf::RenderStates) const;

};
