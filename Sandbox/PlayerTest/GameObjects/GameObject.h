#pragma once

#include "../IEntity.h"

#include <Engine\Objects\Pawn.h>

enum class EEntityType
{
	kPlayerCharacter,
	kBossCharacter,
};

class GameObject : public dpEngine::Pawn, public IEntity
{
public:
	// Use:
	//virtual EENTITYTYPE getType() const = 0;
	// instead of:
	virtual EENTITYTYPE GetType() = 0;

};
