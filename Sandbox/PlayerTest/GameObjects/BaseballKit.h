#pragma once

class Baseball;
class BaseballBat;

struct BaseballKit
{
	Baseball* balls[4];
	
	BaseballBat* bat;
};
