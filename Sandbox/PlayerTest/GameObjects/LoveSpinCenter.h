#pragma once

#include "../IEntity.h"

#include <Engine\Objects\Pawn.h>
#include <Engine\GameObjectCreationKit.h>
#include <Engine\CollisionSystem\CollisionManager.h>
#include <Engine\CollisionSystem\BodyDef.h>
#include <Engine\CollisionSystem\Body.h>
#include <Engine\CollisionSystem\Enums.h>
#include <Engine\CollisionSystem\Shapes\RectangleShape.h>
#include <Engine\CollisionSystem\Shapes\CircleShape.h>

#include <SFML\Graphics\Sprite.hpp>



using namespace dpEngine;
using namespace dpEngine::CollisionSystem;

class LoveSpinCenter : public IEntity, public Pawn
{
public:
	LoveSpinCenter();

	~LoveSpinCenter();

	void update(float p_fDeltaTime);

	EENTITYTYPE GetType();

	void onConstruction(const GameObjectCreationKit&);

	void onDestruction(const GameObjectCreationKit&);

protected:
	void draw(sf::RenderTarget&, sf::RenderStates) const;

};