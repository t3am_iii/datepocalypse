#include "LoveSpinCenter.h"
#include "../Enums.h"

LoveSpinCenter::LoveSpinCenter()
{
	//
}

LoveSpinCenter::~LoveSpinCenter()
{
	//
}

void LoveSpinCenter::update(float p_fDeltaTime)
{
	//
}

EENTITYTYPE LoveSpinCenter::GetType() { return EENTITYTYPE::ENTITY_PLAYER; }

void LoveSpinCenter::onConstruction(const GameObjectCreationKit& kit)
{
	shape = new CircleShape(1.0f);
	BodyDef def(EBodyTypes::kKinematicBody, FILTER(EEntityCategory::kPlayerCharacter, EEntityCategory::kEverything), false, shape, this);
	body = kit.collisionMngr->createBody(def, this);
}

void LoveSpinCenter::onDestruction(const GameObjectCreationKit& kit)
{
	kit.collisionMngr->destroyBody(body);
	body = nullptr;

	delete shape;
	shape = nullptr;
}

void LoveSpinCenter::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	//
}