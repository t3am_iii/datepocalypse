#pragma once

#include "../IEntity.h"

#include <Engine\Objects\Pawn.h>
#include <Engine\CollisionSystem\Shapes\CircleShape.h>

#include <SFML\Graphics\Sprite.hpp>

class PlayerCharacter;

using namespace dpEngine;

class Baseball : public IEntity, public Pawn
{
private:
	sf::Sprite m_sprite;

	bool m_isActive;

	bool m_isVisible;

	int m_id;

	float m_speed;

	bool m_hasReachedGoal;
	
	sf::Vector2f m_goal;

	sf::Vector2f m_direction;

	sf::Vector2i m_modifier;

public:
	Baseball(sf::Texture*);

	~Baseball();

	void update(float);

	void setActive(bool);

	void setVisible(bool);

	void setId(int);

	void setGoal(const sf::Vector2f&);

	int getId() const;
	
	bool isActive() const;

	bool isVisible() const;

	bool hasReachedGoal() const;

	EENTITYTYPE GetType();

	void onConstruction(const dpEngine::GameObjectCreationKit&);

	void onDestruction(const dpEngine::GameObjectCreationKit&);

	void onNotify_OwnerChange(PlayerCharacter*);

protected:
	void draw(sf::RenderTarget&, sf::RenderStates) const;

};
