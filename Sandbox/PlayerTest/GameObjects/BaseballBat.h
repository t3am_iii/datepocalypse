#pragma once

#include "../IEntity.h"

#include <Engine\Objects\Pawn.h>
#include <Engine\CollisionSystem\Shapes\CircleShape.h>

#include <SFML\Graphics\Sprite.hpp>

class PlayerCharacter;

using namespace dpEngine;

class BaseballBat : public IEntity, public Pawn
{
private:
	CollisionSystem::CircleShape* m_hitBox;

	sf::Texture* m_texture;

	sf::Sprite m_sprite;

	bool m_isActive;

public:
	BaseballBat(sf::Texture*);

	~BaseballBat();

	void update(float);

	void setActive(bool);

	bool isActive() const;

	EENTITYTYPE GetType();

	void onConstruction(const dpEngine::GameObjectCreationKit&);

	void onDestruction(const dpEngine::GameObjectCreationKit&);

	void onNotify_OwnerChange(PlayerCharacter*);

protected:
	void draw(sf::RenderTarget&, sf::RenderStates) const;

};
