#include "BaseballBat.h"
#include "../Enums.h"

#include <Engine\GameObjectCreationKit.h>
#include <Engine\CollisionSystem\BodyDef.h>
#include <Engine\CollisionSystem\CollisionManager.h>

#include <SFML\Graphics\Texture.hpp>
#include <SFML\Graphics\RenderTarget.hpp>

using namespace dpEngine::CollisionSystem;

BaseballBat::BaseballBat(sf::Texture* texture)
{
	m_sprite.setTexture(*texture, true);
	m_sprite.setOrigin(64.0f, 64.0f);
	m_sprite.setScale(0.5f, 0.5f);
}

BaseballBat::~BaseballBat() { }

void BaseballBat::update(float deltaTime)
{
	//m_sprite.setPosition(getPosition());
}

void BaseballBat::setActive(bool isActive) { m_isActive = isActive; }

bool BaseballBat::isActive() const { return m_isActive; }

EENTITYTYPE BaseballBat::GetType() { return EENTITYTYPE::ENTITY_BASEBALL_BAT; }

void BaseballBat::onConstruction(const dpEngine::GameObjectCreationKit& kit)
{
	m_hitBox = new CircleShape(20.0f);
	BodyDef def(EBodyTypes::kKinematicBody, FILTER(EEntityCategory::kBoomerang, EEntityCategory::kNothing), true, m_hitBox, this);
	body = kit.collisionMngr->createBody(def, this);

	m_isActive = true;
}

void BaseballBat::onDestruction(const dpEngine::GameObjectCreationKit& kit)
{
	kit.collisionMngr->destroyBody(body);
	delete m_hitBox;
}

void BaseballBat::onNotify_OwnerChange(PlayerCharacter* newOwner)
{
	//
}

void BaseballBat::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	states.transform *= getTransform();
	target.draw(m_sprite, states);
}
