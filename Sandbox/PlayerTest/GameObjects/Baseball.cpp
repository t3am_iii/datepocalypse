#include "Baseball.h"
#include "../Enums.h"

#include <Engine\GameObjectCreationKit.h>
#include <Engine\CollisionSystem\BodyDef.h>
#include <Engine\CollisionSystem\Body.h>
#include <Engine\CollisionSystem\CollisionManager.h>
#include <Engine\Utils\Math.h>

#include <SFML\Graphics\Texture.hpp>
#include <SFML\Graphics\RenderTarget.hpp>

using namespace dpEngine::CollisionSystem;

Baseball::Baseball(sf::Texture* texture)
{
	m_sprite.setTexture(*texture);
	m_sprite.setOrigin(64.f, 64.f);
	m_sprite.setScale(.25f, .25f);
}

Baseball::~Baseball()
{
	//
}

void Baseball::update(float deltaTime)
{
	if (isVisible())
	{
		auto newPos = getPosition();
		newPos += m_speed * m_direction * deltaTime;

		auto diff = (m_goal - newPos);
		diff.x *= m_modifier.x;
		diff.y *= m_modifier.y;

		if ((diff.x + diff.y) < 0.f)
		{
			setVisible(false);
			m_hasReachedGoal = true;
		}
		else
			setPosition(newPos);
	}
}

void Baseball::setActive(bool value)
{
	m_isActive = value;
	body->setActive(value);
}

void Baseball::setVisible(bool isVisible) { m_isVisible = isVisible; }

void Baseball::setId(int id) { m_id = id; }

void Baseball::setGoal(const sf::Vector2f& goal)
{
	m_goal = goal;
	m_direction = dpEngine::FMath::Normalize(m_goal - getPosition());
	m_modifier.x = getPosition().x > m_goal.x ? -1 : 1;
	m_modifier.y = getPosition().y > m_goal.y ? -1 : 1;
	
}

int Baseball::getId() const { return m_id; }

bool Baseball::isActive() const { return m_isActive; }

bool Baseball::isVisible() const { return m_isActive && m_isVisible; }

bool Baseball::hasReachedGoal() const { return m_hasReachedGoal; }

EENTITYTYPE Baseball::GetType() { return EENTITYTYPE::ENTITY_BASEBALL_BALL; }

void Baseball::onConstruction(const dpEngine::GameObjectCreationKit& kit)
{
	shape = new CircleShape(20.0f);
	BodyDef def(EBodyTypes::kKinematicBody, FILTER(EEntityCategory::kBaseball, EEntityCategory::kEnemyCharacter), true, shape, this);
	body = kit.collisionMngr->createBody(def, this);

	m_isActive = true;
	m_isVisible = true;
	m_speed = 1000.0f;
	m_hasReachedGoal = false;
}

void Baseball::onDestruction(const dpEngine::GameObjectCreationKit& kit)
{
	kit.collisionMngr->destroyBody(body);
	delete shape;
}

void Baseball::onNotify_OwnerChange(PlayerCharacter* newOwner)
{
	//
}

void Baseball::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	states.transform = getTransform();
	target.draw(m_sprite, states);
}
