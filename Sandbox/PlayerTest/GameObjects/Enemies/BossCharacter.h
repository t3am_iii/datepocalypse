#pragma once

#include "../../IEntity.h"
#include "../../AnimatedSprite.h"

#include <Engine\Objects\Pawn.h>

#include <SFML\Graphics\Sprite.hpp>

class BossCharacter : public IEntity, public dpEngine::Pawn
{
private:
	AnimatedSprite m_animSprite;

	Animation m_normalAnim;

	bool m_isActive;

	int m_healthPoints;

	bool m_isHit;

	float m_hitEffect;

public:
	BossCharacter(sf::Texture*);

	~BossCharacter();

	void onConstruction(const dpEngine::GameObjectCreationKit&);

	void onDestruction(const dpEngine::GameObjectCreationKit&);

	void update(float);

	void setActive(bool);

	void setHealthPoints(int);

	bool isActive() const;

	int getHealthPoints() const;

	EENTITYTYPE GetType();

protected:
	void draw(sf::RenderTarget&, sf::RenderStates) const;

};
