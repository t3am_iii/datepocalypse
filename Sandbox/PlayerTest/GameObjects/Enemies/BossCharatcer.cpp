#include "BossCharacter.h"
#include "../../Global.h"
#include "../../Enums.h"

#include <Engine\GameObjectCreationKit.h>
#include <Engine\CollisionSystem\Body.h>
#include <Engine\CollisionSystem\BodyDef.h>
#include <Engine\CollisionSystem\CollisionManager.h>
#include <Engine\CollisionSystem\Shapes\CircleShape.h>

#include <SFML\Graphics\RenderTarget.hpp>

BossCharacter::BossCharacter(sf::Texture* texture)
{
	m_normalAnim.setSpriteSheet(*texture);
	m_normalAnim.addFrame(sf::IntRect(0,    0,    1024, 512));
	m_normalAnim.addFrame(sf::IntRect(1024, 0,    1024, 512));
	m_normalAnim.addFrame(sf::IntRect(2048, 0,    1024, 512));
	m_normalAnim.addFrame(sf::IntRect(0,    512,  1024, 512));
	m_normalAnim.addFrame(sf::IntRect(1024, 512,  1024, 512));
	m_normalAnim.addFrame(sf::IntRect(2048, 512,  1024, 512));
	m_normalAnim.addFrame(sf::IntRect(0,    1024, 1024, 512));
	m_normalAnim.addFrame(sf::IntRect(1024, 1024, 1024, 512));
	m_normalAnim.addFrame(sf::IntRect(2048, 1024, 1024, 512));
	
	//m_animSprite.setScale(.5f, .5f);
	m_animSprite.setOrigin(512.f, 256.f);
	m_animSprite.play(m_normalAnim);
}

BossCharacter::~BossCharacter() { }

void BossCharacter::onConstruction(const dpEngine::GameObjectCreationKit& kit)
{
	shape = new dpEngine::CollisionSystem::CircleShape(256.0f);
	// ~EEntityCategory::kEnemyProjectile & ~EEntityCategory::kPlayerCharacter
	dpEngine::CollisionSystem::BodyDef def(dpEngine::CollisionSystem::EBodyTypes::kKinematicBody, FILTER(EEntityCategory::kBossCharacter, EEntityCategory::kPlayerProjectile), false, shape, this);
	body = kit.collisionMngr->createBody(def, this);

	setActive(true);
	m_healthPoints = 15;
}

void BossCharacter::onDestruction(const dpEngine::GameObjectCreationKit& kit)
{
	kit.collisionMngr->destroyBody(body);
	body = nullptr;

	delete shape;
	shape = nullptr;
}

void BossCharacter::update(float deltaTime)
{
	m_animSprite.update(deltaTime * 1000);

	if (m_isHit)
	{
		if (m_hitEffect > 5.f)
		{
			m_animSprite.setColor(sf::Color(255, 255, 255, 255));
			m_isHit = false;
			m_hitEffect = 0.f;
		}
		else if (m_hitEffect > 4.f)
			m_animSprite.setColor(sf::Color(255, 50, 50, 255));
		else if (m_hitEffect > 3.f)
			m_animSprite.setColor(sf::Color(255, 255, 255, 255));
		else if (m_hitEffect > 2.f)
			m_animSprite.setColor(sf::Color(255, 50, 50, 255));
		else if (m_hitEffect > 1.f)
			m_animSprite.setColor(sf::Color(255, 255, 255, 255));
		else if (m_hitEffect > 0.f)
			m_animSprite.setColor(sf::Color(255, 50, 50, 255));

		m_hitEffect += deltaTime * 30.f;
	}
}

void BossCharacter::setActive(bool value) { m_isActive = value; body->setActive(value); }

void BossCharacter::setHealthPoints(int value) { m_healthPoints = value; m_isHit = true; }

bool BossCharacter::isActive() const { return m_isActive; }

int BossCharacter::getHealthPoints() const { return m_healthPoints; }

EENTITYTYPE BossCharacter::GetType() { return EENTITYTYPE::ENTITY_BOSS; }

void BossCharacter::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	states.transform *= getTransform();
	target.draw(m_animSprite, states);
}
