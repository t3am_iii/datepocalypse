#include "MutatedCrow.h"
#include "PlayerManager.h"
#include "AudioManager.h"
#include "PlayerCharacter.h"
#include "Global.h"
#include "Enums.h"

#include <Engine\Utils\Math.h>

#include <SFML\Graphics\RenderTarget.hpp>

MutatedCrow::MutatedCrow(TextureCache* texCache, PlayerManager* playerMngr, AudioManager* audioMngr)
	: m_texCache(texCache)
	, m_playerMngr(playerMngr)
	, m_audioMngr(audioMngr)
{
	m_normalAnim.setSpriteSheet(*texCache->getTexture("../assets/textures/spritesheets/mutatedcrow_spritesheet.png"));
	m_normalAnim.addFrame(sf::IntRect(0, 0, 512, 512));
	m_normalAnim.addFrame(sf::IntRect(512, 0, 512, 512));
	m_normalAnim.addFrame(sf::IntRect(1024, 0, 512, 512));

	m_chargeAnim.setSpriteSheet(*texCache->getTexture("../assets/textures/spritesheets/mutatedcrow_spritesheet.png"));
	m_chargeAnim.addFrame(sf::IntRect(0, 512, 512, 512));
	m_chargeAnim.addFrame(sf::IntRect(512, 512, 512, 512));

	m_animSprite = AnimatedSprite(100);
	m_animSprite.setScale(.35f, .35f);
	m_animSprite.setOrigin(256.f * .35f, 256.f * .35f);

	m_deathAnim.setSpriteSheet(*texCache->getTexture("../assets/textures/spritesheets/crowdeath_spritesheet.png"));
	m_deathAnim.addFrame(sf::IntRect(0, 0, 512, 512));
	m_deathAnim.addFrame(sf::IntRect(512, 0, 512, 512));
	m_deathAnim.addFrame(sf::IntRect(1024, 0, 512, 512));
	m_deathAnim.addFrame(sf::IntRect(1536, 0, 512, 512));
	m_deathAnim.addFrame(sf::IntRect(0, 512, 512, 512));
	m_deathAnim.addFrame(sf::IntRect(512, 512, 512, 512));
	m_deathAnim.addFrame(sf::IntRect(1024, 512, 512, 512));
	m_deathAnim.addFrame(sf::IntRect(1536, 512, 512, 512));

	m_spriteShadow.setTexture(*texCache->getTexture("../assets/textures/shadow.png"));
	m_spriteShadow.setScale(0.5f, 0.5f);
	m_spriteShadow.setOrigin(0, -180);
}

MutatedCrow::~MutatedCrow() { /* EMPTY */ }

void MutatedCrow::onConstruction(const GameObjectCreationKit& kit)
{
	shape = new CircleShape(30.f);
	BodyDef def(EBodyTypes::kKinematicBody, FILTER(EEntityCategory::kEnemyCharacter, EEntityCategory::kPlayerCharacter | EEntityCategory::kPlayerShield | EEntityCategory::kPlayerProjectile), false, shape, this);
	body = kit.collisionMngr->createBody(def, this);

	m_animSprite.setLooped(true);
	m_animSprite.setRotation(0.f);
	m_animSprite.play(m_normalAnim);

	m_prevState = ECrowStates::kIdle;
	m_curState = ECrowStates::kOutsideFoV;
	setActive(true);
	m_isDead = false;
	m_animToggle = true;
}

void MutatedCrow::onDestruction(const GameObjectCreationKit& kit)
{
	delete shape;
	shape = nullptr;

	kit.collisionMngr->destroyBody(body);
	body = nullptr;
}

void MutatedCrow::update(float deltaTime)
{
	m_animSprite.update(deltaTime * 1000);

	if (m_isDead)
	{
		if (m_animToggle)
		{
			m_audioMngr->playSound("../assets/sounds/enemy_death.ogg", false, 100);
			m_animSprite.play(m_deathAnim);
			m_animSprite.setLooped(false);
			m_animToggle = false;
		}

		if (!m_animSprite.isPlaying())
		{
			setActive(false);
			m_isDead = false;
		}

	}
	else if (m_prevState != m_curState)
	{
		if (m_curState == ECrowStates::kOutsideFoV)
		{
			m_direction = { 0.f, 1.f };
			m_targetPos = getPosition();
			m_targetPos.y = 0.f;
			m_speed = 100.f;
		}
		else if (m_curState == ECrowStates::kPreCharge)
		{
			m_targetPos.y += 50.f;
		}
		else if (m_curState == ECrowStates::kCharge)
		{
			sf::Vector2f pos[2] = {
				m_playerMngr->getCharacter(1)->getPosition(),
				m_playerMngr->getCharacter(2)->getPosition()
			};
			float dist[2] = {
				FMath::Length(getPosition() - pos[0]),
				FMath::Length(getPosition() - pos[1])
			};
			
			sf::Vector2f closest;
			if (dist[1] < dist[0])
				closest = pos[1];
			else
				closest = pos[0];
			
			m_direction = FMath::Normalize(closest - getPosition());
			m_targetPos = m_direction * 5000.f;
			m_speed = 550.f;
			m_animSprite.play(m_chargeAnim);
			//m_animSprite.setRotation(FMath::Degrees(m_direction) - 90.f);
		}

		m_prevState = m_curState;
	}

	if (!isDead())
	{
		sf::Vector2f delta = m_targetPos - getPosition();
		if (FMath::Length(delta) < 1.f)
		{
			if (m_curState == ECrowStates::kOutsideFoV)
				m_curState = ECrowStates::kPreCharge;
			else if (m_curState == ECrowStates::kPreCharge)
				m_curState = ECrowStates::kCharge;
		}

		addPosition(m_direction * deltaTime * m_speed);


		if (m_curState == ECrowStates::kCharge)
		{
			auto pos = getPosition();
			if (pos.x < 0.f
				|| pos.y < 0.f
				|| pos.x > SCREEN_WIDTH
				|| pos.y > SCREEN_HEIGHT)
				setActive(false);
		}
	}
}

void MutatedCrow::setHealthPoints(int value) { m_healthPoints = value; }

void MutatedCrow::setActive(bool value)
{
	m_isActive = value;
	body->setActive(value);
}

int MutatedCrow::getHealthPoints() const { return m_healthPoints; }

void MutatedCrow::setDead(bool dead) { m_isDead = dead; }

bool MutatedCrow::isActive() const { return m_isActive; }

bool MutatedCrow::isDead() const { return m_isDead; }

EENTITYTYPE MutatedCrow::GetType() { return EENTITYTYPE::ENTITY_MUTATED_CROW; }

void MutatedCrow::preDraw(sf::RenderTarget& target, sf::RenderStates states) const
{
	states.transform *= getTransform();
	target.draw(m_spriteShadow, states);
}

void MutatedCrow::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	states.transform *= getTransform();
	target.draw(m_animSprite, states);
}
