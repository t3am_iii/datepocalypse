#pragma once

#include "IEntity.h"
#include "AnimatedSprite.h"

#include <Engine\GameObjectCreationKit.h>
#include <Engine\Objects\Pawn.h>
#include <Engine\CollisionSystem\CollisionManager.h>
#include <Engine\CollisionSystem\BodyDef.h>
#include <Engine\CollisionSystem\Body.h>
#include <Engine\CollisionSystem\Enums.h>
#include <Engine\CollisionSystem\Shapes\RectangleShape.h>
#include <Engine\CollisionSystem\Shapes\CircleShape.h>

#include <SFML\Graphics\Sprite.hpp>

using namespace dpEngine;
using namespace dpEngine::CollisionSystem;


struct EProjectileParticle
{
	sf::Sprite sprite;
	sf::Vector2f startPos;
	float ttl;
};

class EnemyProjectile : public IEntity, public dpEngine::Pawn
{
public:
	EnemyProjectile(sf::Texture* p_xTexture, sf::Texture* trailTexture, float p_fX, float p_fY, float p_fSpeed);
	
	~EnemyProjectile();

	void Update(float p_fDeltaTime);

	bool IsActive();
	
	void SetIsActive(bool p_bValue);

	void setDirection(sf::Vector2f);

	EENTITYTYPE GetType();

	void onConstruction(const GameObjectCreationKit&);

	void onDestruction(const GameObjectCreationKit&);

private:
	sf::Sprite m_sprite;

	sf::Vector2f m_dir;

	Animation m_animTrail;

	AnimatedSprite m_animSpriteTrail;

	std::vector<EProjectileParticle*> m_particles;
	sf::Texture pDotText;
	sf::Texture pLineText;
	sf::Texture pLineText2;
	int pAmount;

	float m_speed;
	
	bool m_isActive;

protected:
	void draw(sf::RenderTarget&, sf::RenderStates) const;

};