#pragma once

#include "IEntity.h"
#include "AnimatedSprite.h"

#include <Engine\Objects\Pawn.h>
#include <Engine\ViewSystem\IViewModifier.h>

#include <SFML\Graphics\RectangleShape.hpp>

using namespace dpEngine;
namespace dpEngine {
	struct SystemInfo;
	class ViewManager;
}

using namespace dpEngine::CollisionSystem;

class AudioManager;
class TextureCache;

class Tsunami : public IEntity, public dpEngine::Pawn, private IViewModifier
{
private:
	AudioManager* m_audioMngr;

	ViewManager* m_viewMngr;

	AnimatedSprite m_animatedSprite;
	Animation m_tsunamiAnimation;

	AnimatedSprite m_alertAniSprite;
	Animation m_alertAnimation;
	sf::RectangleShape m_alertRect;
	float m_tmr;
	float m_ttl; // Time to live, when zero the alert will not be drawn


	bool m_warning;
	float m_tbwat;
	bool m_bTsu;
	float m_speed;
	bool m_isActive;

public:
	Tsunami(AudioManager*, TextureCache*, ViewManager*);
	
	~Tsunami();

	void update(float);

	void setActive(bool);

	bool isActive() const;

	EENTITYTYPE GetType();

	void onConstruction(const GameObjectCreationKit&);

	void onDestruction(const GameObjectCreationKit&);

protected:
	void draw(sf::RenderTarget&, sf::RenderStates) const;

private:
	void onViewUpdate(sf::View&, float);

};