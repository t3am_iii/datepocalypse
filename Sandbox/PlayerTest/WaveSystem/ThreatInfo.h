#pragma once

#include <SFML\System\Vector2.hpp>

enum class EThreatType
{
	kMutatedHuman,
	kMutatedCrow,
	kMutatedRiotPolice,
	kHurricane,
	kTsunami
};

struct ThreatInfo
{
	EThreatType type;

	sf::Vector2f location;
};
