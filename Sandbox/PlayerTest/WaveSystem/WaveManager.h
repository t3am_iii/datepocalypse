#pragma once

#include "WaveInfo.h"

class EnemyManager;

class WaveManager
{
private:
	EnemyManager* m_enemyMngr;

	std::vector<WaveInfo> m_waves;
	
	float m_nextWaveTime;

	int m_currentWave;

	int m_currentTutorialState;

	bool m_shouldSpawn;

public:
	WaveManager(EnemyManager*);

	~WaveManager();

	void update(float);

	void setWaves(const std::vector<WaveInfo>&);

	void setSpawning(bool);

	void advanceState();

};
