#pragma once

#include "ThreatInfo.h"

#include <vector>

struct WaveInfo
{
	std::vector<ThreatInfo> threats;

	int delay;
};
