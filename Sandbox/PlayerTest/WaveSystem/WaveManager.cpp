#include "WaveManager.h"
#include "../EnemyManager.h"
/*#include "../MutatedHuman.h"
#include "../MutatedCrow.h"
#include "../MutatedRiotPolice.h"*/
#include "../Global.h"

WaveManager::WaveManager(EnemyManager* enemyMngr)
	: m_enemyMngr(enemyMngr)
	, m_nextWaveTime(0.f)
	, m_currentWave(-1)
	, m_shouldSpawn(false)
	, m_currentTutorialState(4)
{ }

WaveManager::~WaveManager() { /* EMPTY */ }

void WaveManager::update(float deltaTime)
{
	if (!m_shouldSpawn)
		return;

	if (m_waves.size() < 1)
		return;

	m_nextWaveTime -= deltaTime;

	if (m_nextWaveTime < 0.f)
	{
		m_currentWave++;

		if (m_currentWave >= m_waves.size())
			m_currentWave = 0;

		auto wave = &m_waves[m_currentWave];
		for (auto threat : wave->threats)
		{
			auto loc = threat.location;
			switch (threat.type)
			{
			case EThreatType::kMutatedHuman:
				m_enemyMngr->createMutatedHuman(loc.x, loc.y);
				break;
			case EThreatType::kMutatedCrow:
				m_enemyMngr->createMutatedCrow(loc.x, loc.y);
				break;
			case EThreatType::kMutatedRiotPolice:
				m_enemyMngr->createMutatedRiotPolice(loc.x, loc.y);
				break;
			case EThreatType::kHurricane:
				m_enemyMngr->createHurricane(loc.x, loc.y);
				break;
			case EThreatType::kTsunami:
				m_enemyMngr->createTsunami(loc.x, loc.y);
				break;
			}
		}

		m_nextWaveTime += wave->delay;
	}
}

void WaveManager::setWaves(const std::vector<WaveInfo>& waves) { m_waves = waves; }

void WaveManager::setSpawning(bool value) { m_shouldSpawn = value; }

void WaveManager::advanceState()
{
	/*m_currentTutorialState++;

	if (m_currentTutorialState == 1)
	{
		auto e = m_enemyMngr->createMutatedHuman(1920 / 2 - 80, 200);
		e->setCanMove(false);
	}
	else if (m_currentTutorialState == 2)
	{
		auto e = m_enemyMngr->createMutatedHuman(1920 / 2 - 80 - 200, 200 + 50);
		e->setCanMove(false);

		e = m_enemyMngr->createMutatedHuman(1920 / 2 - 80, 200);
		e->setCanMove(false);

		e = m_enemyMngr->createMutatedHuman(1920 / 2 - 80 + 200, 200 + 50);
		e->setCanMove(false);
	}
	else if (m_currentTutorialState == 3)
	{
		auto e = m_enemyMngr->createMutatedHuman(1920 / 2 - 80 - 50, 150);
		e->setCanMove(false);

		e = m_enemyMngr->createMutatedHuman(1920 / 2 - 80, 50);
		e->setCanMove(false);

		e = m_enemyMngr->createMutatedHuman(1920 / 2 - 80 + 50, 150);
		e->setCanMove(false);
	}
	else if (m_currentTutorialState == 4)
	{
		m_enemyMngr->createBoss();*/
		/*m_enemyMngr->createHurricane(1920 / 2, 400);
		m_enemyMngr->createHurricane(1920 / 2, 400);
		m_enemyMngr->createHurricane(1920 / 2, 400);*/
	/*}
	else if (m_currentTutorialState == 5)
	{
		m_enemyMngr->createHurricane(1920 / 2, 400);
	}*/

	// 0 fiender
	// 1 fiende
	// 3 fiender i en rad
	// loop state
}
