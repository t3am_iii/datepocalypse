#pragma once

#include "IEntity.h"
#include "AnimatedSprite.h"
#include "TextureCache.h"

#include <Engine\GameObjectCreationKit.h>
#include <Engine\Objects\Pawn.h>
#include <Engine\CollisionSystem\CollisionManager.h>
#include <Engine\CollisionSystem\BodyDef.h>
#include <Engine\CollisionSystem\Body.h>
#include <Engine\CollisionSystem\Enums.h>
#include <Engine\CollisionSystem\Shapes\CircleShape.h>

#include <SFML/Main.hpp>
#include <SFML/Window.hpp>
#include <SFML/System.hpp>
#include <SFML/graphics.hpp>

using namespace dpEngine;
namespace dpEngine { struct SystemInfo; }

using namespace dpEngine::CollisionSystem;

class ProjectileManager;
class AudioManager;

class MutatedHuman : public IEntity, public dpEngine::Pawn
{
public:
	MutatedHuman(TextureCache*, ProjectileManager*, AudioManager*);
	~MutatedHuman();

	void Update(float p_fDeltaTime);

	int GetHealthPoints();
	void SetHealthPoints(int);
	
	void setCanMove(bool); // TODO: REMOVE THIS LINE

	void setActive(bool);

	void setDead(bool);

	bool isActive() const;

	bool isDead() const;

	EENTITYTYPE GetType();

	void onConstruction(const GameObjectCreationKit&);

	void onReset(const GameObjectCreationKit&);

	void onDestruction(const GameObjectCreationKit&);

	void preDraw(sf::RenderTarget&, sf::RenderStates) const;

private:
	ProjectileManager* m_projectileMngr;
	AudioManager* m_audioMngr;
	TextureCache* m_texCache;

	Animation m_forwardAnimation;
	Animation m_rightAnimation;
	Animation m_leftAnimation;
	Animation m_deathAnimation;
	AnimatedSprite m_animatedSprite;

	sf::Sprite m_spriteShadow;

	bool m_bIsActive;
	bool m_bCanMove; // TODO: REMOVE THIS LINE
	bool m_bHit;
	int m_iHitEffect;
	bool m_isDead;
	bool m_animToggle;

	int m_iHealthPoints;

	float m_fSpeed;
	float m_fMaxSpeed;

	int m_iChangeDir;
	int m_iLastDir;
	bool m_bIsShooting;
	float m_fTimer_NextShot;
	

protected:
	void draw(sf::RenderTarget&, sf::RenderStates) const;
	

};