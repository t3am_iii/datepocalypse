#pragma once
#include "SFML\System\Vector2.hpp"

class ScoreManager
{
public:
	ScoreManager();
	~ScoreManager();
	void resetScore();
	void update(float p_fDeltaTime);
	int getScore();
	void addScore(int p_iValue, sf::Vector2f);
	

	int getMultiplier();
	void addMultiplier(int);
	void resetMultiplier();

private:
	int m_iScore;
	int m_iMultiplier;
};