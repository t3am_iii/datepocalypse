#pragma once

#include "IEntity.h"
#include "AnimatedSprite.h"

#include <Engine\GameObjectCreationKit.h>
#include <Engine\Objects\Pawn.h>
#include <Engine\CollisionSystem\CollisionManager.h>
#include <Engine\CollisionSystem\BodyDef.h>
#include <Engine\CollisionSystem\Body.h>
#include <Engine\CollisionSystem\Enums.h>
#include <Engine\CollisionSystem\Shapes\RectangleShape.h>
#include <Engine\CollisionSystem\Shapes\CircleShape.h>

#include <SFML\Graphics\Sprite.hpp>

using namespace dpEngine;
using namespace dpEngine::CollisionSystem;

struct ProjectileParticle
{
	sf::Sprite sprite;
	sf::Vector2f startPos;
	float ttl;
};

class TextureCache;

class PlayerProjectile : public IEntity, public dpEngine::Pawn
{
public:
	PlayerProjectile(TextureCache*, float p_fX, float p_fY, float p_fSpeed);
	
	~PlayerProjectile();

	void update(float p_fDeltaTime);
	
	void setDirection(sf::Vector2f);

	bool isActive();
	void setIsActive(bool p_bValue);
	
	void setImpact(bool);
	
	EENTITYTYPE GetType();

	void onConstruction(const GameObjectCreationKit&);

	void onDestruction(const GameObjectCreationKit&);

private:
	TextureCache* m_texCache;

	sf::Sprite* m_pxSprite;

	Animation m_animTrail;
	AnimatedSprite m_animSpriteTrail;

	Animation m_animImpact;
	Animation m_animNothing;
	AnimatedSprite m_animSpriteImpact;
	bool m_bImpact;
	bool m_bStartImpact;

	// Particle test
	std::vector<ProjectileParticle*> m_particles;
	std::vector<ProjectileParticle*> m_starticles;
	sf::Texture pHeartText;
	sf::Texture pLineText;
	int pAmount;

	sf::Vector2f prevPos;
	sf::Vector2f curPos;
	
	bool bTest;

	sf::Vector2f m_Dir;
	float m_fSpeed;
	bool m_bActive;

protected:
	void draw(sf::RenderTarget&, sf::RenderStates) const;
};