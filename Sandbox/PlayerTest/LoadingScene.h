#pragma once
#include "SFML\System\Thread.hpp"
#include "SFML\Graphics.hpp"
#include <Engine\SceneSystem\BaseScene.h>




using namespace dpEngine;

class TextureCache;
class AudioManager;

class LoadingScene : public SceneSystem::BaseScene
{
private:
	sf::Texture loadingBackgroundTex;

	sf::Sprite loadingBackground;

	sf::Sprite  m_texSprite;

	sf::Sprite  m_audSprite;

	sf::Font m_font;

	sf::Text m_loadingText;

	int m_opacityValue;

	bool m_toggle;

	TextureCache* m_texCache;

	AudioManager* m_audioMngr;

	sf::Thread m_thread;

	int m_texProgress;

	int m_audioProgress;

	bool m_isReady;

public:
	LoadingScene(TextureCache*, AudioManager*);

	~LoadingScene();

	void loadAssets();

	void onEnter(const SystemInfo&);

	void onFadeIn();

	void onUpdate(float, const SystemInfo&);

	bool nextScene(int&) const;

	//bool nextScene(int&) const { return false; }

	bool shouldClose() const;

	void onRender(sf::RenderWindow&);

	void onFadeOut();

	void onExit(const SystemInfo&);
};