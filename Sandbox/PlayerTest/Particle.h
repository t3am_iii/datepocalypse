#pragma once

#include <SFML/Main.hpp>
#include <SFML/Window.hpp>
#include <SFML/System.hpp>
#include <SFML/graphics.hpp>

#if !defined(NDEBUG)
#pragma comment(lib, "sfml-main-d.lib")
#pragma comment(lib, "sfml-window-d.lib")
#pragma comment(lib, "sfml-system-d.lib")
#pragma comment(lib, "sfml-graphics-d.lib")
#else
#pragma comment(lib, "sfml-main.lib")
#pragma comment(lib, "sfml-window.lib")
#pragma comment(lib, "sfml-system.lib")
#pragma comment(lib, "sfml-graphics.lib")
#endif

#include <Engine\Objects\Pawn.h>
#include "IEntity.h"
#include <Engine\GameObjectCreationKit.h>
#include <Engine\CollisionSystem\CollisionManager.h>
#include <Engine\CollisionSystem\BodyDef.h>
#include <Engine\CollisionSystem\Body.h>
#include <Engine\CollisionSystem\Enums.h>
#include <Engine\CollisionSystem\Shapes\RectangleShape.h>
#include <Engine\CollisionSystem\Shapes\CircleShape.h>


class Particle
{
public:
	Particle(sf::Sprite* p_Sprite, sf::Texture* p_Texture, sf::Vector2f p_Position, sf::Vector2f p_Velocity,
		float p_Angle, float p_AngularVelocity, sf::Color p_Color, float p_Size, int p_ttl);

	~Particle();

	void Update(float p_fDeltaTime);


	sf::Sprite* GetSprite();

	sf::Texture* GetTexture();				// The texture that will be drawn to represent the particle
	void SetTexture(sf::Texture* p_Texture);
	
	sf::Vector2f GetPosition();				// The current position of the particle        
	void SetPosition(sf::Vector2f p_fVector);

	sf::Vector2f GetVelocity();				// The speed of the particle at the current instance
	void SetVelocity(sf::Vector2f p_fVector);
	
	float GetAngle();						// The current angle of rotation of the particle
	void SetAngle(float p_fValue);
	
	float GetAngularVelocity();				// The speed that the angle is changing
	void SetAngularVelocity(float p_fValue);
	
	sf::Color GetColor(); 					// The color of the particle
	void SetColor(sf::Color p_Color);
	
	float GetSize();						// The size of the particle
	void SetSize(float p_fValue);

	int GetTTL();							// The 'time to live' of the particle
	void SetTTL(int p_iValue);

	void Draw();

private:
	sf::Sprite* m_Sprite;
	sf::Texture* m_Texture;
	sf::Vector2f m_Position;
	sf::Vector2f m_Velocity;
	float m_fAngle;
	float m_fAngularVelocity;
	sf::Color m_Color;
	float m_fSize;
	int m_iTTL;
	
};