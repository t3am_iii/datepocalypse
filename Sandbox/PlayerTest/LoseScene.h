#pragma once

#include <Engine\SceneSystem\BaseScene.h>

#include <SFML\Graphics\Texture.hpp>
#include <SFML\Graphics\Sprite.hpp>

using namespace dpEngine;

class LoseScene : public SceneSystem::BaseScene
{
private:
	sf::Texture m_gameOverTex;

	sf::Sprite m_gameOverSpr;

	bool m_closeGame;
	bool m_NextScene;

public:
	LoseScene() { }

	~LoseScene() { }

	void onEnter(const SystemInfo&);

	void onFadeIn() { }

	void onUpdate(float, const SystemInfo&);

	bool nextScene(int&) const;

	bool shouldClose() const;

	void onRender(sf::RenderWindow&);

	void onFadeOut() { }

	void onExit(const SystemInfo&);

};
