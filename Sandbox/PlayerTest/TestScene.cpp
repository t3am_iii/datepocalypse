#include "TestScene.h"
#include "TextureCache.h"
#include "AudioManager.h"
#include "ProjectileManager.h"
#include "EnemyManager.h"
#include "LevelSystem\LevelManager.h"
#include "WaveSystem\WaveManager.h"
#include "PlayerManager.h"
#include "ScoreManager.h"
#include "PlayerCharacter.h"
#include "PlayerState.h"
#include "HUD.h"
#include "GameLogic.h"
#include "Global.h"
#include "Enums.h"
#include <iostream>

#include <Engine\SystemInfo.h>
#include <Engine\InputSystem\InputManager.h>
#include <Engine\CollisionSystem\CollisionManager.h>

#include <SFML\Graphics\RenderWindow.hpp>

TestScene::TestScene(TextureCache* texCache, AudioManager* audioMngr, ScoreManager* scoreMngr)
	: m_texCache(texCache)
	, m_audioMngr(audioMngr)
	, m_scoreMngr(scoreMngr)
{ /* EMPTY */ }

TestScene::~TestScene() 
{ 
	//
	m_pauseButtons.clear();
}

void TestScene::onEnter(const dpEngine::SystemInfo& sysInfo)
{
	srand(time(NULL));

	m_pause = false;
	m_restartGame = false;
	m_closeGame = false;
	m_menuScene = false;
	m_nextSceneTimer = 5;
	m_wonTimer = 2.f;

	m_projectileMngr = new ProjectileManager(sysInfo, m_texCache);
	m_playerMngr = new PlayerManager(sysInfo, m_projectileMngr, m_audioMngr, m_texCache);
	m_enemyMngr = new EnemyManager(sysInfo, m_projectileMngr, m_playerMngr, m_audioMngr, m_texCache);
	m_waveMngr = new WaveManager(m_enemyMngr);
	m_levelMngr = new LevelManager(m_waveMngr, m_enemyMngr, m_texCache);
	m_hud = new HUD(m_scoreMngr, m_playerMngr, m_texCache);
	m_gameLogic = new GameLogic(m_scoreMngr, m_audioMngr, m_hud, sysInfo.viewMngr);

	m_playerMngr->setHUD(m_hud);
	m_playerMngr->setLevelMngr(m_levelMngr);

	m_levelMngr->startup();
	sysInfo.collisionMngr->setContactHandler(m_gameLogic);
	m_audioMngr->playMusic("../assets/audio/tater_tunes_potato_power.ogg", true, 100);

	// Pause Menu
	m_fadeRect.setPosition(0, 0);
	m_fadeRect.setFillColor(sf::Color(255, 255, 255, 100));
	m_fadeRect.setSize(sf::Vector2f(SCREEN_WIDTH, SCREEN_HEIGHT));

	m_pauseMenu.setTexture(*m_texCache->getTexture("../assets/textures/pausemenu.png"));
	m_pauseMenu.setPosition(240, 135);
	m_pauseMenu.setScale(.75f, .75f);

	m_font.loadFromFile("../assets/fonts/NuevaStd-cond.otf");

	m_pauseMenuTitle = new sf::Text();
	m_pauseMenuTitle->setFont(m_font);
	m_pauseMenuTitle->setCharacterSize(100);
	m_pauseMenuTitle->setPosition(sf::Vector2f(820, 250));
	m_pauseMenuTitle->setColor(sf::Color(100, 0, 0, 255));
	m_pauseMenuTitle->setString("PAUSED");

	if (m_pauseButtons.size() < 2)
	{
		for (int i = 0; i < 2; i++)
		{
			m_pauseButtons.push_back(PauseButton());
			m_pauseButtons[i].index = i;
			m_pauseButtons[i].text.setFont(m_font);
			m_pauseButtons[i].text.setCharacterSize(60);
			m_pauseButtons[i].text.setPosition(sf::Vector2f(820, 450 + i * 100));
			if (i == 0)
				m_pauseButtons[i].text.setString("Continue");
			else if (i == 1)
				m_pauseButtons[i].text.setString("Exit");
		}
	}
	else
	{
		for (int i = 0; i < 2; i++)
		{
			m_pauseButtons[i].index = i;
			m_pauseButtons[i].text.setFont(m_font);
			m_pauseButtons[i].text.setCharacterSize(60);
			m_pauseButtons[i].text.setPosition(sf::Vector2f(820, 450 + i * 100));
			if (i == 0)
				m_pauseButtons[i].text.setString("Continue");
			else if (i == 1)
				m_pauseButtons[i].text.setString("Exit");
		}
	}
	

	m_index = 0;

	m_scoreMngr->resetScore();
}

void TestScene::onUpdate(float deltaTime, const SystemInfo& sysInfo)
{
	/*if (sysInfo.inputMngr->wasKeyPressed(sf::Keyboard::Key::R))
		m_restartGame = true;*/

	if (sysInfo.inputMngr->wasKeyPressed(sf::Keyboard::Key::Escape))
		m_closeGame = true;

	if (sysInfo.inputMngr->wasKeyPressed(sf::Keyboard::Key::U))
		m_waveMngr->advanceState();
	
	//std::cout << m_pauseButtons.size() << std::endl;
	// Pause Menu
	if (m_pause)
	{
		if (sysInfo.inputMngr->wasKeyPressed(sf::Keyboard::Key::P))
			m_pause = false;

		if (sysInfo.inputMngr->wasKeyPressed(sf::Keyboard::Key::Up))
		{
			m_index--;
			if (m_index <= 0)
				m_index = 0;
		}

		if (sysInfo.inputMngr->wasKeyPressed(sf::Keyboard::Key::Down))
		{
			m_index++;
			if (m_index >= 1)
				m_index = 1;
		}

		for (int i = 0; i < m_pauseButtons.size(); i++)
			if (m_pauseButtons[i].index == m_index)
				m_pauseButtons[i].text.setColor(sf::Color(255, 0, 0, 255));
			else
				m_pauseButtons[i].text.setColor(sf::Color(0, 0, 0, 255));

		if (m_pauseButtons[0].index == m_index)
		{
			if (sysInfo.inputMngr->wasKeyPressed(sf::Keyboard::Key::Return))
				m_pause = false;
		}
		else if (m_pauseButtons[1].index == m_index)
		{
			if (sysInfo.inputMngr->wasKeyPressed(sf::Keyboard::Key::Return))
				m_menuScene = true;
		}		
	}
	
	// In-Game Updates
	else if (!m_pause)
	{
		if (sysInfo.inputMngr->wasKeyPressed(sf::Keyboard::Key::P))
			m_pause = true;
		else
		{
			m_scoreMngr->update(deltaTime);
			m_hud->Update(deltaTime);

			// Update Players
			m_projectileMngr->Update(deltaTime);
			m_enemyMngr->update(deltaTime);
			m_waveMngr->update(deltaTime);
			m_playerMngr->update(sysInfo.inputMngr, deltaTime);
			m_levelMngr->update(deltaTime);

			if (m_levelMngr->hasWon())
				m_wonTimer -= deltaTime;

			if (!m_playerMngr->getCharacter(1)->isAlive() || !m_playerMngr->getCharacter(2)->isAlive())
				m_nextSceneTimer -= deltaTime;
		}
	}
}

bool TestScene::shouldClose() const
{
	return m_closeGame;
}

bool TestScene::nextScene(int& nextScene) const
{
	if (m_restartGame)
	{
		nextScene = GAME_SCENE;
		return true;
	}

	if (m_menuScene)
	{
		nextScene = MENU_SCENE;
		return true;
	}

	if (m_levelMngr->hasWon())
	{
		if (m_wonTimer < 0.f)
		{
			nextScene = VICTORY_SCENE;
			return true;
		}
	}
	else if(m_nextSceneTimer < 0)
	{
		nextScene = LOSE_SCENE;
		return true;
	}

	return false;
}

void TestScene::onRender(sf::RenderWindow& window)
{
	// Draw level (background)
	m_levelMngr->render(window, sf::RenderStates::Default);
	
	m_enemyMngr->preRender(window);
	m_playerMngr->render(window);
	m_enemyMngr->postRender(window);
	
	// Draw Projectiles (if active)
	m_projectileMngr->Render(window);
	
	// HUD
	m_hud->Draw(window);

	if (m_pause)
	{
		window.draw(m_fadeRect);
		window.draw(m_pauseMenu);
		
		window.draw(*m_pauseMenuTitle);
		for (int i = 0; i < m_pauseButtons.size(); i++)
			window.draw(m_pauseButtons[i].text);
	}
		
}

void TestScene::onExit(const dpEngine::SystemInfo&)
{
	delete m_enemyMngr;
	m_enemyMngr = nullptr;

	delete m_waveMngr;
	m_waveMngr = nullptr;

	delete m_pauseMenuTitle;
	m_pauseButtons.clear();
}
