#pragma once

#include <Engine\Utils\BitwiseEnumHelper.h>

enum class EBoomerangingState
{
	kNotActive,
	//kStartCharge,
	kChargingStrengthBar,
	kStartFly,
	kFlying,
	kDone,
};

enum class EBaseballingState
{
	kNotActive,
	kStartFly,
	kFlying,
	kStartHit,
	kHitting,
};

enum class EEntityTypes
{
	kBoomerang,
	kLoveSpin,
	kShield,
	kPlayerCharacter,
	kEnemyCharacter,
	kBossCharacter,
	kTsunami,
};

enum class EPlayerStates
{
	kInteractiveTutorial,
	kDead,
	kFlying,
	kFiring,
	kShielding,
	kBoomeranging,
	kBaseballing,
	kLoveSpinning,
	
	kPrev,
	kStay,
};

enum class ECrowStates
{
	kIdle,
	kOutsideFoV,
	kPreCharge,
	kCharge
};

enum class ERiotPoliceStates
{
	kIdle,
	kOutsideFoV,
	kPreAggro,
	kAggro,
	kAttack
};

enum class EBossStates
{
	kIdleing,
	kMiniguning,
	kPukeing,
	kGrenading,
	kStay,
};

enum class EActions
{
	kWalkUp		= 1,
	kWalkLeft	= 2,
	kWalkDown	= 3,
	kWalkRight	= 4,
	kFire		= 5,
	kShield		= 6,
	kBoomerang	= 7,
	kBaseball	= 8,
	kLoveSpin	= 9,
};

// I am a ... (category bits)
// I collide with ... (mask bits)

enum class EEntityCategory
{
    kPlayerCharacter	= 0x0002,
	kEnemyCharacter		= 0x0004,
	kBossCharacter		= 0x0008,
    kBlock				= 0x0010,
	kEnemyProjectile	= 0x0020,
	kEnemyShield		= 0x0040,
	kPlayerProjectile	= 0x0080,
	kPlayerShield		= 0x0100,
	kBoomerang			= 0x0200,
	kBaseball			= 0x0400,
	kLoveSpin			= 0x0800,
	kHurricane			= 0x1000,
	kTsunami			= 0x2000,

    kEverything			= 0xffff,
	kNothing			= 0x0000,
};

#define FILTER(c, m) (dpEngine::CollisionSystem::Filter(static_cast<__BIT_TYPE_>(c), static_cast<__BIT_TYPE_>(m)))

BITWISE_ENUM_DECLARE(EEntityCategory)
