#include "Tsunami.h"
#include "AudioManager.h"
#include "TextureCache.h"
#include "Global.h"
#include "Enums.h"

#include <Engine\GameObjectCreationKit.h>
#include <Engine\ViewSystem\ViewManager.h>
#include <Engine\CollisionSystem\CollisionManager.h>
#include <Engine\CollisionSystem\BodyDef.h>
#include <Engine\CollisionSystem\Body.h>
#include <Engine\CollisionSystem\Enums.h>
#include <Engine\CollisionSystem\Shapes\RectangleShape.h>

Tsunami::Tsunami(AudioManager* audioMngr, TextureCache* texCache, ViewManager* viewMngr)
	: m_audioMngr(audioMngr)
	, m_viewMngr(viewMngr)
{
	m_tsunamiAnimation.setSpriteSheet(*texCache->getTexture("../assets/textures/spritesheets/tsunami_spritesheet.png"));
	m_tsunamiAnimation.addFrame(sf::IntRect(0, 0, 500, 814));
	m_tsunamiAnimation.addFrame(sf::IntRect(500, 0, 500, 814));
	m_tsunamiAnimation.addFrame(sf::IntRect(1000, 0, 500, 814));
	m_tsunamiAnimation.addFrame(sf::IntRect(1500, 0, 500, 814));
	
	m_animatedSprite = AnimatedSprite(50);
	m_animatedSprite.setColor(sf::Color(255, 255, 255, 235));
	m_animatedSprite.setScale(4, 8);
	//m_animatedSprite.setOrigin(200, 0);
	m_animatedSprite.play(m_tsunamiAnimation);

	m_alertAnimation.setSpriteSheet(*texCache->getTexture("../assets/textures/alert.png"));
	m_alertAnimation.addFrame(sf::IntRect(0, 0, 960, 128));
	
	m_alertAniSprite = AnimatedSprite(50);
	m_alertAniSprite.setColor(sf::Color(255, 255, 255, 200));
	m_alertAniSprite.setPosition(SCREEN_WIDTH / 2 - 480, 0);
	m_alertAniSprite.setScale(1.f, 1.f);
	m_alertAniSprite.play(m_alertAnimation);
}

Tsunami::~Tsunami()
{
	//
}

void Tsunami::update(float deltaTime)
{
	if (m_warning)
	{
		m_audioMngr->playSound("../assets/sounds/tsunami_warning.ogg", 0, 100);
	
		m_warning = false;
	}
	if (!m_bTsu &&  m_tbwat >= 80)
	{
		m_audioMngr->playSound("../assets/sounds/tsunami.ogg", 0, 100);
		m_tbwat = 0;
		m_bTsu = true;
	}
	else
		m_tbwat++;

	//if(m_animatedSprite.getPosition().y + m_animatedSprite.getGlobalBounds().height > 100 )
		//m_audioMngr->playSound("../assets/sounds/tsunami.wav", 0, 100);
		 
	m_animatedSprite.update(deltaTime * 1000.f);
	addPosition(0.f, 1500.f * deltaTime);

	m_alertAniSprite.update(deltaTime * 1000.f);
	m_alertAniSprite.setColor(sf::Color(255, 255, 255, m_tmr));
	
	m_tmr += 700 * deltaTime;
	m_ttl -= deltaTime;
	
	if (getPosition().y > SCREEN_HEIGHT)
		setActive(false);
}

void Tsunami::setActive(bool value)
{
	if (value) m_viewMngr->addModifier(this);
	//else m_viewMngr->rem
	body->setActive(value);
	m_isActive = value;
}

bool Tsunami::isActive() const { return m_isActive; }

EENTITYTYPE Tsunami::GetType() { return EENTITYTYPE::ENTITY_TSUNAMI;}

void Tsunami::onConstruction(const GameObjectCreationKit& kit)
{
	// This method is called immediately after
	// the constructor is called.

	shape = new RectangleShape(1920.f, 407.f);
	BodyDef def(EBodyTypes::kKinematicBody, FILTER(EEntityCategory::kTsunami, EEntityCategory::kEverything), true, shape, this);
	body = kit.collisionMngr->createBody(def, this);

	m_tmr = 0.f;
	m_ttl = 10.f;
	m_tbwat = 0;
	m_bTsu = false;
	m_warning = true;

	setActive(true);
}

void Tsunami::onDestruction(const GameObjectCreationKit& kit)
{
	kit.collisionMngr->destroyBody(body);
	body = nullptr;

	delete shape;
	shape = nullptr;
}

void Tsunami::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	states.transform *= getTransform();
	target.draw(m_animatedSprite, states);

	if (m_ttl > 0.f)
		target.draw(m_alertAniSprite);
}

void Tsunami::onViewUpdate(sf::View& view, float deltaTime)
{
	auto pos = view.getCenter();
	auto screenShake = ((rand() % 3000 - 1500) / 100);
	//view.setCenter(pos + sf::Vector2f(screenShake, -screenShake));
}
