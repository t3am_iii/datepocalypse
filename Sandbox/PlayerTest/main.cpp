#if !defined(NDEBUG)
  #pragma comment(lib, "sfml-main-d.lib")
  #pragma comment(lib, "sfml-window-d.lib")
  #pragma comment(lib, "sfml-system-d.lib")
  #pragma comment(lib, "sfml-graphics-d.lib")
  #pragma comment(lib, "sfml-audio-d.lib")
#else
  #pragma comment(lib, "sfml-main.lib")
  #pragma comment(lib, "sfml-window.lib")
  #pragma comment(lib, "sfml-system.lib")
  #pragma comment(lib, "sfml-graphics.lib")
  #pragma comment(lib, "sfml-audio.lib")
#endif

#pragma comment(lib, "Engine.lib")

#include "TestScene.h"
#include "LoadingScene.h"
#include "LoseScene.h"
#include "MenuScene.h"
#include "OptionsScene.h"
#include "ScoreBoard.h"
#include "Scenes\VictoryScene.h"
#include "Global.h"
#include "TextureCache.h"
#include "AudioManager.h"
#include "ScoreManager.h"

#include <Engine\Engine.h>


int main(int argc, char** argv)
{

	dpEngine::Engine engine;
	engine.startup(SCREEN_WIDTH, SCREEN_HEIGHT, "Datepocalypse - Beta");
	TextureCache* m_texCache = new TextureCache();
	AudioManager* m_audioMngr = new AudioManager();
	ScoreManager* m_scoreMngr = new ScoreManager();

	engine.addScene(new MenuScene(m_audioMngr), true);
	engine.addScene(new OptionsScene);
	engine.addScene(new LoadingScene(m_texCache, m_audioMngr));
	engine.addScene(new TestScene(m_texCache, m_audioMngr, m_scoreMngr));
	engine.addScene(new LoseScene);
	engine.addScene(new VictoryScene(m_scoreMngr, m_texCache, m_audioMngr));
	engine.addScene(new ScoreBoard);

	engine.run();
	engine.shutdown();

	//system("PAUSE");
	
	return 0;


	//sf::RenderWindow window;
	//window.create(sf::VideoMode(1280, 720), "sfml-app", sf::Style::Titlebar | sf::Style::Close);
	//if (!window.isOpen()) return -1;

	//const float targettime = 1.0f / 60.0f;
	//float accumulator = 0.0f;
	//float frametime = 0.0f;

	//sf::Clock clock;

	//while (window.isOpen())
	//{
	//	sf::Time deltatime = clock.restart();
	//	frametime = std::min(deltatime.asSeconds(), 0.1f);
	//	if (frametime > 0.1f)
	//		frametime = 0.1f;

	//	accumulator += frametime;
	//	while (accumulator > targettime)
	//	{
	//		accumulator -= targettime;
	//		sf::Event event;
	//		while (window.pollEvent(event))
	//		{
	//			if (event.type == sf::Event::Closed)
	//				window.close();
	//		}
	//	}
	//	window.clear(sf::Color(0x11, 0x22, 0x33, 0xff));
	//	window.display();

	//	//sf::sleep(sf::milliseconds(10));
	//}
	//return 0;
	
}