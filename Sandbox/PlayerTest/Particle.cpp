
#include "Particle.h"

Particle::Particle(sf::Sprite* p_Sprite, sf::Texture* p_Texture, sf::Vector2f p_Position, sf::Vector2f p_Velocity,
	float p_Angle, float p_AngularVelocity, sf::Color p_Color, float p_Size, int p_ttl)
{
	m_Sprite = p_Sprite;
	m_Texture = p_Texture;
	m_Position = p_Position;
	m_Velocity = p_Velocity;
	m_fAngle =p_Angle;
	m_fAngularVelocity = p_AngularVelocity;
	m_Color = p_Color;
	m_fSize = p_Size;
	m_iTTL = p_ttl;
}

Particle::~Particle()
{

}

void Particle::Update(float p_fDeltaTime)
{
	m_iTTL--;
	m_Position += m_Velocity;
	m_fAngle += m_fAngularVelocity;
}

sf::Sprite* Particle::GetSprite() { return m_Sprite; }

sf::Texture* Particle::GetTexture() { return m_Texture;}				// The texture that will be drawn to represent the particle
void Particle::SetTexture(sf::Texture* p_Texture) { m_Texture = p_Texture; }

sf::Vector2f Particle::GetPosition() { return m_Position; }					// The current position of the particle        
void Particle::SetPosition(sf::Vector2f p_fVector) { m_Position = p_fVector; }

sf::Vector2f Particle::GetVelocity() { return m_Velocity; }					// The speed of the particle at the current instance
void Particle::SetVelocity(sf::Vector2f p_fVector) { m_Velocity = p_fVector; }

float Particle::GetAngle() { return m_fAngle; }						// The current angle of rotation of the particle
void Particle::SetAngle(float p_fValue) { m_fAngle = p_fValue; }

float Particle::GetAngularVelocity() { return m_fAngularVelocity; }					// The speed that the angle is changing
void Particle::SetAngularVelocity(float p_fValue) { m_fAngularVelocity = p_fValue; }

sf::Color Particle::GetColor() { return m_Color; }	 					// The color of the particle
void Particle::SetColor(sf::Color p_Color) { m_Color = p_Color; }

float Particle::GetSize() { return m_fSize; }							// The size of the particle
void Particle::SetSize(float p_fValue) { m_fSize = p_fValue; }

int Particle::GetTTL() { return m_iTTL; }							// The 'time to live' of the particle
void Particle::SetTTL(int p_iValue) { m_iTTL = p_iValue; }

//void Particle::Draw(SpriteBatch spriteBatch)
//{
//	sf::RectangleShape sourceRectangle;
//	sourceRectangle.setSize(sf::Vector2f(m_Texture->getSize().x, (m_Texture->getSize().y)));
//	sf::Vector2f origin = sf::Vector2f(m_Texture->getSize().x / 2, (m_Texture->getSize().y) / 2);
//
//	spriteBatch.Draw(m_Texture, m_Position, sourceRectangle, m_Color,
//		m_fAngle, origin, m_fSize, SpriteEffects.None, 0f);
//}