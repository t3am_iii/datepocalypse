#pragma once

#include <string>

#include <SFML\Graphics\Text.hpp>

struct ScoreEntry
{
	std::string name;
	int score;
	sf::Text text;
};
