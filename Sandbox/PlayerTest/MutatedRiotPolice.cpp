#include "MutatedRiotPolice.h"
#include "EnemyShield.h"
#include "EnemyManager.h"
#include "PlayerManager.h"
#include "PlayerCharacter.h"
#include "Global.h"
#include "Enums.h"
#include "AudioManager.h"

#include <Engine\CollisionSystem\Shapes\RectangleShape.h>

#include <SFML\Graphics\RenderTarget.hpp>

using CollisionSystem::BodyDef;
using CollisionSystem::EBodyTypes;


//note:
//animationerna b�r k�ras
//normal frames: 1,1,2,2,3,3
//attack frames: 1,2,3

MutatedRiotPolice::MutatedRiotPolice(TextureCache* texCache, EnemyManager* enemyMngr, PlayerManager* playerMngr, AudioManager* audioMngr)
	: m_texCache(texCache)
	, m_enemyMngr(enemyMngr)
	, m_playerMngr(playerMngr)
	, m_audioMngr(audioMngr)
	, m_shield(nullptr)
{
	m_sprite.setTexture(*texCache->getTexture("../assets/textures/spritesheets/mutatedriotpolice_spritesheet.png"));
	m_sprite.setScale(.25f, .25f);
	m_sprite.setOrigin(714.f * .5f * .25f, 1029.f * .5f * .25f);

	m_normalAnim.setSpriteSheet(*texCache->getTexture("../assets/textures/spritesheets/mutatedriotpolice_spritesheet.png"));
	m_normalAnim.addFrame(sf::IntRect(0, 0, 512, 512));
	m_normalAnim.addFrame(sf::IntRect(0, 0, 512, 512));
	m_normalAnim.addFrame(sf::IntRect(512, 0, 512, 512));
	m_normalAnim.addFrame(sf::IntRect(512, 0, 512, 512));
	m_normalAnim.addFrame(sf::IntRect(1024, 0, 512, 512));
	m_normalAnim.addFrame(sf::IntRect(1024, 0, 512, 512));

	m_attackAnim.setSpriteSheet(*texCache->getTexture("../assets/textures/spritesheets/mutatedriotpolice_spritesheet.png"));
	m_attackAnim.addFrame(sf::IntRect(1536, 0, 512, 512));
	m_attackAnim.addFrame(sf::IntRect(0, 512, 512, 512));
	m_attackAnim.addFrame(sf::IntRect(512, 512, 512, 512));

	m_deathAnim.setSpriteSheet(*texCache->getTexture("../assets/textures/spritesheets/enemydeath_spritesheet.png"));
	m_deathAnim.addFrame(sf::IntRect(0, 0, 512, 512));
	m_deathAnim.addFrame(sf::IntRect(512, 0, 512, 512));
	m_deathAnim.addFrame(sf::IntRect(1024, 0, 512, 512));
	m_deathAnim.addFrame(sf::IntRect(1536, 0, 512, 512));
	m_deathAnim.addFrame(sf::IntRect(0, 512, 512, 512));
	m_deathAnim.addFrame(sf::IntRect(512, 512, 512, 512));
	m_deathAnim.addFrame(sf::IntRect(1024, 512, 512, 512));
	m_deathAnim.addFrame(sf::IntRect(1536, 512, 512, 512));

	m_animSprite = AnimatedSprite(100);
	m_animSprite.setScale(.5f, .5f);
	m_animSprite.setOrigin(512.f * .25f, 512.f * .25f);

	m_spriteShadow.setTexture(*texCache->getTexture("../assets/textures/shadow.png"));
	m_spriteShadow.setScale(.75f, .75f);
	m_spriteShadow.setOrigin(10, -140);
}

MutatedRiotPolice::~MutatedRiotPolice() { /* EMPTY */ }

void MutatedRiotPolice::onConstruction(const GameObjectCreationKit& kit)
{
	shape = new CollisionSystem::CircleShape(60.0f);
	BodyDef def(EBodyTypes::kKinematicBody, FILTER(EEntityCategory::kEnemyCharacter, EEntityCategory::kEverything), false, shape, this);
	body = kit.collisionMngr->createBody(def, this);

	if (m_shield)
		m_shield->setActive(false);
	m_shield = m_enemyMngr->createShield();

	setActive(true);
	m_prevState = ERiotPoliceStates::kIdle;
	m_curState = ERiotPoliceStates::kOutsideFoV;
	m_animSprite.play(m_normalAnim);
}

void MutatedRiotPolice::onDestruction(const GameObjectCreationKit& kit)
{
	delete shape;
	shape = nullptr;
	
	kit.collisionMngr->destroyBody(body);
	body = nullptr;
}

void MutatedRiotPolice::update(float deltaTime)
{
	m_animSprite.update(deltaTime * 1000);

	if (m_isDead)
	{
		if (m_animToggle)
		{
			m_audioMngr->playSound("../assets/sounds/enemy_death.ogg", false, 100);
			m_animSprite.play(m_deathAnim);
			m_animSprite.setLooped(false);
			m_animToggle = false;
		}

		if (!m_animSprite.isPlaying())
		{
			setActive(false);
			m_isDead = false;
		}
	}
	else if (!m_isDead)
	{

		// Movement //
		if (m_prevState != m_curState)
		{
			if (m_curState == ERiotPoliceStates::kOutsideFoV)
			{
				m_direction = { 0.f, 1.f };
				m_targetPos = getPosition();
				m_targetPos.y = 0.f;
				m_speed = 100.f;
			}
			else if (m_curState == ERiotPoliceStates::kPreAggro)
			{
				m_targetPos.y += 50.f;
			}
			else if (m_curState == ERiotPoliceStates::kAggro)
			{
				sf::Vector2f pos[2] = {
					m_playerMngr->getCharacter(1)->getPosition(),
					m_playerMngr->getCharacter(2)->getPosition()
				};
				float dist[2] = {
					FMath::Length(getPosition() - pos[0]),
					FMath::Length(getPosition() - pos[1])
				};

				sf::Vector2f closest;
				if (dist[1] < dist[0])
					closest = pos[1];
				else
					closest = pos[0];

				m_direction = FMath::Normalize(closest - getPosition());
				m_targetPos = m_direction * 5000.f;
				m_speed = 200.f;
				//m_animSprite.setAnimation(m_attackAnim);
			}
			else if (m_curState == ERiotPoliceStates::kAttack)
			{
				m_animSprite.setAnimation(m_attackAnim);
				m_animSprite.setLooped(false);
			}

			m_prevState = m_curState;
		}

		sf::Vector2f delta = m_targetPos - getPosition();
		if (FMath::Length(delta) < 1.f)
		{
			if (m_curState == ERiotPoliceStates::kOutsideFoV)
				m_curState = ERiotPoliceStates::kPreAggro;
			else if (m_curState == ERiotPoliceStates::kPreAggro)
				m_curState = ERiotPoliceStates::kAggro;
		}

		addPosition(m_direction * deltaTime * m_speed);

		if (m_curState == ERiotPoliceStates::kAggro)
		{
			auto pos = getPosition();
			if (pos.x < 0.f
				|| pos.y < 0.f
				|| pos.x > SCREEN_WIDTH
				|| pos.y > SCREEN_HEIGHT)
				setActive(false);
		}

		if (m_curState == ERiotPoliceStates::kAttack)
		{
			if (!m_animSprite.isPlaying())
			{
				//m_curState = ERiotPoliceStates::kPreAggro;
				m_animSprite.setAnimation(m_normalAnim);
				m_animSprite.setLooped(true);
				m_animSprite.play(m_normalAnim);
				m_direction = { 0.f, 1.f };
				m_speed = 100;
				//m_targetPos = getPosition();
			}
		}

		// -End- Movement //

		//if (getPosition().y > SCREEN_HEIGHT)
		//	setPosition(getPosition().x, -200.0f);
		//
		if (getPosition().x < 450)
		{
			setPosition(450, getPosition().y);
		}
		else if (getPosition().x > SCREEN_WIDTH - 560)
		{
			setPosition(SCREEN_WIDTH - 560, getPosition().y);
		}

		m_shield->setPosition(getPosition());
		m_shield->addPosition(0, 0.f);

		// Hit FeedBack // 
		if (m_isHit && m_hitEffect < 6)
		{
			if (m_hitEffect == 0)
				m_animSprite.setColor(sf::Color(255, 50, 50, 255));
			else if (m_hitEffect == 1)
				m_animSprite.setColor(sf::Color(255, 255, 255, 255));
			else if (m_hitEffect == 2)
				m_animSprite.setColor(sf::Color(255, 50, 50, 255));
			else if (m_hitEffect == 3)
				m_animSprite.setColor(sf::Color(255, 255, 255, 255));
			else if (m_hitEffect == 4)
				m_animSprite.setColor(sf::Color(255, 50, 50, 255));
			else if (m_hitEffect == 5)
			{
				m_animSprite.setColor(sf::Color(255, 255, 255, 255));
				m_isHit = false;
				m_hitEffect = 0;
			}

			m_hitEffect++;
		}
		else
			m_animSprite.setColor(sf::Color(255, 255, 255, 255));
		// -End- Hit FeedBack //
	}


}

void MutatedRiotPolice::setHealthPoints(int value) { m_healthPoints = value; m_isHit = true;}

void MutatedRiotPolice::setDead(bool dead) { m_isDead = dead; }

void MutatedRiotPolice::setActive(bool value)
{
	if (value)
	{
		m_healthPoints = 10;
		m_animToggle = true;
		m_isDead = false;
		m_isHit = false;
		m_hitEffect = 0;
		m_aggroRefresh = 0;
		m_bAggro = false;
	}

	m_isActive = value;
	body->setActive(value);
	if (!value || (value && m_shield->getArmourPoints() > 0))
		m_shield->setActive(value);
}

void MutatedRiotPolice::isAttacking() { m_curState = ERiotPoliceStates::kAttack; }

int MutatedRiotPolice::getHealthPoints() const { return m_healthPoints; }

bool MutatedRiotPolice::isActive() const { return m_isActive; }

EENTITYTYPE MutatedRiotPolice::GetType() { return EENTITYTYPE::ENTITY_MUTATED_RIOT_POLICE; }

void MutatedRiotPolice::preDraw(sf::RenderTarget& target, sf::RenderStates states) const
{
	states.transform *= getTransform();
	target.draw(m_spriteShadow, states);
}


void MutatedRiotPolice::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	auto parentTransform = states.transform;
	
	states.transform *= getTransform();
	target.draw(m_animSprite, states);

	if (m_shield->isActive())
		target.draw(*m_shield, parentTransform);
}
