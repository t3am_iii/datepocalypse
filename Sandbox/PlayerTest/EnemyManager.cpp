#include "EnemyManager.h"
#include "TextureCache.h"
#include "MutatedHuman.h"
#include "MutatedCrow.h"
#include "MutatedRiotPolice.h"
#include "EnemyShield.h"
#include "Tsunami.h"
#include "GameObjects\Enemies\BossCharacter.h"
#include "Controllers\BossController.h"
#include "EnemyShield.h"
#include "NaturalDisasters\HurricaneAgent.h"

#include <Engine\SystemInfo.h>
#include <algorithm>



EnemyManager::EnemyManager(const dpEngine::SystemInfo& sysInfo, ProjectileManager* projectileMngr, PlayerManager* playerMngr, AudioManager* audioMngr, TextureCache* texCache)
	: m_projectileMngr(projectileMngr)
	, m_playerMngr(playerMngr)
	, m_audioMngr(audioMngr)
	, m_texCache(texCache)
{
	m_kit.collisionMngr = sysInfo.collisionMngr;
	m_viewMngr = sysInfo.viewMngr;

	m_bossTexture.loadFromFile("../assets/textures/spritesheets/boss.png");
}

EnemyManager::~EnemyManager()
{
	for (auto e : m_mutatedHumans)
	{
		e->onDestruction(m_kit);
		delete e;
	}
	m_mutatedHumans.clear();

	for (auto e : m_mutatedCrows)
	{
		e->onDestruction(m_kit);
		delete e;
	}
	m_mutatedCrows.clear();

	for (auto e : m_mutatedRiotPolices)
	{
		e->onDestruction(m_kit);
		delete e;
	}
	m_mutatedRiotPolices.clear();

	for (auto e : m_shields)
	{
		e->onDestruction(m_kit);
		delete e;
	}
	m_shields.clear();

	if (m_bossCharacter != nullptr)
		delete m_bossCharacter;

	if (m_bossController != nullptr)
		delete m_bossController;
}

MutatedHuman* EnemyManager::createMutatedHuman(float x, float y)
{
	for (auto e : m_mutatedHumans)
	{
		if (!e->isActive())
		{
			e->onDestruction(m_kit);
			e->onConstruction(m_kit);
			e->setPosition(x, y);
			return e;
		}
	}

	auto e = new MutatedHuman(m_texCache, m_projectileMngr, m_audioMngr);
	m_mutatedHumans.push_back(e);
	e->onConstruction(m_kit);
	e->setPosition(x, y);
	return e;
}

MutatedCrow* EnemyManager::createMutatedCrow(float x, float y)
{
	for (auto e : m_mutatedCrows)
	{
		if (!e->isActive())
		{
			e->onDestruction(m_kit);
			e->onConstruction(m_kit);
			e->setPosition(x, y);
			return e;
		}
	}

	auto e = new MutatedCrow(m_texCache, m_playerMngr, m_audioMngr);
	m_mutatedCrows.push_back(e);
	e->onConstruction(m_kit);
	e->setPosition(x, y);
	return e;
}

MutatedRiotPolice* EnemyManager::createMutatedRiotPolice(float x, float y)
{
	for (auto e : m_mutatedRiotPolices)
	{
		if (!e->isActive())
		{
			e->onDestruction(m_kit);
			e->onConstruction(m_kit);
			e->setPosition(x, y);
			return e;
		}
	}

	auto e = new MutatedRiotPolice(m_texCache, this, m_playerMngr, m_audioMngr);
	m_mutatedRiotPolices.push_back(e);
	e->onConstruction(m_kit);
	e->setPosition(x, y);
	return e;
}

Tsunami* EnemyManager::createTsunami(float x, float y)
{
	m_tsunami = new Tsunami(m_audioMngr, m_texCache, m_viewMngr);
	m_tsunami->onConstruction(m_kit);
	m_tsunami->setPosition(x, y);
	return m_tsunami;
}

BossCharacter* EnemyManager::createBoss(float x, float y)
{
	auto character = createBossCharacter();
	character->setPosition(x, y);
	auto controller = createBossController();
	controller->setCharacter(character);

	m_bossCharacter = character;
	m_bossController = controller;

	return m_bossCharacter;
}

EnemyShield* EnemyManager::createShield()
{
	for (auto e : m_shields)
	{
		if (!e->isActive())
		{
			e->onDestruction(m_kit);
			e->onConstruction(m_kit);
			return e;
		}
	}

	auto e = new EnemyShield(m_texCache);
	m_shields.push_back(e);
	e->onConstruction(m_kit);
	return e;
}

void EnemyManager::createHurricane(float x, float y)
{
	Path path;
	path.loop = true;

	path.points.push_back(sf::Vector2f(x, y));
	path.points.push_back(sf::Vector2f(x + 500, y));
	path.points.push_back(sf::Vector2f(x + 500, y + 200));
	path.points.push_back(sf::Vector2f(x - 500, y + 200));
	path.points.push_back(sf::Vector2f(x - 500, y + 400));
	path.points.push_back(sf::Vector2f(x + 500, y + 400));

	createHurricaneAgent(x, y, path);
}

void EnemyManager::update(float deltaTime)
{
	for (auto e : m_mutatedCrows)
		if (e->isActive())
			e->update(deltaTime);

	for (auto e : m_mutatedRiotPolices)
		if (e->isActive())
			e->update(deltaTime);

	for (auto e : m_shields)
		if (e->isActive())
			e->update(deltaTime);

	for (auto e : m_mutatedHumans)
		if (e->isActive())
			e->Update(deltaTime);

	std::vector<MutatedHuman*> mHumans = m_mutatedHumans;

	if (m_bossController)
		m_bossController->update(deltaTime);
	if (m_bossCharacter)
		m_bossCharacter->update(deltaTime);

	for (auto e : m_hurricanes)
		if (e->isActive())
			e->update(deltaTime);

	if (m_tsunami != nullptr)
		m_tsunami->update(deltaTime);

	m_orderables.clear();
	for (auto e : m_mutatedHumans)
	{
		if (!e->isActive())
			continue;
		orderable o;
		o.pos = e->getPosition();
		o.obj = e;
		m_orderables.push_back(o);
	}
	for (auto e : m_mutatedCrows)
	{
		if (!e->isActive())
			continue;
		orderable o;
		o.pos = e->getPosition();
		o.obj = e;
		m_orderables.push_back(o);
	}
	for (auto e : m_mutatedRiotPolices)
	{
		if (!e->isActive())
			continue;
		orderable o;
		o.pos = e->getPosition();
		o.obj = e;
		m_orderables.push_back(o);
	}

	std::sort(m_orderables.begin(), m_orderables.end(), [this](auto l, auto r) {return l.pos.y < r.pos.y; });
}

void EnemyManager::preRender(sf::RenderTarget& target)
{
	sf::RenderStates states = sf::RenderStates::Default;
	

	for (auto e : m_mutatedHumans)
		if (e->isActive())
			e->preDraw(target, states);

	for (auto e : m_mutatedCrows)
		if (e->isActive())
			e->preDraw(target, states);

	for (auto e : m_mutatedRiotPolices)
		if (e->isActive())
			e->preDraw(target, states);

	//for (auto e : m_mutatedHumans)
	//	if (e->isActive())
	//		target.draw(*e, states);

	//for (auto e : m_mutatedCrows)
	//	if (e->isActive())
	//		target.draw(*e, states);

	//for (auto e : m_mutatedRiotPolices)
	//	if (e->isActive())
	//		target.draw(*e, states);

	for (auto e : m_orderables)
		target.draw(*e.obj,states);

	if (m_bossCharacter)
		if (m_bossCharacter->isActive())
			target.draw(*m_bossCharacter);

	for (auto e : m_hurricanes)
		if (e->isActive())
			target.draw(*e);

}

void EnemyManager::postRender(sf::RenderTarget& target)
{
	sf::RenderStates states = sf::RenderStates::Default;

	if (m_tsunami != nullptr)
		target.draw(*m_tsunami);
}



BossCharacter* EnemyManager::createBossCharacter()
{
	auto c = new BossCharacter(&m_bossTexture);
	c->onConstruction(m_kit);
	return c;
}

BossController* EnemyManager::createBossController()
{
	auto c = new BossController(this, m_projectileMngr);
	return c;
}

HurricaneAgent* EnemyManager::createHurricaneAgent(float x, float y, Path path)
{
	for (auto e : m_hurricanes)
	{
		if (!e->isActive())
		{
			e->onDestruction(m_kit);
			e->onConstruction(m_kit);
			e->setPosition(x, y);
			return e;
		}
	}

	auto e = new HurricaneAgent(m_texCache,m_audioMngr, path);
	m_hurricanes.push_back(e);
	e->onConstruction(m_kit);
	e->setPosition(x, y);
	return e;
}
