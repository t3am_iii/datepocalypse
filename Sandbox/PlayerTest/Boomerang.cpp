#include "Boomerang.h"
#include "Enums.h"

#include <Engine\GameObjectCreationKit.h>
#include <Engine\CollisionSystem\Body.h>
#include <Engine\CollisionSystem\BodyDef.h>
#include <Engine\CollisionSystem\CollisionManager.h>

#include <SFML\Graphics\Texture.hpp>
#include <SFML\Graphics\RenderTarget.hpp>

using namespace dpEngine::CollisionSystem;

const float Boomerang::SpawnPathAngle = -(180.0f / 12);

const float Boomerang::ThrowEndPathAngle = -(180.0f / 12);

const float Boomerang::ReturnEndPathAngle = 180.0f + (180.0f / 12);

Boomerang::Boomerang() { /* EMPTY */ }

Boomerang::~Boomerang() { /* EMPTY */ }

void Boomerang::update(float deltaTime)
{
	//m_sprite.setPosition(getPosition());
}

void Boomerang::setActive(bool isActive)
{
	m_isActive = isActive;
	body->setActive(isActive);
}

bool Boomerang::isActive() const { return m_isActive; }

EENTITYTYPE Boomerang::GetType() { return EENTITYTYPE::ENTITY_BOOMERANG; }

void Boomerang::onConstruction(const dpEngine::GameObjectCreationKit& kit)
{
	sf::Texture* tex = new sf::Texture;
	tex->loadFromFile("../assets/textures/boomerang.png");
	m_sprite.setTexture(*tex, true);
	m_sprite.setOrigin(128.0f, 128.0f);
	m_sprite.setScale(0.5f, 0.5f);

	m_hitBox = new CircleShape(20.0f);
	BodyDef def(EBodyTypes::kKinematicBody, FILTER(EEntityCategory::kBoomerang, EEntityCategory::kEverything), true, m_hitBox, this);
	body = kit.collisionMngr->createBody(def, this);


	setActive(true);
}

void Boomerang::onDestruction(const dpEngine::GameObjectCreationKit& kit)
{
	kit.collisionMngr->destroyBody(body);
	delete m_hitBox;
}

void Boomerang::onNotify_OwnerChange(PlayerCharacter* newOwner)
{
	//
}

void Boomerang::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	states.transform = getTransform();
	target.draw(m_sprite, states);
}
