#include "MenuScene.h"
#include "Global.h"
#include "AudioManager.h"

#include <Engine\SystemInfo.h>
#include <Engine\InputSystem\inputManager.h>

MenuScene::MenuScene(AudioManager* audioMngr)
	: m_audioMngr(audioMngr)
{
	
}

void MenuScene::onEnter(const SystemInfo&)
{
	font.loadFromFile("../assets/fonts/NuevaStd-Cond.otf");

	Button* b = new Button;
	m_Buttons.push_back(b);
	b->Index = 0;
	b->text.setString("Play");
	b->text.setFont(font);
	b->text.setCharacterSize(80);
	b->text.setPosition(sf::Vector2f(585, 358));
	b->text.setColor(sf::Color(0, 0, 0, 255));

	Button* b2 = new Button;
	m_Buttons.push_back(b2);
	b2->Index = 1;
	b2->text.setString("Options");
	b2->text.setFont(font);
	b2->text.setCharacterSize(80);
	b2->text.setPosition(sf::Vector2f(820, 448));
	b2->text.setColor(sf::Color(0, 0, 0, 255));

	Button* b3 = new Button;
	m_Buttons.push_back(b3);
	b3->Index = 2;
	b3->text.setString("LeaderBoard");
	b3->text.setFont(font);
	b3->text.setCharacterSize(70);
	b3->text.setPosition(sf::Vector2f(520, 548));
	b3->text.setColor(sf::Color(0, 0, 0, 255));

	Button* b4 = new Button;
	m_Buttons.push_back(b4);
	b4->Index = 3;
	b4->text.setString("Quit");
	b4->text.setFont(font);
	b4->text.setCharacterSize(80);
	b4->text.setPosition(sf::Vector2f(815, 630));
	b4->text.setColor(sf::Color(0, 0, 0, 255));


	index = 0;
	m_playScene = false;
	m_optionsScene = false;
	m_scoreBoard = false;
	m_closeGame = false;
	
	m_audioMngr->playMusic("../assets/audio/saturday_mornings_alright-.ogg", true, 80); 
	
	m_MenuTex.loadFromFile("../assets/textures/Menu_notext.png");
	m_Menuspr.setTexture(m_MenuTex);
	m_audioMngr->loadSounds("../assets/sounds/menu_button1.ogg");
	m_audioMngr->loadSounds("../assets/sounds/menu_button2.ogg");
}

void MenuScene::onUpdate(float, const SystemInfo& sysInfo)
{
	if (sysInfo.inputMngr->wasKeyPressed(sf::Keyboard::Key::Up))
	{
		index--;
		m_audioMngr->playSound("../assets/sounds/menu_button1.ogg", false, 100);
		if (index < 0)
		{
			index = 3;
		}
	}

	if (sysInfo.inputMngr->wasKeyPressed(sf::Keyboard::Key::Down))
	{
		index++;
		m_audioMngr->playSound("../assets/sounds/menu_button1.ogg", false, 100);
		if (index > 3)
		{
			index = 0;
		}
	}

	for (int i = 0; i < m_Buttons.size(); i++)
	{
		if (m_Buttons.at(i)->Index == index)
		{
			m_Buttons.at(i)->text.setColor(sf::Color(255, 0, 0, 255));
		}
		else
		{
			m_Buttons.at(i)->text.setColor(sf::Color(0, 0, 0, 255));
		}
	}

	if (m_Buttons.at(0)->Index == index)
	{
		if (sysInfo.inputMngr->wasKeyPressed(sf::Keyboard::Key::Return))
		{
			m_playScene = true;
			m_audioMngr->playSound("../assets/sounds/menu_button2.ogg", false, 80);
		}
	}
	if (m_Buttons.at(1)->Index == index)
	{
		if (sysInfo.inputMngr->wasKeyPressed(sf::Keyboard::Key::Return))
		{
			m_optionsScene = true;
			m_audioMngr->playSound("../assets/sounds/menu_button2.ogg", false, 80);
		}
	}
	if (m_Buttons.at(2)->Index == index)
	{
		if (sysInfo.inputMngr->wasKeyPressed(sf::Keyboard::Key::Return))
		{
			m_scoreBoard = true;
			m_audioMngr->playSound("../assets/sounds/menu_button2.ogg", false, 80);
		}
	}
	if (m_Buttons.at(3)->Index == index)
	{
		if (sysInfo.inputMngr->wasKeyPressed(sf::Keyboard::Key::Return))
		{
			m_closeGame = true;
		}
	}

	if (sysInfo.inputMngr->wasKeyPressed(sf::Keyboard::Key::Escape))
	{
		m_closeGame = true;
	}
}

bool MenuScene::nextScene(int& nextScene) const
{
	if (m_playScene == true)
	{
		nextScene = LOADING_SCENE;
		return true;
	}

	else if (m_optionsScene == true)
	{
		nextScene = OPTIONS_SCENE;
		return true;
	}

	else if (m_scoreBoard == true)
	{
		nextScene = SCOREBOARD_SCENE;
		return true;
	}

	return false;
}

bool MenuScene::shouldClose() const { return m_closeGame; }

void MenuScene::onRender(sf::RenderWindow& window)
{
	window.setMouseCursorVisible(false);
	window.draw(m_Menuspr);

	for (int i = 0; i < m_Buttons.size(); i++)
	{
		window.draw(m_Buttons.at(i)->text);
	}
}

void MenuScene::onExit(const SystemInfo&)
{

}