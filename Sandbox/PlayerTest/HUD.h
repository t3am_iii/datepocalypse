#pragma once

#include "AnimatedSprite.h"
#include "Animation.h"
#include "TextureCache.h"
#include <SFML\Graphics\Sprite.hpp>
#include <SFML\Graphics\CircleShape.hpp>
#include <SFML\Graphics\Font.hpp>
#include <SFML\Graphics\Text.hpp>
#include <SFML\Graphics\RectangleShape.hpp>

#include <vector>

#define STRENGTH_VERTEX_COUNT 6
#define STRENGTH_COLORS_COUNT 3

struct HealthbarHeart;
struct Portrait;
struct ScoreText;
struct PlayerState;


class PlayerCharacter;
class Shield;
class ScoreManager;
class PlayerManager;

class HUD
{
public:
	HUD(ScoreManager*, PlayerManager*, TextureCache*);

	~HUD();

	void Update(float p_fDeltaTime);

	void Draw(sf::RenderTarget& target);

	sf::Sprite* GetSprite(int);

	void createScoreText(int, sf::Vector2f);

	void addPowerProgress(int);

	void onBaseBallUsed();

	void onBoomerangUsed();

private:
	void recalcBoomerangArc(sf::Vector2f, sf::Vector2f, PlayerState& state);

	void recalcStrengthBar(sf::Vector2f, sf::Vector2f, float);

private:
	
	sf::Texture discoText;
	sf::Sprite discoSprite;
	float discoFloat;

	sf::Sprite m_HUDbgSprite_bot;
	sf::Sprite m_HUDbgSprite_top;

	TextureCache* m_texCache;

	ScoreManager* m_scoreMngr;

	PlayerManager* m_playerMngr;

	std::vector<HealthbarHeart> m_axHealthbarHearts;

	std::vector<Portrait> m_axPortrait;
	sf::Sprite* m_xPortrait_Sprite;

	int iTimerTest;
	bool m_bPlayerHit;

	//Health
	int m_prevHP_p1;
	int m_prevHP_p2;
	AnimatedSprite m_animatedSprite;

	// Score Counter
	sf::Font m_xScore_Font;
	sf::Text m_xScoreCounter_txt;
	sf::Text m_xScoreMultiplier_txt;

	std::vector<ScoreText*> m_scoreTxts;

	//std::vector<sf::Text> m_scoreTxt;
	sf::Text m_scoreTxt;

	// Shield Bar
	Animation m_shieldBarAni;
	AnimatedSprite m_aniSprite_HUDShield;
	sf::Sprite* m_shieldBar;
	sf::Sprite* m_shieldBarContainerSprite;
	sf::RectangleShape m_shieldbarRect;
	sf::RectangleShape m_shieldbarContainerRect;

	// Powerup Bars
	Animation m_eKeyAni;
	Animation m_qKeyAni;
	AnimatedSprite m_eKeySprite;
	AnimatedSprite m_qKeySprite;

	sf::RectangleShape m_batBarRect;
	sf::RectangleShape m_batContainerRect;
	float m_batProgress;

	sf::RectangleShape m_boomBarRect;
	sf::RectangleShape m_boomContainerRect;
	float m_boomProgress;

	struct
	{
		bool displayMove[2][4];
		bool displayFire;
		sf::Sprite moveSprites[2][4];
		sf::Sprite fireSprite;
	} m_tutorial;
	
	// Boomerang data
	sf::Vector2f m_lastCalcPos[2];


	struct
	{
		std::vector<sf::Vertex> vertices;
		int count;
		//float targetStrength;
		//float currentStrength;
	} m_boomerangArc[2];

	sf::CircleShape m_partnerTarget;

	sf::Vertex m_strengthBarInner[STRENGTH_VERTEX_COUNT];

	sf::RectangleShape m_strengthBarOutline;

};
