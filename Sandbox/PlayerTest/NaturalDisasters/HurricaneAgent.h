#pragma once

#include "../IEntity.h"
#include "../Utils/Path.h"
#include "../AnimatedSprite.h"

#include <Engine\Objects\Pawn.h>
#include <Engine\CollisionSystem\Shapes\CircleShape.h>

#include <SFML\Graphics\Sprite.hpp>

class PlayerCharacter;
class TextureCache;
class AudioManager;

struct PlayerData
{
	PlayerCharacter* character;
	float degrees;
	float radius;
	float time;
};

class HurricaneAgent : public IEntity, public dpEngine::Pawn
{
private:
	TextureCache* m_texCache;

	AudioManager* m_audioMngr;

	Animation m_anim;

	AnimatedSprite m_animSprite;

	bool m_isActive;

	float m_speed;

	bool m_hasReachedGoal;

	Path m_path;

	int m_curIdx;

	sf::Vector2f m_curDir;

	float m_ttl; // Time to live
	
	//std::vector<dpEngine::Pawn*> m_objects;
	std::vector<PlayerData> m_players;

public:
	HurricaneAgent(TextureCache*, AudioManager*, Path);

	~HurricaneAgent();

	void update(float);

	//void addObject(dpEngine::Pawn*);
	void addPlayer(PlayerCharacter*);
	
	void setActive(bool);

	bool isActive() const;

	bool hasReachedGoal() const;

	EENTITYTYPE GetType();

	void onConstruction(const dpEngine::GameObjectCreationKit&);

	void onDestruction(const dpEngine::GameObjectCreationKit&);

protected:
	void draw(sf::RenderTarget&, sf::RenderStates) const;

};
