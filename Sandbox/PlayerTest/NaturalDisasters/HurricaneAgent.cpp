#include "HurricaneAgent.h"
#include "../PlayerCharacter.h"
#include "../Enums.h"
#include "../TextureCache.h"
#include "../AudioManager.h"

#include <Engine\GameObjectCreationKit.h>
#include <Engine\CollisionSystem\BodyDef.h>
#include <Engine\CollisionSystem\Body.h>
#include <Engine\CollisionSystem\CollisionManager.h>
#include <Engine\Utils\Math.h>

#include <SFML\Graphics\Texture.hpp>
#include <SFML\Graphics\RenderTarget.hpp>

using namespace dpEngine;
using namespace dpEngine::CollisionSystem;

HurricaneAgent::HurricaneAgent(TextureCache* texCache, AudioManager* audioMngr, Path path)
	: m_texCache(texCache)
	, m_audioMngr(audioMngr)
	, m_path(path)
{
	m_animSprite = AnimatedSprite(50);
	m_anim.setSpriteSheet(*texCache->getTexture("../assets/textures/spritesheets/tornado_spritesheet.png"));
	m_anim.addFrame(sf::IntRect(   0,    0, 512, 512));
	m_anim.addFrame(sf::IntRect( 512,    0, 512, 512));
	m_anim.addFrame(sf::IntRect(1024,    0, 512, 512));
	m_anim.addFrame(sf::IntRect(   0,  512, 512, 512));
	m_anim.addFrame(sf::IntRect( 512,  512, 512, 512));
	m_anim.addFrame(sf::IntRect(1024,  512, 512, 512));
	m_anim.addFrame(sf::IntRect(   0, 1024, 512, 512));
	m_anim.addFrame(sf::IntRect( 512, 1024, 512, 512));
	m_anim.addFrame(sf::IntRect(1024, 1024, 512, 512));
	m_anim.addFrame(sf::IntRect(   0, 1536, 512, 512));
	m_anim.addFrame(sf::IntRect( 512, 1536, 512, 512));
	m_anim.addFrame(sf::IntRect(1024, 1536, 512, 512));

	m_animSprite.setOrigin(128.f, 128.f);
	m_animSprite.setScale(.5f, .5f);
}

HurricaneAgent::~HurricaneAgent() { }

void HurricaneAgent::update(float deltaTime)
{
	m_animSprite.update(deltaTime * 1000);

	m_ttl -= deltaTime;
	if (m_ttl < 0.f)
	{
		for (auto data : m_players)
		{
			data.character->setFlyModifier(1.f, 1.f);
			data.character->setInvincibility(false);
		}

		setActive(false);
		return;
	}

	if (m_hasReachedGoal)
		return;

	sf::Vector2f goalPos = m_path.points[std::max(m_curIdx, 0)];
	sf::Vector2f curPos = getPosition();
	sf::Vector2f deltaPos = goalPos - curPos;

	bool refreshTarget = (m_curIdx < 0
		|| FMath::Length(deltaPos) < 1.f);

	if (refreshTarget)
	{
		if (++m_curIdx >= m_path.points.size())
		{
			if (m_path.loop)
			{
				m_curIdx = -1;
			}
			else
				m_hasReachedGoal = true;
			return;
		}

		goalPos = m_path.points[m_curIdx];
		deltaPos = goalPos - curPos;
		
		m_curDir = FMath::Normalize(deltaPos);
	}

	addPosition(m_curDir * m_speed);


	for (auto it = m_players.begin(); it != m_players.end(); it++)
	{
		it->time -= deltaTime;
		if (it->time < 0)
		{
			it->character->setFlyModifier(1.f, 1.f);
			it->character->setInvincibility(false);
			it->character->addKnockBack(FMath::Normalize(FMath::Angle(it->degrees)), 35.f);
			
			it = m_players.erase(it);
			if (m_players.size() < 1)
				break;

			continue;
		}

		auto newPos = getPosition();
		newPos.x += cos(it->degrees * M_PI / 180) * it->radius;
		newPos.y += sin(it->degrees * M_PI / 180) * it->radius;

		it->degrees += 600.f * deltaTime;
		if (it->degrees > 360.f)
			it->degrees = 0.f;

		it->character->setPosition(newPos);
	}
}

void HurricaneAgent::addPlayer(PlayerCharacter* player)
{
	for (auto it : m_players)
		if (it.character == player)
			return;

	player->setFlyModifier(0.f, 0.f);
	player->setInvincibility(true);

	PlayerData data;
	data.character = player;
	data.degrees = FMath::Degrees(FMath::Normalize(player->getPosition() - getPosition()));
	data.radius = 40.f;
	data.time = 2.f;
	m_players.push_back(data);
}

void HurricaneAgent::setActive(bool value)
{
	if(value)
		m_audioMngr->playSound("../assets/sounds/tornado.ogg", false, 100);

	m_isActive = value;
	body->setActive(value);
}

bool HurricaneAgent::isActive() const { return m_isActive; }

bool HurricaneAgent::hasReachedGoal() const { return m_hasReachedGoal; }

EENTITYTYPE HurricaneAgent::GetType() { return EENTITYTYPE::ENTITY_HURRICANE; }

void HurricaneAgent::onConstruction(const dpEngine::GameObjectCreationKit& kit)
{
	shape = new CircleShape(58.0f);
	BodyDef def(EBodyTypes::kKinematicBody, FILTER(EEntityCategory::kHurricane, EEntityCategory::kEverything), true, shape, this);
	body = kit.collisionMngr->createBody(def, this);

	m_speed = 5.0f;
	m_hasReachedGoal = false;
	m_ttl = 20.f;
	
	m_curIdx = -1;

	setActive(true);

	m_players.clear();
	m_animSprite.play(m_anim);
}

void HurricaneAgent::onDestruction(const dpEngine::GameObjectCreationKit& kit)
{
	kit.collisionMngr->destroyBody(body);
	delete shape;
}

void HurricaneAgent::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	states.transform *= getTransform();
	target.draw(m_animSprite, states);
}
