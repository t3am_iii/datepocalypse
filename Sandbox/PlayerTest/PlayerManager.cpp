#include "PlayerManager.h"
#include "PlayerCharacter.h"
#include "PlayerShield.h"
#include "Boomerang.h"
#include "GameObjects\BaseballKit.h"
#include "GameObjects\Baseball.h"
#include "GameObjects\BaseballBat.h"
#include "GameObjects\LoveSpinCenter.h"
#include "Controllers\PlayerController.h"
#include "Enums.h"
#include "AudioManager.h"
#include "TextureCache.h"
#include "HUD.h"

#include <Engine\SystemInfo.h>
#include <Engine\Utils\Math.h>

#include <SFML\Window\Keyboard.hpp>

PlayerManager::PlayerManager(const SystemInfo& sysInfo, ProjectileManager* projectileMngr, AudioManager* audioMngr, TextureCache* texCache)
	: m_projectileMngr(projectileMngr)
	, m_audioMngr(audioMngr)
{
	// Setup kit
	m_kit.collisionMngr = sysInfo.collisionMngr;

	// Load assets
	m_playerTextures[0] = texCache->getTexture("../assets/textures/spritesheets/male_spritesheet.png");
	m_playerTextures[1] = texCache->getTexture("../assets/textures/spritesheets/female_spritesheet.png");
	m_playerTrailTextures[0].loadFromFile("../assets/textures/vfx/male_trail_effects.png");
	m_playerTrailTextures[1].loadFromFile("../assets/textures/vfx/female_trail_effects.png");

	m_shadowTexture.loadFromFile("../assets/textures/shadow.png");
	m_baseballTexture.loadFromFile("../assets/textures/baseball.png");
	m_baseballBatTexture.loadFromFile("../assets/textures/baseball_bat.png");
	m_shieldTexture.loadFromFile("../assets/Shield.png");
}

PlayerManager::~PlayerManager()
{
	delete m_characters[0];
	delete m_characters[1];

	delete m_controllers[0];
	delete m_controllers[1];

	for (auto b : m_boomerangs)
		delete b;

	for (auto b : m_baseballs)
		delete b;

	for (auto b : m_baseballBats)
		delete b;
}

void PlayerManager::setHUD(HUD* hud)
{
	m_hud = hud;
}

void PlayerManager::setLevelMngr(LevelManager* levelMngr)
{
	// Define player controllers
	m_controllers[0] = new PlayerController(1, this, m_audioMngr, levelMngr,
		sf::Keyboard::Key::W,
		sf::Keyboard::Key::A,
		sf::Keyboard::Key::S,
		sf::Keyboard::Key::D);

	m_controllers[1] = new PlayerController(2, this, m_audioMngr, levelMngr,
		sf::Keyboard::Key::Up,
		sf::Keyboard::Key::Left,
		sf::Keyboard::Key::Down,
		sf::Keyboard::Key::Right);

	// Create objects
	createCharacter(1, 1920 / 2 - 150.f, 600, m_controllers[0]);
	createCharacter(2, 1920 / 2 + 150.f, 600, m_controllers[1]);

	// Setup shared data
	m_playerState.throwerChar = m_characters[1];
	m_playerState.partnerChar = m_characters[0];
	m_playerState.throwerId = 2;

	m_playerState.projectiles.mngr = m_projectileMngr;
	m_playerState.hasBoomerang = false;
	m_playerState.boomerangingState = EBoomerangingState::kNotActive;
	m_playerState.hasBaseballKit = false;
	m_playerState.baseballingState = EBaseballingState::kNotActive;

	m_shield = new PlayerShield(&m_shieldTexture, 0, 0, 64);
	m_shield->onConstruction(m_kit);
	m_currentShieldTime = 500.f;
	m_prevShieldState = false;
}

PlayerCharacter* PlayerManager::createCharacter(int id, float x, float y, PlayerController* ctrl)
{
	auto c = new PlayerCharacter(m_playerTextures[id - 1], &m_playerTrailTextures[id - 1], &m_shadowTexture, x, y, &m_playerState, this);
	c->onConstruction(m_kit);
	c->setPosition(x, y);
	ctrl->setCharacter(c);

	m_characters[id - 1] = c;
	return c;
}

Boomerang* PlayerManager::createBoomerang(float x, float y, bool resetHUD)
{
	if (resetHUD)
	{
		// Bad way to solve this, but atleast it works (;
		// reset hud will only be true when called by the last player controller,
		// in our case this will always be by the controller with id 2

		m_playerState.hasBoomerang = false;
		m_hud->onBoomerangUsed();
	}

	for (auto b : m_boomerangs)
	{
		if (!b->isActive())
		{
			b->onDestruction(m_kit);
			b->onConstruction(m_kit);
			b->setPosition(x, y);
			return b;
		}
	}

	auto b = new Boomerang;
	b->onConstruction(m_kit);
	b->setPosition(x, y);
	
	m_boomerangs.push_back(b);
	return b;
}

BaseballKit PlayerManager::createBaseballKit()
{
	m_playerState.hasBaseballKit = false;
	m_hud->onBaseBallUsed();

	auto throwerPos = m_playerState.throwerChar->getPosition();
	auto partnerPos = m_playerState.partnerChar->getPosition();
	
	BaseballKit kit;
	kit.balls[0] = createBaseball(1, throwerPos.x, throwerPos.y, true);
	kit.balls[1] = createBaseball(2, partnerPos.x, partnerPos.y, false);
	kit.balls[2] = createBaseball(3, partnerPos.x, partnerPos.y, false);
	kit.balls[3] = createBaseball(4, partnerPos.x, partnerPos.y, false);
	
	kit.bat = createBaseballBat(partnerPos.x, partnerPos.y);
	
	return kit;
}

PlayerCharacter* PlayerManager::getCharacter(int id) const
{
	if (id > 0 && id < 3)
		return m_characters[id - 1];

	return nullptr;
}

PlayerShield* PlayerManager::getShield() const { return m_shield; }

void PlayerManager::update(InputManager* inputMngr, float deltaTime)
{
	//--- Shield Testing ---//


	auto c1 = getCharacter(1);
	auto c2 = getCharacter(2);

	if (c1->isShielding() &&
		abs(c1->getPosition().x - c2->getPosition().x) < 200 
		&& abs(c1->getPosition().y - c2->getPosition().y) < 200)
	{
		m_shield->setPosition((c1->getPosition().x + c2->getPosition().x) / 2, (c1->getPosition().y + c2->getPosition().y) / 2);
		c1->setInvincibility(true);
		c2->setInvincibility(true);
		//if (c1->getPosition().x < c2->getPosition().x)
		//{
		//	m_shield->setPosition((c1->getPosition().x + c2->getPosition().x) / 2, (c1->getPosition().y + c2->getPosition().y) / 2);
		//}
		//else if (c2->getPosition().x < c1->getPosition().x)
		//{
		//	m_shield->setPosition(c2->getPosition().x, c2->getPosition().y);
		//}

		//m_xHUD_ShieldBar.setSize(sf::Vector2f(m_fShield_Timer, 25));
		m_shield->SetIsActive(true);
		if (!m_prevShieldState)
		{
			m_audioMngr->playSound("../assets/sounds/playershield_start.ogg", false, 50);
			m_prevShieldState = true;
		}	
	}
	else
	{
		c1->setInvincibility(false);
		c2->setInvincibility(false);
		m_shield->setPosition(0, 3000);
		c1->setIsShielding(false);
		c2->setIsShielding(false);
		m_shield->SetIsActive(false);
		m_prevShieldState = false;
	}

	m_shield->Update(deltaTime);
	//---End Shield Testing ---//
	
	if (m_playerState.boomerangingState == EBoomerangingState::kChargingStrengthBar)
	{
		//
	}
	else if (m_playerState.boomerangingState == EBoomerangingState::kStartFly)
	{
		m_audioMngr->playSound("../assets/sounds/boomerang.ogg", false, 100);
		m_playerState.boomerangingState = EBoomerangingState::kFlying;
	}
	else if (m_playerState.boomerangingState == EBoomerangingState::kFlying)
	{
		for (int idx = 0; idx < 2; idx++)
		{
			auto pathData = &m_playerState.boomerangPath[idx];
			if (!pathData->boomerang->isActive())
				return;

			if (pathData->isReturningToThrower)
			{
				pathData->pathAngle -= deltaTime * pathData->pathSpeed;
				if (pathData->pathAngle < Boomerang::ThrowEndPathAngle)
					m_playerState.boomerangingState = EBoomerangingState::kDone;
			}
			else
			{
				pathData->pathAngle += deltaTime * pathData->pathSpeed;
				if (pathData->pathAngle > Boomerang::ReturnEndPathAngle)
					pathData->isReturningToThrower = true;
			}

			if (pathData->isReturningToThrower)
				pathData->rotationAngle -= deltaTime * pathData->rotationSpeed * pathData->modifier.x;
			else
				pathData->rotationAngle += deltaTime * pathData->rotationSpeed * pathData->modifier.x;


			auto pAng = (int)(pathData->pathAngle + 180) % 360;

			if (pathData->rotationAngle < 0)
				pathData->rotationAngle += 360;
			else if (pathData->rotationAngle > 359)
				pathData->rotationAngle -= 360;

			sf::Vector2f newPos = pathData->startingPoint;
			newPos.x += std::cos(pAng * M_PI / 180.0f) * pathData->radius.x * pathData->modifier.x;
			newPos.y += std::sin(pAng * M_PI / 180.0f) * pathData->radius.y * pathData->modifier.y;

			pathData->boomerang->setPosition(newPos.x, newPos.y);
			pathData->boomerang->setRotation(pathData->rotationAngle);
		}
	}


	if (m_playerState.baseballingState == EBaseballingState::kFlying)
	{
		//m_playerState.baseballKit.bat->setPosition(m_playerState.partnerChar->getPosition());

		auto ball = m_playerState.baseballKit.balls[0];
		if (ball->hasReachedGoal())
			m_playerState.baseballingState = EBaseballingState::kStartHit;
	}
	else if (m_playerState.baseballingState == EBaseballingState::kStartHit)
	{
		m_playerState.baseballingState = EBaseballingState::kHitting;
		m_playerState.baseballKit.balls[0]->setActive(false);
		m_playerState.baseballKit.bat->setActive(false);
		for (int idx = 1; idx < 4; idx++)
		{
			auto b = m_playerState.baseballKit.balls[idx];
			b->setVisible(true);

			sf::Vector2f goal(b->getPosition());
			goal.x += (idx - 2) * 100.f;
			goal.y -= 600.f;
			b->setGoal(goal);
		}
		m_audioMngr->playSound("../assets/sounds/baseballbat.ogg", false, 100);
	}
	else if (m_playerState.baseballingState == EBaseballingState::kHitting)
	{

		int counter = 0;
		for (auto b : m_playerState.baseballKit.balls)
		{
			if (!b->isVisible())
			{
				b->setActive(false);
				counter++;
			}
		}

		if (counter > 3)
		{
			m_playerState.baseballKit.bat->setActive(false);
			m_playerState.baseballingState = EBaseballingState::kNotActive;
		}
	}

	for (auto c : m_characters)
		c->update(deltaTime);

	for (auto c : m_controllers)
		c->update(inputMngr, deltaTime);

	for (auto b : m_boomerangs)
		if (b->isActive())
			b->update(deltaTime);

	for (auto b : m_baseballs)
		if (b->isActive())
			b->update(deltaTime);

	m_shield->Update(deltaTime);
}

void PlayerManager::render(sf::RenderTarget& target)
{
	for (auto c : m_characters)
		target.draw(*c);

	for (auto b : m_boomerangs)
		if (b->isActive())
			target.draw(*b);

	for (auto b : m_baseballs)
		if (b->isVisible())
			target.draw(*b);

	for (auto b : m_baseballBats)
		if (b->isActive())
			target.draw(*b);

	// Draw shield (if active)
	if (m_shield->IsActive())
		target.draw(*m_shield);
}

Baseball* PlayerManager::createBaseball(int id, float x, float y, bool isVisible)
{
	for (auto b : m_baseballs)
	{
		if (!b->isActive())
		{
			b->onDestruction(m_kit);
			b->onConstruction(m_kit);
			b->setPosition(x, y);
			b->setVisible(isVisible);
			b->setId(id);
			return b;
		}
	}

	auto b = new Baseball(&m_baseballTexture);
	b->onConstruction(m_kit);
	b->setPosition(x, y);
	b->setVisible(isVisible);
	b->setId(id);
	m_baseballs.push_back(b);

	return b;
}

BaseballBat* PlayerManager::createBaseballBat(float x, float y)
{
	for (auto b : m_baseballBats)
	{
		if (!b->isActive())
		{
			b->onDestruction(m_kit);
			b->onConstruction(m_kit);
			b->setPosition(x, y);
			return b;
		}
	}

	auto b = new BaseballBat(&m_baseballBatTexture);
	b->onConstruction(m_kit);
	b->setPosition(x, y);
	m_baseballBats.push_back(b);

	return b;
}

LoveSpinCenter* PlayerManager::createLoveSpinCenter()
{
	auto l = new LoveSpinCenter();
	l->onConstruction(m_kit);
	l->setPosition(-100,-100);

	return l;
}

PlayerState* PlayerManager::getState() { return &m_playerState; }
