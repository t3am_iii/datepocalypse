#include "MutatedHuman.h"
#include "Global.h"
#include "Enums.h"
#include "ProjectileManager.h"
#include "AudioManager.h"

MutatedHuman::MutatedHuman(TextureCache* texCache, ProjectileManager* projectileMngr, AudioManager* audioMngr)
{
	m_projectileMngr = projectileMngr;
	m_audioMngr = audioMngr;
	m_texCache = texCache;

	m_forwardAnimation.setSpriteSheet(*texCache->getTexture("../assets/textures/spritesheets/mutatedhuman_spritesheet.png"));
	m_forwardAnimation.addFrame(sf::IntRect(0, 0, 512, 512));
	m_forwardAnimation.addFrame(sf::IntRect(512, 0, 512, 512));
	m_forwardAnimation.addFrame(sf::IntRect(1024, 0, 512, 512));

	m_rightAnimation.setSpriteSheet(*texCache->getTexture("../assets/textures/spritesheets/mutatedhuman_spritesheet.png"));
	m_rightAnimation.addFrame(sf::IntRect(0, 1024, 512, 512));
	m_rightAnimation.addFrame(sf::IntRect(512, 1024, 512, 512));
	m_rightAnimation.addFrame(sf::IntRect(1024, 1024, 512, 512));

	m_leftAnimation.setSpriteSheet(*texCache->getTexture("../assets/textures/spritesheets/mutatedhuman_spritesheet.png"));
	m_leftAnimation.addFrame(sf::IntRect(0, 512, 512, 512));
	m_leftAnimation.addFrame(sf::IntRect(512, 512, 512, 512));
	m_leftAnimation.addFrame(sf::IntRect(1024, 512, 512, 512));

	m_deathAnimation.setSpriteSheet(*texCache->getTexture("../assets/textures/spritesheets/enemydeath_spritesheet.png"));
	m_deathAnimation.addFrame(sf::IntRect(0, 0, 512, 512));
	m_deathAnimation.addFrame(sf::IntRect(512, 0, 512, 512));
	m_deathAnimation.addFrame(sf::IntRect(1024, 0, 512, 512));
	m_deathAnimation.addFrame(sf::IntRect(1536, 0, 512, 512));
	m_deathAnimation.addFrame(sf::IntRect(0, 512, 512, 512));
	m_deathAnimation.addFrame(sf::IntRect(512, 512, 512, 512));
	m_deathAnimation.addFrame(sf::IntRect(1024, 512, 512, 512));
	m_deathAnimation.addFrame(sf::IntRect(1536, 512, 512, 512));
	

	m_animatedSprite = AnimatedSprite(100, false, true);
	m_animatedSprite.setScale(.35f, .35f);
	m_animatedSprite.setOrigin(256.f * .35f, 256.f * .35f);
	m_animatedSprite.play(m_forwardAnimation);

	m_spriteShadow.setTexture(*texCache->getTexture("../assets/textures/shadow.png"));
	m_spriteShadow.setScale(0.5f, 0.5f);
	m_spriteShadow.setOrigin(20, -180);


}

MutatedHuman::~MutatedHuman() { }

void MutatedHuman::Update(float p_fDeltaTime)
{
	m_animatedSprite.update(p_fDeltaTime * 1000.f);

	if (m_isDead)
	{
		if (m_animToggle)
		{
			m_audioMngr->playSound("../assets/sounds/enemy_death.ogg", false, 100);
			m_animatedSprite.play(m_deathAnimation);
			m_animatedSprite.setLooped(false);
			m_animToggle = false;
		}

		if (!m_animatedSprite.isPlaying())
		{
			setActive(false);
			m_isDead = false;
		}
	}

	if (!m_bCanMove)
		return;

	if (!isDead())
	{
		if (m_iChangeDir < 250)
		{
			addPosition(m_fSpeed * p_fDeltaTime, 50.0f * p_fDeltaTime);

			if (m_iLastDir != 1)
			{
				m_animatedSprite.play(m_rightAnimation);
				m_iLastDir = 1;
			}
		}
		else if (m_iChangeDir > 250)
		{
			addPosition(-m_fSpeed * p_fDeltaTime, 50.0f * p_fDeltaTime);

			if (m_iLastDir != 2)
			{
				m_animatedSprite.play(m_leftAnimation);
				m_iLastDir = 2;
			}
		}

		if (m_iChangeDir > 500)
		{
			m_iChangeDir = 0;
		}

		if (getPosition().y > SCREEN_HEIGHT)
			setPosition(getPosition().x, -200.0f);

		m_iChangeDir++;


		if (getPosition().x < 450)
		{
			setPosition(450, getPosition().y);
		}
		else if (getPosition().x > SCREEN_WIDTH - 560)
		{
			setPosition(SCREEN_WIDTH - 560, getPosition().y);
		}

		m_fTimer_NextShot -= p_fDeltaTime;
		if (m_fTimer_NextShot <= 0)
		{
			m_projectileMngr->CreateEnemyProjectile(getPosition().x, getPosition().y);
			m_audioMngr->playSound("../assets/sounds/enemy_shot.ogg", true, 80);
			m_fTimer_NextShot = rand() % 10 / 5 + 1;
		}
	}

	if (m_bHit && m_iHitEffect < 6)
	{
		if (m_iHitEffect == 0)
			m_animatedSprite.setColor(sf::Color(255, 50, 50, 255));
		else if(m_iHitEffect == 1)
			m_animatedSprite.setColor(sf::Color(0, 0, 0, 255));
		else if (m_iHitEffect == 2)
			m_animatedSprite.setColor(sf::Color(255, 255, 255, 255));
		else if (m_iHitEffect == 3)
			m_animatedSprite.setColor(sf::Color(255, 50, 50, 255));
		else if (m_iHitEffect == 4)
			m_animatedSprite.setColor(sf::Color(0, 0, 0, 255));
		else if (m_iHitEffect == 5)
		{
			m_animatedSprite.setColor(sf::Color(255, 255, 255, 255));
			m_bHit = false;
			m_iHitEffect = 0;
		}
		
		m_iHitEffect++;
	}
}

int MutatedHuman::GetHealthPoints() { return m_iHealthPoints; }
void MutatedHuman::SetHealthPoints(int p_iValue) { m_iHealthPoints = p_iValue; m_bHit = true; }

void MutatedHuman::setCanMove(bool canMove) { m_bCanMove = canMove; }

void MutatedHuman::setActive(bool value)
{
	m_animatedSprite.setColor(sf::Color(255, 255, 255, 255));
	m_bIsActive = value;
	body->setActive(value);
}

void MutatedHuman::setDead(bool dead) { m_isDead = dead; }

bool MutatedHuman::isActive() const { return m_bIsActive; }

bool MutatedHuman::isDead() const { return m_isDead; }

EENTITYTYPE MutatedHuman::GetType() { return EENTITYTYPE::ENTITY_MUTATED_HUMAN; }

void MutatedHuman::onConstruction(const GameObjectCreationKit& kit)
{
	// This method is called immediately after
	// the constructor is called.
	m_iHealthPoints = 3;
	
	shape = new CircleShape(40.0f);
	BodyDef def(EBodyTypes::kKinematicBody, FILTER(EEntityCategory::kEnemyCharacter, ~EEntityCategory::kEnemyProjectile), false, shape, this);
	body = kit.collisionMngr->createBody(def, this);

	
	m_animatedSprite.setAnimation(m_leftAnimation);
	setActive(true);
	m_fSpeed = 200;
	m_fMaxSpeed = 1;
	m_iChangeDir = 0;
	m_iLastDir = 0;

	m_bIsShooting = false;
	m_fTimer_NextShot = 2;

	m_bIsActive = true;
	m_bCanMove = true;
	m_bHit = false;
	m_iHitEffect = 0;
	m_isDead = false;
	m_animToggle = true;

}

void MutatedHuman::onReset(const GameObjectCreationKit& kit)
{
	// This method is called when we are
	// recyling inactive objects
	m_iHealthPoints = 3;

	setActive(true);
}

void MutatedHuman::onDestruction(const GameObjectCreationKit& kit)
{
	kit.collisionMngr->destroyBody(body);
	body = nullptr;

	delete shape;
	shape = nullptr;
}
void MutatedHuman::preDraw(sf::RenderTarget& target, sf::RenderStates states) const
{
	states.transform *= getTransform();
	target.draw(m_spriteShadow, states);
}

void MutatedHuman::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	states.transform *= getTransform();
	
	
	target.draw(m_animatedSprite, states);
	
}
