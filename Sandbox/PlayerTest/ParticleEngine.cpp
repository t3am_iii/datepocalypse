#pragma once

#include "ParticleEngine.h"
#include "Particle.h"
#include <math.h>

ParticleEngine::ParticleEngine(std::vector<sf::Texture*> p_apxTextures, sf::Vector2f p_aLocation)
{
	m_aEmitterLocation = p_aLocation;
	m_apxTextures = p_apxTextures;

	m_apxParticles.clear();
	m_iRandom = 0;
}

ParticleEngine::~ParticleEngine()
{

}

void ParticleEngine::Update(float p_fDeltaTime)
{
	int total = 10;

	for (int i = 0; i < total; i++)
	{
		m_apxParticles.push_back(GenerateNewParticle());
	}

	for (int particle = 0; particle < m_apxParticles.size(); particle++)
	{
		m_apxParticles.at(particle)->Update(p_fDeltaTime);
		if (m_apxParticles.at(particle)->GetTTL() <= 0)
		{
			m_apxParticles.erase(m_apxParticles.begin() + particle);
			particle--;
		}
	}
}

Particle* ParticleEngine::GenerateNewParticle()
{
	sf::Texture* texture = m_apxTextures[rand() % m_apxTextures.size() + 0]; //[random.Next(textures.Count)];
	sf::Sprite* sprite = new sf::Sprite;
	sprite->setTexture(*texture);

	sf::Vector2f position = m_aEmitterLocation;
	sf::Vector2f velocity = sf::Vector2f(
		1.0f * (float)(rand() % 2 - 0),
		1.0f * (float)(rand() % 2 - 0));
	float angle = 0;
	float angularVelocity = 0.1f * (float)(rand() % 2 - 0);
	sf::Color color = sf::Color(
		rand() % 225 + 1,
		rand() % 225 + 1,
		rand() % 225 + 1);
	float size = (float)(rand() % 2 - 0);
	int ttl = 20 + rand() % 40 + 1;

	Particle* pxParticle = new Particle(sprite, texture, position, velocity, angle, angularVelocity, color, size, ttl);
	return pxParticle;
}

//void ParticleEngine::Draw(SpriteBatch spriteBatch)
//{
//	spriteBatch.Begin();
//	for (int index = 0; index < particles.Count; index++)
//	{
//		particles[index].Draw(spriteBatch);
//	}
//	spriteBatch.End();
//}

sf::Vector2f ParticleEngine::GetEmitterLocatrion() { return m_aEmitterLocation; }
void ParticleEngine::SetEmitterLocation(sf::Vector2f p_fVector) { m_aEmitterLocation = p_fVector; }