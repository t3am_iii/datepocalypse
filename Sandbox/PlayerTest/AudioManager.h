#pragma once

#include <SFML\Audio\SoundBuffer.hpp>
#include <SFML\Audio\Sound.hpp>
#include <SFML\Audio\Music.hpp>

#include <vector>
#include <map>
#include <list>
#include <unordered_map>
#include <queue>


class AudioManager
{
public:
	AudioManager();

	~AudioManager();

	/*
	* Should be used for pre-loading sounds.
	* Stores sound buffer pointer and it's file path in a map.
	* Stores sound pointer and it's buffer's file path in a map.
	*/
	void loadSounds(const std::string&);

	/*
	* Should be used for storing new sounds.
	* Stores a sound pointer and it's buffer's filepath in a map.
	* Also stores a sound buffer pointer and it's filepath in a map.
	*/
	sf::Sound* createSounds(const std::string&);

	/*
	* Play a sound.
	* Checks through createSound() if the requested sound exists or is currently playing.
	*/
	void playSound(std::string s, bool pitch, int volume);

	void playMusic(std::string s, bool loop, int volume);

	void stopAllSounds();

	void unloadSounds();

	void unloadMusic();
private:

	std::map< std::string, std::vector<sf::Sound*>> m_sounds;
	std::map<std::string, sf::SoundBuffer*> m_buffers;

	sf::Music music;
	int test;
};
