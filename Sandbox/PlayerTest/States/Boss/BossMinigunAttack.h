#pragma once

#include "BaseBossState.h"

#include <SFML\System\Vector2.hpp>

class BossMinigunAttack : public BaseBossState
{
private:
	float m_nextShotTime;

	float m_delayTime;

	float m_reloadTime;

	sf::Vector2f m_directions[5];
	
	int m_nextShotDir;

public:
	void onEnter(BossController*);

	EBossStates onUpdate(BossController*, float);

	void onExit(BossController*);

};
