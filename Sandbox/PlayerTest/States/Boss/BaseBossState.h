#pragma once

class BossController;
enum class EBossStates;

class BaseBossState
{
public:
	virtual ~BaseBossState();

	virtual void onEnter(BossController*) = 0;

	virtual EBossStates onUpdate(BossController*, float) = 0;

	virtual void onExit(BossController*) = 0;

};
