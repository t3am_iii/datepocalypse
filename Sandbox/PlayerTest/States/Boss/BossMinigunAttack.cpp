#include "BossMinigunAttack.h"
#include "../../Controllers/BossController.h"
#include "../../Enums.h"

#include <Engine\Utils\Math.h>

void BossMinigunAttack::onEnter(BossController*)
{
	m_nextShotTime = 0.f;
	m_delayTime  = .0f;
	m_reloadTime = .7f;
	m_nextShotDir = 0;

	float deg;
	for (int idx = 0, deg = 135.f; idx < 5; idx++, deg -= 22.5f)
		m_directions[idx] = dpEngine::FMath::Angle(deg);
}

EBossStates BossMinigunAttack::onUpdate(BossController* ctrl, float deltaTime)
{
	m_nextShotTime -= deltaTime;
	if (m_nextShotTime < 0.f)
	{
		/*ctrl->createProjectile(m_directions[m_nextShotDir++]);
		if (m_nextShotDir > 4)
		{
			m_nextShotDir = 0;
			m_nextShotTime += m_reloadTime;
		}
		else
			m_nextShotTime += m_delayTime;*/

		for (int i = 0; i < 5; i++)
			ctrl->createProjectile(m_directions[i]);
		m_nextShotTime += m_reloadTime;
	}

	return EBossStates::kStay;
}

void BossMinigunAttack::onExit(BossController*)
{
	//
}
