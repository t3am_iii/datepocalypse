#pragma once

#include "BaseBossState.h"

class BossRadioactivePuke : public BaseBossState
{
public:
	void onEnter(BossController*);

	EBossStates onUpdate(BossController*, float);

	void onExit(BossController*);

};
