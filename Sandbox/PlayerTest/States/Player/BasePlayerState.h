#pragma once

class PlayerController;
enum class EPlayerStates;
namespace dpEngine { class InputManager; }

class BasePlayerState
{
public:
	virtual ~BasePlayerState();

	virtual void onEnter(PlayerController*) = 0;

	virtual EPlayerStates handleInput(PlayerController*, dpEngine::InputManager*) = 0;

	virtual void update(PlayerController*, float);

	virtual void onExit(PlayerController*) = 0;

};
