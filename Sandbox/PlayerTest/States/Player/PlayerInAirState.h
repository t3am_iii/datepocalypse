#pragma once

#include "BasePlayerState.h"

#include <SFML\System\Vector2.hpp>

class PlayerInAirState : public virtual BasePlayerState
{
private:
	sf::Vector2f m_velocity;

public:
	virtual EPlayerStates handleInput(PlayerController*, dpEngine::InputManager*);

	virtual void update(PlayerController*, float);

};
