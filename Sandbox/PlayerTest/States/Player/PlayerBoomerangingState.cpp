#include "PlayerBoomerangingState.h"
#include "../../Boomerang.h"
#include "../../PlayerCharacter.h"
#include "../../Controllers/PlayerController.h"
#include "../../PlayerState.h"
#include "../../Enums.h"

#include <Engine\InputSystem\InputManager.h>
#include <Engine\Utils\Math.h>

void PlayerBoomerangingState::onEnter(PlayerController* playerCtrl)
{
	PlayerInFireState::onEnter(playerCtrl);
	playerCtrl->getState()->boomerangingState = EBoomerangingState::kChargingStrengthBar;
}

EPlayerStates PlayerBoomerangingState::handleInput(PlayerController* playerCtrl, InputManager* inputMngr)
{
	auto state = playerCtrl->getState();
	if (state->hasBoomerang/* && state->throwerId == playerCtrl->getId()*/)
	{
		if (state->boomerangingState == EBoomerangingState::kChargingStrengthBar)
		{
			if (inputMngr->wasKeyReleased(playerCtrl->getActionKey(EActions::kBoomerang)))
			{
				playerCtrl->throwBoomerang();

				auto pathData = &state->boomerangPath[playerCtrl->getId() - 1];
				pathData->startingPoint = pathData->boomerang->getPosition();
				pathData->pathSpeed = 200.0f;
				pathData->rotationSpeed = 2000.0f;
				pathData->radius.y = playerCtrl->getId() == 1 ? 250.f : 350.f;
				pathData->pathAngle = Boomerang::SpawnPathAngle;
				pathData->rotationAngle = 0.0f;
				pathData->isReturningToThrower = false;

				if (playerCtrl->getId() == 2)
				{
					state->boomerangingState = EBoomerangingState::kStartFly;
					state->inBoomState = true;
				}

				return EPlayerStates::kStay;
			}
		}
	}

	if (playerCtrl->getState()->boomerangingState == EBoomerangingState::kDone)
		return EPlayerStates::kFlying;

	PlayerInFireState::handleInput(playerCtrl, inputMngr);
	return PlayerInAirState::handleInput(playerCtrl, inputMngr);
}

void PlayerBoomerangingState::update(PlayerController* playerCtrl, float deltaTime)
{
	PlayerInAirState::update(playerCtrl, deltaTime);
	PlayerInFireState::update(playerCtrl, deltaTime);
}

void PlayerBoomerangingState::onExit(PlayerController* playerCtrl)
{
	auto state = playerCtrl->getState();
	int idx = playerCtrl->getId() - 1;
	if (playerCtrl->getId() == 2)
	{
		state->boomerangingState = EBoomerangingState::kNotActive;
		state->inBoomState = false;
	}

	state->boomerangPath[idx].boomerang->setActive(false);
}
