#include "PlayerFlyingState.h"
#include "../../Controllers/PlayerController.h"
#include "../../PlayerState.h"
#include "../../Enums.h"

#include <Engine\InputSystem\InputManager.h>

void PlayerFlyingState::onEnter(PlayerController* playerCtrl) { }

EPlayerStates PlayerFlyingState::handleInput(PlayerController* playerCtrl, dpEngine::InputManager* inputMngr)
{
	if (inputMngr->isKeyDown(playerCtrl->getActionKey(EActions::kFire)))
		return EPlayerStates::kFiring;

	auto state = playerCtrl->getState();
	if (inputMngr->wasKeyPressed(playerCtrl->getActionKey(EActions::kBoomerang))
		&& playerCtrl->allowBoomerangUsage())
		return EPlayerStates::kBoomeranging;

	if (inputMngr->wasKeyPressed(playerCtrl->getActionKey(EActions::kBaseball))
		&& playerCtrl->allowBaseballUsage())
		return EPlayerStates::kBaseballing;

	return PlayerInAirState::handleInput(playerCtrl, inputMngr);
}

void PlayerFlyingState::update(PlayerController* playerCtrl, float deltaTime)
{
	PlayerInAirState::update(playerCtrl, deltaTime);
}

void PlayerFlyingState::onExit(PlayerController* playerCtrl) { }
