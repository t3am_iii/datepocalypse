#pragma once

#include "BasePlayerState.h"

#include <SFML\System\Vector2.hpp>

class PlayerInFireState : public virtual BasePlayerState
{
public:
	virtual void onEnter(PlayerController*);

	virtual EPlayerStates handleInput(PlayerController*, dpEngine::InputManager*);

	virtual void update(PlayerController*, float);

};
