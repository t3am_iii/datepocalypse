#include "PlayerFiringState.h"
#include "../../Controllers/PlayerController.h"
#include "../../PlayerCharacter.h"
#include "../../PlayerState.h"
#include "../../ProjectileManager.h"
#include "../../AudioManager.h"
#include "../../Enums.h"

#include <Engine\InputSystem\InputManager.h>
#include <Engine\Utils\Math.h>

void PlayerFiringState::onEnter(PlayerController* playerCtrl)
{
	PlayerInFireState::onEnter(playerCtrl);
}

EPlayerStates PlayerFiringState::handleInput(PlayerController* playerCtrl, dpEngine::InputManager* inputMngr)
{
	auto state = PlayerInAirState::handleInput(playerCtrl, inputMngr);
	if (state != EPlayerStates::kStay)
		return state;

	return PlayerInFireState::handleInput(playerCtrl, inputMngr);
}

void PlayerFiringState::update(PlayerController* playerCtrl, float deltaTime)
{
	PlayerInAirState::update(playerCtrl, deltaTime);
	PlayerInFireState::update(playerCtrl, deltaTime);
}

void PlayerFiringState::onExit(PlayerController* playerCtrl) { }
