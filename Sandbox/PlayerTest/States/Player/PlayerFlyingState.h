#pragma once

#include "PlayerInAirState.h"

class PlayerFlyingState : public PlayerInAirState
{
public:
	void onEnter(PlayerController*);
	
	EPlayerStates handleInput(PlayerController*, dpEngine::InputManager*);

	void update(PlayerController*, float);

	void onExit(PlayerController*);

};
