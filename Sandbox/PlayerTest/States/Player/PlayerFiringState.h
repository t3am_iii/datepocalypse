#pragma once

#include "PlayerInAirState.h"
#include "PlayerInFireState.h"

class PlayerFiringState : public PlayerInAirState, public PlayerInFireState
{
public:
	void onEnter(PlayerController*);
	
	EPlayerStates handleInput(PlayerController*, dpEngine::InputManager*);

	void update(PlayerController*, float);

	void onExit(PlayerController*);

};
