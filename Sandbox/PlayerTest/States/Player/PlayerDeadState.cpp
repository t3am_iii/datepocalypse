#include "PlayerDeadState.h"
#include "../../Controllers/PlayerController.h"
#include "../../PlayerCharacter.h"
#include "../../PlayerState.h"
#include "../../Enums.h"

#include <Engine\InputSystem\InputManager.h>

void PlayerDeadState::onEnter(PlayerController* playerCtrl)
{
	auto state = playerCtrl->getState();
	state->baseballingState = EBaseballingState::kNotActive;
	state->boomerangingState = EBoomerangingState::kNotActive;
}

EPlayerStates PlayerDeadState::handleInput(PlayerController* playerCtrl, InputManager* inputMngr)
{
	return EPlayerStates::kStay;
}

void PlayerDeadState::update(PlayerController* playerCtrl, float deltaTime) { }

void PlayerDeadState::onExit(PlayerController*) { }
