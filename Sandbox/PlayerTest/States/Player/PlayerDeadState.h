#pragma once

#include "BasePlayerState.h"

#include <SFML\System\Vector2.hpp>

class PlayerDeadState : public BasePlayerState
{
public:
	virtual void onEnter(PlayerController*);
	
	virtual EPlayerStates handleInput(PlayerController*, dpEngine::InputManager*);

	virtual void update(PlayerController*, float);

	virtual void onExit(PlayerController*);

};
