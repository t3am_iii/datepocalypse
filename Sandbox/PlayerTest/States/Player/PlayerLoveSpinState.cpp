#include "PlayerLoveSpinState.h"
#include "../../Controllers/PlayerController.h"
#include "../../PlayerCharacter.h"
#include "../../PlayerState.h"
#include "../../Enums.h"

#include <Engine\Utils\Math.h>

void PlayerLoveSpinState::onEnter(PlayerController* playerCtrl)
{
	//auto state = playerCtrl->getState();
	//if (playerCtrl->getId() == state->throwerId)
	//{
	//	auto throwerPos = state->throwerChar->getPosition();
	//	auto partnerPos = state->partnerChar->getPosition();
	//	auto pathData = &state->baseballPath;

	//	pathData->startingPoint = playerCtrl->getCharacter()->getPosition();
	//	pathData->speed[0] = 800.f;
	//	pathData->speed[1] = 1600.f;

	//	playerCtrl->throwBaseball();
	//	state->baseballKit.balls[0]->setGoal(partnerPos);
	//}
	//else
	//{
	//	playerCtrl->setCanMove(false);
	//}
}

EPlayerStates PlayerLoveSpinState::handleInput(PlayerController* playerCtrl, dpEngine::InputManager* inputMngr)
{
	const auto state = playerCtrl->getState()->baseballingState;
	if (state == EBaseballingState::kNotActive)
		return EPlayerStates::kFlying;

	return PlayerInAirState::handleInput(playerCtrl, inputMngr);
}

void PlayerLoveSpinState::update(PlayerController* playerCtrl, float deltaTime)
{
	PlayerInAirState::update(playerCtrl, deltaTime);
}

void PlayerLoveSpinState::onExit(PlayerController* playerCtrl)
{
	playerCtrl->setCanMove(true);

	auto state = playerCtrl->getState();
	state->baseballingState = EBaseballingState::kNotActive;
}
