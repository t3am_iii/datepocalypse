#include "PlayerInteractiveTutorialState.h"
#include "../../WaveSystem/WaveManager.h"
#include "../../LevelSystem/LevelManager.h"
#include "../../Controllers/PlayerController.h"
#include "../../PlayerCharacter.h"
#include "../../PlayerState.h"
#include "../../Enums.h"

#include <Engine\InputSystem\InputManager.h>

#define _UP 0
#define _LEFT 1
#define _DOWN 2
#define _RIGHT 3
#define _FIRE 4

PlayerInteractiveTutorialState::PlayerInteractiveTutorialState(LevelManager* levelMngr)
	: m_levelMngr(levelMngr)
{
	for (auto& a : m_actions)
	{
		a.allow = false;
	}

	m_actions[0].func = [&](PlayerController* pc, PlayerCharacter* c) { return m_startPos.y - c->getPosition().y > 60.f; };
	m_actions[1].func = [&](PlayerController* pc, PlayerCharacter* c) { return m_startPos.x - c->getPosition().x > (pc->getId() == 1 ? 60.f : 130.f); };
	m_actions[2].func = [&](PlayerController* pc, PlayerCharacter* c) { return c->getPosition().y - m_startPos.y > 60.f; };
	m_actions[3].func = [&](PlayerController* pc, PlayerCharacter* c) { return c->getPosition().x - m_startPos.x > (pc->getId() == 1 ? 130.f : 60.f); };
	//m_actions[4].func = [this](PlayerController* pc, PlayerCharacter* c) { return m_hasFired; };

	m_hasFired = false;
}

void PlayerInteractiveTutorialState::onEnter(PlayerController* playerCtrl)
{
	const int id = playerCtrl->getId();
	auto state = playerCtrl->getState();

	for (int keyIdx = 0; keyIdx < 4; keyIdx++)
		state->tutorialState.displayMove[id - 1][keyIdx] = false;
	state->tutorialState.displayFire = false;

	for (auto& state : m_states)
	{
		state.isDone = false;
	}

	m_states[0].action = _UP;
	m_states[1].action = id == 1 ? _LEFT : _RIGHT;
	m_states[2].action = _DOWN;
	m_states[3].action = id == 2 ? _LEFT : _RIGHT;
	m_states[4].action = _FIRE;
}

EPlayerStates PlayerInteractiveTutorialState::handleInput(PlayerController* playerCtrl, dpEngine::InputManager* inputMngr)
{
	if (!playerCtrl->getCharacter()->isAlive())
		return EPlayerStates::kDead;

	if (m_hasFired)
		return EPlayerStates::kFlying;

	m_velocity = { 0.f, 0.f };

	if (inputMngr->isKeyDown(playerCtrl->getActionKey(EActions::kWalkUp)) && m_actions[0].allow)
		m_velocity.y--;
	if (inputMngr->isKeyDown(playerCtrl->getActionKey(EActions::kWalkLeft)) && m_actions[1].allow)
		m_velocity.x--;
	if (inputMngr->isKeyDown(playerCtrl->getActionKey(EActions::kWalkDown)) && m_actions[2].allow)
		m_velocity.y++;
	if (inputMngr->isKeyDown(playerCtrl->getActionKey(EActions::kWalkRight)) && m_actions[3].allow)
		m_velocity.x++;

	if (inputMngr->wasKeyPressed(playerCtrl->getActionKey(EActions::kFire)) && m_actions[4].allow)
	{
		m_hasFired = true;
		m_levelMngr->setScrolling(true);
		return EPlayerStates::kFiring;
	}

	return EPlayerStates::kStay;
}

void PlayerInteractiveTutorialState::update(PlayerController* playerCtrl, float deltaTime)
{
	if (playerCtrl->canMove())
	{
		const float SPEED = 300.f;
		PlayerCharacter* c = playerCtrl->getCharacter();

		sf::Vector2f deltaPos = m_velocity * deltaTime * SPEED;
		sf::Vector2f modifiers = c->getFlyModifier();
		deltaPos.x *= modifiers.x;
		deltaPos.y *= modifiers.y;

		c->addPosition(deltaPos);
		c->setAnimation(m_velocity.x * modifiers.x);
	}


	const int id = playerCtrl->getId();
	auto state = playerCtrl->getState();
	auto t = &state->tutorialState;

	for (int idx = 0; idx < 5; idx++)
	{
		const int curAction = m_states[idx].action;

		if (!m_states[curAction].isDone)
		{
			if (idx == 0)
			{
				m_states[curAction].isDone = true;
				m_actions[curAction].allow = true;
				t->displayMove[id - 1][curAction] = true;
				m_startPos = playerCtrl->getCharacter()->getPosition();
			}
			else
			{
				const int prevAction = m_states[idx - 1].action;
				
				if (m_actions[prevAction].func(playerCtrl, playerCtrl->getCharacter()))
				{
					m_states[curAction].isDone = true;
					m_actions[curAction].allow = true;
					//m_actions[prevAction].allow = false;
					if (idx < 4)
						t->displayMove[id - 1][curAction] = true;
					else if (idx == 4)
					{
						t->displayFire = true;
						m_levelMngr->getWaveMngr()->setSpawning(true);
					}
					t->displayMove[id - 1][prevAction] = false;
				}
			}

			break;
		}
	}
}

void PlayerInteractiveTutorialState::onExit(PlayerController* playerCtrl)
{
	playerCtrl->getState()->tutorialState.displayFire = false;
}
