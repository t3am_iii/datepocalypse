#pragma once

#include "BasePlayerState.h"

#include <SFML\System\Vector2.hpp>

#include <functional>

class LevelManager;

class PlayerInteractiveTutorialState : public BasePlayerState
{
private:
	LevelManager* m_levelMngr;

	sf::Vector2f m_velocity;

	sf::Vector2f m_startPos;

	struct
	{
		bool allow;
		std::function<bool(PlayerController*, class PlayerCharacter*)> func;
	} m_actions[5];

	struct
	{
		int action;
		bool isDone;
	} m_states[5];

	bool m_hasFired;

public:
	PlayerInteractiveTutorialState(LevelManager*);
	
	void onEnter(PlayerController*);

	EPlayerStates handleInput(PlayerController*, dpEngine::InputManager*);

	void update(PlayerController*, float);

	void onExit(PlayerController*);

};
