#include "PlayerInFireState.h"
#include "../../Controllers/PlayerController.h"
#include "../../PlayerCharacter.h"
#include "../../PlayerState.h"
#include "../../ProjectileManager.h"
#include "../../AudioManager.h"
#include "../../Enums.h"

#include <Engine\InputSystem\InputManager.h>
#include <Engine\Utils\Math.h>

void PlayerInFireState::onEnter(PlayerController* playerCtrl)
{
	auto projs = &playerCtrl->getState()->projectiles;
	projs->moveSpeed = 800.f;
	projs->reloadTimeHalf = .2f;
	projs->nextShotTime = 0.f;
}

EPlayerStates PlayerInFireState::handleInput(PlayerController* playerCtrl, dpEngine::InputManager* inputMngr)
{
	if (!inputMngr->isKeyDown(playerCtrl->getActionKey(EActions::kFire)))
		return EPlayerStates::kPrev;

	return EPlayerStates::kStay;
}

void PlayerInFireState::update(PlayerController* playerCtrl, float deltaTime)
{
	auto projs = &playerCtrl->getState()->projectiles;
	if (projs->shooterId != playerCtrl->getId())
		return;

	projs->nextShotTime -= deltaTime;
	if (projs->nextShotTime < 0.f)
	{
		projs->nextShotTime += projs->reloadTimeHalf;

		projs->shooterId = projs->shooterId != 1 ? 1 : 2;

		auto pos = playerCtrl->getCharacter()->getPosition();
		auto goal = (rand() % 200 - 100) / 4000.0f;

		auto dir = dpEngine::FMath::Normalize(sf::Vector2f(goal / 500, 0));
		dir.y = -1;
		projs->mngr->CreatePlayerProjectile(pos.x, pos.y, projs->moveSpeed, sf::Vector2f(goal, -1));
		playerCtrl->getAudio()->playSound("../assets/sounds/player_shot.ogg", true, 90);
	}
}
