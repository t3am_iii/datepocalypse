#include "PlayerInAirState.h"
#include "../../Controllers/PlayerController.h"
#include "../../PlayerCharacter.h"
#include "../../Enums.h"

#include <Engine\InputSystem\InputManager.h>

EPlayerStates PlayerInAirState::handleInput(PlayerController* playerCtrl, InputManager* inputMngr)
{
	if (!playerCtrl->getCharacter()->isAlive())
		return EPlayerStates::kDead;

	m_velocity = { 0.f, 0.f };

	if (inputMngr->isKeyDown(playerCtrl->getActionKey(EActions::kWalkUp)))
		m_velocity.y--;
	if (inputMngr->isKeyDown(playerCtrl->getActionKey(EActions::kWalkLeft)))
		m_velocity.x--;
	if (inputMngr->isKeyDown(playerCtrl->getActionKey(EActions::kWalkDown)))
		m_velocity.y++;
	if (inputMngr->isKeyDown(playerCtrl->getActionKey(EActions::kWalkRight)))
		m_velocity.x++;

	return EPlayerStates::kStay;
}

void PlayerInAirState::update(PlayerController* playerCtrl, float deltaTime)
{
	if (playerCtrl->canMove())
	{
		const float SPEED = 300.f;
		PlayerCharacter* c = playerCtrl->getCharacter();

		sf::Vector2f deltaPos = m_velocity * deltaTime * SPEED;
		sf::Vector2f modifiers = c->getFlyModifier();
		deltaPos.x *= modifiers.x;
		deltaPos.y *= modifiers.y;

		c->addPosition(deltaPos);
		c->setAnimation(m_velocity.x * modifiers.x);
	}
}
