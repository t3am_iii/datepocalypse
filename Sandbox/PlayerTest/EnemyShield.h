#pragma once

#include "IEntity.h"
#include "AnimatedSprite.h"

#include <Engine\GameObjectCreationKit.h>
#include <Engine\Objects\Pawn.h>
#include <Engine\CollisionSystem\CollisionManager.h>
#include <Engine\CollisionSystem\BodyDef.h>
#include <Engine\CollisionSystem\Body.h>
#include <Engine\CollisionSystem\Enums.h>
#include <Engine\CollisionSystem\Shapes\RectangleShape.h>

#include <SFML\Graphics\Sprite.hpp>

using namespace dpEngine;

class TextureCache;

class EnemyShield : public IEntity, public dpEngine::Pawn
{
private:
	std::vector<Animation> m_anims;
	AnimatedSprite m_animSprite;

	bool m_isActive;

	bool m_isHit;
	int m_hitEffect;

	int m_armour;
	bool m_enterAnim;

public:
	EnemyShield(TextureCache*);
	
	~EnemyShield();

	void update(float);

	void setActive(bool);
	
	bool isActive() const;

	void setArmourPoints(int);

	int getArmourPoints() const;

	EENTITYTYPE GetType();

	void onConstruction(const GameObjectCreationKit&);

	void onDestruction(const GameObjectCreationKit&);

protected:
	void draw(sf::RenderTarget&, sf::RenderStates) const;
	
};
