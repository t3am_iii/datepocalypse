#pragma once

#include "PlayerState.h"
#include "GameObjects\BaseballKit.h"

#include <Engine\GameObjectCreationKit.h>

#include <SFML\Graphics\RenderTarget.hpp>
#include <SFML\Graphics\Texture.hpp>

#include <vector>

class PlayerCharacter;
class PlayerShield;
class Boomerang;
class Baseball;
class BaseballBat;
class PlayerController;
class ProjectileManager;
class AudioManager;
class TextureCache;
class LevelManager;
class HUD;

namespace dpEngine { struct SystemInfo; }
namespace dpEngine { class InputManager; }
using namespace dpEngine;

class PlayerManager
{
private:
	sf::Texture* m_playerTextures[2];

	sf::Texture m_playerTrailTextures[2];

	sf::Texture m_shadowTexture;

	sf::Texture m_baseballTexture;

	sf::Texture m_baseballBatTexture;

	sf::Texture m_shieldTexture;
	
	PlayerCharacter* m_characters[2];

	PlayerShield* m_shield;

	bool m_prevShieldState;

	float m_currentShieldTime;

	PlayerController* m_controllers[2];

	std::vector<Boomerang*> m_boomerangs;

	std::vector<Baseball*> m_baseballs;

	std::vector<BaseballBat*> m_baseballBats;

	GameObjectCreationKit m_kit;

	ProjectileManager* m_projectileMngr;
	
	AudioManager* m_audioMngr;

	HUD* m_hud;

	PlayerState m_playerState;


public:
	PlayerManager(const SystemInfo&, ProjectileManager*, AudioManager*, TextureCache*);

	~PlayerManager();

	void setHUD(HUD*);

	void setLevelMngr(LevelManager*);

	Boomerang* createBoomerang(float, float, bool);

	BaseballKit createBaseballKit();

	LoveSpinCenter* createLoveSpinCenter();

	PlayerCharacter* getCharacter(int) const;

	PlayerShield* getShield() const;

	void update(InputManager*, float);

	void render(sf::RenderTarget& target);

	PlayerState* getState();

private:
	PlayerCharacter* createCharacter(int, float, float, PlayerController*);

	Baseball* createBaseball(int, float, float, bool);

	BaseballBat* createBaseballBat(float, float);

};
