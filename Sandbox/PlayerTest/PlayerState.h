#pragma once

#include "GameObjects\BaseballKit.h"
#include "Engine\CollisionSystem\Body.h"
#include "Engine\CollisionSystem\Shapes\CircleShape.h"
#include <SFML\System\Vector2.hpp>

enum class EBoomerangingState;
enum class EBaseballingState;
class PlayerCharacter;
class Boomerang;
class ProjectileManager;
class LoveSpinCenter;

namespace sf { class Sound; }

using namespace dpEngine::CollisionSystem;

struct PlayerState
{
	// Projectile
	struct
	{
		ProjectileManager* mngr;
		float moveSpeed;
		float reloadTimeHalf;
		//bool isShooting;
		float nextShotTime;
		int shooterId = 1;

		//bool createProjectile;
		//bool swapChar;
	} projectiles;
	
	
	int throwerId;
	PlayerCharacter* throwerChar;
	PlayerCharacter* partnerChar;
	

	// Love Boomerang
	bool hasBoomerang;
	bool inBoomState;
	EBoomerangingState boomerangingState;

	struct
	{
		Boomerang* boomerang;
		sf::Vector2f startingPoint;
		bool isReturningToThrower;
		sf::Vector2i modifier;
		sf::Vector2f radius;
		float pathSpeed;
		float rotationSpeed;
		float pathAngle;
		float rotationAngle;
	} boomerangPath[2];

	// Love Baseball
	bool hasBaseballKit;
	EBaseballingState baseballingState;
	EBaseballingState prevBaseballingState;
	BaseballKit baseballKit; 

	struct
	{
		sf::Vector2f startingPoint;
		float speed[2];
	} baseballPath;

	// Love Spin
	bool hasLoveSpin;
	struct
	{
		LoveSpinCenter* centerBody;
		float rotationSpeed;
		float rotationAngle;
	} loveSpinPath;

	
	// HUD stuff
	struct
	{
		bool displayMove[2][4];
		bool displayFire;
	} tutorialState;
};
