#include "ScoreBoard.h"
#include "ScoreManager.h"
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <SFML/Graphics.hpp>
#include <Engine\SystemInfo.h>
#include <Engine\InputSystem\inputManager.h>
#include "HUD.h"
#include "Global.h"

void ScoreBoard::onEnter(const SystemInfo&)
{
	m_ScoreBoardTex.loadFromFile("../assets/textures/leaderboard.png");
	m_ScoreBoardSpr.setPosition(1034, 0);
	m_ScoreBoardSpr.setTexture(m_ScoreBoardTex);

	m_menuTex.loadFromFile("../assets/textures/Menu_text.png");
	m_menuspr.setTexture(m_menuTex);

	m_ScoreButtonTex.loadFromFile("../assets/textures/Menu_Select.png");
	m_scoreButtonspr.setTexture(m_ScoreButtonTex);
	m_scoreButtonspr.setPosition(sf::Vector2f(1732, 850));

	font.loadFromFile("../assets/fonts/NuevaStd-Cond.otf");

	ScoreButton* b = new ScoreButton();
	m_ScoreButton.push_back(b);
	b->Index = 0;
	b->text.setString("Return");
	b->text.setFont(font);
	b->text.setPosition(sf::Vector2f(1590, 850));
	b->text.setCharacterSize(58);
	b->text.setColor(sf::Color(0, 0, 0, 255));

	int i = 0;
	std::ifstream filein;
	std::string line = "";

	filein.open("../assets/example.txt");
	m_entries.clear();
	while (std::getline(filein, line))
	{
		if (line.empty())
			continue;
		ScoreEntry e;
		int pos = line.find(' ', 0);
		if (pos < 1)
			continue;

		e.name = line.substr(0, pos);
		e.score = std::stoi(line.substr(pos + 1, line.length() - pos));
		
		e.text.setString(e.name + "\t" + std::to_string(e.score));
		e.text.setFont(font);
		e.text.setColor(sf::Color(0, 0, 0, 255));
		e.text.setPosition(1500, 150 + i * 50);
		m_entries.push_back(e);

		i++;
		if (i > 9)
			break;
	}
	filein.close();

	index = 0;
	m_return = false;
	m_closeGame = false;
}

void ScoreBoard::onUpdate(float, const SystemInfo& sysInfo)
{
	for (int i = 0; i < m_ScoreButton.size(); i++)
	{
		if (m_ScoreButton.at(i)->Index == index)
		{
			m_ScoreButton.at(i)->text.setColor(sf::Color(255, 0, 0, 255));
		}
	}

	if (m_ScoreButton.at(0)->Index == index)
	{
		if (sysInfo.inputMngr->wasKeyPressed(sf::Keyboard::Key::Return))
		{
			m_return = true;
		}
	}

	if (sysInfo.inputMngr->wasKeyPressed(sf::Keyboard::Key::Escape))
	{
		m_closeGame = true;
	}

}

bool ScoreBoard::nextScene(int& NextScene) const
{
	if (m_return == true)
	{
		 NextScene = MENU_SCENE;
		 return true;
	}
	return false;
}

bool ScoreBoard::shouldClose() const { return m_closeGame; }

void ScoreBoard::onRender(sf::RenderWindow& Window)
{
	Window.draw(m_menuspr);
	Window.draw(m_ScoreBoardSpr);
	Window.draw(m_scoreButtonspr);
	
	for (int i = 0; i < m_entries.size(); i++)
		Window.draw(m_entries[i].text);

	for (int i = 0; i < m_ScoreButton.size(); i++)
	{
		Window.draw(m_ScoreButton.at(i)->text);
		Window.draw(m_ScoreButton.at(i)->sprite);
	}
}

void ScoreBoard::onExit(const SystemInfo&)
{
	for (auto it : m_ScoreButton)
		delete it;
	m_ScoreButton.clear();
}

