#pragma once

#include "IEntity.h"
#include "AnimatedSprite.h"
#include "TextureCache.h"

#include <Engine\GameObjectCreationKit.h>
#include <Engine\Objects\Pawn.h>
#include <Engine\CollisionSystem\CollisionManager.h>
#include <Engine\CollisionSystem\BodyDef.h>
#include <Engine\CollisionSystem\Body.h>
#include <Engine\CollisionSystem\Enums.h>
#include <Engine\CollisionSystem\Shapes\CircleShape.h>
#include <Engine\Utils\\Math.h>
#include <SFML/Graphics/Sprite.hpp>

enum class ERiotPoliceStates;
class EnemyShield;
class EnemyManager;
class PlayerManager;
class AudioManager;

namespace dpEngine {
	namespace CollisionSystem { struct RectangleShape; }
}

using namespace dpEngine;

class MutatedRiotPolice : public IEntity, public Pawn
{
private:
	bool m_isActive;

	bool m_isDead;

	bool m_isHit;

	int m_hitEffect;

	bool m_animToggle;

	bool m_bAggro;

	float m_aggroRefresh;

	sf::Sprite m_sprite;

	int m_healthPoints;

	float m_speed;

	sf::Sprite m_spriteShadow;

	sf::Vector2f m_direction;

	sf::Vector2f m_targetPos;

	ERiotPoliceStates m_prevState;

	ERiotPoliceStates m_curState;

	TextureCache* m_texCache;

	EnemyManager* m_enemyMngr;

	PlayerManager* m_playerMngr;

	AudioManager* m_audioMngr;

	EnemyShield* m_shield;

	AnimatedSprite m_animSprite;

	Animation m_normalAnim;

	Animation m_attackAnim;

	Animation m_deathAnim;

public:
	MutatedRiotPolice(TextureCache*, EnemyManager*, PlayerManager*, AudioManager*);
	
	~MutatedRiotPolice();

	void onConstruction(const GameObjectCreationKit&);

	void onDestruction(const GameObjectCreationKit&);

	void update(float);

	void setHealthPoints(int);

	void setDead(bool);

	void setActive(bool);

	void isAttacking();

	int getHealthPoints() const;

	bool isActive() const;

	EENTITYTYPE GetType();

	void preDraw(sf::RenderTarget&, sf::RenderStates) const;

protected:
	void draw(sf::RenderTarget&, sf::RenderStates) const;

};
