#include "EnemyShield.h"
#include "Enums.h"
#include "TextureCache.h"

#include <SFML\Graphics\RenderTarget.hpp>
#include <iostream>

EnemyShield::EnemyShield(TextureCache* texCache)
{
	// Normal
	m_anims.push_back(Animation());
	m_anims[0].setSpriteSheet(*texCache->getTexture("../assets/textures/spritesheets/mutatedriotpolice_spritesheet.png"));
	m_anims[0].addFrame(sf::IntRect(1024, 512, 512, 512));


	// First Crack
	m_anims.push_back(Animation());
	m_anims[1].setSpriteSheet(*texCache->getTexture("../assets/textures/spritesheets/mutatedriotpolice_spritesheet.png"));
	m_anims[1].addFrame(sf::IntRect(1536, 512, 512, 512));

	// Second Crack
	m_anims.push_back(Animation());
	m_anims[2].setSpriteSheet(*texCache->getTexture("../assets/textures/spritesheets/mutatedriotpolice_spritesheet.png"));
	m_anims[2].addFrame(sf::IntRect(0, 1024, 512, 512));

	// Corner Break Animation
	m_anims.push_back(Animation());
	m_anims[3].setSpriteSheet(*texCache->getTexture("../assets/textures/spritesheets/mutatedriotpolice_spritesheet.png"));
	m_anims[3].addFrame(sf::IntRect(512, 1024, 512, 512));
	m_anims[3].addFrame(sf::IntRect(1024, 1024, 512, 512));
	m_anims[3].addFrame(sf::IntRect(1536, 1024, 512, 512));

	// Last Frame of Corner Break Animation
	m_anims.push_back(Animation());
	m_anims[4].setSpriteSheet(*texCache->getTexture("../assets/textures/spritesheets/mutatedriotpolice_spritesheet.png"));
	m_anims[4].addFrame(sf::IntRect(1536, 1024, 512, 512));

	// Shield Break Animation
	m_anims.push_back(Animation());
	m_anims[5].setSpriteSheet(*texCache->getTexture("../assets/textures/spritesheets/mutatedriotpolice_spritesheet.png"));
	m_anims[5].addFrame(sf::IntRect(0, 1536, 512, 512));
	m_anims[5].addFrame(sf::IntRect(512, 1536, 512, 512));
	m_anims[5].addFrame(sf::IntRect(1024, 1536, 512, 512));
	m_anims[5].addFrame(sf::IntRect(0, 0, 1, 1));

	// Empty Frame
	m_anims.push_back(Animation());
	m_anims[6].setSpriteSheet(*texCache->getTexture("../assets/textures/spritesheets/mutatedriotpolice_spritesheet.png"));
	m_anims[6].addFrame(sf::IntRect(0, 0, 1, 1));


	m_animSprite = AnimatedSprite(100);
	m_animSprite.setScale(.35f, .35f);
	//m_animSprite.setOrigin(100, 200);
	m_animSprite.play(m_anims[0]);
	m_animSprite.setLooped(true);
}

EnemyShield::~EnemyShield()
{
	//
}

void EnemyShield::update(float deltaTime)
{
	m_animSprite.update(deltaTime * 1000);
	//m_animSprite.setColor(sf::Color(255, 50, 50, 255));
	if (m_isHit && m_hitEffect < 6)
	{
		if (m_hitEffect == 0)
			m_animSprite.setColor(sf::Color(255, 50, 50, 255));
		else if (m_hitEffect == 1)
			m_animSprite.setColor(sf::Color(20, 20, 20, 255));
		else if (m_hitEffect == 2)
			m_animSprite.setColor(sf::Color(255, 50, 50, 255));
		else if (m_hitEffect == 3)
			m_animSprite.setColor(sf::Color(255, 255, 255, 255));
		else if (m_hitEffect == 4)
			m_animSprite.setColor(sf::Color(255, 50, 50, 255));
		else if (m_hitEffect == 5)
		{
			m_animSprite.setColor(sf::Color(255, 255, 255, 255));
			m_isHit = false;
			m_hitEffect = 0;
		}
		m_hitEffect += 1;
	}

	if (m_armour == 18)
		m_animSprite.play(m_anims[1]);
	else if (m_armour == 12)
		m_animSprite.play(m_anims[2]);
	else if (m_armour <= 8 && m_armour > 0)
	{
		if (m_animSprite.getCurrentFrame() < 2 && m_enterAnim)
			m_animSprite.play(m_anims[3]);
		else
		{
			m_enterAnim = false;
			m_animSprite.play(m_anims[4]);
		}	
	}
	else if (m_armour <= 0)
	{
		if(m_animSprite.getCurrentFrame() < 2)
			m_animSprite.play(m_anims[5]);
		else
		{
			m_animSprite.play(m_anims[6]);
			setActive(false);
		}	
	}
}

void EnemyShield::setActive(bool value)
{
	m_isActive = value;
	body->setActive(value);
}

bool EnemyShield::isActive() const { return m_isActive; }

void EnemyShield::setArmourPoints(int value) { m_armour = value; m_isHit = true;}

int EnemyShield::getArmourPoints() const { return m_armour; }

EENTITYTYPE EnemyShield::GetType() { return EENTITYTYPE::ENTITY_ENEMY_SHIELD; }

void EnemyShield::onConstruction(const GameObjectCreationKit& kit)
{
	shape = new CollisionSystem::RectangleShape(92.f, 152.f);
	CollisionSystem::BodyDef def(CollisionSystem::EBodyTypes::kKinematicBody, FILTER(EEntityCategory::kEnemyShield, EEntityCategory::kPlayerProjectile), false, shape, this);
	body = kit.collisionMngr->createBody(def, this);

	m_hitEffect = 0;
	m_isHit = false;
	m_enterAnim = true;
	m_animSprite.play(m_anims[0]);
	m_animSprite.setColor(sf::Color(255, 255, 255, 255));
	m_armour = 20;

	setActive(true);
}

void EnemyShield::onDestruction(const GameObjectCreationKit& kit)
{
	kit.collisionMngr->destroyBody(body);
	body = nullptr;

	delete shape;
	shape = nullptr;
}

void EnemyShield::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	states.transform *= getTransform();

	if (m_isActive)
		target.draw(m_animSprite, states);
}
