#pragma once

#include "Utils\Path.h"

#include <Engine\GameObjectCreationKit.h>

#include <vector>

#include <SFML\Graphics\Texture.hpp>

#include <SFML\Graphics\Drawable.hpp>

class MutatedHuman;
class MutatedCrow;
class MutatedRiotPolice;
class Tsunami;
class BossCharacter;
class BossController;
class EnemyShield;
class HurricaneAgent;


class ProjectileManager;
class PlayerManager;
class AudioManager;
class TextureCache;

namespace dpEngine {
	struct SystemInfo;
	class ViewManager;
}

using namespace dpEngine;

struct orderable
{
	sf::Vector2f pos;
	sf::Drawable* obj;
};

class EnemyManager
{
private:
	std::vector<MutatedHuman*> m_mutatedHumans;

	std::vector<MutatedCrow*> m_mutatedCrows;

	std::vector<MutatedRiotPolice*> m_mutatedRiotPolices;

	BossCharacter* m_bossCharacter;

	BossController* m_bossController;
	
	Tsunami* m_tsunami;

	std::vector<EnemyShield*> m_shields;

	std::vector<HurricaneAgent*> m_hurricanes;

	std::vector<orderable> m_orderables;

	sf::Texture m_bossTexture;
	
	GameObjectCreationKit m_kit;

	ProjectileManager* m_projectileMngr;

	PlayerManager* m_playerMngr;

	AudioManager* m_audioMngr;

	TextureCache* m_texCache;

	ViewManager* m_viewMngr;

public:
	EnemyManager(const dpEngine::SystemInfo&, ProjectileManager*, PlayerManager*, AudioManager*, TextureCache*);

	~EnemyManager();

	MutatedHuman* createMutatedHuman(float, float);

	MutatedCrow* createMutatedCrow(float, float);

	MutatedRiotPolice* createMutatedRiotPolice(float, float);

	Tsunami* createTsunami(float, float);

	BossCharacter* createBoss(float, float);

	EnemyShield* createShield();

	void createHurricane(float, float);

	void update(float);

	void preRender(sf::RenderTarget& target);
	
	void postRender(sf::RenderTarget& target); 

	bool sort(orderable, orderable);

private:
	BossCharacter* createBossCharacter();

	BossController* createBossController();

	HurricaneAgent* createHurricaneAgent(float, float, Path);

};
