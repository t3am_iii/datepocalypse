#include "HUD.h"
#include "AnimatedSprite.h"
#include "PlayerState.h"
#include "PlayerCharacter.h"
#include "PlayerShield.h"
#include "PlayerManager.h"
#include "Enums.h"
#include "Global.h"
#include "ScoreManager.h"

#include <Engine\Utils\Math.h>

#include <Engine\Utils\Math.h>
#include <SFML\Graphics\Font.hpp>
#include <SFML\Graphics\Text.hpp>
#include <SFML\Graphics\Texture.hpp>
#include <SFML\Graphics\Sprite.hpp>
#include <SFML\Graphics\RenderTarget.hpp>

#include <string.h>
#include <iostream>

//struct struct_HealthbarHeart
//{
//	int iHeartIndex;
//	sf::Sprite* xSprite;
//
//	sf::Sprite* xSprite_Normal;
//	sf::Sprite* xSprite_Hit;
//};

struct HealthbarHeart
{
	int iHeartIndex;
	Animation m_inActiveAnimation;
	Animation m_normalAnimation;
	Animation m_hitAnimation;
	AnimatedSprite m_animatedSprite;
};

struct Portrait
{
	int iPlayerIndex;
	sf::Sprite* xSprite;

	sf::Sprite* xSprite_Idle;
	sf::Sprite* xSprite_Hit;
	sf::Sprite* xSprite_Death;
};

struct ScoreText
{
	sf::Text text;
	sf::Vector2f pos;
	float ttl;
};

HUD::HUD(ScoreManager* scoreMngr, PlayerManager* playerMngr, TextureCache* texCache)
	: m_scoreMngr(scoreMngr)
	, m_playerMngr(playerMngr)
	, m_texCache(texCache)
{
	m_prevHP_p1 = -1;
	m_prevHP_p2 = -1;
	iTimerTest = 0;
	m_bPlayerHit = false;

	discoText.loadFromFile("../assets/disco.png");
	discoSprite.setTexture(discoText);
	discoSprite.setPosition(-40, 400);
	discoFloat = 0;

	m_HUDbgSprite_bot.setTexture(*m_texCache->getTexture("../assets/textures/hud/hud_bottom.png"));
	m_HUDbgSprite_bot.setPosition(0, 0);
	m_HUDbgSprite_top.setTexture(*m_texCache->getTexture("../assets/textures/hud/hud_top.png"));
	m_HUDbgSprite_bot.setPosition(0, 0);

	// Score Counter / Multiplier Initialization
	m_xScore_Font.loadFromFile("../assets/fonts/NuevaStd-Cond.otf");
	
	m_xScoreCounter_txt.setColor(sf::Color(0, 0, 0, 255));
	m_xScoreCounter_txt.setFont(m_xScore_Font);
	m_xScoreCounter_txt.setPosition(sf::Vector2f(20, 5));
	m_xScoreCounter_txt.setCharacterSize(40);

	m_xScoreMultiplier_txt.setColor(sf::Color(0, 0, 0, 255));
	m_xScoreMultiplier_txt.setFont(m_xScore_Font);
	m_xScoreMultiplier_txt.setPosition(sf::Vector2f(20, 170));
	m_xScoreMultiplier_txt.setCharacterSize(60);

	// Shield Bar Intitialization
	m_shieldBar = new sf::Sprite;
	m_shieldBar->setTexture(*m_texCache->getTexture("../assets/textures/hud/hud_sheet_placeholder.png"));

	m_shieldBarAni.setSpriteSheet(*m_texCache->getTexture("../assets/textures/hud/hudshield_spritesheet.png"));

	m_shieldBarAni.addFrame(sf::IntRect(0, 0, 256, 256));
	m_shieldBarAni.addFrame(sf::IntRect(256, 0, 256, 256));
	m_shieldBarAni.addFrame(sf::IntRect(512, 0, 256, 256));
	m_shieldBarAni.addFrame(sf::IntRect(768, 0, 256, 256));
	m_shieldBarAni.addFrame(sf::IntRect(0, 256, 256, 256));
	m_shieldBarAni.addFrame(sf::IntRect(256, 256, 256, 256));
	m_shieldBarAni.addFrame(sf::IntRect(512, 256, 256, 256));
	m_shieldBarAni.addFrame(sf::IntRect(768, 256, 256, 256));

	m_aniSprite_HUDShield.setAnimation(m_shieldBarAni);
	m_aniSprite_HUDShield.setPosition(1318, 980);
	m_aniSprite_HUDShield.setScale(0.4f, 0.4f);
	m_aniSprite_HUDShield.play();

	m_shieldbarRect.setTexture(m_shieldBar->getTexture());
	m_shieldbarRect.setTextureRect(sf::IntRect(128, 64, 256, 32));
	m_shieldbarRect.setPosition(630, 1020);
	m_shieldbarRect.setSize(sf::Vector2f(950, 100));

	m_shieldBarContainerSprite = new sf::Sprite;
	m_shieldBarContainerSprite->setTexture(*m_texCache->getTexture("../assets/textures/hud/hud_sheet_placeholder.png"));
	m_shieldbarContainerRect.setTextureRect(sf::IntRect(160, 96, 5, 5));
	m_shieldbarContainerRect.setPosition(630, 1020);
	m_shieldbarContainerRect.setFillColor(sf::Color(255, 255, 255, 100));
	m_shieldbarContainerRect.setSize(sf::Vector2f(650, 32));

	m_shieldbarContainerRect.setTexture(m_shieldBar->getTexture());

	// Powerup bars
	// Batbar
	m_batProgress = 0;
	m_batBarRect.setFillColor(sf::Color(255, 255, 255, 255));
	m_batBarRect.setSize(sf::Vector2f(30, 155));
	m_batBarRect.setPosition(560, 800); // 945 1060
	m_batBarRect.setTexture(m_texCache->getTexture("../assets/textures/hud/colorpallette.png"));
	m_batBarRect.setTextureRect(sf::IntRect(40, 0, 10, 10));

	m_boomProgress = 0;
	m_batContainerRect.setFillColor(sf::Color(255, 255, 255, 100));
	m_batContainerRect.setSize(sf::Vector2f(30, 160)); 
	m_batContainerRect.setPosition(545, 905);
	m_batContainerRect.setTexture(m_texCache->getTexture("../assets/textures/hud/colorpallette.png"));
	m_batContainerRect.setTextureRect(sf::IntRect(30, 0, 10, 10));

	m_qKeyAni.setSpriteSheet(*m_texCache->getTexture("../assets/textures/hud/eq.png"));
	m_qKeyAni.addFrame(sf::IntRect(256, 0, 128, 128));
	m_qKeyAni.addFrame(sf::IntRect(384, 0, 128, 128));


	m_qKeySprite = AnimatedSprite(300);
	m_qKeySprite.play(m_qKeyAni);
	m_qKeySprite.setPosition(475, 865);
	m_qKeySprite.setScale(.5f, .5f);

	// Boomerangbar
	m_boomBarRect.setFillColor(sf::Color(255, 255, 255, 255));
	m_boomBarRect.setSize(sf::Vector2f(32, 155));
	m_boomBarRect.setPosition(1340, 1060);
	m_boomBarRect.setTexture(m_texCache->getTexture("../assets/textures/hud/colorpallette.png"));
	m_boomBarRect.setTextureRect(sf::IntRect(30, 0, 10, 10));

	m_boomContainerRect.setFillColor(sf::Color(255, 255, 255, 100));
	m_boomContainerRect.setSize(sf::Vector2f(32, 160));
	m_boomContainerRect.setPosition(1340, 905);
	m_boomContainerRect.setTexture(m_texCache->getTexture("../assets/textures/hud/colorpallette.png"));
	m_boomContainerRect.setTextureRect(sf::IntRect(30, 0, 10, 10));

	m_eKeyAni.setSpriteSheet(*m_texCache->getTexture("../assets/textures/hud/eq.png"));
	m_eKeyAni.addFrame(sf::IntRect(0, 0, 128, 128));
	m_eKeyAni.addFrame(sf::IntRect(128, 0, 128, 128));

	m_eKeySprite = AnimatedSprite(300);
	m_eKeySprite.play(m_eKeyAni);
	m_eKeySprite.setPosition(1380, 865);
	m_eKeySprite.setScale(.5f, .5f);
	//Animation m_qKeyAni;
	//AnimatedSprite m_eKeySprite;
	//AnimatedSprite m_qKeySprite;

	for (int i = 0; i < 6; i++)
	{
		if (i < 3)
		{
			Animation m_normal;
			m_normal.setSpriteSheet(*m_texCache->getTexture("../assets/textures/hud/health_spritesheet.png"));
			m_normal.addFrame(sf::IntRect(0, 0, 256, 256));
			m_normal.addFrame(sf::IntRect(256, 0, 256, 256));
			m_normal.addFrame(sf::IntRect(512, 0, 256, 256));
			m_normal.addFrame(sf::IntRect(768, 0, 256, 256));
			m_normal.addFrame(sf::IntRect(512, 0, 256, 256));
			m_normal.addFrame(sf::IntRect(256, 0, 256, 256));

			Animation m_hit;
			m_hit.setSpriteSheet(*m_texCache->getTexture("../assets/textures/hud/health_spritesheet.png"));
			m_hit.addFrame(sf::IntRect(1024, 0, 256, 256));
			m_hit.addFrame(sf::IntRect(1280, 0, 256, 256));
			m_hit.addFrame(sf::IntRect(1536, 0, 256, 256));
			m_hit.addFrame(sf::IntRect(1792, 0, 256, 256));
			m_hit.addFrame(sf::IntRect(0, 256, 256, 256));
			m_hit.addFrame(sf::IntRect(256, 256, 256, 256));
			m_hit.addFrame(sf::IntRect(512, 256, 256, 256));
			m_hit.addFrame(sf::IntRect(768, 256, 256, 256));
			m_hit.addFrame(sf::IntRect(1024, 256, 256, 256));
			m_hit.addFrame(sf::IntRect(1280, 256, 256, 256));
			m_hit.addFrame(sf::IntRect(1536, 256, 256, 256));

			AnimatedSprite m_animation;
			m_animation = AnimatedSprite(100, false, true);
			m_animation.setScale(0.20f, 0.20f); //0.45f
			m_animation.setOrigin(0, 0);
			m_animation.setPosition(sf::Vector2f(20, i * 128 + 350));

			m_axHealthbarHearts.push_back(HealthbarHeart());
			m_axHealthbarHearts[i].iHeartIndex = i;
			m_axHealthbarHearts[i].m_inActiveAnimation = m_hit;
			m_axHealthbarHearts[i].m_normalAnimation = m_normal;
			m_axHealthbarHearts[i].m_hitAnimation = m_hit;
			m_axHealthbarHearts[i].m_animatedSprite = m_animation;
		}
		else if (i >= 3 && i < 6)
		{
			Animation m_normal;
			m_normal.setSpriteSheet(*m_texCache->getTexture("../assets/textures/hud/health_spritesheet.png"));
			m_normal.addFrame(sf::IntRect(  0, 0, 256, 256));
			m_normal.addFrame(sf::IntRect(256, 0, 256, 256));
			m_normal.addFrame(sf::IntRect(512, 0, 256, 256));
			m_normal.addFrame(sf::IntRect(768, 0, 256, 256));
			m_normal.addFrame(sf::IntRect(512, 0, 256, 256));
			m_normal.addFrame(sf::IntRect(256, 0, 256, 256));

			Animation m_hit;
			m_hit.setSpriteSheet(*m_texCache->getTexture("../assets/textures/hud/health_spritesheet.png"));
			m_hit.addFrame(sf::IntRect(1024,   0, 256, 256));
			m_hit.addFrame(sf::IntRect(1280,   0, 256, 256));
			m_hit.addFrame(sf::IntRect(1536,   0, 256, 256));
			m_hit.addFrame(sf::IntRect(1792,   0, 256, 256));
			m_hit.addFrame(sf::IntRect(   0, 256, 256, 256));
			m_hit.addFrame(sf::IntRect( 256, 256, 256, 256));
			m_hit.addFrame(sf::IntRect( 512, 256, 256, 256));
			m_hit.addFrame(sf::IntRect( 768, 256, 256, 256));
			m_hit.addFrame(sf::IntRect(1024, 256, 256, 256));
			m_hit.addFrame(sf::IntRect(1280, 256, 256, 256));
			m_hit.addFrame(sf::IntRect(1536, 256, 256, 256));

			AnimatedSprite m_animation;
			m_animation = AnimatedSprite(100, false, true);
			m_animation.setScale(0.20f, 0.20f);
			m_animation.setOrigin(0, 0);
			m_animation.setPosition(sf::Vector2f(SCREEN_WIDTH - 230, (i - 3) * 128 + 350));

			m_axHealthbarHearts.push_back(HealthbarHeart());
			m_axHealthbarHearts[i].iHeartIndex = i;
			m_axHealthbarHearts[i].m_inActiveAnimation = m_hit;
			m_axHealthbarHearts[i].m_normalAnimation = m_normal;
			m_axHealthbarHearts[i].m_hitAnimation = m_hit;
			m_axHealthbarHearts[i].m_animatedSprite = m_animation;
		}
	}

	for (int i = 0; i < 2; i++)
	{
		if (i == 0)
		{
			sf::Sprite* xSprite_Idle = new sf::Sprite();
			xSprite_Idle->setTexture(*m_texCache->getTexture("../assets/textures/hud/portraits_spritesheet.png"));

			sf::Sprite* xSprite_Hit = new sf::Sprite();
			xSprite_Hit->setTexture(*m_texCache->getTexture("../assets/textures/hud/portraits_spritesheet.png"));
			xSprite_Hit->setTextureRect(sf::IntRect(256, 256, 256, 256));

			sf::Sprite* xSprite_Death = new sf::Sprite();
			xSprite_Death->setTexture(*m_texCache->getTexture("../assets/textures/hud/portraits_spritesheet.png"));
			xSprite_Death->setTextureRect(sf::IntRect(512, 256, 256, 256));

			xSprite_Idle->setTextureRect(sf::IntRect(0, 256, 256, 256));

			xSprite_Idle->setPosition(sf::Vector2f(64, SCREEN_HEIGHT - 320 ));
			xSprite_Idle->setScale(.75f, .75f);

			xSprite_Hit->setPosition(sf::Vector2f(64, SCREEN_HEIGHT - 320));
			xSprite_Hit->setScale(.75f, .75f);

			xSprite_Death->setPosition(sf::Vector2f(64, SCREEN_HEIGHT - 320));
			xSprite_Death->setScale(.75f, .75f);

			m_axPortrait.push_back(Portrait());
			m_axPortrait[i].iPlayerIndex = i;
			m_axPortrait[i].xSprite = xSprite_Idle;
			m_axPortrait[i].xSprite_Idle = xSprite_Idle;
			m_axPortrait[i].xSprite_Hit = xSprite_Hit;
			m_axPortrait[i].xSprite_Death = xSprite_Death;
		}
		if (i == 1)
		{
			sf::Sprite* xSprite_Idle2 = new sf::Sprite();
			xSprite_Idle2->setTexture(*m_texCache->getTexture("../assets/textures/hud/portraits_spritesheet.png"));

			sf::Sprite* xSprite_Hit2 = new sf::Sprite();
			xSprite_Hit2->setTexture(*m_texCache->getTexture("../assets/textures/hud/portraits_spritesheet.png"));
			xSprite_Hit2->setTextureRect(sf::IntRect(256, 0, 256, 256));

			sf::Sprite* xSprite_Death2 = new sf::Sprite();
			xSprite_Death2->setTexture(*m_texCache->getTexture("../assets/textures/hud/portraits_spritesheet.png"));
			xSprite_Death2->setTextureRect(sf::IntRect(512, 0, 256, 256));

			xSprite_Idle2->setTextureRect(sf::IntRect(0, 0, 256, 256));

			xSprite_Idle2->setPosition(sf::Vector2f(SCREEN_WIDTH - 256, SCREEN_HEIGHT - 320));
			xSprite_Idle2->setScale(.75f, .75f);

			xSprite_Hit2->setPosition(sf::Vector2f(SCREEN_WIDTH - 256, SCREEN_HEIGHT - 320));
			xSprite_Hit2->setScale(.75f, .75f);

			xSprite_Death2->setPosition(sf::Vector2f(SCREEN_WIDTH - 256, SCREEN_HEIGHT - 320));
			xSprite_Death2->setScale(.75f, .75f);

			m_axPortrait.push_back(Portrait());
			m_axPortrait[i].iPlayerIndex = i;
			m_axPortrait[i].xSprite = xSprite_Idle2;
			m_axPortrait[i].xSprite_Idle = xSprite_Idle2;
			m_axPortrait[i].xSprite_Hit = xSprite_Hit2;
			m_axPortrait[i].xSprite_Death = xSprite_Death2;
		}
	}

	/*for (int idx = 0; idx < 2; idx++)
	{
		m_boomerangArc[idx].currentStrength = .5f;
		m_boomerangArc[idx].targetStrength = idx;
	}*/

	
	sf::Texture* tutorialTex = texCache->getTexture("../assets/textures/hud/tutorial.png");
	for (int charIdx = 0; charIdx < 2; charIdx++)
	{
		for (int keyIdx = 0; keyIdx < 4; keyIdx++)
		{
			sf::Sprite& refSpr = m_tutorial.moveSprites[charIdx][keyIdx];
			refSpr.setTexture(*tutorialTex);
			refSpr.setTextureRect(sf::IntRect((charIdx * 4 + keyIdx) * 128, 0, 128, 128));
			refSpr.setScale(.4f, .4f);
			refSpr.setOrigin(128 * .2f, 128 * .2f);
		}
	}

	m_tutorial.fireSprite.setTexture(*tutorialTex);
	m_tutorial.fireSprite.setTextureRect(sf::IntRect(1024, 0, 512, 128));
	m_tutorial.fireSprite.setScale(.4f, .4f);
	m_tutorial.fireSprite.setOrigin(512 * .2f, 128 * .2f);
	m_tutorial.fireSprite.setPosition(960.f, 500.f);
}

HUD::~HUD() { }

void HUD::Update(float p_fDeltaTime)
{
	discoSprite.setColor(sf::Color(255,255,255,210));
	discoSprite.setRotation(discoFloat);
	discoFloat += p_fDeltaTime * 100;

	for (int i = 0; i < m_axHealthbarHearts.size(); i++)
		m_axHealthbarHearts[i].m_animatedSprite.update(p_fDeltaTime * 1500.f);

	PlayerCharacter* playerOne = m_playerMngr->getCharacter(1);
	PlayerCharacter* playerTwo = m_playerMngr->getCharacter(2);
		
	// Player one hearts
	if (playerOne->GetHealthPoints() != m_prevHP_p1)
	{
		if (playerOne->GetHealthPoints() == 3)
		{
			m_axHealthbarHearts[0].m_animatedSprite.setAnimation(m_axHealthbarHearts[0].m_normalAnimation);
			m_axHealthbarHearts[1].m_animatedSprite.setAnimation(m_axHealthbarHearts[1].m_normalAnimation);
			m_axHealthbarHearts[2].m_animatedSprite.setAnimation(m_axHealthbarHearts[2].m_normalAnimation);

		}
		else if (playerOne->GetHealthPoints() == 2)
		{
			if (m_axHealthbarHearts[0].m_animatedSprite.isLooped())
				m_axHealthbarHearts[0].m_animatedSprite.setAnimation(m_axHealthbarHearts[0].m_hitAnimation);
		

			m_axHealthbarHearts[1].m_animatedSprite.setAnimation(m_axHealthbarHearts[1].m_normalAnimation);
			m_axHealthbarHearts[2].m_animatedSprite.setAnimation(m_axHealthbarHearts[2].m_normalAnimation);

		}
		else if (playerOne->GetHealthPoints() == 1)
		{
				
			if (m_axHealthbarHearts[1].m_animatedSprite.isLooped())
				m_axHealthbarHearts[1].m_animatedSprite.setAnimation(m_axHealthbarHearts[1].m_hitAnimation);
			
			m_axHealthbarHearts[2].m_animatedSprite.setAnimation(m_axHealthbarHearts[2].m_normalAnimation);
		}
		else if (playerOne->GetHealthPoints() == 0)
		{
			if (m_axHealthbarHearts[2].m_animatedSprite.isLooped())
				m_axHealthbarHearts[2].m_animatedSprite.setAnimation(m_axHealthbarHearts[2].m_hitAnimation);
		}

		for (int idx = 0; idx < 3; idx++)
		{
			m_axHealthbarHearts[idx].m_animatedSprite.setLooped((3 - idx) < playerOne->GetHealthPoints() + 1);
			if (m_axHealthbarHearts[idx].m_animatedSprite.getCurrentFrame() == 10)
			{
				m_axHealthbarHearts[idx].m_animatedSprite.pause();
			}
		}

		m_prevHP_p1 = playerOne->GetHealthPoints();
	}
	// Player two hearts
	if (playerTwo->GetHealthPoints() != m_prevHP_p2)
	{
		if (playerTwo->GetHealthPoints() == 3)
		{
			m_axHealthbarHearts[3].m_animatedSprite.setAnimation(m_axHealthbarHearts[3].m_normalAnimation);
			m_axHealthbarHearts[4].m_animatedSprite.setAnimation(m_axHealthbarHearts[4].m_normalAnimation);
			m_axHealthbarHearts[5].m_animatedSprite.setAnimation(m_axHealthbarHearts[5].m_normalAnimation);
		}
		else if (playerTwo->GetHealthPoints() == 2)
		{
			if (m_axHealthbarHearts[3].m_animatedSprite.isLooped())
				m_axHealthbarHearts[3].m_animatedSprite.setAnimation(m_axHealthbarHearts[3].m_hitAnimation);

			m_axHealthbarHearts[4].m_animatedSprite.setAnimation(m_axHealthbarHearts[4].m_normalAnimation);
			m_axHealthbarHearts[5].m_animatedSprite.setAnimation(m_axHealthbarHearts[5].m_normalAnimation);
		}
		else if (playerTwo->GetHealthPoints() == 1)
		{

			if (m_axHealthbarHearts[3].m_animatedSprite.isLooped())
				m_axHealthbarHearts[3].m_animatedSprite.setAnimation(m_axHealthbarHearts[3].m_hitAnimation);

			m_axHealthbarHearts[4].m_animatedSprite.setAnimation(m_axHealthbarHearts[4].m_hitAnimation);
		}
		else if (playerTwo->GetHealthPoints() == 0)
		{
			if (m_axHealthbarHearts[5].m_animatedSprite.isLooped())
				m_axHealthbarHearts[5].m_animatedSprite.setAnimation(m_axHealthbarHearts[5].m_hitAnimation);
		}

		for (int idx = 0; idx < 3; idx++)
		{
			m_axHealthbarHearts[idx + 3].m_animatedSprite.setLooped((3 - idx) < playerTwo->GetHealthPoints() +1);
			if (m_axHealthbarHearts[idx ].m_animatedSprite.getCurrentFrame() == 10)
			{
				m_axHealthbarHearts[idx ].m_animatedSprite.pause();
			}
		}

		m_prevHP_p2 = playerTwo->GetHealthPoints();
	}
	for (int i = 0; i < 3; i++)
		m_axHealthbarHearts[i].m_animatedSprite.setPosition(playerOne->getPosition().x + i - 70, playerOne->getPosition().y + i * 40 + 50);
	//m_axHealthbarHearts[i].m_animatedSprite.setPosition(playerOne->getPosition().x + i * 40 - 50, playerOne->getPosition().y + 100);

	for (int i = 3; i < 6; i++)
		m_axHealthbarHearts[i].m_animatedSprite.setPosition(playerTwo->getPosition().x + i + 20, playerTwo->getPosition().y + (i - 3) * 40 + 50);


	// TODO:
	// change portrait on hit / death
	if (playerOne->getIsHit())
		m_axPortrait[0].xSprite = m_axPortrait[0].xSprite_Hit;
	else if(playerOne->GetHealthPoints() <= 0)
		m_axPortrait[0].xSprite = m_axPortrait[0].xSprite_Death;
	else
		m_axPortrait[0].xSprite = m_axPortrait[0].xSprite_Idle;
	
	if (playerTwo->getIsHit())
		m_axPortrait[1].xSprite = m_axPortrait[1].xSprite_Hit;
	else if (playerTwo->GetHealthPoints() <= 0)
		m_axPortrait[1].xSprite = m_axPortrait[1].xSprite_Death;
	else
		m_axPortrait[1].xSprite = m_axPortrait[1].xSprite_Idle;


	PlayerState& state = *m_playerMngr->getState();
	
	// Boomerang code, used primarly to show arc
	if (state.inBoomState)
	{
		if (state.boomerangingState == EBoomerangingState::kFlying)
		{
			const auto throwerCharPos = state.throwerChar->getPosition()
				- sf::Vector2f(32.f, 64.f);
			const auto partnerCharPos = state.partnerChar->getPosition()
				- sf::Vector2f(32.f, 64.f);

			/*if (abs(FMath::Distance(throwerCharPos, m_lastCalcPos[0])) > 10.0f)
			{
				recalcBoomerangArc(throwerCharPos, partnerCharPos, state);
				m_lastCalcPos[0] = throwerCharPos;
			}
			else if (abs(FMath::Distance(partnerCharPos, m_lastCalcPos[1])) > 10.0f)
			{
				recalcBoomerangArc(throwerCharPos, partnerCharPos, state);
				m_lastCalcPos[1] = partnerCharPos;
			}*/
			recalcBoomerangArc(throwerCharPos, partnerCharPos, state);

			auto deltaCharPos = FMath::Abs(throwerCharPos - partnerCharPos);
			auto startBarPos = sf::Vector2f(std::min(throwerCharPos.x, partnerCharPos.x), throwerCharPos.y);
			startBarPos.x += deltaCharPos.x / 2;
			auto endBarPos = startBarPos - sf::Vector2f(0.0f, 200.0f);

			//recalcStrengthBar(startBarPos, endBarPos, m_currentStrength);

			/*for (int idx = 0; idx < 2; idx++)
			{
				m_boomerangArc[idx].currentStrength = FMath::Lerp(m_boomerangArc[idx].currentStrength, m_boomerangArc[idx].targetStrength, p_fDeltaTime);
				if (m_boomerangArc[idx].currentStrength < 0.02f)
				{
					m_boomerangArc[idx].currentStrength = 0.02f;
					m_boomerangArc[idx].targetStrength = 1.0f;
				}
				else if (m_boomerangArc[idx].currentStrength > 0.98f)
				{
					m_boomerangArc[idx].currentStrength = 0.98f;
					m_boomerangArc[idx].targetStrength = 0.0f;
				}
			}*/
		}
	}
	// Boomerang -End- //

	// Score Counter Update
	m_xScoreCounter_txt.setString("Score: \n" + std::to_string(m_scoreMngr->getScore()));
	m_xScoreMultiplier_txt.setString(" x" + std::to_string(m_scoreMngr->getMultiplier()));

	if (m_scoreMngr->getMultiplier() > 0 && m_scoreMngr->getMultiplier() <= 10)
	{
		playerOne->setDamageStrength(1);
		playerTwo->setDamageStrength(1);
	}
	else if (m_scoreMngr->getMultiplier() > 10 && m_scoreMngr->getMultiplier() < 20)
	{
		playerOne->setDamageStrength(2);
		playerTwo->setDamageStrength(2);
	}
	else if (m_scoreMngr->getMultiplier() >= 20)
	{
		playerOne->setDamageStrength(3);
		playerTwo->setDamageStrength(3);
	}

	// Power-Up Bars 
	// Bat
	if (m_batProgress < 100)
	{
		m_batBarRect.setScale(1, m_batProgress / 100);
		m_batBarRect.setPosition(545, 1060 - (m_batProgress / 100 ) * 155);
	}
	else
	{
		m_batProgress = 100;
		m_batBarRect.setScale(1, m_batProgress / 100);
		m_batBarRect.setPosition(545, 1060 - (m_batProgress / 100) * 155);
	}
	m_qKeySprite.update(p_fDeltaTime * 1000);

	// Boomerang
	if (m_boomProgress < 100)
	{
		m_boomBarRect.setScale(1, m_boomProgress / 100);
		m_boomBarRect.setPosition(1340, 1060 - (m_boomProgress / 100) * 155);
	}
	else
	{
		m_boomProgress = 100;
		m_boomBarRect.setScale(1, m_boomProgress / 100);
		m_boomBarRect.setPosition(1340, 1060 - (m_boomProgress / 100) * 155);
	}

	m_eKeySprite.update(p_fDeltaTime * 1000);
	// -End- Power-Up Bars //

	// Score Text on Enemy. Time to live and position update
	for (int i = 0; i < m_scoreTxts.size(); i++)
	{
		if (m_scoreTxts.at(i)->ttl > 0)
		{
			m_scoreTxts.at(i)->ttl -= p_fDeltaTime;
			m_scoreTxts.at(i)->text.setPosition(m_scoreTxts.at(i)->text.getPosition().x, m_scoreTxts.at(i)->text.getPosition().y - p_fDeltaTime * 100);
			
			if(m_scoreTxts.at(i)->text.getPosition().y < 0)
				m_scoreTxts.at(i)->text.setPosition(m_scoreTxts.at(i)->text.getPosition().x, 0);
		}

	}
	// Score Counter -End-//

	// ShieldBar Update
	m_aniSprite_HUDShield.update(p_fDeltaTime * 2000);
	
	const auto s = m_playerMngr->getShield();
	float dur = s->GetDuration();

	m_aniSprite_HUDShield.setPosition(dur * 3 + 580, 970);
	m_shieldbarRect.setSize(sf::Vector2f(dur * 3, 32));
	m_shieldbarRect.setFillColor(sf::Color(dur, dur, dur, 255));

	// ShieldBar -End- //

	for (int charIdx = 0; charIdx < 2; charIdx++)
	{
		PlayerCharacter* c = m_playerMngr->getCharacter(charIdx + 1);

		for (int keyIdx = 0; keyIdx < 4; keyIdx++)
		{
			bool display = m_playerMngr->getState()->tutorialState.displayMove[charIdx][keyIdx];
			m_tutorial.displayMove[charIdx][keyIdx] = display;

			if (!display)
				continue;

			sf::Sprite& refSpr = m_tutorial.moveSprites[charIdx][keyIdx];
			sf::Vector2f dir;
			if (keyIdx == 0) dir = { 0, -1 };
			else if (keyIdx == 1) dir = { -1, 0 };
			else if (keyIdx == 2) dir = { 0, 1 };
			else if (keyIdx == 3) dir = { 1, 0 };

			refSpr.setPosition(c->getPosition() + (dir * 70.f));
		}
	}

	m_tutorial.displayFire = m_playerMngr->getState()->tutorialState.displayFire;
}

void HUD::Draw(sf::RenderTarget& target)
{
	const PlayerState& state = *m_playerMngr->getState();



	// Draw hearts
	for (int i = 0; i < m_axHealthbarHearts.size(); i++)
	{
		target.draw(m_axHealthbarHearts[i].m_animatedSprite);
	}

	// Draw ShieldBar
	target.draw(m_shieldbarRect);
	target.draw(m_shieldbarContainerRect);

	// Draw powerup bars
	target.draw(m_batBarRect);
	target.draw(m_batContainerRect);

	target.draw(m_boomBarRect);
	target.draw(m_boomContainerRect);

	if (m_batProgress >= 100)
		target.draw(m_qKeySprite);
	if (m_boomProgress >= 100)
		target.draw(m_eKeySprite);

	target.draw(m_HUDbgSprite_bot);

	target.draw(m_aniSprite_HUDShield);

	// Draw ScoreCounter
	target.draw(m_xScoreCounter_txt);
	target.draw(m_xScoreMultiplier_txt);

	for (int i = 0; i < m_scoreTxts.size(); i++)
		if(m_scoreTxts.at(i)->ttl > 0)
			target.draw(m_scoreTxts[i]->text);

	// Draw Portraits
	for (int i = 0; i < m_axPortrait.size(); i++)
	{
		target.draw(*m_axPortrait[i].xSprite);
	}

	target.draw(m_HUDbgSprite_top);
	

	// Draw boomerang arc & strength meter
	if (state.inBoomState && state.boomerangingState == EBoomerangingState::kFlying)
	{
		for (int idx = 0; idx < 2; idx++)
		{
			target.draw(m_boomerangArc[idx].vertices.data(), m_boomerangArc[idx].count, sf::LinesStrip);
			//target.draw(m_partnerTarget);

			//target.draw(m_strengthBarInner, STRENGTH_VERTEX_COUNT, sf::PrimitiveType::TrianglesStrip);
			//target.draw(m_strengthBarOutline);
		}
	}
	//target.draw(discoSprite);


	for (int charIdx = 0; charIdx < 2; charIdx++)
		for (int keyIdx = 0; keyIdx < 4; keyIdx++)
			if (m_tutorial.displayMove[charIdx][keyIdx])
				target.draw(m_tutorial.moveSprites[charIdx][keyIdx]);

	if (m_tutorial.displayFire)
		target.draw(m_tutorial.fireSprite);
}

void HUD::recalcBoomerangArc(sf::Vector2f throwerCharPos, sf::Vector2f partnerCharPos, PlayerState& state)
{
	const float arcStartAngle(-(180.0f / 12));
	const float arcEndAngle(180.0f + (180.0f / 12));

	/*sf::Vector2f lowerPos(std::min(throwerCharPos.x, partnerCharPos.x), std::min(throwerCharPos.y, partnerCharPos.y));
	sf::Vector2f higherPos(std::max(throwerCharPos.x, partnerCharPos.x), std::max(throwerCharPos.y, partnerCharPos.y));
	sf::Vector2f deltaCharPos = higherPos - lowerPos;*/

	bool firstWasPos;

	for (int idx = 0; idx < 2; idx++)
	{
		auto throwerPos = m_playerMngr->getCharacter(idx + 1)->getPosition();
		auto partnerPos = m_playerMngr->getCharacter((idx == 1 ? 1 : 2))->getPosition();

		sf::Vector2f lowerPos(std::min(throwerPos.x, partnerPos.x), std::min(throwerPos.y, partnerPos.y));
		sf::Vector2f higherPos(std::max(throwerPos.x, partnerPos.x), std::max(throwerPos.y, partnerPos.y));
		sf::Vector2f deltaCharPos = higherPos - lowerPos;
		
		auto pathData = &state.boomerangPath[idx];
		pathData->modifier.x = throwerPos.x < partnerPos.x ? 1 : -1;
		
		if (idx == 0) { pathData->modifier.y = throwerPos.y < partnerPos.y ? 1 : -1; firstWasPos = pathData->modifier.y != -1; }
		else pathData->modifier.y = firstWasPos ? -1 : 1;

		pathData->radius.x = deltaCharPos.x / 2;
		//pathData->radius = { deltaCharPos.x / 2, 200.f + m_playerMngr->getState()->boomerangPath[idx].radius.y/*600.f * m_boomerangArc[idx].currentStrength*/ };
		pathData->startingPoint = throwerPos + sf::Vector2f{ deltaCharPos.x / 2 * pathData->modifier.x, 0.f };

		std::vector<sf::Vector2f> points;
		for (int ang = arcStartAngle; ang < arcEndAngle; ang += 5)
		{
			auto pos = pathData->startingPoint;
			pos.x += std::cos(ang * M_PI / 180.0f) * pathData->radius.x * pathData->modifier.x;
			pos.y -= std::sin(ang * M_PI / 180.0f) * pathData->radius.y * pathData->modifier.y;

			points.push_back(pos);
		}

		std::vector<sf::Vertex> line;
		for (int i = 0; i < points.size(); i++)
		{
			sf::Color colors[STRENGTH_COLORS_COUNT]{ sf::Color::Red, sf::Color::Red, sf::Color::Red };
			int idx = (int)(i / (points.size() / STRENGTH_COLORS_COUNT));
			auto col = colors[idx];

			line.push_back(sf::Vertex(points[i], col));
		}

		m_boomerangArc[idx].vertices = line;
		m_boomerangArc[idx].count = line.size();
	
		auto pos = pathData->startingPoint - sf::Vector2f(30.0f, 30.0f);
		pos.x += pathData->radius.x * pathData->modifier.x;
		m_partnerTarget.setPosition(pos);
	}

	m_partnerTarget.setFillColor(sf::Color(0, 0, 0, 0));
	m_partnerTarget.setOutlineColor(sf::Color::Red);
	m_partnerTarget.setOutlineThickness(4.0f);
	m_partnerTarget.setRadius(30.0f);
}

void HUD::recalcStrengthBar(sf::Vector2f startBarPos, sf::Vector2f endBarPos, float percentage)
{
	float halfBarWidth = 10.0f;

	m_strengthBarOutline.setFillColor(sf::Color(0, 0, 0, 0));
	m_strengthBarOutline.setOutlineColor(sf::Color::White);
	m_strengthBarOutline.setOutlineThickness(4.0f);

	sf::Vector2f size(halfBarWidth * 2, startBarPos.y - endBarPos.y);
	m_strengthBarOutline.setPosition(endBarPos.x - halfBarWidth, endBarPos.y);
	m_strengthBarOutline.setSize(size);


	sf::Vector2f deltaBarPos = startBarPos - endBarPos;
	endBarPos += deltaBarPos * percentage;
	deltaBarPos *= percentage;

	m_strengthBarInner[0].position = endBarPos;
	m_strengthBarInner[0].position.x -= halfBarWidth;
	m_strengthBarInner[0].color = sf::Color::Red;

	m_strengthBarInner[1].position = endBarPos;
	m_strengthBarInner[1].position.x += halfBarWidth;
	m_strengthBarInner[1].color = sf::Color::Red;

	m_strengthBarInner[2].position = endBarPos + deltaBarPos / 2.0f;
	m_strengthBarInner[2].position.x -= halfBarWidth;
	m_strengthBarInner[2].color = sf::Color(255, 99, 0);

	m_strengthBarInner[3].position = endBarPos + deltaBarPos / 2.0f;
	m_strengthBarInner[3].position.x += halfBarWidth;
	m_strengthBarInner[3].color = sf::Color(255, 99, 0);

	m_strengthBarInner[4].position = startBarPos;
	m_strengthBarInner[4].position.x -= halfBarWidth;
	m_strengthBarInner[4].color = sf::Color::Yellow;

	m_strengthBarInner[5].position = startBarPos;
	m_strengthBarInner[5].position.x += halfBarWidth;
	m_strengthBarInner[5].color = sf::Color::Yellow;
}

void HUD::createScoreText(int points, sf::Vector2f pos)
{
	ScoreText* newST = new ScoreText;
	m_scoreTxts.push_back(newST);
	newST->ttl = 1.f;

	auto st = m_scoreTxts.begin();
	while (st != m_scoreTxts.end())
	{
		if ((*st)->ttl < 0)
		{
			(*st)->text.setFont(m_xScore_Font);
			(*st)->text.setCharacterSize(60);
			(*st)->text.setStyle(sf::Text::Bold);
			(*st)->text.setString(std::to_string(points) + "p");
			(*st)->text.setPosition(pos);
			(*st)->ttl = 1.f;
			m_scoreTxts.erase(m_scoreTxts.end() - 1);
			delete newST;
			break;
		}
		else 
		{
			newST->text.setFont(m_xScore_Font);
			newST->text.setCharacterSize(60);
			newST->text.setStyle(sf::Text::Bold);
			newST->text.setString(std::to_string(points) + "p");
			newST->text.setPosition(pos);
			newST->ttl = 1.f;
			break;
		}
	}
}

void HUD::addPowerProgress(int value)
{
	m_batProgress += value;
	m_boomProgress += value * 2;

	if (m_batProgress >= 100)
		m_playerMngr->getState()->hasBaseballKit = true;

	if (m_boomProgress >= 100)
		m_playerMngr->getState()->hasBoomerang = true;
}

void HUD::onBaseBallUsed() { m_batProgress = 0; }

void HUD::onBoomerangUsed() { m_boomProgress = 0; }
