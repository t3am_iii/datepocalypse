#include "LoadingScene.h"
#include "TextureCache.h"
#include "AudioManager.h"
#include "Global.h"
#include "SFML\Graphics\RenderWindow.hpp"
#include <string>


LoadingScene::LoadingScene(TextureCache* texCache, AudioManager* audioMngr) 
	:m_thread(&LoadingScene::loadAssets, this)
	, m_texCache(texCache)
	, m_audioMngr(audioMngr)
{ 
	//loadingBarTex.loadFromFile("../assets/textures/heart400x400.png");
	loadingBackgroundTex.loadFromFile("../assets/textures/loading_screen.png");
	m_font.loadFromFile("../assets/fonts/NuevaStd-Cond.otf");
}

LoadingScene::~LoadingScene() { }

void LoadingScene::loadAssets()
{
	// load audio
	int audAss = 5;
	m_audioMngr->loadSounds("../assets/sounds/baseballbat.ogg");
	m_audioProgress += audAss;
	m_audioMngr->loadSounds("../assets/sounds/boomerang.ogg");
	m_audioProgress += audAss;

	m_audioMngr->loadSounds("../assets/sounds/crows.ogg");
	m_audioProgress += audAss;
	m_audioMngr->loadSounds("../assets/sounds/enemy_death.ogg");
	m_audioProgress += audAss;
	m_audioMngr->loadSounds("../assets/sounds/enemy_shot.ogg");
	m_audioProgress += audAss;
	m_audioMngr->loadSounds("../assets/sounds/enemyshield_hit.ogg");
	m_audioProgress += audAss;

	m_audioMngr->loadSounds("../assets/sounds/tsunami.ogg");
	m_audioProgress += audAss;
	m_audioMngr->loadSounds("../assets/sounds/tsunami_warning.ogg");
	m_audioProgress += audAss;
	m_audioMngr->loadSounds("../assets/sounds/tornado.ogg");
	m_audioProgress += audAss;

	m_audioMngr->loadSounds("../assets/sounds/female_hit.ogg");
	m_audioProgress += audAss;
	m_audioMngr->loadSounds("../assets/sounds/male_hit.ogg");
	m_audioProgress += audAss;
	m_audioMngr->loadSounds("../assets/sounds/player_shot.ogg");
	m_audioProgress += audAss;
	m_audioMngr->loadSounds("../assets/sounds/playershield_hit.ogg");
	m_audioProgress += audAss;
	m_audioMngr->loadSounds("../assets/sounds/playershield_start.ogg");
	m_audioProgress += audAss;

	//m_audioMngr->loadSounds("../assets/sounds/menu_button1.ogg");
	m_audioProgress += audAss;
	//m_audioMngr->loadSounds("../assets/sounds/menu_button2.ogg");
	m_audioProgress = 100;


	// load textures
	int ass = 2;
	//Enemies
	m_texCache->loadTexture("../assets/textures/spritesheets/mutatedhuman_spritesheet.png");
	m_texProgress += ass;
	m_texCache->loadTexture("../assets/textures/spritesheets/mutatedcrow_spritesheet.png");
	m_texProgress += ass;
	m_texCache->loadTexture("../assets/textures/spritesheets/mutatedriotpolice_spritesheet.png");
	m_texProgress += ass;
	m_texCache->loadTexture("../assets/textures/spritesheets/enemydeath_spritesheet.png");
	m_texProgress += ass;
	m_texCache->loadTexture("../assets/textures/spritesheets/crowdeath_spritesheet.png");
	m_texProgress += ass;
	m_texCache->loadTexture("../assets/textures/shadow.png");
	
	// Disasters / Misc.
	m_texCache->loadTexture("../assets/textures/spritesheets/tsunami_spritesheet.png");
	m_texProgress += ass;
	m_texCache->loadTexture("../assets/textures/alert.png");
	m_texProgress += ass;
	m_texCache->loadTexture("../assets/textures/spritesheets/tornado_spritesheet.png");
	m_texProgress += ass;
	m_texCache->loadTexture("../assets/textures/spritesheets/projectile_spritesheet.png");
	m_texProgress += ass;
	m_texCache->loadTexture("../assets/textures/spritesheets/projectileimpact_spritesheet.png");
	m_texProgress += ass;
	m_texCache->loadTexture("../assets/textures/hud/colorpallette.png");
	m_texProgress += ass;

	
	
	// Avatars
	m_texCache->loadTexture("../assets/textures/spritesheets/male_spritesheet.png");
	m_texProgress += ass;
	m_texCache->loadTexture("../assets/textures/spritesheets/female_spritesheet.png");
	m_texProgress += ass;

	// Level Backgrounds
	m_texCache->loadTexture("../assets/textures/test_bg5.png");
	m_texProgress += ass;
	m_texCache->loadTexture("../assets/textures/end_level.png");
	m_texProgress += ass;


	// HUD
	m_texCache->loadTexture("../assets/textures/hud/health_spritesheet.png");
	m_texProgress += ass;
	m_texCache->loadTexture("../assets/textures/hud/hudshield_spritesheet.png");
	m_texProgress += ass;
	m_texCache->loadTexture("../assets/textures/hud/hud_sheet_placeholder.png");
	m_texProgress += ass;
	m_texCache->loadTexture("../assets/textures/hud/hud_bottom.png");
	m_texProgress += ass;
	m_texCache->loadTexture("../assets/textures/hud/hud_top.png");
	m_texProgress += ass;
	m_texCache->loadTexture("../assets/textures/hud/tutorial.png");
	m_texProgress += ass;
	m_texCache->loadTexture("../assets/textures/hud/eq.png");
	m_texProgress = 100;
	m_texCache->loadTexture("../assets/textures/hud/portraits_spritesheet.png");
	m_texProgress = 100;
	m_texCache->loadTexture("../assets/textures/pausemenu.png");
	m_texProgress = 100;
	m_texCache->loadTexture("../assets/textures/spritesheets/end.png");
	m_texProgress = 100;

}

void LoadingScene::onEnter(const SystemInfo&)
{
	loadingBackground.setTexture(loadingBackgroundTex);
	loadingBackground.setPosition(0, 0);

	m_loadingText.setFont(m_font);
	m_loadingText.setColor(sf::Color::Black);
	m_loadingText.setCharacterSize(50);
	m_loadingText.setPosition(900, 900);
	

	//m_texSprite.setTexture(loadingBarTex);
	m_texSprite.setTextureRect(sf::IntRect(0, 0, 0, 0));
	m_texSprite.setPosition(SCREEN_WIDTH / 2 - 600, SCREEN_HEIGHT / 2 - 200);
	//m_texSprite.setRotation(180);

	//m_audSprite.setTexture(loadingBarTex);
	m_audSprite.setPosition(SCREEN_WIDTH / 2 + 200, SCREEN_HEIGHT / 2 - 200);
	m_audSprite.setColor(sf::Color(100, 255, 100, 255));
	//m_audSprite.setRotation(180);

	m_toggle = false;
	m_isReady = false;
	m_texProgress = 0;
	m_audioProgress = 0;
	m_opacityValue = 255;
	
	//loadAssets();
	m_thread.launch();
}

void LoadingScene::onFadeIn() { }

void LoadingScene::onUpdate(float, const SystemInfo&)
{
	if (m_texProgress < 100)
		m_loadingText.setString("Loading.. " + std::to_string(m_texProgress) + " %");
	else
	{
		m_loadingText.setString("Press SPACE to start");
		m_loadingText.setColor(sf::Color(0, 0, 0, m_opacityValue));
		m_loadingText.setPosition(850, 900);
		if(m_toggle)
		{ 
			if (m_opacityValue >= 255)
				m_toggle = false;
			else
				m_opacityValue += 5;
		}
		else
		{
			if (m_opacityValue <= 0)
				m_toggle = true;
			else
				m_opacityValue -= 5;
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Space))
			m_isReady = true;

	}
	

	//m_texSprite.setScale(m_texProgress /10, 1);
	m_texSprite.setTextureRect(sf::IntRect(0, 0, 400, m_texProgress * 4));

	//m_audSprite.setScale(m_audioProgress / 10, 1);
	m_audSprite.setTextureRect(sf::IntRect(0, 0, 400, m_audioProgress * 4));
}

bool LoadingScene::nextScene(int& nextScene) const
{
	nextScene = GAME_SCENE;
	return m_isReady;
}

//bool nextScene(int&) const { return false; }

bool LoadingScene::shouldClose() const { return false; }

void LoadingScene::onRender(sf::RenderWindow& window)
{
	window.draw(loadingBackground);

	window.draw(m_loadingText);
	//if(m_texProgress > 0)
	//	window.draw(m_texSprite);

	//if (m_audioProgress > 0)
	//	window.draw(m_audSprite);
}

void LoadingScene::onFadeOut() { }

void LoadingScene::onExit(const SystemInfo&)
{
	// clear stuff
}