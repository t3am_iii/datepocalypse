#pragma once

#include <vector>
#include <map>
#include <SFML\Graphics\Texture.hpp>

class TextureCache
{
private:
	std::map<std::string, sf::Texture*> m_textures;

public:
	TextureCache();

	~TextureCache();

	void loadTexture(std::string);

	sf::Texture* getTexture(std::string);

	void unloadTextures();
};