#pragma once

#include <Engine\GameObjectCreationKit.h>
#include "AnimatedSprite.h"
#include <vector>

#include <SFML\Graphics\Texture.hpp>

namespace dpEngine { struct SystemInfo; }
using namespace dpEngine;

class TextureCache;
class PlayerProjectile;
class EnemyProjectile;

class ProjectileManager
{
public:
	ProjectileManager(const dpEngine::SystemInfo& systemInfo, TextureCache*);

	~ProjectileManager();

	PlayerProjectile* CreatePlayerProjectile(float p_fX, float p_fY, float p_fSpeed, sf::Vector2f p_Dir);
	
	EnemyProjectile* CreateEnemyProjectile(float p_fX, float p_fY);

	void Update(float p_fDeltaTime);

	void Render(sf::RenderTarget& target);
private:
	GameObjectCreationKit kit;
	TextureCache* m_texCache;
	std::vector<PlayerProjectile*> m_apxPlayerProjectiles;
	std::vector<EnemyProjectile*> m_apxEnemyProjectiles;

	sf::Texture m_electricTrailTexture;

};