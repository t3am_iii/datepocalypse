#include "ScoreManager.h"

ScoreManager::ScoreManager()
{
	m_iScore = 0;
	m_iMultiplier = 1;
}

ScoreManager::~ScoreManager()
{
	//
}

void ScoreManager::resetScore()
{
	m_iScore = 0;
	m_iMultiplier = 1;
}

void ScoreManager::update(float p_fDeltaTime)
{
	//
	//if (m_iTimer_mpDecay <= 0)
	//{
	//	m_iTimer_mpDecay = 100;
	//	
	//	if (m_iMultiplier > 1)	
	//		m_iMultiplier -= 1;
	//}
	//else
	//	m_iTimer_mpDecay--;
}

int ScoreManager::getScore() { return m_iScore; }

void ScoreManager::addScore(int value, sf::Vector2f pos) 
{ 
	m_iScore += (value * getMultiplier());
}

int ScoreManager::getMultiplier() { return m_iMultiplier; }

void ScoreManager::addMultiplier(int value) { m_iMultiplier += value; }

void ScoreManager::resetMultiplier() { m_iMultiplier = 1; }