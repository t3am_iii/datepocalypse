#pragma once

#include "../ScoreEntry.h"
#include "../AnimatedSprite.h"
#include <Engine\SceneSystem\BaseScene.h>

#include <SFML\Graphics\Texture.hpp>
#include <SFML\Graphics\Sprite.hpp>
#include <SFML\Graphics\Text.hpp>
#include <SFML\Graphics\Font.hpp>

using namespace dpEngine;

class ScoreManager;
class TextureCache;
class AudioManager;

class VictoryScene : public SceneSystem::BaseScene
{
private:
	Animation m_endAnim;

	AnimatedSprite m_endSprite;

	sf::Sprite m_credits;

	float m_credX;

	sf::Font font;

	std::vector<ScoreEntry> m_entries;
	std::string Name;
	sf::Text Entry1;


	bool m_closeGame;
	bool m_return;

	ScoreManager* m_scoreMngr;

	TextureCache* m_texCache;

	AudioManager* m_audioMngr;

public:

	VictoryScene(ScoreManager*, TextureCache*, AudioManager*);

	void onEnter(const SystemInfo&);

	void onFadeIn() { }

	void onUpdate(float, const SystemInfo&);

	bool nextScene(int&) const;

	bool shouldClose() const;

	void onRender(sf::RenderWindow&);

	void onFadeOut() { }

	void onExit(const SystemInfo&);

};
