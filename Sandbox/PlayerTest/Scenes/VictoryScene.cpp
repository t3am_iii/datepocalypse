#include "VictoryScene.h"
#include <fstream>
#include <limits>
#include <iostream>
#include <sstream>
#include <string>
#include <SFML/Graphics.hpp>
#include <Engine\SystemInfo.h>
#include <Engine\InputSystem\inputManager.h>
#include "../HUD.h"
#include "../Global.h"
#include "../ScoreManager.h"
#include "../TextureCache.h"
#include "../AudioManager.h"

VictoryScene::VictoryScene(ScoreManager* scoreMngr, TextureCache* texCache, AudioManager* audioMngr)
	: m_scoreMngr(scoreMngr)
	, m_texCache(texCache)
	, m_audioMngr(audioMngr)
{
	font.loadFromFile("../assets/fonts/NuevaStd-Cond.otf");
}

void VictoryScene::onEnter(const SystemInfo& sysinfo)
{
	//m_gameOverSpr.setTexture(*m_texCache->getTexture("../assets/textures/victoryscreen.png"));
	m_closeGame = false;
	m_return = false;

	m_audioMngr->playMusic("../assets/audio/Steven-Universe-8-bit-Ring.ogg", true, 100);

	m_texCache->loadTexture("../assets/textures/spritesheets/end.png");
	m_endAnim.setSpriteSheet(*m_texCache->getTexture("../assets/textures/spritesheets/end.png"));
	m_endAnim.addFrame(sf::IntRect(  0,    0, 950, 535));
	m_endAnim.addFrame(sf::IntRect(950,    0, 950, 535));
	m_endAnim.addFrame(sf::IntRect(  0,  535, 950, 535));
	m_endAnim.addFrame(sf::IntRect(950,  535, 950, 535));
	m_endAnim.addFrame(sf::IntRect(  0, 1070, 950, 535));
	m_endAnim.addFrame(sf::IntRect(950, 1070, 950, 535));
	m_endSprite = AnimatedSprite(800);
	m_endSprite.play(m_endAnim);
	m_endSprite.setLooped(false);
	m_endSprite.setPosition(0, 0);
	m_endSprite.setScale(2,2);

	m_credX = -886;
	m_texCache->loadTexture("../assets/textures/credits.png");
	m_credits.setTexture(*m_texCache->getTexture("../assets/textures/credits.png"));
	
}

void VictoryScene::onUpdate(float deltaTime, const SystemInfo& sysInfo)
{
	m_endSprite.update(deltaTime * 1000);
	
	if (m_credX < -2)
		m_credX += deltaTime * 100;

	m_credits.setPosition(m_credX, 0);
	Entry1.setPosition(m_credX + 40, 870);

	if (sysInfo.inputMngr->wasKeyPressed(sf::Keyboard::Key::BackSpace))
	{
		if (Name.size() > 0)
			Name.pop_back();
	}
	else if (sysInfo.inputMngr->wasKeyPressed(sf::Keyboard::Key::Return))
	{
		std::fstream filein;
		std::string line = "";
		
		filein.open("../assets/example.txt");
		m_entries.clear();
		while (std::getline(filein, line))
		{
			if (line.empty())
				continue;
			ScoreEntry e;
			int pos = line.find(' ', 0);
			if (pos < 1)
				continue;

			e.name = line.substr(0, pos);
			e.score = std::stoi(line.substr(pos + 1, line.length() - pos));
			m_entries.push_back(e);
		}
		
		bool added = false;
		for (int i = 0; i < m_entries.size(); i++)
		{
			if (m_scoreMngr->getScore() > m_entries[i].score)
			{
				ScoreEntry e;
				e.name = Name;
				e.score = m_scoreMngr->getScore();
				m_entries.insert(m_entries.begin() + i, e);
				added = true;
				break;
			}
		}

		if (!added && m_entries.size() < 10)
		{
			ScoreEntry e;
			e.name = Name;
			e.score = m_scoreMngr->getScore();
			m_entries.push_back(e);
		}
		filein.close();


		std::ofstream out;
		out.open("../assets/example.txt", std::ios_base::trunc);
		for (auto it : m_entries)
		{
			out << it.name << " " << it.score << std::endl;
		}
		out.close();


	}
	else if (sysInfo.inputMngr->wasKeyPressed())
	{
		Name += sysInfo.inputMngr->getLatestKeyChar();
	}

	Entry1.setFont(font);
	Entry1.setCharacterSize(60);
	Entry1.setString("Name:\t" + Name);
	Entry1.setColor(sf::Color(255, 0, 0, 255));
	//Entry1.setPosition(960, 100);

	if (sysInfo.inputMngr->wasKeyPressed(sf::Keyboard::Key::Return))
		m_return = true;
	if (sysInfo.inputMngr->wasKeyPressed(sf::Keyboard::Key::Escape))
		m_closeGame = true;
}

bool VictoryScene::nextScene(int& nextScene) const
{
	if (m_return == true)
	{
		 nextScene = MENU_SCENE;
		 return true;
	}	
	return false;
}

bool VictoryScene::shouldClose() const { return m_closeGame; }

void VictoryScene::onRender(sf::RenderWindow& window)
{
	window.draw(m_endSprite);
	window.draw(m_credits);
	window.draw(Entry1);
}

void VictoryScene::onExit(const SystemInfo&)
{
	//
}
