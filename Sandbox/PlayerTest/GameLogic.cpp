#include "GameLogic.h"
#include "PlayerProjectile.h"
#include "Controllers\PlayerController.h"
#include "PlayerCharacter.h"
#include "PlayerShield.h"
#include "Boomerang.h"
#include "GameObjects\Baseball.h"
#include "EnemyProjectile.h"
#include "MutatedHuman.h"
#include "MutatedCrow.h"
#include "MutatedRiotPolice.h"
#include "GameObjects\Enemies\BossCharacter.h"
#include "NaturalDisasters\HurricaneAgent.h"
#include "Tsunami.h"
#include "EnemyShield.h"
#include "ScoreManager.h"
#include "AudioManager.h"
#include "HUD.h"

#include <Engine\ViewSystem\ViewManager.h>
#include <Engine\CollisionSystem\Body.h>
#include <Engine\CollisionSystem\Data\ContactInfo.h>

using namespace dpEngine::CollisionSystem;

GameLogic::GameLogic(ScoreManager* scoreMngr, AudioManager* audioMngr, HUD* hud, ViewManager* viewMngr)
	: m_scoreMngr(scoreMngr)
	, m_audioMngr(audioMngr)
	, m_hud(hud)
	, m_viewMngr(viewMngr)
{
	dmg = 1;

	m_crowValue = 20;
	m_humanValue = 10;
	m_riotPoliceValue = 30;

	m_viewMngr->addModifier(this);
}

GameLogic::~GameLogic()
{
	m_viewMngr->removeModifier(this);
}

void GameLogic::onContact(const dpEngine::ContactInfo& info)
{
	BEGIN_HANDLER_LIST()
	
	// Player vs Enemy
	ADD_CONTACT_HANDLER(ENTITY_MUTATED_RIOT_POLICE, ENTITY_PLAYER,
		handleMutatedRiotPolice, MutatedRiotPolice, PlayerCharacter)

	// Player projectile vs enemies
	ADD_CONTACT_HANDLER(ENTITY_PLAYER_PROJECTILE, ENTITY_MUTATED_HUMAN,
		handlePlayerProjectile, PlayerProjectile, MutatedHuman)
	ADD_CONTACT_HANDLER(ENTITY_PLAYER_PROJECTILE, ENTITY_MUTATED_CROW,
		handlePlayerProjectile, PlayerProjectile, MutatedCrow)
	ADD_CONTACT_HANDLER(ENTITY_PLAYER_PROJECTILE, ENTITY_MUTATED_RIOT_POLICE,
		handlePlayerProjectile, PlayerProjectile, MutatedRiotPolice)
	ADD_CONTACT_HANDLER(ENTITY_PLAYER_PROJECTILE, ENTITY_BOSS,
		handlePlayerProjectile, PlayerProjectile, BossCharacter)
	ADD_CONTACT_HANDLER(ENTITY_PLAYER_PROJECTILE, ENTITY_ENEMY_SHIELD,
		handleEnemyShield, PlayerProjectile, EnemyShield)

	// Enemy projectile vs player
	ADD_CONTACT_HANDLER(ENTITY_ENEMY_PROJECTILE, ENTITY_PLAYER,
		handleEnemyProjectile, EnemyProjectile, PlayerCharacter)
	ADD_CONTACT_HANDLER(ENTITY_ENEMY_PROJECTILE, ENTITY_PLAYER_SHIELD,
		handleEnemyProjectile, EnemyProjectile, PlayerShield)

	// Power-ups vs enemies
	ADD_CONTACT_HANDLER(ENTITY_BOOMERANG, ENTITY_MUTATED_HUMAN,
		handleBoomerang, Boomerang, MutatedHuman)
	ADD_CONTACT_HANDLER(ENTITY_BOOMERANG, ENTITY_MUTATED_CROW,
		handleBoomerang, Boomerang, MutatedCrow)
	ADD_CONTACT_HANDLER(ENTITY_BOOMERANG, ENTITY_MUTATED_RIOT_POLICE,
		handleBoomerang, Boomerang, MutatedRiotPolice)
	ADD_CONTACT_HANDLER(ENTITY_BOOMERANG, ENTITY_BOSS,
		handleBoomerang, Boomerang, BossCharacter)

	ADD_CONTACT_HANDLER(ENTITY_BASEBALL_BALL, ENTITY_MUTATED_HUMAN,
		handleBaseball, Baseball, MutatedHuman)
	ADD_CONTACT_HANDLER(ENTITY_BASEBALL_BALL, ENTITY_MUTATED_CROW,
		handleBaseball, Baseball, MutatedCrow)
	ADD_CONTACT_HANDLER(ENTITY_BASEBALL_BALL, ENTITY_MUTATED_RIOT_POLICE,
		handleBaseball, Baseball, MutatedRiotPolice)
	ADD_CONTACT_HANDLER(ENTITY_BASEBALL_BALL, ENTITY_BOSS,
		handleBaseball, Baseball, BossCharacter)

	ADD_CONTACT_HANDLER(ENTITY_HURRICANE, ENTITY_PLAYER,
		handleHurricane, HurricaneAgent, PlayerCharacter)
	ADD_CONTACT_HANDLER(ENTITY_TSUNAMI, ENTITY_PLAYER,
		handleTsunami, Tsunami, PlayerCharacter)
	ADD_CONTACT_HANDLER(ENTITY_TSUNAMI, ENTITY_MUTATED_HUMAN,
		handleTsunami, Tsunami, MutatedHuman)

	// Player vs Player
	ADD_CONTACT_HANDLER_SINGLE(ENTITY_PLAYER, handlePlayers, PlayerCharacter)

	ADD_CONTACT_HANDLER(ENTITY_PLAYER_PROJECTILE, ENTITY_ENEMY_SHIELD,
		handleEnemyShield, PlayerProjectile, EnemyShield)

	ADD_CONTACT_HANDLER(ENTITY_MUTATED_CROW, ENTITY_PLAYER,
		handleMutatedCrow, MutatedCrow, PlayerCharacter)

	ADD_CONTACT_HANDLER(ENTITY_MUTATED_CROW, ENTITY_PLAYER_SHIELD,
		handleMutatedCrow, MutatedCrow, PlayerShield)

	END_HANDLER_LIST()
}


void GameLogic::handlePlayerProjectile(PlayerProjectile* projectile, MutatedHuman* mutated)
{
	if (mutated->isDead())
		return;

	int newHP = mutated->GetHealthPoints() - dmg;
	if (newHP <= 0)
	{
		mutated->setDead(true);
		m_scoreMngr->addScore(m_humanValue, mutated->getPosition());
		int mp = m_scoreMngr->getMultiplier();
		m_hud->createScoreText(mp * m_humanValue, mutated->getPosition());
	}
	else
	{
		mutated->SetHealthPoints(newHP);
		// Show visual
		// Play sfx
	}

	m_hud->addPowerProgress(5);
	m_scoreMngr->addMultiplier(1);
	projectile->setImpact(true);
	//projectile->setIsActive(false);
	//onProjectileHit();
}

void GameLogic::handlePlayerProjectile(PlayerProjectile* projectile, MutatedCrow* mutated)
{
	int newHP = mutated->getHealthPoints() - 1;
	if (newHP <= 0)
	{
		mutated->setDead(true);
		m_scoreMngr->addScore(m_crowValue, mutated->getPosition());
		int mp = m_scoreMngr->getMultiplier();
		m_hud->createScoreText(mp * m_crowValue, mutated->getPosition());
		// Show visual
		// Play sfx
	}
	else
	{
		mutated->setHealthPoints(newHP);
		// Show visual
		// Play sfx
	}

	m_hud->addPowerProgress(5);
	m_scoreMngr->addMultiplier(1);
	projectile->setImpact(true);
}

void GameLogic::handlePlayerProjectile(PlayerProjectile* projectile, MutatedRiotPolice* mutated)
{
	int newHP = mutated->getHealthPoints() - 1;
	if (newHP <= 0)
	{
		mutated->setDead(true);
		m_scoreMngr->addScore(m_riotPoliceValue, mutated->getPosition());
		int mp = m_scoreMngr->getMultiplier();
		m_hud->createScoreText(mp * m_riotPoliceValue, mutated->getPosition());
		// Show visual
		// Play sfx
	}
	else
	{
		mutated->setHealthPoints(newHP);
	}

	m_hud->addPowerProgress(5);
	m_scoreMngr->addMultiplier(1);
	projectile->setImpact(true);
}

void GameLogic::handlePlayerProjectile(PlayerProjectile* projectile, BossCharacter* boss)
{
	int newHP = boss->getHealthPoints() - dmg;
	if (newHP < 1)
	{
		boss->setActive(false);
		m_scoreMngr->addScore(500, boss->getPosition());
		// Show visual
		// Play sfx
	}
	else
	{
		// Show visual
		// Play sfx
	}

	boss->setHealthPoints(newHP);
	projectile->setIsActive(false);
}

void GameLogic::handleEnemyProjectile(EnemyProjectile* projectile, PlayerCharacter* player)
{
	dealDamage(player, 1);

	//player->setIshit(true);
	projectile->SetIsActive(false);
	
	if (player->getController()->getId() == 1)
		m_audioMngr->playSound("../assets/sounds/male_hit.ogg", true, 100);
	else
		m_audioMngr->playSound("../assets/sounds/female_hit.ogg", true, 100);
}

void GameLogic::handleEnemyProjectile(EnemyProjectile* projectile, PlayerShield* shield)
{
	dealDamage(shield, 10);
	projectile->SetIsActive(false);
}


void GameLogic::handleBoomerang(Boomerang* boomerang, MutatedHuman* mutated)
{
	int newHP = mutated->GetHealthPoints() - 1;
	if (newHP <= 0)
	{
		int mp = m_scoreMngr->getMultiplier();
		m_hud->createScoreText(mp * m_humanValue, mutated->getPosition());
		m_scoreMngr->addScore(m_humanValue, mutated->getPosition());
		mutated->setPosition(-300.0f, 0.0f);
		mutated->setDead(true);
	}
	else
		mutated->SetHealthPoints(newHP);


	m_scoreMngr->addMultiplier(1);
	m_hud->addPowerProgress(5);
	//boomerang->setActive(false);
	//boomerang->setPosition(3000.0f, 0.0f);
}

void GameLogic::handleBoomerang(Boomerang* boomerang, MutatedCrow* mutated)
{
	int newHP = mutated->getHealthPoints() - 1;
	if (newHP <= 0)
	{
		int mp = m_scoreMngr->getMultiplier();
		m_hud->createScoreText(mp * m_crowValue, mutated->getPosition());
		m_scoreMngr->addScore(m_crowValue, mutated->getPosition());
		mutated->setPosition(-300.0f, 0.0f);
		mutated->setDead(true);
	}
	else
		mutated->setHealthPoints(newHP);

	m_scoreMngr->addMultiplier(1);
	m_hud->addPowerProgress(5);
}

void GameLogic::handleBoomerang(Boomerang* boomerang, MutatedRiotPolice* mutated)
{
	int newHP = mutated->getHealthPoints() - 1;
	if (newHP <= 0)
	{
		int mp = m_scoreMngr->getMultiplier();
		m_hud->createScoreText(mp * m_riotPoliceValue, mutated->getPosition());
		m_scoreMngr->addScore(m_riotPoliceValue, mutated->getPosition());
		mutated->setPosition(-300.0f, 0.0f);
		mutated->setDead(true);
	}
	else
		mutated->setHealthPoints(newHP);
	
	m_scoreMngr->addMultiplier(1);
	m_hud->addPowerProgress(5);
}

void GameLogic::handleBoomerang(Boomerang* boomerang, BossCharacter* boss)
{
	int newHP = boss->getHealthPoints() - dmg;
	if (newHP < 1)
	{
		boss->setActive(false);
		m_scoreMngr->addScore(500, boss->getPosition());
		// Show visual
		// Play sfx
	}
	else
	{
		// Show visual
		// Play sfx
	}

	boss->setHealthPoints(newHP);
}


void GameLogic::handleBaseball(Baseball* baseball, MutatedHuman* mutated)
{
	if (baseball->getId() == 1)
		return;

	int newHP = mutated->GetHealthPoints() - 1;
	if (newHP <= 0)
	{
		int mp = m_scoreMngr->getMultiplier();
		m_hud->createScoreText(mp * m_humanValue, mutated->getPosition());
		mutated->setDead(true);
	}	
	else
		mutated->SetHealthPoints(newHP);

	m_scoreMngr->addMultiplier(1);
	m_hud->addPowerProgress(5);
}

void GameLogic::handleBaseball(Baseball* baseball, MutatedCrow* mutated)
{
	if (baseball->getId() == 1)
		return;

	int newHP = mutated->getHealthPoints() - 1;
	if (newHP <= 0)
	{
		int mp = m_scoreMngr->getMultiplier();
		m_hud->createScoreText(mp * m_crowValue, mutated->getPosition());
		mutated->setDead(true);
	}	
	else
		mutated->setHealthPoints(newHP);

	m_scoreMngr->addMultiplier(1);
	m_hud->addPowerProgress(5);
}

void GameLogic::handleBaseball(Baseball* baseball, MutatedRiotPolice* mutated)
{
	if (baseball->getId() == 1)
		return;

	int newHP = mutated->getHealthPoints() - 1;
	if (newHP <= 0)
	{
		int mp = m_scoreMngr->getMultiplier();
		m_hud->createScoreText(mp * m_riotPoliceValue, mutated->getPosition());
		mutated->setActive(false);
	}
		
	else
		mutated->setHealthPoints(newHP);

	m_scoreMngr->addMultiplier(1);
	m_hud->addPowerProgress(5);
}

void GameLogic::handleBaseball(Baseball* baseball, BossCharacter* boss)
{
	if (baseball->getId() == 1)
		return;

	int newHP = boss->getHealthPoints() - dmg;
	if (newHP < 1)
	{
		boss->setActive(false);
		m_scoreMngr->addScore(500, boss->getPosition());
		// Show visual
		// Play sfx
	}
	else
	{
		// Show visual
		// Play sfx
	}

	boss->setHealthPoints(newHP);
}


void GameLogic::handleHurricane(HurricaneAgent* hurricane, PlayerCharacter* player)
{
	if (player->isShielding())
		return;

	if (player->hasInvincibility())
		return;
	
	hurricane->addPlayer(player);
}


void GameLogic::handlePlayers(PlayerCharacter* lp, PlayerCharacter* rp)
{
	// Shield
	lp->setIsShielding(true);
	rp->setIsShielding(true);
}

void GameLogic::handleEnemyShield(PlayerProjectile* projectile, EnemyShield* shield)
{
	int newAP = shield->getArmourPoints() - 1;
	//if (newAP < 1)
	//{
	//	//shield->setActive(false);
	//	// Show visual
	//	// Play sfx
	//}
	//else
	//{
	//	//shield->setArmourPoints(newAP);
	//	// Show visual
	//	
	//}
	m_audioMngr->playSound("../assets/sounds/playershield_hit.ogg", true, 50);
	shield->setArmourPoints(newAP);

	m_hud->addPowerProgress(5);
	m_scoreMngr->addMultiplier(1);
	projectile->setImpact(true);
}

void GameLogic::handleMutatedCrow(MutatedCrow* crow, PlayerCharacter* player)
{
	if (crow->isDead())
		return;
	crow->setDead(true);

	dealDamage(player, 1);
}

void GameLogic::handleMutatedCrow(MutatedCrow* crow, PlayerShield* shield)
{
	crow->setDead(true);
	dealDamage(shield, 25);
}

void GameLogic::handleMutatedRiotPolice(MutatedRiotPolice* police, PlayerCharacter* player)
{
	police->isAttacking();

	int x = rand() % 10 + 1;
	if (x <= 5)
		x = -1;
	else
		x = 1;

	int y = rand() % 10 + 1;
	if (y <= 5)
		y = -1;
	else
		y = 1;

	player->addKnockBack(FMath::Normalize(sf::Vector2f(x, y)), 2.f);
	//dealDamage(player, dmg); // Todo: fr�ga alex, skadar f�r snabbt insta-d�dar on contact
}

void GameLogic::handleTsunami(Tsunami* tsunami, PlayerCharacter* player)
{
	if (player->hasInvincibility())
		return;
	
	dealDamage(player, 1);
}

void GameLogic::handleTsunami(Tsunami* tsunami, MutatedHuman* human)
{
	human->setDead(true);
}


void GameLogic::dealDamage(PlayerCharacter* player, int amount)
{
	if (player->hasInvincibility())
		return;

	int newHP = player->GetHealthPoints() - amount;
	if (newHP <= 0)
	{
		player->SetHealthPoints(0);
	}
	else
	{
		m_scoreMngr->resetMultiplier();
		player->SetHealthPoints(newHP);
		player->setIshit(true);
	}
}

void GameLogic::dealDamage(PlayerShield* shield, int amount)
{
	int newDur = shield->GetDuration() - amount;
	if (newDur <= 0)
	{
		shield->SetIsActive(false);
		shield->SetDuration(0);
	}
	else
		shield->SetDuration(newDur);

	m_audioMngr->playSound("../assets/sounds/playershield_hit.ogg", true, 50);

	m_screenShake.time = 1.5f;
	m_screenShake.hasReset = false;
}

void GameLogic::onProjectileHit(float minStr, float minTime)
{
	if (m_screenShake.strength < minStr)
		m_screenShake.strength = minStr;
	if (m_screenShake.time < minTime)
		m_screenShake.time = minTime;

	m_screenShake.hasReset = false;
}

void GameLogic::onViewUpdate(sf::View& view, float deltaTime)
{
	if (m_screenShake.time > 0.f)
	{
		m_screenShake.time -= deltaTime;

		auto pos = view.getCenter();
		auto screenShake = ((rand() % 3000 - 1500) / 300);
		view.setCenter(pos + sf::Vector2f(screenShake, -screenShake));
	}
	else if (!m_screenShake.hasReset)
	{
		m_viewMngr->resetCurrentView();
		m_screenShake.hasReset = true;
	}
}
