#pragma once

#include "IEntity.h"
#include "AnimatedSprite.h"

#include <Engine\Objects\Pawn.h>
#include <Engine\GameObjectCreationKit.h>
#include <Engine\CollisionSystem\CollisionManager.h>
#include <Engine\CollisionSystem\BodyDef.h>
#include <Engine\CollisionSystem\Body.h>
#include <Engine\CollisionSystem\Enums.h>
#include <Engine\CollisionSystem\Shapes\RectangleShape.h>
#include <Engine\CollisionSystem\Shapes\CircleShape.h>

#include <SFML\Graphics\Sprite.hpp>

struct PlayerState;
struct PlayerTrail;
class PlayerManager;
class PlayerController;

using namespace dpEngine;
using namespace dpEngine::CollisionSystem;

struct PlayerTrail
{
	float ttl;
	AnimatedSprite animSprite;
};

struct Particle
{
	sf::Sprite sprite;
	sf::Vector2f startPos;
	float ttl;
};

class PlayerCharacter : public IEntity, public Pawn
{
private:
	PlayerController* m_controller;

public:
	PlayerCharacter(sf::Texture* texture, sf::Texture* trailTexture, sf::Texture* shadowTexture, float p_fX, float p_fY, PlayerState*, PlayerManager*);
	
	~PlayerCharacter();
	
	void update(float p_fDeltaTime);

	void setAnimation(int p_fValue);

	void setFlyModifier(float, float);

	void setInvincibility(bool);

	const sf::Vector2f& getFlyModifier() const;

	bool hasInvincibility() const;

	void SetMoveSpeed(float p_fValue);

	int GetHealthPoints();
	void SetHealthPoints(int);
	
	void setDamageStrength(int);
	int m_hitEffect;

	bool getIsHit();
	void setIshit(bool);

	bool isAlive();
	// timer will be made <--------------

	bool isShielding();
	void setIsShielding(bool p_bValue);

	void addKnockBack(sf::Vector2f, float);

	void onNotifyControllerChange(PlayerController*);
	
	PlayerController* getController() const;
	
	EENTITYTYPE GetType();

	void onConstruction(const GameObjectCreationKit&);

	void onDestruction(const GameObjectCreationKit&);

private:
	PlayerState* m_playerState;
	PlayerManager* m_playerMngr;
	
	std::vector<Animation> m_animations;

	AnimatedSprite m_animatedSprite;
	
	Animation m_animTrail;

	std::vector<PlayerTrail> m_playerTrails;

	std::vector<Particle*> m_particles;
	sf::Texture pHeartText;
	sf::Texture pDotText;
	int pAmount;


	float m_ttl;
	float m_ttmove;
	int m_nextTrail;

	sf::Sprite m_spriteShadow;

	int m_prevAnimation;

	sf::Vector2f m_flyModifier;

	bool m_hasInvincibility;

	int m_iHealthPoints;
	int m_damageStrength;

	bool m_isAlive;
	bool m_isHit;
	float m_invincibleTimer;

	bool m_isShielding;

	struct
	{
		bool inState;
		float curStrenght;
		float maxStrength;
		sf::Vector2f dir;
	} m_knockBack;

	float m_fSpeed;
	float m_fMaxSpeed;

	
protected:
	void draw(sf::RenderTarget&, sf::RenderStates) const;

};