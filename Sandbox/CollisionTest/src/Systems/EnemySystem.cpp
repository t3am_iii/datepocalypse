#include <CollisionTest\Systems\EnemySystem.h>
#include <CollisionTest\Objects\ShieldedEnemy.h>

EnemySystem::EnemySystem(const dpEngine::GameObjectCreationKit& kit)
	: m_kit(kit)
{ /* EMPTY */ }

EnemySystem::~EnemySystem() { /* EMPTY */ }

ShieldedEnemy* EnemySystem::createShieldedEnemy()
{
	auto newEnemy = new ShieldedEnemy;
	m_shieldedEnemies.push_back(newEnemy);
	newEnemy->onConstruction(m_kit);
	return newEnemy;
}

void EnemySystem::destroyEnemy(ShieldedEnemy* enemy)
{
	auto it = std::find(m_shieldedEnemies.begin(), m_shieldedEnemies.end(), enemy);
	if (it != m_shieldedEnemies.end())
	{
		(*it)->onDestruction(m_kit);
		delete (*it);
		m_shieldedEnemies.erase(it);
	}
}

void EnemySystem::update(float deltaTime)
{
	std::vector<ShieldedEnemy*> m_shieldedEnemiesDestroyQue;
	for (auto enemy : m_shieldedEnemies)
	{
		if (!enemy->isActive())
		{
			m_shieldedEnemiesDestroyQue.push_back(enemy);
			continue;
		}

		enemy->update(deltaTime);
	}

	for (auto enemy : m_shieldedEnemiesDestroyQue)
		destroyEnemy(enemy);
}

void EnemySystem::render(sf::RenderTarget& target, sf::RenderStates states) const
{
	for (auto enemey : m_shieldedEnemies)
		target.draw(*enemey, states);
}
