#include <CollisionTest\States\OnGroundStates.h>
#include <CollisionTest\Controllers\PlayerController.h>
#include <CollisionTest\Objects\PlayerCharacter.h>
#include <CollisionTest\Objects\Boomerang.h>
#include <CollisionTest\PlayerState.h>
#include <CollisionTest\Enums.h>

#include <Engine\InputSystem\InputManager.h>
#include <Engine\Services\ServiceLocator.h>
#include <Engine\Services\IEntityService.h>

#define _USE_MATH_DEFINES
#include <math.h>

void OnGroundState::handleInput(PlayerController* playerCtrl, InputManager* inputMngr)
{
	//if (!m_canMove)
	//	return;
	
	m_velocity = { 0.0f, 0.0f };

	const float SPEED = 300.0f;

	if (inputMngr->isKeyDown(playerCtrl->getActionKey(EActionKey::kWalkUp)))
		m_velocity.y += -SPEED;
	if (inputMngr->isKeyDown(playerCtrl->getActionKey(EActionKey::kWalkLeft)))
		m_velocity.x += -SPEED;
	if (inputMngr->isKeyDown(playerCtrl->getActionKey(EActionKey::kWalkDown)))
		m_velocity.y += SPEED;
	if (inputMngr->isKeyDown(playerCtrl->getActionKey(EActionKey::kWalkRight)))
		m_velocity.x += SPEED;
}

void OnGroundState::update(PlayerController* playerCtrl, float deltaTime)
{
	//if (!m_canMove)
		playerCtrl->getCharacter()->addPosition(m_velocity.x * deltaTime, m_velocity.y * deltaTime);
}

void WalkingAvatarState::onEnter(PlayerController*)
{
	m_gotoBoomerangingState = false;
}

void WalkingAvatarState::handleInput(PlayerController* playerCtrl, InputManager* inputMngr)
{
	auto state = playerCtrl->getState();
	if (inputMngr->wasKeyPressed(playerCtrl->getActionKey(EActionKey::kBoomerang))
		&& state->hasBoomerang
		&& state->throwerId == playerCtrl->getId())
		m_gotoBoomerangingState = true;
	else
		OnGroundState::handleInput(playerCtrl, inputMngr);
}

void WalkingAvatarState::update(PlayerController* playerCtrl, float deltaTime)
{
	OnGroundState::update(playerCtrl, deltaTime);
}

bool WalkingAvatarState::shouldPushState(EAvatarState& state)
{
	if (m_gotoBoomerangingState)
		state = EAvatarState::kBoomeranging;

	return m_gotoBoomerangingState;
}

bool WalkingAvatarState::shouldPopState() { return false; }

void FiringAvatarState::handleInput(PlayerController* playerCtrl, InputManager* inputMngr)
{
	OnGroundState::handleInput(playerCtrl, inputMngr);
}

void FiringAvatarState::update(PlayerController* playerCtrl, float deltaTime)
{
	OnGroundState::update(playerCtrl, deltaTime);
}

void BoomerangingAvatarState::onEnter(PlayerController* playerCtrl)
{
	m_gotoWalkingState = false;

	playerCtrl->getState()->boomerangingState = EBoomerangingState::kChargingStrengthBar;
	
	
	playerCtrl->getState();
	auto throwerCharPos = playerCtrl->getState()->throwerChar->getPosition();
	auto partnerCharPos = playerCtrl->getState()->partnerChar->getPosition();
	sf::Vector2f lowerPos(std::min(throwerCharPos.x, partnerCharPos.x), std::min(throwerCharPos.y, partnerCharPos.y));
	sf::Vector2f higherPos(std::max(throwerCharPos.x, partnerCharPos.x), std::max(throwerCharPos.y, partnerCharPos.y));
	sf::Vector2f delta = higherPos - lowerPos;

	m_modifier = throwerCharPos.x < partnerCharPos.x ? 1 : -1;
	m_startingPoint = playerCtrl->getCharacter()->getPosition();
	m_radius = { delta.x / 2.0f, 300.0f };
	m_pathSpeed = 250.0f;
	m_rotationSpeed = 360.0f * 4;
	m_pathAngle = -(180.0f / 12);
	m_rotationAngle = 0.0f;
	m_isReturningToThrower = false;
}

void BoomerangingAvatarState::handleInput(PlayerController* playerCtrl, InputManager* inputMngr)
{
	OnGroundState::handleInput(playerCtrl, inputMngr);

	if (playerCtrl->getState()->boomerangingState == EBoomerangingState::kChargingStrengthBar)
	{
		if (inputMngr->wasKeyPressed(playerCtrl->getActionKey(EActionKey::kBoomerang)))
		{
			m_boomerang = new Boomerang;
			dpEngine::ServiceLocator::getEntityService()->addEntity(m_boomerang);
			PlayerCharacter* c = playerCtrl->getCharacter();
			c->setBoomerang(m_boomerang);
			m_boomerang->setPosition(c->getPosition().x, c->getPosition().y);
			m_startingPoint = m_boomerang->getPosition();

			playerCtrl->getState()->boomerangingState = EBoomerangingState::kFlying;
		}
	}
}

void BoomerangingAvatarState::update(PlayerController* playerCtrl, float deltaTime)
{
	OnGroundState::update(playerCtrl, deltaTime);

	if (playerCtrl->getState()->boomerangingState != EBoomerangingState::kFlying)
		return;

	if (!m_boomerang->isActive())
	{
		m_gotoWalkingState = true;
		return;
	}

	if (m_isReturningToThrower)
	{
		m_pathAngle -= deltaTime * m_pathSpeed;
		if (m_pathAngle < -(180.0f / 12))
			m_gotoWalkingState = true;
	}
	else
	{
		m_pathAngle += deltaTime * m_pathSpeed;
		if (m_pathAngle > 180.0f + (180.0f / 12))
			m_isReturningToThrower = true;
	}

	if (m_isReturningToThrower)
		m_rotationAngle -= deltaTime * m_rotationSpeed * m_modifier;
	else
		m_rotationAngle += deltaTime * m_rotationSpeed * m_modifier;
	
	
	auto pAng = (int)(m_pathAngle + 180) % 360;

	if (m_rotationAngle < 0)
		m_rotationAngle += 360;
	else if (m_rotationAngle > 359)
		m_rotationAngle -= 360;

	sf::Vector2f newPos = m_startingPoint;
	newPos.x += m_radius.x * m_modifier;
	newPos.x += std::cos(pAng * M_PI / 180.0f) * m_radius.x * m_modifier;
	newPos.y += std::sin(pAng * M_PI / 180.0f) * m_radius.y;

	m_boomerang->setPosition(newPos.x, newPos.y);
	m_boomerang->setRotation(m_rotationAngle);
}

bool BoomerangingAvatarState::shouldPopState() { return m_gotoWalkingState; }

void BoomerangingAvatarState::onExit(PlayerController* playerCtrl)
{
	dpEngine::ServiceLocator::getEntityService()->removeEntity(m_boomerang);
	playerCtrl->getCharacter()->setBoomerang(nullptr);
	m_boomerang = nullptr; // Boomerang gets delete in Engine -> EntityManager -> removeEntity(...) : void
}
