#include <CollisionTest\Scenes\TestScene.h>
#include <CollisionTest\Systems\EnemySystem.h>
#include <CollisionTest\Controllers\PlayerController.h>
#include <CollisionTest\Objects\PlayerCharacter.h>
#include <CollisionTest\Objects\ShieldedEnemy.h>
#include <CollisionTest\PlayerState.h>
#include <CollisionTest\Game.h>
#include <CollisionTest\HUD.h>

#include <Engine\SystemInfo.h>
#include <Engine\GameObjectCreationKit.h>
#include <Engine\InputSystem\InputManager.h>
#include <Engine\CollisionSystem\CollisionManager.h>
#include <Engine\CollisionSystem\BodyDef.h>
#include <Engine\CollisionSystem\Body.h>
#include <Engine\CollisionSystem\Enums.h>
#include <Engine\CollisionSystem\Shapes\RectangleShape.h>
#include <Engine\CollisionSystem\Shapes\CircleShape.h>
#include <Engine\Services\ServiceLocator.h>
#include <Engine\Services\IEntityService.h>
#include <Engine\Utils\Math.h>

#include <SFML\Graphics\RenderWindow.hpp>

using namespace dpEngine::CollisionSystem;

TestScene::TestScene() { /* EMPTY */ }

TestScene::~TestScene() { /* EMPTY */ }

void TestScene::onEnter(const dpEngine::SystemInfo& sysInfo)
{
	m_sysInfo = &sysInfo;

	m_game = new Game;

	m_hud = new HUD;

	GameObjectCreationKit kit;
	kit.collisionMngr = sysInfo.collisionMngr;
	m_enemySystem = new EnemySystem(kit);

	sysInfo.collisionMngr->setContactHandler(m_game);
	
	m_rectangularBlocks[0] = new RectangularBlock(sf::Vector2f(30.0f, 40.0f), sf::Vector2f(40.0f, 40.0f), sf::Color::Red);
	m_rectangularBlocks[1] = new RectangularBlock(sf::Vector2f(50.0f, 200.0f), sf::Vector2f(60.0f, 60.0f), sf::Color::Blue);
	m_circularBlocks[0] = new CircularBlock(sf::Vector2f(370.0f, 60.0f), 40.0f, sf::Color::Green);
	m_circularBlocks[1] = new CircularBlock(sf::Vector2f(400.0f, 250.0f), 30.0f, sf::Color::Yellow);
	
	m_game->characters[0] = new PlayerCharacter;
	m_game->characters[1] = new PlayerCharacter;
	
	m_game->enemyCharacters[0] = m_enemySystem->createShieldedEnemy();
	m_game->enemyCharacters[1] = m_enemySystem->createShieldedEnemy();
	m_game->enemyCharacters[2] = m_enemySystem->createShieldedEnemy();

	m_game->controllers[0] = new PlayerController(1, m_game->playerState, sf::Keyboard::Key::W, sf::Keyboard::Key::A, sf::Keyboard::Key::S, sf::Keyboard::Key::D);
	m_game->controllers[1] = new PlayerController(2, m_game->playerState, sf::Keyboard::Key::Up, sf::Keyboard::Key::Left, sf::Keyboard::Key::Down, sf::Keyboard::Key::Right);
	m_game->controllers[0]->setCharacter(m_game->characters[0]);
	m_game->controllers[1]->setCharacter(m_game->characters[1]);
	m_game->playerState->throwerChar = m_game->characters[0];
	m_game->playerState->partnerChar = m_game->characters[1];
	
	for (auto b : m_rectangularBlocks)
		dpEngine::ServiceLocator::getEntityService()->addEntity(b);

	for (auto b : m_circularBlocks)
		dpEngine::ServiceLocator::getEntityService()->addEntity(b);

	for (auto c : m_game->characters)
		dpEngine::ServiceLocator::getEntityService()->addEntity(c);

	/*for (auto c : m_game->enemyCharacters)
		dpEngine::ServiceLocator::getEntityService()->addEntity(c);*/
	
	//m_character.setPosition(1280.0f - 20.0f, 720.0f - 20.0f);
	m_game->characters[0]->setPosition(140.0f, 640.0f);
	m_game->characters[1]->setPosition(580.0f, 640.0f);

	m_game->enemyCharacters[0]->setPosition(280.0f, 320.0f);
	m_game->enemyCharacters[1]->setPosition(580.0f, 320.0f);
	m_game->enemyCharacters[2]->setPosition(880.0f, 320.0f);
}

void TestScene::onFadeIn() { }

void TestScene::onUpdate(float deltaTime, const dpEngine::SystemInfo&)
{
	for (auto c : m_game->controllers)
		c->update(m_sysInfo->inputMngr, deltaTime);

	PlayerCharacter* thrower = m_game->characters[0];
	PlayerCharacter* partner = m_game->characters[1];
	
	m_hud->update(deltaTime, *m_game->playerState);
	m_enemySystem->update(deltaTime);
}

bool TestScene::nextScene(int&) const { return false; }

bool TestScene::shouldClose() const { return false; }

void TestScene::onRender(sf::RenderWindow& window)
{
	sf::RenderStates states;
	
	for (auto b : m_rectangularBlocks)
		window.draw(*b, states);

	for (auto b : m_circularBlocks)
		window.draw(*b, states);
	
	for (auto c : m_game->characters)
		window.draw(*c, states);

	/*for (auto c : m_game->enemyCharacters)
		window.draw(*c, states);*/

	m_enemySystem->render(window, sf::RenderStates::Default);

	m_hud->render(window, *m_game->playerState);
}

void TestScene::onFadeOut() { }

void TestScene::onExit(const dpEngine::SystemInfo& sysInfo)
{
	for (auto block : m_rectangularBlocks)
		dpEngine::ServiceLocator::getEntityService()->removeEntity(block);

	for (auto block : m_circularBlocks)
		dpEngine::ServiceLocator::getEntityService()->removeEntity(block);

	for (auto c : m_game->characters)
		dpEngine::ServiceLocator::getEntityService()->removeEntity(c);

	/*for (auto c : m_game->enemyCharacters)
		dpEngine::ServiceLocator::getEntityService()->removeEntity(c);*/

	delete m_hud;

	sysInfo.collisionMngr->setContactHandler(nullptr);
	delete m_game;
}
