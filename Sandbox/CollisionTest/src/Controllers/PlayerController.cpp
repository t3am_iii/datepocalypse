#include <CollisionTest\Controllers\PlayerController.h>
#include <CollisionTest\Objects\PlayerCharacter.h>
#include <CollisionTest\Enums.h>

#include <Engine\InputSystem\InputManager.h>

PlayerController::PlayerController(int id, PlayerState* state, const sf::Keyboard::Key& up, const sf::Keyboard::Key& left, const sf::Keyboard::Key& down, const sf::Keyboard::Key& right)
	: m_id(id)
	, m_state(state)
	, m_character(nullptr)
	, m_walkingAvatarState(new WalkingAvatarState())
	, m_firingAvatarState(new FiringAvatarState())
	, m_boomeraningAvatarState(new BoomerangingAvatarState())
	, m_currentState(nullptr)
	, m_moveKeys{ up, left, down, right }
{
	m_currentState = m_walkingAvatarState;
	m_states.push_back(m_currentState);
}

PlayerController::~PlayerController() { /* EMPTY */ }

void PlayerController::setCharacter(PlayerCharacter* newCharacter)
{
	m_character = newCharacter;
	m_character->onNotify_PlayerControllerChange(this);
}

void PlayerController::setCanWalk(bool newCanWalk)
{
	m_canWalk = newCanWalk;
}

PlayerCharacter* PlayerController::getCharacter() { return m_character; }

int PlayerController::getId() const { return m_id; }

PlayerState* PlayerController::getState() const { return m_state; }

sf::Keyboard::Key PlayerController::getActionKey(const EActionKey& key) const
{
	auto idx = (int)key;
	if (idx > -1 && idx < 4)
		return m_moveKeys[idx];
	else if (key == EActionKey::kFire)
		return sf::Keyboard::Key::Space;
	else if (key == EActionKey::kBoomerang)
		return sf::Keyboard::Key::E;
	
	return sf::Keyboard::Key::Escape;
}

void PlayerController::update(dpEngine::InputManager* inputMngr, float deltaTime)
{
	if (m_character != nullptr)
	{
		if (inputMngr->wasKeyPressed(sf::Keyboard::Key::Q))
			m_character->changeShapeType();

		m_currentState->handleInput(this, inputMngr);
		m_currentState->update(this, deltaTime);

		EAvatarState state;
		if (m_currentState->shouldPushState(state))
		{
			m_currentState->onExit(this);

			switch (state)
			{
			case EAvatarState::kWalking:
				m_currentState = m_walkingAvatarState;
				break;
			case EAvatarState::kFiring:
				m_currentState = m_firingAvatarState;
				break;
			case EAvatarState::kBoomeranging:
				m_currentState = m_boomeraningAvatarState;
				break;
			}

			m_states.push_back(m_currentState);
			m_currentState->onEnter(this);
		}
		else if (m_currentState->shouldPopState())
		{
			m_currentState->onExit(this);
			m_states.pop_back();
			m_currentState = m_states.back();
			m_currentState->onEnter(this);
		}
	}
}

bool PlayerController::canWalk() const { return m_canWalk; }
