#include <CollisionTest\Objects\Boomerang.h>
#include <CollisionTest\Enums.h>

#include <Engine\GameObjectCreationKit.h>
#include <Engine\CollisionSystem\BodyDef.h>
#include <Engine\CollisionSystem\CollisionManager.h>

#include <SFML\Graphics\Texture.hpp>
#include <SFML\Graphics\RenderTarget.hpp>

using namespace dpEngine::CollisionSystem;

Boomerang::Boomerang() { /* EMPTY */ }

Boomerang::~Boomerang() { /* EMPTY */ }

void Boomerang::onConstruction(const dpEngine::GameObjectCreationKit& kit)
{
	m_data.type = EEntityTypes::kBoomerang;
	m_data.entity = this;

	sf::Texture* tex = new sf::Texture;
	tex->loadFromFile("../assets/boomerang.png");
	m_shape.setTexture(tex, true);
	m_shape.setSize(sf::Vector2f(256.0f, 256.0f));
	m_shape.setOrigin(128.0f, 128.0f);
	m_shape.setScale(0.2f, 0.2f);

	m_hitBox = new CircleShape(13.0f);
	BodyDef def(EBodyTypes::kKinematicBody, FILTER(EEntityCategory::kBoomerang, ~EEntityCategory::kBoomerang), true, m_hitBox, &m_data);
	body = kit.collisionMngr->createBody(def, this);


	m_isActive = true;
}

void Boomerang::setActive(bool isActive) { m_isActive = isActive; }

bool Boomerang::isActive() const { return m_isActive; }

void Boomerang::onDestruction(const dpEngine::GameObjectCreationKit& kit)
{
	kit.collisionMngr->destroyBody(body);
	delete m_hitBox;
}

void Boomerang::onNotify_OwnerChange(PlayerCharacter* newOwner)
{
	//
}

void Boomerang::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	states.transform = getTransform();
	target.draw(m_shape, states);
}
