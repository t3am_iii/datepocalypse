#include <CollisionTest\Objects\PlayerCharacter.h>
#include <CollisionTest\Objects\Boomerang.h>
#include <CollisionTest\Enums.h>

#include <Engine\GameObjectCreationKit.h>
#include <Engine\CollisionSystem\CollisionManager.h>
#include <Engine\CollisionSystem\BodyDef.h>
#include <Engine\CollisionSystem\Body.h>
#include <Engine\CollisionSystem\Enums.h>
#include <Engine\CollisionSystem\Shapes\RectangleShape.h>
#include <Engine\CollisionSystem\Shapes\CircleShape.h>

#include <SFML\Graphics\RectangleShape.hpp>
#include <SFML\Graphics\CircleShape.hpp>
#include <SFML\Graphics\RenderTarget.hpp>
#include <SFML\Graphics\RenderStates.hpp>

using namespace dpEngine::CollisionSystem;

PlayerCharacter::PlayerCharacter() { /* EMPTY */ }

PlayerCharacter::~PlayerCharacter() { /* EMPTY */ }

void PlayerCharacter::onConstruction(const GameObjectCreationKit& kit)
{
	m_data.type = EEntityTypes::kPlayerCharacter;
	m_data.entity = this;

	auto graShape = new sf::CircleShape;
	graShape->setFillColor(sf::Color(127, 127, 127, 255));
	graShape->setRadius(20.0f);
	graShape->setOrigin(20.0f, 20.0f);
	m_shape = graShape;
	
	shape = new CircleShape(20.0f);
	BodyDef def(EBodyTypes::kKinematicBody, FILTER(EEntityCategory::kPlayerCharacter, ~EEntityCategory::kPlayerCharacter), false, shape, &m_data);
	body = kit.collisionMngr->createBody(def, this);

	m_boomerang = nullptr;
	m_colMngr = kit.collisionMngr;
}

void PlayerCharacter::onDestruction(const GameObjectCreationKit& kit)
{
	kit.collisionMngr->destroyBody(body);
	body = nullptr;

	delete shape;
	shape = nullptr;

	delete m_shape;
}

void PlayerCharacter::setBoomerang(Boomerang* boomerang)
{
	m_boomerang = boomerang;
	m_boomerang->onNotify_OwnerChange(this);
}

void PlayerCharacter::onNotify_PlayerControllerChange(PlayerController* newController)
{
	m_controller = newController;
}

PlayerController* PlayerCharacter::getController() const { return m_controller; }

void PlayerCharacter::changeShapeType()
{
	auto type = body->getShape()->getType();
	BaseShape* newShape;
	delete m_shape;

	if (type == EShapeTypes::kCircle)
	{
		newShape = new RectangleShape(40.0f, 40.0f);
		auto graShape = new sf::RectangleShape;
		graShape->setFillColor(sf::Color(127, 127, 127, 255));
		graShape->setSize(sf::Vector2f(40.0f, 40.0f));
		graShape->setOrigin(0.0f, 0.0f);
		m_shape = graShape;
	}
	else
	{
		newShape = new CircleShape(20.0f);
		auto graShape = new sf::CircleShape;
		graShape->setFillColor(sf::Color(127, 127, 127, 255));
		graShape->setRadius(20.0f);
		graShape->setOrigin(20.0f, 20.0f);
		m_shape = graShape;
	}

	m_colMngr->destroyBody(body);

	BodyDef def(EBodyTypes::kKinematicBody, FILTER(EEntityCategory::kPlayerCharacter, ~EEntityCategory::kPlayerCharacter), false, newShape, &m_data);
	body = m_colMngr->createBody(def, this);
}

void PlayerCharacter::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	auto parentTransform = states.transform;
	states.transform = getTransform();
	target.draw(*m_shape, states);

	if (m_boomerang != nullptr && m_boomerang->isActive())
	{
		states.transform = m_boomerang->getTransform();
		target.draw(*m_boomerang, states);
	}
}
