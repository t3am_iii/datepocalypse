#include <CollisionTest\Objects\RectangularBlock.h>
#include <CollisionTest\Enums.h>

#include <Engine\GameObjectCreationKit.h>
#include <Engine\CollisionSystem\CollisionManager.h>
#include <Engine\CollisionSystem\BodyDef.h>
#include <Engine\CollisionSystem\Enums.h>
#include <Engine\CollisionSystem\Shapes\RectangleShape.h>
#include <Engine\Utils\Macros.h>

#include <SFML\Graphics\RenderTarget.hpp>
#include <SFML\Graphics\RenderStates.hpp>

using namespace dpEngine::CollisionSystem;

RectangularBlock::RectangularBlock(const sf::Vector2f& position, const sf::Vector2f& size, const sf::Color& color)
{
	dp_ASSERT(size.x > 0.0f);
	dp_ASSERT(size.y > 0.0f);
	
	m_shape.setSize(size);
	m_shape.setFillColor(color);

	Transformable::setPosition(position.x, position.y);
}

RectangularBlock::~RectangularBlock() { /* EMPTY */ }

void RectangularBlock::onConstruction(const dpEngine::GameObjectCreationKit& kit)
{

	BodyDef def(EBodyTypes::kStaticBody, FILTER(EEntityCategory::kBlock, EEntityCategory::kEverything), false, new RectangleShape(m_shape.getSize().x, m_shape.getSize().y));
	body = kit.collisionMngr->createBody(def, this);
}

void RectangularBlock::onDestruction(const dpEngine::GameObjectCreationKit& kit)
{
	kit.collisionMngr->destroyBody(body);
}

void RectangularBlock::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	states.transform *= getTransform();
	target.draw(m_shape, states);
}
