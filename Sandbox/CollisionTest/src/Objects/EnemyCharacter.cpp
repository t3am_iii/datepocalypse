#include <CollisionTest\Objects\EnemyCharacter.h>
#include <CollisionTest\Enums.h>

#include <Engine\GameObjectCreationKit.h>
#include <Engine\CollisionSystem\CollisionManager.h>
#include <Engine\CollisionSystem\BodyDef.h>
#include <Engine\CollisionSystem\Body.h>
#include <Engine\CollisionSystem\Enums.h>
#include <Engine\CollisionSystem\Shapes\CircleShape.h>
#include <Engine\Utils\Math.h>

#include <SFML\Graphics\CircleShape.hpp>
#include <SFML\Graphics\RenderTarget.hpp>

using namespace CollisionSystem;

EnemyCharacter::EnemyCharacter() { /* EMPTY */ }

EnemyCharacter::~EnemyCharacter() { /* EMPTY */ }

void EnemyCharacter::onConstruction(const GameObjectCreationKit& kit)
{
	m_data.type = EEntityTypes::kEnemyCharacter;
	m_data.entity = this;

	m_graphicalShape = new sf::CircleShape;
	m_graphicalShape->setFillColor(sf::Color(127, 127, 127, 255));
	m_graphicalShape->setRadius(20.0f);
	m_graphicalShape->setOrigin(20.0f, 20.0f);

	shape = new CircleShape(20.0f);
	BodyDef def(EBodyTypes::kKinematicBody, FILTER(EEntityCategory::kEnemyCharacter, ~EEntityCategory::kEnemyCharacter), false, shape, &m_data);
	body = kit.collisionMngr->createBody(def, this);


	m_health = 5.0f;
	m_isActive = true;
	m_hasTakenDamage = false;
	m_showDamageEffect = false;
	m_startCol = m_graphicalShape->getFillColor();
	m_defaultVec = sf::Vector3f(m_startCol.r / 255.0f, m_startCol.g / 255.0f, m_startCol.b / 255.0f);
}

void EnemyCharacter::onDestruction(const GameObjectCreationKit& kit)
{
	kit.collisionMngr->destroyBody(body);
	body = nullptr;

	delete shape;
	shape = nullptr;

	delete m_graphicalShape;
}

void EnemyCharacter::update(float deltaTime)
{
	if (m_hasTakenDamage)
	{
		m_targetVec = sf::Vector3f(1.0f, 0.0f, 0.0f);
		m_currentVec = m_defaultVec;

		m_damageEffectTime = 0.2f;
		m_showDamageEffect = true;
		m_hasTakenDamage = false;
	}

	if (m_showDamageEffect)
	{
		m_damageEffectTime -= deltaTime;
		if (m_damageEffectTime > 0.0f)
		{
			if (m_damageEffectTime < 0.1f)
				m_targetVec = m_defaultVec;

			m_currentVec = FMath::Lerp(m_currentVec, m_targetVec, 0.05f);
			m_graphicalShape->setFillColor(sf::Color(m_currentVec.x * 255, m_currentVec.y * 255, m_currentVec.z * 255));
		}
		else
		{
			m_showDamageEffect = false;
			m_graphicalShape->setFillColor(m_startCol);
		}
	}
}

void EnemyCharacter::takeDamage(float amount)
{
	m_health -= amount;
	if (m_health <= 0.0f)
		m_isActive = false;
	else
		m_hasTakenDamage = true;
}

bool EnemyCharacter::isActive() const { return m_isActive; }

void EnemyCharacter::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	auto parentTransform = states.transform;
	states.transform = getTransform();
	target.draw(*m_graphicalShape, states);
}
