#include <CollisionTest\Objects\ShieldedEnemy.h>
#include <CollisionTest\Enums.h>

#include <Engine\GameObjectCreationKit.h>
#include <Engine\CollisionSystem\BodyDef.h>
#include <Engine\CollisionSystem\Body.h>
#include <Engine\CollisionSystem\Enums.h>
#include <Engine\CollisionSystem\CollisionManager.h>
#include <Engine\CollisionSystem\Shapes\RectangleShape.h>

#include <SFML\Graphics\RectangleShape.hpp>
#include <SFML\Graphics\RenderTarget.hpp>

using namespace dpEngine::CollisionSystem;

ShieldedEnemy::ShieldedEnemy() { /* EMPTY */ }

ShieldedEnemy::~ShieldedEnemy() { /* EMPTY */ }

void ShieldedEnemy::onConstruction(const GameObjectCreationKit& kit)
{
	m_shieldData.type = EEntityTypes::kShield;
	m_shieldData.entity = this;

	EnemyCharacter::onConstruction(kit);

	m_shieldShape = new sf::RectangleShape;
	m_shieldShape->setSize(sf::Vector2f(60.0f, 15.0f));
	m_shieldShape->setFillColor(sf::Color::Blue);
	
	auto bodyShape = new RectangleShape(60.0f, 15.0f);
	BodyDef def(EBodyTypes::kKinematicBody, FILTER(EEntityCategory::kShield, EEntityCategory::kEverything), false, bodyShape, &m_shieldData);
	m_shieldBody = kit.collisionMngr->createBody(def, &m_shieldTransformable);
}

void ShieldedEnemy::onDestruction(const GameObjectCreationKit& kit)
{
	delete m_shieldShape;

	kit.collisionMngr->destroyBody(m_shieldBody);

	EnemyCharacter::onDestruction(kit);
}

void ShieldedEnemy::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	EnemyCharacter::draw(target, states);

	sf::Vector2f pos = EnemyCharacter::getPosition() + sf::Vector2f(-30.0f, 20.0f);
	m_shieldBody->setPosition(pos.x, pos.y);

	states.transform = m_shieldTransformable.getTransform();
	target.draw(*m_shieldShape, states);
}
