#include <CollisionTest\Objects\CircularBlock.h>
#include <CollisionTest\Enums.h>

#include <Engine\GameObjectCreationKit.h>
#include <Engine\CollisionSystem\CollisionManager.h>
#include <Engine\CollisionSystem\BodyDef.h>
#include <Engine\CollisionSystem\Enums.h>
#include <Engine\CollisionSystem\Shapes\CircleShape.h>
#include <Engine\Utils\Macros.h>

#include <SFML\Graphics\RenderTarget.hpp>
#include <SFML\Graphics\RenderStates.hpp>

using namespace dpEngine::CollisionSystem;

CircularBlock::CircularBlock(const sf::Vector2f& position, float radius, const sf::Color& color)
{
	dp_ASSERT(radius > 0.0f);

	m_shape.setRadius(radius);
	m_shape.setFillColor(color);
	m_shape.setOrigin(radius, radius);

	Transformable::setPosition(position.x, position.y);
}

CircularBlock::~CircularBlock() { /* EMPTY */ }

void CircularBlock::onConstruction(const dpEngine::GameObjectCreationKit& kit)
{
	
	BodyDef def(EBodyTypes::kStaticBody, FILTER(EEntityCategory::kBlock, EEntityCategory::kEverything), false, new CircleShape(m_shape.getRadius()));
	body = kit.collisionMngr->createBody(def, this);
}

void CircularBlock::onDestruction(const dpEngine::GameObjectCreationKit& kit)
{
	kit.collisionMngr->destroyBody(body);
}

void CircularBlock::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	states.transform *= getTransform();
	target.draw(m_shape, states);
}
