#include <CollisionTest\HUD.h>
#include <CollisionTest\PlayerState.h>
#include <CollisionTest\Enums.h>
#include <CollisionTest\Objects\PlayerCharacter.h>
#include <CollisionTest\Controllers\PlayerController.h>

#include <Engine\Utils\Math.h>

#include <SFML\Graphics\RenderTarget.hpp>

#define _USE_MATH_DEFINES
#include <math.h>

HUD::HUD()
	: m_currentStrength(0.5f)
	, m_targetStrength(1.0f)
{ /* EMPTY */ }

HUD::~HUD() { /* EMPTY */ }

void HUD::update(float deltaTime, const PlayerState& state)
{
	if (state.hasBoomerang)
	{
		if (state.boomerangingState == EBoomerangingState::kChargingStrengthBar)
		{
			const auto throwerCharPos = state.throwerChar->getPosition();
			const auto partnerCharPos = state.partnerChar->getPosition();

			if (abs(FMath::Distance(throwerCharPos, m_lastCalcPos[0])) > 10.0f)
			{
				recalcBoomerangArc(throwerCharPos, partnerCharPos);
				m_lastCalcPos[0] = throwerCharPos;
			}
			else if (abs(FMath::Distance(partnerCharPos, m_lastCalcPos[1])) > 10.0f)
			{
				recalcBoomerangArc(throwerCharPos, partnerCharPos);
				m_lastCalcPos[1] = partnerCharPos;
			}

			

			auto deltaCharPos = FMath::Abs(throwerCharPos - partnerCharPos);
			auto startBarPos = sf::Vector2f(std::min(throwerCharPos.x, partnerCharPos.x), throwerCharPos.y);
			startBarPos.x += deltaCharPos.x / 2.0f;
			auto endBarPos = startBarPos - sf::Vector2f(0.0f, 200.0f);

			recalcStrengthBar(startBarPos, endBarPos, m_currentStrength);

			m_currentStrength = FMath::Lerp(m_currentStrength, m_targetStrength, deltaTime);
			if (m_currentStrength < 0.1f)
			{
				m_currentStrength = 0.1f;
				m_targetStrength = 1.0f;
			}
			else if (m_currentStrength > 0.9f)
			{
				m_currentStrength = 0.9f;
				m_targetStrength = 0.0f;
			}
		}
	}
}

void HUD::render(sf::RenderTarget& target, const PlayerState& state)
{
	if (state.boomerangingState == EBoomerangingState::kChargingStrengthBar)
	{
		target.draw(m_arcLine.data(), m_arcCount, sf::LinesStrip);
		target.draw(m_partnerTarget);

		target.draw(m_strengthBarInner, STRENGTH_VERTEX_COUNT, sf::PrimitiveType::TrianglesStrip);
		target.draw(m_strengthBarOutline);
	}
}

void HUD::recalcBoomerangArc(sf::Vector2f throwerCharPos, sf::Vector2f partnerCharPos)
{
	const float arcStartAngle(-(180.0f / 12));
	const float arcEndAngle(180.0f + (180.0f / 12));
	
	std::vector<sf::Vector2f> points;
	
	sf::Vector2f lowerPos(std::min(throwerCharPos.x, partnerCharPos.x), std::min(throwerCharPos.y, partnerCharPos.y));
	sf::Vector2f higherPos(std::max(throwerCharPos.x, partnerCharPos.x), std::max(throwerCharPos.y, partnerCharPos.y));
	sf::Vector2f deltaCharPos = higherPos - lowerPos;

	int modifier = throwerCharPos.x < partnerCharPos.x ? 1 : -1;
	
	sf::Vector2f startPos = throwerCharPos;
	startPos.x += deltaCharPos.x * modifier / 2.0f;

	const sf::Vector2f radius = { deltaCharPos.x / 2.0f, 300.0f };

	for (int ang = arcStartAngle; ang < arcEndAngle; ang += 5)
	{
		auto pos = startPos;
		pos.x += std::cos(ang * M_PI / 180.0f) * radius.x * modifier;
		pos.y -= std::sin(ang * M_PI / 180.0f) * radius.y;

		points.push_back(pos);
	}

	std::vector<sf::Vertex> line;
	for (int i = 0; i < points.size(); i++)
	{
		sf::Color colors[STRENGTH_COLORS_COUNT]{ sf::Color::White, sf::Color::Cyan, sf::Color::White };
		int idx = (int)(i / (points.size() / STRENGTH_COLORS_COUNT));
		auto col = colors[idx];
		
		line.push_back(sf::Vertex(points[i], col));
	}
	
	m_arcLine = line;
	m_arcCount = m_arcLine.size();


	auto pos = startPos - sf::Vector2f(30.0f, 30.0f);
	pos.x += radius.x * modifier;
	m_partnerTarget.setPosition(pos);

	m_partnerTarget.setFillColor(sf::Color(0, 0, 0, 0));
	m_partnerTarget.setOutlineColor(sf::Color::Red);
	m_partnerTarget.setOutlineThickness(4.0f);
	m_partnerTarget.setRadius(30.0f);
}

void HUD::recalcStrengthBar(sf::Vector2f startBarPos, sf::Vector2f endBarPos, float percentage)
{
	float halfBarWidth = 10.0f;

	m_strengthBarOutline.setFillColor(sf::Color(0, 0, 0, 0));
	m_strengthBarOutline.setOutlineColor(sf::Color::White);
	m_strengthBarOutline.setOutlineThickness(4.0f);
	
	sf::Vector2f size(halfBarWidth * 2, startBarPos.y - endBarPos.y);
	m_strengthBarOutline.setPosition(endBarPos.x - halfBarWidth, endBarPos.y);
	m_strengthBarOutline.setSize(size);

	
	sf::Vector2f deltaBarPos = startBarPos - endBarPos;
	endBarPos += deltaBarPos * percentage;
	deltaBarPos *= percentage;

	m_strengthBarInner[0].position = endBarPos;
	m_strengthBarInner[0].position.x -= halfBarWidth;
	m_strengthBarInner[0].color = sf::Color::Red;

	m_strengthBarInner[1].position = endBarPos;
	m_strengthBarInner[1].position.x += halfBarWidth;
	m_strengthBarInner[1].color = sf::Color::Red;

	m_strengthBarInner[2].position = endBarPos + deltaBarPos / 2.0f;
	m_strengthBarInner[2].position.x -= halfBarWidth;
	m_strengthBarInner[2].color = sf::Color(255, 99, 0);

	m_strengthBarInner[3].position = endBarPos + deltaBarPos / 2.0f;
	m_strengthBarInner[3].position.x += halfBarWidth;
	m_strengthBarInner[3].color = sf::Color(255, 99, 0);

	m_strengthBarInner[4].position = startBarPos;
	m_strengthBarInner[4].position.x -= halfBarWidth;
	m_strengthBarInner[4].color = sf::Color::Yellow;

	m_strengthBarInner[5].position = startBarPos;
	m_strengthBarInner[5].position.x += halfBarWidth;
	m_strengthBarInner[5].color = sf::Color::Yellow;
}
