#include <CollisionTest\Game.h>
#include <CollisionTest\EntityData.h>
#include <CollisionTest\Enums.h>
#include <CollisionTest\PlayerState.h>
#include <CollisionTest\Controllers\PlayerController.h>
#include <CollisionTest\Objects\Boomerang.h>
#include <CollisionTest\Objects\PlayerCharacter.h>
#include <CollisionTest\Objects\ShieldedEnemy.h>

#include <Engine\CollisionSystem\Body.h>
#include <Engine\CollisionSystem\Data\ContactInfo.h>
#include <Engine\Services\ServiceLocator.h>
#include <Engine\Services\IEntityService.h>
#include <Engine\Utils\Macros.h>

Game::Game()
{
	playerState = new PlayerState;
	playerState->hasBoomerang = true;
	playerState->boomerangingState = EBoomerangingState::kNotActive;
	playerState->throwerId = 1;
}

Game::~Game()
{
	delete playerState;
}

void Game::onContact(const dpEngine::ContactInfo& info)
{
	void* leftUserData = info.leftBody->getUserData();
	void* rightUserData = info.rightBody->getUserData();

	if (leftUserData == nullptr || rightUserData == nullptr)
		return;

	// Boomerang - playercharacter
	auto leftData = static_cast<EntityData*>(leftUserData);
	auto rightData = static_cast<EntityData*>(rightUserData);
	int type = -1;

	Boomerang* boomerang = nullptr;
	PlayerCharacter* playerChar = nullptr;
	EnemyCharacter* enemyChar = nullptr;
	ShieldedEnemy* shielded = nullptr;

	if (leftData->type == EEntityTypes::kBoomerang && rightData->type == EEntityTypes::kPlayerCharacter)
	{
		boomerang = static_cast<Boomerang*>(leftData->entity);
		playerChar = static_cast<PlayerCharacter*>(rightData->entity);

		type = 0;
	}
	else if (leftData->type == EEntityTypes::kPlayerCharacter && rightData->type == EEntityTypes::kBoomerang)
	{
		boomerang = static_cast<Boomerang*>(rightData->entity);
		playerChar = static_cast<PlayerCharacter*>(leftData->entity);

		type = 0;
	}
	
	if (leftData->type == EEntityTypes::kBoomerang && rightData->type == EEntityTypes::kEnemyCharacter)
	{
		boomerang = static_cast<Boomerang*>(leftData->entity);
		enemyChar = static_cast<EnemyCharacter*>(rightData->entity);

		type = 1;
	}
	else if (leftData->type == EEntityTypes::kEnemyCharacter && rightData->type == EEntityTypes::kBoomerang)
	{
		boomerang = static_cast<Boomerang*>(rightData->entity);
		enemyChar = static_cast<EnemyCharacter*>(leftData->entity);

		type = 1;
	}

	if (leftData->type == EEntityTypes::kBoomerang && rightData->type == EEntityTypes::kShield)
	{
		boomerang = static_cast<Boomerang*>(leftData->entity);
		shielded = static_cast<ShieldedEnemy*>(rightData->entity);

		type = 2;
	}
	else if (leftData->type == EEntityTypes::kShield && rightData->type == EEntityTypes::kBoomerang)
	{
		boomerang = static_cast<Boomerang*>(rightData->entity);
		shielded = static_cast<ShieldedEnemy*>(leftData->entity);

		type = 2;
	}

	if (type == 0)
	{
		int id = playerChar->getController()->getId();
		if (id == 0)
			return;

		//
	}
	else if (type == 1)
	{
		enemyChar->takeDamage(1.0f);
		boomerang->setActive(false);
		/*if (!enemyChar->isActive())
			ServiceLocator::getEntityService()->removeEntity(enemyChar);*/
	}
	else if (type == 2)
	{
		boomerang->setActive(false);
	}
	/*else
		dp_ASSERT(false);*/
}
