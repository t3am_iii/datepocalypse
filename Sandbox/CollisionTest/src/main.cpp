/*#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>*/

#pragma comment(lib, "Engine.lib")

#include <CollisionTest\Scenes\TestScene.h>

#include <Engine\Engine.h>

int main(int argc, char** argv)
{
	dpEngine::Engine engine;
	engine.startup(1280, 720, "Datepocalypse");
	engine.addScene(new TestScene, true);
	engine.run();
	engine.shutdown();

	/*auto i = _CrtDumpMemoryLeaks();
	if (i > 0)
		bool foundLeaks = true;*/

	return 0;
}
