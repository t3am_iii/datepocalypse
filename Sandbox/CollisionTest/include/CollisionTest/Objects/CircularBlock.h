#pragma once

#include <Engine\Objects\WorldObject.h>

#include <SFML\Graphics\CircleShape.hpp>

class CircularBlock : public dpEngine::WorldObject
{
private:
	sf::CircleShape m_shape;

public:
	CircularBlock(const sf::Vector2f&, float, const sf::Color&);

	~CircularBlock();

	void onConstruction(const dpEngine::GameObjectCreationKit&);

	void onDestruction(const dpEngine::GameObjectCreationKit&);

protected:
	void draw(sf::RenderTarget&, sf::RenderStates) const;

};
