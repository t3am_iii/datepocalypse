#pragma once

#include <CollisionTest\EntityData.h>

#include <Engine\Objects\Pawn.h>
#include <Engine\CollisionSystem\Shapes\CircleShape.h>

#include <SFML\Graphics\Sprite.hpp>
#include <SFML\Graphics\RectangleShape.hpp>

class PlayerCharacter;

class Boomerang : public dpEngine::Pawn
{
private:
	EntityData m_data;
	
	dpEngine::CollisionSystem::CircleShape* m_hitBox;
	
	sf::Texture* m_texture;

	sf::RectangleShape m_shape;

	bool m_isActive;

public:
	Boomerang();

	~Boomerang();

	void onConstruction(const dpEngine::GameObjectCreationKit&);

	void setActive(bool);

	bool isActive() const;

	void onDestruction(const dpEngine::GameObjectCreationKit&);

	void onNotify_OwnerChange(PlayerCharacter*);

protected:
	void draw(sf::RenderTarget&, sf::RenderStates) const;

};
