#pragma once

#include <CollisionTest\EntityData.h>

#include <Engine\Objects\Pawn.h>

class PlayerController;
class Boomerang;

using namespace dpEngine;

namespace dpEngine {
	namespace CollisionSystem { class CollisionManager; }
}

namespace sf { class Shape; }

class PlayerCharacter : public dpEngine::Pawn
{
private:
	EntityData m_data;

	PlayerController* m_controller;
	
	sf::Shape* m_shape;

	Boomerang* m_boomerang;

	CollisionSystem::CollisionManager* m_colMngr; // Temporary

public:
	PlayerCharacter();

	~PlayerCharacter();

	void onConstruction(const GameObjectCreationKit&);

	void onDestruction(const GameObjectCreationKit&);

	void setBoomerang(Boomerang*);

	void onNotify_PlayerControllerChange(PlayerController*);

	PlayerController* getController() const;
	
	void changeShapeType(); // This is a test function and should NOT be used in the REAL game.

protected:
	void draw(sf::RenderTarget&, sf::RenderStates) const;

};
