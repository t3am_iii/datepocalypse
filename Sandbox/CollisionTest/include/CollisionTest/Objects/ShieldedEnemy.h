#pragma once

#include <CollisionTest\EntityData.h>

#include <CollisionTest\Objects\EnemyCharacter.h>

namespace sf { class RectangleShape; }

class ShieldedEnemy : public EnemyCharacter
{
private:
	EntityData m_shieldData;

	sf::RectangleShape* m_shieldShape;

	sf::Transformable m_shieldTransformable;
	
	CollisionSystem::Body* m_shieldBody;

public:
	ShieldedEnemy();
	
	~ShieldedEnemy();

	void onConstruction(const GameObjectCreationKit&);

	void onDestruction(const GameObjectCreationKit&);

protected:
	void draw(sf::RenderTarget&, sf::RenderStates) const;

};
