#pragma once

#include <Engine\Objects\WorldObject.h>

#include <SFML\Graphics\RectangleShape.hpp>

class RectangularBlock : public dpEngine::WorldObject
{
private:
	sf::RectangleShape m_shape;

public:
	RectangularBlock(const sf::Vector2f&, const sf::Vector2f&, const sf::Color&);

	~RectangularBlock();

	void onConstruction(const dpEngine::GameObjectCreationKit&);

	void onDestruction(const dpEngine::GameObjectCreationKit&);

protected:
	void draw(sf::RenderTarget&, sf::RenderStates) const;

};
