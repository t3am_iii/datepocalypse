#pragma once

#include <CollisionTest\EntityData.h>

#include <Engine\Objects\Pawn.h>

#include <SFML\Graphics\Color.hpp>
#include <SFML\System\Vector3.hpp>

using namespace dpEngine;

namespace sf { class CircleShape; }

class EnemyCharacter : public Pawn
{
protected:
	EntityData m_data;

	sf::CircleShape* m_graphicalShape;

	float m_health;

	bool m_isActive;

	bool m_hasTakenDamage;

	float m_showDamageEffect;

	float m_damageEffectTime;

	sf::Color m_startCol;

	sf::Vector3f m_currentVec;

	sf::Vector3f m_targetVec;

	sf::Vector3f m_defaultVec;

public:
	EnemyCharacter();

	~EnemyCharacter();

	virtual void onConstruction(const GameObjectCreationKit&);

	virtual void onDestruction(const GameObjectCreationKit&);

	void update(float);

	void takeDamage(float);

	bool isActive() const;

	//void onNotify_PlayerControllerChange(PlayerController*);

	//PlayerController* getController() const;
	
protected:
	//virtual void onDamageTaken(float) { }
	
	virtual void draw(sf::RenderTarget&, sf::RenderStates) const;

};
