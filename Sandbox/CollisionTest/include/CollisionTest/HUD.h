#pragma once

#include <SFML\Graphics\CircleShape.hpp>
#include <SFML\Graphics\RectangleShape.hpp>

#include <vector>

#define STRENGTH_VERTEX_COUNT 6
#define STRENGTH_COLORS_COUNT 3

struct PlayerState;

class HUD
{
private:
	sf::Vector2f m_lastCalcPos[2];


	std::vector<sf::Vertex> m_arcLine;

	int m_arcCount;

	sf::CircleShape m_partnerTarget;

	sf::Vertex m_strengthBarInner[STRENGTH_VERTEX_COUNT];

	sf::RectangleShape m_strengthBarOutline;

	float m_targetStrength;

	float m_currentStrength;

public:
	HUD();

	~HUD();

	void update(float, const PlayerState&);
	
	void render(sf::RenderTarget&, const PlayerState& state);

private:
	void recalcBoomerangArc(sf::Vector2f, sf::Vector2f);

	void recalcStrengthBar(sf::Vector2f, sf::Vector2f, float);

};
