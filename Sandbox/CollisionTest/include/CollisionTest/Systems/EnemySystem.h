#pragma once

#include <Engine\GameObjectCreationKit.h>

#include <SFML\Graphics\RenderTarget.hpp>

#include <vector>

class ShieldedEnemy;

class EnemySystem
{
private:
	dpEngine::GameObjectCreationKit m_kit;

	std::vector<ShieldedEnemy*> m_shieldedEnemies;

public:
	EnemySystem(const dpEngine::GameObjectCreationKit&);

	~EnemySystem();

	ShieldedEnemy* createShieldedEnemy();

	void destroyEnemy(ShieldedEnemy*);

	void update(float);

	void render(sf::RenderTarget&, sf::RenderStates) const;

};
