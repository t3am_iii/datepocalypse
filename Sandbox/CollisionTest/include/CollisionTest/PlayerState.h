#pragma once

enum class EBoomerangingState;
class PlayerCharacter;

struct PlayerState
{
	bool hasBoomerang;

	EBoomerangingState boomerangingState;

	int throwerId;

	PlayerCharacter* throwerChar;

	PlayerCharacter* partnerChar;
};
