#pragma once

#include <CollisionTest\Objects\RectangularBlock.h>
#include <CollisionTest\Objects\CircularBlock.h>

#include <Engine\SceneSystem\BaseScene.h>

class Game;
class HUD;
class EnemySystem;

class TestScene : public dpEngine::SceneSystem::BaseScene
{
private:
	const dpEngine::SystemInfo* m_sysInfo;

	Game* m_game;

	HUD* m_hud;

	EnemySystem* m_enemySystem;

	RectangularBlock* m_rectangularBlocks[2];

	CircularBlock* m_circularBlocks[2];

public:
	TestScene();

	~TestScene();

	void onEnter(const dpEngine::SystemInfo&);

	void onFadeIn();

	void onUpdate(float, const dpEngine::SystemInfo&);

	bool nextScene(int&) const;

	bool shouldClose() const;

	void onRender(sf::RenderWindow&);

	void onFadeOut();
	
	void onExit(const dpEngine::SystemInfo&);

};
