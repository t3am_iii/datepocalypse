#pragma once

#include <Engine\Utils\BitwiseEnumHelper.h>

enum class EBoomerangingState
{
	kNotActive,
	kStartCharge,
	kChargingStrengthBar,
	kStartFly,
	kFlying,
};

enum class EEntityTypes
{
	kBoomerang,
	kShield,
	kPlayerCharacter,
	kEnemyCharacter,
	kBossCharacter,
};

enum class EAvatarState
{
	kWalking,
	kFiring,
	kBoomeranging,
};

enum class EActionKey
{
	kWalkUp		= 0,
	kWalkLeft	= 1,
	kWalkDown	= 2,
	kWalkRight	= 3,
	kFire		= 4,
	kBoomerang	= 5,
};

// I am a ... (category bits)
// I collide with ... (mask bits)

enum class EEntityCategory
{
    kPlayerCharacter	= 0x0002,
	kEnemyCharacter		= 0x0004,
    kBlock				= 0x0008,
	kBoomerang			= 0x0010,
	kShield				= 0x0020,

    kEverything			= 0xffff,
	kNothing			= 0x0000,
};

#define FILTER(c, m) (dpEngine::CollisionSystem::Filter(static_cast<__BIT_TYPE_>(c), static_cast<__BIT_TYPE_>(m)))

BITWISE_ENUM_DECLARE(EEntityCategory)
