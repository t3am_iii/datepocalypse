#pragma once

#include <CollisionTest\States\OnGroundStates.h>

#include <SFML\Window\Keyboard.hpp>

#include <vector>

class PlayerCharacter;
struct PlayerState;
enum class EActionKey;

namespace dpEngine { class InputManager; }

class PlayerController
{
private:
	sf::Keyboard::Key m_moveKeys[4];

	PlayerCharacter* m_character;

	PlayerState* m_state;

	int m_id;

	bool m_canWalk;

	WalkingAvatarState* m_walkingAvatarState;

	FiringAvatarState* m_firingAvatarState;

	BoomerangingAvatarState* m_boomeraningAvatarState;
	
	std::vector<BaseState*> m_states;

	BaseState* m_currentState;

public:
	PlayerController(int, PlayerState*, const sf::Keyboard::Key&, const sf::Keyboard::Key&, const sf::Keyboard::Key&, const sf::Keyboard::Key&);

	~PlayerController();

	void update(dpEngine::InputManager*, float);
	
	void setCharacter(PlayerCharacter*);

	void setCanWalk(bool);

	PlayerCharacter* getCharacter();

	int getId() const;

	PlayerState* getState() const;

	sf::Keyboard::Key getActionKey(const EActionKey&) const;

	bool canWalk() const;

};
