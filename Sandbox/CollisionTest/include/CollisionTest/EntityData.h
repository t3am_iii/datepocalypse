#pragma once

enum class EEntityTypes;

struct EntityData
{
	EEntityTypes type;

	void* entity;
};
