#pragma once

#include <CollisionTest\States\BaseState.h>

class Boomerang;

class OnGroundState : public BaseState
{
protected:
	sf::Vector2f m_velocity;

public:
	virtual void onEnter(PlayerController*) { }
	
	virtual void handleInput(PlayerController*, InputManager*);

	virtual void update(PlayerController*, float);

	virtual bool shouldPushState(EAvatarState&) { return false; }

	virtual bool shouldPopState() { return false; }

	virtual void onExit(PlayerController*) { }

};

class WalkingAvatarState : public OnGroundState
{
private:
	bool m_gotoBoomerangingState;

public:
	virtual void onEnter(PlayerController*);

	void handleInput(PlayerController*, InputManager*);

	void update(PlayerController*, float);

	virtual bool shouldPushState(EAvatarState&);

	virtual bool shouldPopState();

};

class FiringAvatarState : public WalkingAvatarState
{
public:
	void handleInput(PlayerController*, InputManager*);

	void update(PlayerController*, float);

};

class BoomerangingAvatarState : public OnGroundState
{
private:
	bool m_gotoWalkingState;

	Boomerang* m_boomerang;

	bool m_isReturningToThrower;

	int m_modifier;

	sf::Vector2f m_startingPoint;

	sf::Vector2f m_radius;

	float m_pathSpeed;

	float m_rotationSpeed;

	float m_pathAngle;

	float m_rotationAngle;

public:
	void onEnter(PlayerController*);

	void handleInput(PlayerController*, InputManager*);

	void update(PlayerController*, float);

	bool shouldPopState();

	void onExit(PlayerController*);

};
