#pragma once

#include <SFML\System\Vector2.hpp>

class PlayerController;
enum class EAvatarState;
namespace dpEngine { class InputManager; }

using dpEngine::InputManager;

class BaseState
{
public:
	virtual ~BaseState() { }

	virtual void onEnter(PlayerController*) = 0;

	virtual void handleInput(PlayerController*, InputManager*) = 0;

	virtual void update(PlayerController*, float) = 0;

	virtual bool shouldPushState(EAvatarState&) = 0;

	virtual bool shouldPopState() = 0;

	virtual void onExit(PlayerController*) = 0;

};

/*class OnPowerUpState : public BaseState
{
	//
};*/
