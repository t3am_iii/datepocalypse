#include <ParticleTest\ParticleScene.h>

#include <Engine\SystemInfo.h>
#include <Engine\InputSystem\InputManager.h>

#include <SFML\Graphics\RenderWindow.hpp>

void perspectiveGL(GLdouble fovY, GLdouble aspect, GLdouble zNear, GLdouble zFar)
{
	const GLdouble pi = 3.1415926535897932384626433832795;
	GLdouble fW, fH;

	//fH = tan( (fovY / 2) / 180 * pi ) * zNear;
	fH = tan(fovY / 360 * pi) * zNear;
	fW = fH * aspect;

	glFrustum(-fW, fW, -fH, fH, zNear, zFar);
}

void ParticleScene::onEnter(const SystemInfo&)
{
	//
}

void ParticleScene::onUpdate(float, const SystemInfo& sysInfo)
{
	//
}

void ParticleScene::onRender(sf::RenderWindow& window)
{
	glClearColor(0.3f, 0.3f, 0.3f, 1.f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	sf::Vector2u wsize = window.getSize();
	glViewport(0, 0, wsize.x, wsize.y);
	
	// Enable blending (we need this to be able to use an alpha component)
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_POINT_SMOOTH);
	perspectiveGL(60, wsize.x / wsize.y, 0.1, 512);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glPushMatrix();
	glTranslatef(0.f, 0.f, -5.f);


	// Take the contents of the current accumulation buffer and copy it to the colour buffer so that it entirely overwrites it
	glAccum(GL_RETURN, 1.0f);

	// Clear the accumulation buffer (don't worry, we re-grab the screen into the accumulation buffer after drawing our current frame!)
	glClear(GL_ACCUM_BUFFER_BIT);

	// Set ModelView matrix mode and reset to the default identity matrix
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(0.375, 0.375, 0);

	glColor3f(0.0f, 0.0f, 1.0f); // Let it be blue
	glBegin(GL_QUADS); // 2x2 pixels
	glVertex2f(1.0f, 1.0f);
	glVertex2f(2.0f, 1.0f);
	glVertex2f(20.0f, 20.0f);
	glVertex2f(1.0f, 20.0f);
	glEnd();

	// Displacement trick for exact pixelisation
	glTranslatef(0.375, 0.375, 0);

	// Draw our fireworks
	for (int loop = 0; loop < FIREWORKS; loop++)
	{
		for (int particleLoop = 0; particleLoop < FIREWORK_PARTICLES; particleLoop++)
		{

			// Set the point size of the firework particles (this needs to be called BEFORE opening the glBegin(GL_POINTS) section!)
			glPointSize(m_fireworks[loop].particleSize);

			glBegin(GL_POINTS);
			// Set colour to yellow on the way up, then whatever colour firework should be when exploded
			if (m_fireworks[loop].hasExploded == false)
			{
				glColor4f(1.0f, 1.0f, 0.0f, 1.0f);
			}
			else
			{
				glColor4f(m_fireworks[loop].red, m_fireworks[loop].green, m_fireworks[loop].blue, m_fireworks[loop].alpha);
			}

			// Draw the point
			glVertex2f(m_fireworks[loop].x[particleLoop], m_fireworks[loop].y[particleLoop]);
			glEnd();
		}

		// Move the firework appropriately depending on its explosion state
		if (m_fireworks[loop].hasExploded == false)
		{
			m_fireworks[loop].move();
		}
		else
		{
			m_fireworks[loop].explode();
		}
	}

	// ----- Stop Drawing Stuff! ------

	// Take the contents of the current draw buffer and copy it to the accumulation buffer with each pixel modified by a factor
	// The closer the factor is to 1.0f, the longer the trails... Don't exceed 1.0f - you get garbage!
	glAccum(GL_ACCUM, 0.99f);

	glFlush();

	//glfwSwapBuffers(); // Swap the buffers to display the scene (so we don't have to watch it being drawn!)


	glPopMatrix();

	GLenum err = glGetError();
	if (err != 0)
		int x = 666;
}

void ParticleScene::onExit(const SystemInfo&)
{
	//
}
