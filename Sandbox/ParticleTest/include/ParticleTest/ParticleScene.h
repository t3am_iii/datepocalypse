#pragma once

const int FIREWORKS = 15; // Number of fireworks

#include <ParticleTest\Firework.h>

#include <Engine\SceneSystem\BaseScene.h>

using namespace dpEngine;

class ParticleScene : public SceneSystem::BaseScene
{
private:
	// Create our array of fireworks
	Firework m_fireworks[FIREWORKS];

public:
	ParticleScene() { }

	~ParticleScene() { }

	void onEnter(const SystemInfo&);

	void onFadeIn() { }

	void onUpdate(float, const SystemInfo&);

	bool nextScene(int&) const { return false; }

	bool shouldClose() const { return false; }

	void onRender(sf::RenderWindow&);

	void onFadeOut() { }

	void onExit(const SystemInfo&);

};
