#pragma once
#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <SFML/OpenGL.hpp>

sf::Vector2f Diff(sf::Vector2f const& v1, sf::Vector2f const& v2, float const& deltaTime);
float Diff(float const& a1, float const& a2, float const& deltaTime);

float Distance(sf::Vector2f const& v1, sf::Vector2f const& v2);
float Norm(sf::Vector2f const& v);
float determinant(sf::Vector2f const& u, sf::Vector2f const& v);
float dotProduct(sf::Vector2f const& u, sf::Vector2f const& v);

sf::Vector2f rotatePoint(sf::Vector2f const& point, sf::Vector2f const& center, float const& angle);

bool checkSegmentIntersection(sf::Vector2f const& A, sf::Vector2f const& B, sf::Vector2f const& C, sf::Vector2f const& D);
sf::Vector2f getSegmentIntersection(sf::Vector2f const& A, sf::Vector2f const& B, sf::Vector2f const& C, sf::Vector2f const& D);

void drawLine(sf::Vector2f A, sf::Vector2f B, sf::RenderWindow &window, sf::Color color);
void drawLine(sf::Vector2f pos, sf::Vector2f dir, float norm, sf::RenderWindow &window, sf::Color color);

float gaussianFunction(float maxVal, float wideness, float x);

float clamp(float value, float min, float max);
