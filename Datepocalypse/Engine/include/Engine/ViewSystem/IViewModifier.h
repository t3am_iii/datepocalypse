#pragma once

#include <SFML\Graphics\View.hpp>

namespace dpEngine
{
	class IViewModifier
	{
	public:
		virtual ~IViewModifier() { }

		virtual void onViewUpdate(sf::View&, float) = 0;

	};
}
