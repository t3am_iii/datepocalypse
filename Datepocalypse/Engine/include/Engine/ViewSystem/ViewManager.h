#pragma once

#include <SFML\Graphics\RenderWindow.hpp>

#include <vector>
#include <functional>

namespace dpEngine
{
	class IViewModifier;

	class ViewManager
	{
	private:
		std::vector<IViewModifier*> m_modifiers;

		bool m_hasCreatedWindow;
		
		sf::RenderWindow* m_window;
		
		sf::View m_defView;
		
		sf::View m_view;

		sf::View* m_curView;

		bool m_isFullscreen;

		std::string m_title;

		std::function<void(sf::RenderWindow*)> m_callback;

	public:
		ViewManager(std::function<void(sf::RenderWindow*)>, unsigned int, unsigned int, std::string);

		void addModifier(IViewModifier*);

		void removeModifier(IViewModifier*);

		void resetCurrentView();

		void setFullscreen(bool);

		void createWindow(unsigned int, unsigned int, bool, std::string);

		void setupDefaultWindow();

		void update(float);

	};
}
