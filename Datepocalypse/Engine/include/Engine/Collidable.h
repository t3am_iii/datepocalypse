#pragma once

namespace dpEngine {
	namespace CollisionSystem { class Body; }
	namespace CollisionSystem { class BaseShape; }

	class Collidable
	{
	public:
		virtual ~Collidable() { }

	protected:
		CollisionSystem::Body* body;

		CollisionSystem::BaseShape* shape;

	};
}
