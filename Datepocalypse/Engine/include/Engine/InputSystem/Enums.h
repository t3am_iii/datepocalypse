#pragma once

namespace dpEngine {
	enum class EInputState
	{
		DoubleClick,
		Pressed,
		Down,
		Released,
		Up
	};
}
