#pragma once

#include <SFML\Window\Mouse.hpp>
#include <SFML\System\Vector2.hpp>

#include <array>

namespace dpEngine { enum class EInputState; }

namespace dpEngine { namespace InputSystem {
	class Mouse
	{
	private:
		sf::Vector2f m_previousFramePosition;

		sf::Vector2f m_position;

		std::array<EInputState, 3> m_states;

	public:
		Mouse();

		~Mouse();

		void onMouseButtonEvent(const sf::Mouse::Button&, bool, int, int);

		void onMouseMoved(int, int);

		const sf::Vector2f& getPreviousFramePosition();

		const sf::Vector2f& getPosition();

		const EInputState& getState(const sf::Mouse::Button&);

		void swap();

	};
} }
#pragma once
