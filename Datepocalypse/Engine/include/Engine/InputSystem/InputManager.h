#pragma once

#include <SFML\Window\Keyboard.hpp>
#include <SFML\Window\Mouse.hpp>

namespace dpEngine {
	namespace InputSystem { class Keyboard; }
	namespace InputSystem { class Mouse; }

	class InputManager
	{
	private:
		InputSystem::Keyboard* m_keyboard;

		InputSystem::Mouse* m_mouse;

	public:
		InputManager(InputSystem::Keyboard*, InputSystem::Mouse*);

		~InputManager();

		bool wasMouseButtonPressed(const sf::Mouse::Button&);

		bool wasMouseButtonReleased(const sf::Mouse::Button&);

		bool isMouseButtonDown(const sf::Mouse::Button&);

		bool wasKeyPressed(const sf::Keyboard::Key&);

		bool wasKeyReleased(const sf::Keyboard::Key&);

		bool isKeyDown(const sf::Keyboard::Key&);

		int getLatestKeyIDPressed();

		char getLatestKeyChar();

		bool wasKeyPressed(bool=true);

	};
}
