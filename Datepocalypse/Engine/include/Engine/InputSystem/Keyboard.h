#pragma once

#include <SFML\Window\Keyboard.hpp>

#include <array>

namespace dpEngine { enum class EInputState; }

namespace dpEngine { namespace InputSystem {
	class Keyboard
	{
	private:
		std::array<EInputState, sf::Keyboard::Key::KeyCount> m_states;

		int m_latestKey;

		bool m_wasKeyPressed;

	public:
		Keyboard();

		~Keyboard();

		void onKeyEvent(const sf::Keyboard::Key&, bool, int, int);

		EInputState getState(const sf::Keyboard::Key&);

		int getLatestKeyIDPressed();

		bool wasKeyPressed(bool);

		void swap();

	};
} }
