#pragma once

#include <SFML\System\Vector2.hpp>
#include <SFML\System\Vector3.hpp>

#define _USE_MATH_DEFINES
#include <math.h>

namespace dpEngine {
	class FMath
	{
	public:
		static sf::Vector2f Normalize(const sf::Vector2f& source)
		{
			float length = sqrt((source.x * source.x) + (source.y * source.y));
			if (length != 0)
				return sf::Vector2f(source.x / length, source.y / length);
			else
				return source;
		}

		static sf::Vector2f Abs(const sf::Vector2f& source)
		{
			return sf::Vector2f(fabsf(source.x), fabsf(source.y));
		}

		static sf::Vector2f Angle(float source)
		{
			return sf::Vector2f(cos(source * M_PI / 180.0)
				, sin(source * M_PI / 180.0));
		}

		static float Degrees(const sf::Vector2f& source)
		{
			return atan2f(source.y, source.x) * 180.0 / M_PI;
		}

		static float Distance(const sf::Vector2f& lhs, const sf::Vector2f& rhs)
		{
			return (lhs - rhs).x + (lhs - rhs).y;
		}

		static float Length(const sf::Vector2f& source)
		{
			return fabsf(source.x) + fabsf(source.y);
		}

		static int Sign(const float& source)
		{
			return (source < 0.0f) ? -1 : (source > 0.0f) ? 1 : 0;
		}

		static sf::Vector2i Sign(const sf::Vector2f& source)
		{
			return sf::Vector2i(FMath::Sign(source.x), FMath::Sign(source.y));
		}

		static float Clamp(const float& val, const float& min, const float& max)
		{
			return (val < min) ? min : (val > max) ? max : val;
		}
		
		static sf::Vector2f Clamp(const sf::Vector2f& val, const sf::Vector2f& min, const sf::Vector2f& max)
		{
			return sf::Vector2f(Clamp(val.x, min.x, max.x), Clamp(val.y, min.y, max.y));
		}

		static float Lerp(float source, float target, float t)
		{
			return source * (1.0f - t) + target * t;
		}

		static sf::Vector2f Lerp(const sf::Vector2f& source, const sf::Vector2f& target, float t)
		{
			return source * (1.0f - t) + target * t;
		}

		static sf::Vector3f Lerp(const sf::Vector3f& source, const sf::Vector3f& target, float t)
		{
			return source * (1.0f - t) + target * t;
		}

	};
}
