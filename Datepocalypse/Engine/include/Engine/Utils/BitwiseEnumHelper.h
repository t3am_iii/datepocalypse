#pragma once

/*#include <string>

#define BITWISE_ENUM_BEGIN(NAME) \
	enum class NAME { \

#define BITWISE_ENUM_ADD(KEY, VALUE, STRING) \
	KEY = VALUE, \

#define BITWISE_ENUM_END() \
		}; \*/


#define __BIT_TYPE_ unsigned __int16

#define BITWISE_ENUM_DECLARE(NAME) \
	NAME operator~(const NAME&); \
	\
	bool operator==(const NAME&, const int&); \
	\
	NAME operator|(const NAME&, const NAME&); \
	\
	void operator|=(NAME&, const NAME&); \
	\
	NAME operator&(const NAME&, const NAME&); \
	\
	void operator&=(NAME&, const NAME&); \
	/*\
	operator __BIT_TYPE_(NAME& source); \
	\*/
	//std::string ToString(const NAME& state); \

#define BITWISE_ENUM_DEFINE(NAME) \
	NAME operator~(const NAME& other) \
	{ \
		return static_cast<NAME>(~static_cast<__BIT_TYPE_>(other)); \
	} \
	\
	bool operator==(const NAME& lhs, const int& rhs) \
	{ \
		return static_cast<__BIT_TYPE_>(lhs) == static_cast<__BIT_TYPE_>(rhs); \
	} \
	\
	NAME operator|(const NAME& lhs, const NAME& rhs) \
	{ \
		return static_cast<NAME>(static_cast<__BIT_TYPE_>(lhs) | static_cast<__BIT_TYPE_>(rhs)); \
	} \
	\
	void operator|=(NAME& lhs, const NAME& rhs) \
	{ \
		lhs = lhs | rhs; \
	} \
	\
	NAME operator&(const NAME& lhs, const NAME& rhs) \
	{ \
		return static_cast<NAME>(static_cast<__BIT_TYPE_>(lhs)& static_cast<__BIT_TYPE_>(rhs)); \
	} \
	\
	void operator&=(NAME& lhs, const NAME& rhs) \
	{ \
		lhs = lhs & rhs; \
	} \
	\
	/*operator __BIT_TYPE_(NAME& source) \
		{ \
		return static_cast<__BIT_TYPE_>(source); \
		} \
	\*/
	/*std::string ToString(const NAME& other) \
	{ \
		return "Unknown"; \
	} \*/
