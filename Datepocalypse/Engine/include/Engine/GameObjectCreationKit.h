#pragma once

namespace dpEngine {
	namespace CollisionSystem { class CollisionManager; }
	struct GameObjectCreationKit
	{
		CollisionSystem::CollisionManager* collisionMngr;
	};
}
