#pragma once

#include <Engine\Services\IEntityService.h>
#include <Engine\GameObjectCreationKit.h>

#include <vector>

namespace dpEngine { struct SystemInfo; }

namespace dpEngine { namespace EntitySystem {
	class EntityManager : public IEntityService
	{
	private:
		GameObjectCreationKit kit;

		std::vector<GameObject*> m_gameObjects;

	public:
		EntityManager(const SystemInfo&);

		void addEntity(GameObject*);

		void removeEntity(GameObject*);

		void onRender(sf::RenderTarget&, sf::RenderStates) const;

	};
} }
