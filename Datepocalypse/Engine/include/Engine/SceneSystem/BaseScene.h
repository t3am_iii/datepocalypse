#pragma once

namespace dpEngine { struct SystemInfo; }

namespace sf { class RenderWindow; }

namespace dpEngine { namespace SceneSystem {
	class BaseScene
	{
	public:
		virtual void onEnter(const SystemInfo&) = 0;

		virtual void onFadeIn() = 0;
		
		virtual void onUpdate(float, const SystemInfo&) = 0;

		virtual bool nextScene(int&) const = 0;

		virtual bool shouldClose() const = 0;

		virtual void onRender(sf::RenderWindow&) = 0;

		virtual void onFadeOut() = 0;
		
		virtual void onExit(const SystemInfo&) = 0;

	};
} }
