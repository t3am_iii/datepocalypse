#pragma once

#include <vector>

namespace dpEngine { struct SystemInfo; }

namespace sf { class RenderWindow; }

namespace dpEngine { namespace SceneSystem {
	class BaseScene;

	class SceneManager
	{
	private:
		const SystemInfo& m_sysInfo;

		std::vector<BaseScene*> m_scenes;

		int m_activeScene;

	public:
		SceneManager(const SystemInfo&);

		~SceneManager();

		void update(float);

		void render(sf::RenderWindow&);

		bool shouldClose() const;

		void changeScene(int);

		int addScene(BaseScene*);

	};
} }
