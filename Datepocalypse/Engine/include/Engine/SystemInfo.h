#pragma once

namespace dpEngine {
	namespace CollisionSystem { class CollisionManager; }
	class InputManager;
	class ViewManager;

	namespace SceneSystem { class SceneManager; }

	struct SystemInfo
	{
		const unsigned int screenWidth;

		const unsigned int screenHeight;
		
		CollisionSystem::CollisionManager* collisionMngr;

		InputManager* inputMngr;

		ViewManager* viewMngr;

		SceneSystem::SceneManager* sceneMngr;

		SystemInfo(const unsigned int screenWidth, const unsigned int screenHeight)
			: screenWidth(screenWidth), screenHeight(screenHeight)
		{ /* EMPTY */ }
	};
}
