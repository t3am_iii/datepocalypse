#pragma once

#include <SFML\Graphics\Drawable.hpp>
#include <SFML\Graphics\Transformable.hpp>

namespace dpEngine {
	struct GameObjectCreationKit;

	class GameObject : public sf::Drawable, public sf::Transformable
	{
	public:
		virtual ~GameObject() { }

		virtual void onConstruction(const GameObjectCreationKit&) = 0;

		virtual void onDestruction(const GameObjectCreationKit&) = 0;

	};
}
