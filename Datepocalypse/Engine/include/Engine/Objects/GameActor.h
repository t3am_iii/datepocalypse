#pragma once

#include <Engine\Objects\GameObject.h>

namespace dpEngine {
	namespace CollisionSystem { class Body; }
	namespace CollisionSystem { class BaseShape; }

	class GameActor : public GameObject
	{
	protected:
		CollisionSystem::Body* body;

		CollisionSystem::BaseShape* shape;

	};
}
