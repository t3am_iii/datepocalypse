#pragma once

#include <Engine\Collidable.h>
#include <Engine\Objects\GameObject.h>

namespace dpEngine {
	class WorldObject : public GameObject, public Collidable
	{
	public:
		virtual ~WorldObject() { }

		void setPosition(float, float);

		void setPosition(const sf::Vector2f);

		void addPosition(float, float);

		void addPosition(const sf::Vector2f);

		const sf::Vector2f& getPosition() const;

	};
}
