#pragma once

namespace dpEngine { class ContactListener; }

namespace dpEngine { namespace CollisionSystem {
	class BroadPhase;
	class Body;
	struct AABB;
	struct CollisionInfo;
	struct CollisionReport;

	class CollisionCore
	{
	private:
		BroadPhase* m_broadPhase;

		ContactListener* m_handler;

	public:
		static bool CheckAABB(const CollisionInfo&, CollisionReport&);

		static bool CheckCircle(const CollisionInfo&, CollisionReport&);

		static bool CheckCircleAABB(const CollisionInfo&, CollisionReport&);

		CollisionCore(BroadPhase*);

		~CollisionCore();

		void check();

		void setContactHandler(ContactListener*);

	private:
		void resolveCollision(const CollisionInfo&, Body*, Body*, const CollisionReport&);

		void resolveSensor(const CollisionInfo&);

	};
} }
