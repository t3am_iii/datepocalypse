#pragma once

#include <Engine\CollisionSystem\Data\Pair.h>

#include <vector>

namespace dpEngine { namespace CollisionSystem {
	struct PhysicsInfo;
	class PhysicsWorld;
	class Body;
	enum class EBodyTypes;
	struct AABB;

	class BroadPhase
	{
	private:
		std::vector<Body*> m_staticBodies;

		std::vector<Body*> m_kinematicBodies;

	public:
		BroadPhase();

		~BroadPhase();

		void addBody(Body*);

		void removeBody(Body*);

		void queryCollisionPairs(std::vector<Pair>&);

		//void queryFilteredBodies(const EBodyTypes&, std::vector<Body*>&);

	private:
		bool checkCollisionPair(Body*, Body*);

	};
} }
