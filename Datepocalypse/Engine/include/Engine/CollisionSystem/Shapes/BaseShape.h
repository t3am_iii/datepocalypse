#pragma once

namespace dpEngine { namespace CollisionSystem {
	enum class EShapeTypes;

	class BaseShape
	{
	public:
		virtual EShapeTypes getType() const = 0;

	};
} }
