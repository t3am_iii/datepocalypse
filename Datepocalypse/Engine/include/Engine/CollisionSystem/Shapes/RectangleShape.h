#pragma once

#include <Engine\CollisionSystem\Shapes\BaseShape.h>
#include <Engine\CollisionSystem\Enums.h>

namespace dpEngine { namespace CollisionSystem {
	struct RectangleShape : public BaseShape
	{
		float width;

		float height;

		RectangleShape(float width, float height)
			: width(width)
			, height(height)
		{ /* EMPTY */ }

		EShapeTypes getType() const { return EShapeTypes::kRectangle; }
	};
} }
