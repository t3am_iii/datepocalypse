#pragma once

#include <Engine\CollisionSystem\Shapes\BaseShape.h>
#include <Engine\CollisionSystem\Enums.h>

namespace dpEngine { namespace CollisionSystem {
	struct CircleShape : public BaseShape
	{
		float radius;

		CircleShape(float radius)
			: radius(radius)
		{ /* EMPTY */ }

		EShapeTypes getType() const { return EShapeTypes::kCircle; }
	};
} }
