#pragma once

namespace dpEngine {
	struct ContactInfo;

	class ContactListener
	{
	public:
		virtual ~ContactListener() { }

		//virtual void onBeginContact(const ContactInfo&) = 0;

		//virtual void onContinuousContact(const ContactInfo&) = 0;

		//virtual void onEndContact(const ContactInfo&) = 0;

		virtual void onContact(const ContactInfo&) = 0;

	};
}
