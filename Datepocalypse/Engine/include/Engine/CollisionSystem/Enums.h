#pragma once

namespace dpEngine { namespace CollisionSystem {
	enum class EBodyTypes
	{
		kNone,
		kStaticBody,
		kKinematicBody,
		//kDynamicBody, // We don't use real physics anyways =P
	};

	enum class EShapeTypes
	{
		kRectangle,
		kCircle,
		kTriangle,
		kPoint,
		kLine,
	};
} }
