#pragma once

#include <Engine\CollisionSystem\BroadPhase.h>
#include <Engine\CollisionSystem\CollisionCore.h>

namespace dpEngine { class ContactListener; }

namespace sf { class Transformable; }

namespace dpEngine { namespace CollisionSystem {
	struct BodyDef;
	class Body;

	class CollisionManager
	{
	private:
		CollisionCore m_collisionCore;

		BroadPhase m_broadPhase;

	public:
		~CollisionManager();

		Body* createBody(const BodyDef&, sf::Transformable*);

		void destroyBody(Body*);

		void setContactHandler(ContactListener*);

	private:
		friend class Engine;
		CollisionManager(CollisionCore*&);

	};
} }
