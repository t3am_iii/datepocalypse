#pragma once

#include <Engine\CollisionSystem\Data\Filter.h>

namespace dpEngine { namespace CollisionSystem {
	enum class EBodyTypes;
	class BaseShape;

	struct BodyDef
	{
		EBodyTypes bodyType;

		Filter filter;

		bool isSensor;

		BaseShape* shape;

		void* userData;

		BodyDef(const EBodyTypes& bodyType, const Filter& filter, bool isSensor, BaseShape* shape)
			: BodyDef(bodyType, filter, isSensor, shape, nullptr)
		{ /* EMPTY */ }
		
		BodyDef(const EBodyTypes& bodyType, const Filter& filter, bool isSensor, BaseShape* shape, void* userData)
			: bodyType(bodyType)
			, filter(filter)
			, isSensor(isSensor)
			, shape(shape)
			, userData(userData)
		{ /* EMPTY */ }
	};
} }
