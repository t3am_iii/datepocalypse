#pragma once

#include <Engine\CollisionSystem\Data\AABB.h>
#include <Engine\CollisionSystem\Data\Filter.h>

#include <SFML\Graphics\Transformable.hpp>

namespace dpEngine { namespace CollisionSystem {
	enum class EBodyTypes;
	class BaseShape;

	class Body
	{
	private:
		BaseShape* m_shape;

		EBodyTypes m_type;

		Filter m_filter;

		const bool m_isSensor;

		void* m_userData;

		bool m_isActive;
		
		bool m_isAABBDirty;

		AABB m_cachedAABB;

		sf::Transformable* m_transformable;

	public:
		~Body();

		void computeAABB(AABB&);

		void setPosition(float, float);

		void addPosition(float, float);

		void setActive(bool);

		bool isSensor() const;

		bool isActive() const;

		bool hasMoved() const;

		const sf::Vector2f& getPosition() const;

		BaseShape* getShape() const;

		//const sf::Transform getTransform() const;

		EBodyTypes getType() const;

		const Filter& getFilter() const;

		void* getUserData() const;

	private:
		friend class CollisionManager;
		Body(BaseShape*, const EBodyTypes&, const Filter&, bool, void*, sf::Transformable*);

	};
} }
