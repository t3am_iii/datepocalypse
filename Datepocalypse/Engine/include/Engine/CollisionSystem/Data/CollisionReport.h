#pragma once

#include <SFML\System\Vector2.hpp>

namespace dpEngine { namespace CollisionSystem {
	struct CollisionReport
	{
		sf::Vector2f overlap[2];
	};
} }
