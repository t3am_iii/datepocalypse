#pragma once

#include <SFML\System\Vector2.hpp>

namespace dpEngine { namespace CollisionSystem {
	struct AABB
	{
		sf::Vector2f lowerBound;

		sf::Vector2f upperBound;

		static bool Intersects(const AABB& lhs, const AABB& rhs)
		{
			return !(lhs.lowerBound.x > rhs.upperBound.x || rhs.lowerBound.x > lhs.upperBound.x
				|| lhs.lowerBound.y > rhs.upperBound.y || rhs.lowerBound.y > lhs.upperBound.y);
		}
	};
} }
