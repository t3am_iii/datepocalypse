#pragma once

namespace dpEngine {
	namespace CollisionSystem { class Body; }

	struct ContactInfo
	{
		const CollisionSystem::Body* leftBody;

		const CollisionSystem::Body* rightBody;
	};
}
