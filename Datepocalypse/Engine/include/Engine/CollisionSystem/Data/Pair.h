#pragma once

#include <Engine\CollisionSystem\Data\AABB.h>

namespace dpEngine { namespace CollisionSystem {
	class Body;

	struct Pair
	{
		struct {
			Body* body;
			AABB aabb;
		} left;

		struct {
			Body* body;
			AABB aabb;
		} right;

		Pair(Body* leftBody, const AABB& leftAABB, Body* rightBody, const AABB& rightAABB)
		{
			left.body = leftBody;
			left.aabb = leftAABB;
			right.body = rightBody;
			right.aabb = rightAABB;
		}
	};
} }
