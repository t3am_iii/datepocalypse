#pragma once

namespace dpEngine { namespace CollisionSystem {
	struct Filter
	{
		const unsigned int maskBits;

		const unsigned int categoryBits;

		Filter(const unsigned int& categoryBits, const unsigned int& maskBits)
			: categoryBits(categoryBits)
			, maskBits(maskBits)
		{ }
	};
} }
