#pragma once

#include <Engine\CollisionSystem\Data\Pair.h>

namespace dpEngine { namespace CollisionSystem {
	struct CollisionInfo
	{
		struct {
			const Body* body;
			AABB aabb;
		} left;

		struct {
			const Body* body;
			AABB aabb;
		} right;

		bool isBothNonStatic;

		CollisionInfo(const Pair& source)
		{
			left.body = source.left.body;
			left.aabb = source.left.aabb;
			right.body = source.right.body;
			right.aabb = source.right.aabb;
		}
	};
} }
