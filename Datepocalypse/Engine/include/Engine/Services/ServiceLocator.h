#pragma once

namespace dpEngine {
	class IEntityService;

	class ServiceLocator
	{
	private:
		static IEntityService* m_entityService;

	public:
		static void provideEntityService(IEntityService*);

		static IEntityService* getEntityService();

	};
}
