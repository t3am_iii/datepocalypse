#pragma once

#include <SFML\Graphics\RenderTarget.hpp>

namespace dpEngine {
	class GameObject;

	class IEntityService
	{
	public:
		virtual ~IEntityService() { }

		virtual void addEntity(GameObject*) = 0;

		virtual void removeEntity(GameObject*) = 0;

		virtual void onRender(sf::RenderTarget&, sf::RenderStates) const = 0;

	};
}
