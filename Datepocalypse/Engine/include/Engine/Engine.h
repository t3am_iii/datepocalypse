#pragma once

#pragma comment(lib, "opengl32.lib")

#include <Engine\InputSystem\Keyboard.h>
#include <Engine\InputSystem\Mouse.h>

#include <string>

namespace sf { class RenderWindow; }

namespace dpEngine {
	struct SystemInfo;
	class ViewManager;
	namespace SceneSystem { class BaseScene; }
	namespace CollisionSystem { class CollisionCore; }

	class Engine
	{
	private:
		sf::RenderWindow* m_window;

		SystemInfo* m_sysInfo;

		InputSystem::Keyboard m_keyboard;

		InputSystem::Mouse m_mouse;
		
		CollisionSystem::CollisionCore* m_collisionCore;

		ViewManager* m_viewMngr;

		bool m_isRunning;

	public:
		Engine();
		
		~Engine();

		void startup(unsigned int, unsigned int, const std::string&);

		void addScene(SceneSystem::BaseScene*, bool=false);

		void run();

		void shutdown();
		
	private:
		bool processEvents();

		bool update(float);

		void render();

	};
}
