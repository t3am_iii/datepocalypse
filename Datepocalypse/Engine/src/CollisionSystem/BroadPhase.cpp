#include <Engine\CollisionSystem\BroadPhase.h>
#include <Engine\CollisionSystem\Body.h>
#include <Engine\CollisionSystem\Enums.h>
#include <Engine\CollisionSystem\Shapes\BaseShape.h>
#include <Engine\CollisionSystem\Data\AABB.h>
#include <Engine\Utils\Macros.h>

namespace dpEngine { namespace CollisionSystem {
	BroadPhase::BroadPhase() { }

	BroadPhase::~BroadPhase() { }

	void BroadPhase::addBody(Body* body)
	{
		if (body->getType() == EBodyTypes::kStaticBody)
		{
			auto it = std::find(m_staticBodies.begin(), m_staticBodies.end(), body);
			dp_ASSERT(it == m_staticBodies.end());
			m_staticBodies.push_back(body);
		}
		else if (body->getType() == EBodyTypes::kKinematicBody)
		{
			auto it = std::find(m_kinematicBodies.begin(), m_kinematicBodies.end(), body);
			dp_ASSERT(it == m_kinematicBodies.end());
			m_kinematicBodies.push_back(body);
		}
	}

	void BroadPhase::removeBody(Body* body)
	{
		if (body->getType() == EBodyTypes::kStaticBody)
		{
			auto it = std::find(m_staticBodies.begin(), m_staticBodies.end(), body);
			dp_ASSERT(it != m_staticBodies.end());
			m_staticBodies.erase(it);
		}
		else if (body->getType() == EBodyTypes::kKinematicBody)
		{
			auto it = std::find(m_kinematicBodies.begin(), m_kinematicBodies.end(), body);
			dp_ASSERT(it != m_kinematicBodies.end());
			m_kinematicBodies.erase(it);
		}
	}

	void BroadPhase::queryCollisionPairs(std::vector<Pair>& pairs)
	{
		AABB a;
		AABB b;

		for (auto outerIt = m_kinematicBodies.begin(); outerIt != m_kinematicBodies.end(); outerIt++)
		{
			for (auto innerIt = m_staticBodies.begin(); innerIt != m_staticBodies.end(); innerIt++)
			{
				auto lhs = (*outerIt);
				auto rhs = (*innerIt);
				
				if (!checkCollisionPair(lhs, rhs))
					continue;

				lhs->computeAABB(a);
				rhs->computeAABB(b);

				if (AABB::Intersects(a, b))
					pairs.push_back(Pair(lhs, a, rhs, b));
			}
		}

		int offset = 0;
		for (auto outerIt = m_kinematicBodies.begin(); outerIt != m_kinematicBodies.end(); outerIt++)
		{
			offset++;
			for (auto innerIt = std::next(outerIt, 1); innerIt != m_kinematicBodies.end(); innerIt++)
			{
				auto lhs = (*outerIt);
				auto rhs = (*innerIt);

				if (!checkCollisionPair(lhs, rhs))
					continue;
				
				lhs->computeAABB(a);
				rhs->computeAABB(b);

				if (AABB::Intersects(a, b))
					pairs.push_back(Pair(lhs, a, rhs, b));
			}
		}
	}

	bool BroadPhase::checkCollisionPair(Body* lhs, Body* rhs)
	{
		if (!lhs->isActive() || !rhs->isActive())
			return false;

		if (!lhs->hasMoved() && !rhs->hasMoved())
			return false;

		if ((lhs->getFilter().maskBits & rhs->getFilter().categoryBits) == 0
			|| (lhs->getFilter().categoryBits & rhs->getFilter().maskBits) == 0)
			return false;

		// TODO: Layering, bitmask

		return true;
	}
} }
