#include <Engine\CollisionSystem\CollisionCore.h>
#include <Engine\CollisionSystem\BroadPhase.h>
#include <Engine\CollisionSystem\Body.h>
#include <Engine\CollisionSystem\ContactListener.h>
#include <Engine\CollisionSystem\Enums.h>
#include <Engine\CollisionSystem\Data\Pair.h>
#include <Engine\CollisionSystem\Data\CollisionInfo.h>
#include <Engine\CollisionSystem\Data\CollisionReport.h>
#include <Engine\CollisionSystem\Data\ContactInfo.h>
#include <Engine\CollisionSystem\Shapes\BaseShape.h>
#include <Engine\CollisionSystem\Shapes\RectangleShape.h>
#include <Engine\CollisionSystem\Shapes\CircleShape.h>
#include <Engine\Utils\Macros.h>
#include <Engine\Utils\Math.h>

#include <math.h>
#include <vector>

namespace dpEngine { namespace CollisionSystem {
	bool CollisionCore::CheckAABB(const CollisionInfo& info, CollisionReport& report)
	{
		// TODO: New function for Separated Axis Theorem

		const sf::Vector2f leftSize = info.left.aabb.upperBound - info.left.aabb.lowerBound;
		const sf::Vector2f rightSize = info.right.aabb.upperBound - info.right.aabb.lowerBound;

		const sf::Vector2f leftCenter = info.left.aabb.lowerBound + leftSize / 2.0f;
		const sf::Vector2f rightCenter = info.right.aabb.lowerBound + rightSize / 2.0f;

		const sf::Vector2f centerDelta = { leftCenter.x - rightCenter.x, leftCenter.y - rightCenter.y };
		const sf::Vector2f delta = { (leftSize.x / 2.0f + rightSize.x / 2.0f) - std::abs(centerDelta.x), (leftSize.y / 2.0f + rightSize.y / 2.0f) - std::abs(centerDelta.y) };

		if (delta.y < delta.x)
			report.overlap[0] = { 0.0f, delta.y * (centerDelta.y < 0 ? -1 : 1) };
		else if (delta.y > delta.x)
			report.overlap[0] = { delta.x * (centerDelta.x < 0 ? -1 : 1), 0.0f };
		else
			report.overlap[0] = {
			delta.x * (centerDelta.x < 0 ? -1 : 1),
			delta.y * (centerDelta.y < 0 ? -1 : 1)
		};

		if (info.isBothNonStatic)
		{
			report.overlap[0] /= 2.0f;
			report.overlap[1] = report.overlap[0];
		}
		/*else
			dp_ASSERT(rhs->getType() == EBodyTypes::kStaticBody);*/

		return true;
	}

	bool CollisionCore::CheckCircle(const CollisionInfo& info, CollisionReport& report)
	{
		const sf::Vector2f lpos(info.left.body->getPosition());
		const sf::Vector2f rpos(info.right.body->getPosition());
		const auto lc = static_cast<CircleShape*>(info.left.body->getShape());
		const auto rc = static_cast<CircleShape*>(info.right.body->getShape());

		const double DIST = sqrt(((lpos.x - rpos.x) * (lpos.x - rpos.x)) + ((lpos.y - rpos.y) * (lpos.y - rpos.y)));
		const double SUM_OF_RADII = lc->radius + rc->radius;
		const float DEPTH = (const float)(SUM_OF_RADII - DIST);

		if (DEPTH <= 0.0f)
			return false;

		if (info.isBothNonStatic)
		{
			report.overlap[0] = FMath::Normalize(lpos - rpos) * (DEPTH / 2.0f);
			report.overlap[1] = FMath::Normalize(rpos - lpos) * (DEPTH / 2.0f);
		}
		else
		{
			dp_ASSERT(info.right.body->getType() == EBodyTypes::kStaticBody);
			report.overlap[0] = FMath::Normalize(lpos - rpos) * DEPTH;
		}

		return true;
	}

	bool CollisionCore::CheckCircleAABB(const CollisionInfo& info, CollisionReport& report)
	{
		/*// Get center point circle first 
		glm::vec2 center(one.Position + one.Radius);

		// Calculate AABB info (center, half-extents)
		glm::vec2 aabb_half_extents(two.Size.x / 2, two.Size.y / 2);
		glm::vec2 aabb_center(
			two.Position.x + aabb_half_extents.x,
			two.Position.y + aabb_half_extents.y
			);

		// Get difference vector between both centers
		glm::vec2 difference = center - aabb_center;
		glm::vec2 clamped = glm::clamp(difference, -aabb_half_extents, aabb_half_extents);

		// Add clamped value to AABB_center and we get the value of box closest to circle
		glm::vec2 closest = aabb_center + clamped;

		// Retrieve vector between center circle and closest point AABB and check if length <= radius
		difference = closest - center;

		return glm::length(difference) < one.Radius;*/

		const Body* circleBody = (info.left.body->getShape()->getType() == EShapeTypes::kCircle) ? info.left.body : info.right.body;
		const Body* rectangleBody = (info.left.body->getShape()->getType() == EShapeTypes::kRectangle) ? info.left.body : info.right.body;

		sf::Vector2f center(circleBody->getPosition());

		auto r = static_cast<RectangleShape*>(rectangleBody->getShape());
		sf::Vector2f aabbHalfExtends(r->width / 2.0f, r->height / 2.0f);
		sf::Vector2f aabbCenter(rectangleBody->getPosition() + aabbHalfExtends);

		sf::Vector2f diff(center - aabbCenter);
		sf::Vector2f clamped(FMath::Clamp(diff, -aabbHalfExtends, aabbHalfExtends));

		sf::Vector2f closest(aabbCenter + clamped);

		diff = closest - center;
		
		auto c = static_cast<CircleShape*>(circleBody->getShape());
		if (!(FMath::Length(diff) < c->radius))
			return false;

		if (info.isBothNonStatic)
		{
			//dp_ASSERT(false);

			report.overlap[0] = { 0, 0 };
			report.overlap[1] = { 0, 0 };
			return true;
		}
		else
		{
			if (circleBody->getType() != EBodyTypes::kStaticBody) // Move ONLY circle
			{
				auto dir(FMath::Normalize(diff));
				report.overlap[0] = diff - dir * c->radius;
			}
			else // Move ONLY rectangle
			{
				const sf::Vector2f leftSize(r->width, r->height);
				const sf::Vector2f rightSize(c->radius * 2.0f, c->radius * 2.0f);
				const sf::Vector2f leftCenter(rectangleBody->getPosition() + (leftSize / 2.0f));
				const sf::Vector2f rightCenter(circleBody->getPosition());

				const sf::Vector2f centerDelta = { leftCenter.x - rightCenter.x, leftCenter.y - rightCenter.y };
				const sf::Vector2f delta = {
					(leftSize.x / 2.0f + rightSize.x / 2.0f) - std::abs(centerDelta.x),
					(leftSize.y / 2.0f + rightSize.y / 2.0f) - std::abs(centerDelta.y)
				};

				if (delta.y < delta.x)
					report.overlap[0] = { 0.0f, delta.y * (centerDelta.y < 0 ? -1 : 1) };
				else if (delta.y > delta.x)
					report.overlap[0] = { delta.x * (centerDelta.x < 0 ? -1 : 1), 0.0f };
				else
					report.overlap[0] = {
						delta.x * (centerDelta.x < 0 ? -1 : 1),
						delta.y * (centerDelta.y < 0 ? -1 : 1)
					};
				
				//report.overlap[0] -= sf::Vector2f(c->radius, 0.0f);
			}
		}

		return true;
	}

	CollisionCore::CollisionCore(BroadPhase* broadPhase)
		: m_broadPhase(broadPhase)
	{ /* EMPTY */ }

	CollisionCore::~CollisionCore() { /* EMPTY */ }

	void CollisionCore::check()
	{
		std::vector<Pair> pairs;
		m_broadPhase->queryCollisionPairs(pairs);

		for (auto pair : pairs)
		{
			dp_ASSERT(pair.left.body != nullptr);
			dp_ASSERT(pair.right.body != nullptr);
			
			const EShapeTypes lst = pair.left.body->getShape()->getType();
			const EShapeTypes rst = pair.right.body->getShape()->getType();
			
			const EBodyTypes ldt = pair.left.body->getType();
			const EBodyTypes rdt = pair.right.body->getType();
			CollisionInfo info(pair);
			info.isBothNonStatic = (ldt != EBodyTypes::kStaticBody && rdt != EBodyTypes::kStaticBody);
			CollisionReport report;

			if (lst == EShapeTypes::kRectangle && rst == EShapeTypes::kRectangle)
			{
				if (pair.left.body->isSensor() || pair.right.body->isSensor())
					resolveSensor(info);
				else
				{
					if (CollisionCore::CheckAABB(info, report))
						resolveCollision(info, pair.left.body, pair.right.body, report);
				}
			}
			else if (lst == EShapeTypes::kCircle && rst == EShapeTypes::kCircle)
			{
				if (CollisionCore::CheckCircle(info, report))
				{
					if (pair.left.body->isSensor() || pair.right.body->isSensor())
						resolveSensor(info);
					else
						resolveCollision(info, pair.left.body, pair.right.body, report);
				}
			}
			else if (lst == EShapeTypes::kCircle && rst == EShapeTypes::kRectangle
				|| lst == EShapeTypes::kRectangle && rst == EShapeTypes::kCircle)
			{
				if (CollisionCore::CheckCircleAABB(info, report))
				{
					if (pair.left.body->isSensor() || pair.right.body->isSensor())
						resolveSensor(info);
					else
						resolveCollision(info, pair.left.body, pair.right.body, report);
				}
			}
		}
	}

	void CollisionCore::setContactHandler(ContactListener* handler) { m_handler = handler; }

	void CollisionCore::resolveCollision(const CollisionInfo& info, Body* lhs, Body* rhs, const CollisionReport& report)
	{
		EBodyTypes lType = lhs->getType();
		EBodyTypes rType = rhs->getType();

		if (info.isBothNonStatic)
		{
			lhs->addPosition(report.overlap[0].x, report.overlap[0].y);
			rhs->addPosition(report.overlap[1].x, report.overlap[1].y);
		}
		else
		{
			dp_ASSERT(lType != EBodyTypes::kStaticBody);
			dp_ASSERT(rType == EBodyTypes::kStaticBody);
			lhs->addPosition(report.overlap[0].x, report.overlap[0].y);
		}

		
		if (m_handler == nullptr)
			return;

		ContactInfo contact;
		contact.leftBody = info.left.body;
		contact.rightBody = info.right.body;
		m_handler->onContact(contact);
	}

	void CollisionCore::resolveSensor(const CollisionInfo& info)
	{
		if (m_handler == nullptr)
			return;
		
		ContactInfo contact;
		contact.leftBody = info.left.body;
		contact.rightBody = info.right.body;
		m_handler->onContact(contact);
	}
} }
