#pragma once

#include <Engine\CollisionSystem\Body.h>
#include <Engine\CollisionSystem\Enums.h>
#include <Engine\CollisionSystem\Data\AABB.h>
#include <Engine\CollisionSystem\Shapes\RectangleShape.h>
#include <Engine\CollisionSystem\Shapes\CircleShape.h>
#include <Engine\Utils\Macros.h>

namespace dpEngine { namespace CollisionSystem {
	Body::Body(BaseShape* shape, const EBodyTypes& type, const Filter& filter, bool isSensor, void* userData, sf::Transformable* transformable)
		: m_shape(shape)
		, m_type(type)
		, m_filter(filter)
		, m_isSensor(isSensor)
		, m_isActive(true)
		, m_userData(userData)
		, m_isAABBDirty(true)
		, m_transformable(transformable)
	{ /* EMPTY */ }

	Body::~Body() { /* EMPTY */ }

	void Body::computeAABB(AABB& aabb)
	{
		if (m_isAABBDirty)
		{
			dp_ASSERT(m_shape != nullptr);
			const auto& type = m_shape->getType();
			if (type == EShapeTypes::kRectangle)
			{
				const auto r = static_cast<RectangleShape*>(m_shape);
				m_cachedAABB.lowerBound = getPosition();
				m_cachedAABB.upperBound = getPosition() + sf::Vector2f(r->width, r->height);
			}
			else if (type == EShapeTypes::kCircle)
			{
				const auto c = static_cast<CircleShape*>(m_shape);
				const sf::Vector2f halfSize(c->radius, c->radius);
				m_cachedAABB.lowerBound = getPosition() - halfSize;
				m_cachedAABB.upperBound = getPosition() + halfSize;
			}

			m_isAABBDirty = false;
		}

		aabb = m_cachedAABB;
	}

	void Body::setPosition(float x, float y)
	{
		dp_ASSERT(m_type != EBodyTypes::kStaticBody);
		m_transformable->setPosition(x, y);
		m_isAABBDirty = true;
	}

	void Body::addPosition(float x, float y)
	{
		dp_ASSERT(m_type != EBodyTypes::kStaticBody);
		m_transformable->move(x, y);
		m_isAABBDirty = true;
	}

	void Body::setActive(bool value) { m_isActive = value; }

	bool Body::isSensor() const { return m_isSensor; }

	bool Body::isActive() const { return m_isActive; }

	bool Body::hasMoved() const { return true/*m_isAABBDirty*/; } // TODO: New variable???

	const sf::Vector2f& Body::getPosition() const { return m_transformable->getPosition(); }

	BaseShape* Body::getShape() const { return m_shape; }

	//const sf::Transform Body::getTransform() const { return Transformable::getTransform(); }

	EBodyTypes Body::getType() const { return m_type; }

	const Filter& Body::getFilter() const { return m_filter; }

	void* Body::getUserData() const { return m_userData; }
} }
