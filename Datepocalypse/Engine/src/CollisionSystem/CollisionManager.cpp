#include <Engine\CollisionSystem\CollisionManager.h>
#include <Engine\CollisionSystem\BodyDef.h>
#include <Engine\CollisionSystem\Body.h>

namespace dpEngine { namespace CollisionSystem {
	CollisionManager::CollisionManager(CollisionCore*& collisionCoreRef)
		: m_collisionCore(&m_broadPhase)
	{
		collisionCoreRef = &m_collisionCore;
	}

	CollisionManager::~CollisionManager() { /* EMPTY */ }

	Body* CollisionManager::createBody(const BodyDef& bodyDef, sf::Transformable* transformable)
	{
		Body* newBody = new Body(bodyDef.shape, bodyDef.bodyType, bodyDef.filter, bodyDef.isSensor, bodyDef.userData, transformable);
		m_broadPhase.addBody(newBody);
		return newBody;
	}

	void CollisionManager::destroyBody(Body* body)
	{
		m_broadPhase.removeBody(body);
		delete body;
	}

	void CollisionManager::setContactHandler(ContactListener* handler) { m_collisionCore.setContactHandler(handler); }
} }
