#include <Engine\Engine.h>
#include <Engine\SystemInfo.h>
#include <Engine\SceneSystem\SceneManager.h>
#include <Engine\InputSystem\InputManager.h>
#include <Engine\ViewSystem\ViewManager.h>
#include <Engine\EntitySystem\EntityManager.h>
#include <Engine\CollisionSystem\CollisionManager.h>
#include <Engine\CollisionSystem\CollisionCore.h>
#include <Engine\Services\ServiceLocator.h>

#include <SFML\OpenGL.hpp>
#include <SFML\Window\Event.hpp>
#include <SFML\Graphics\RenderWindow.hpp>

#include <math.h>

namespace dpEngine {
	Engine::Engine()
		: m_isRunning(false)
	{ /* EMPTY */ }

	Engine::~Engine() { /* EMPTY */ }

	void Engine::startup(unsigned int screenWidth, unsigned int screenHeight, const std::string& title)
	{
		m_viewMngr = new ViewManager([this](sf::RenderWindow* newWindow) { m_window = newWindow; }, screenWidth, screenWidth, title);
		//m_viewMngr->createWindow(screenWidth, screenHeight, false, title);

		m_sysInfo = new SystemInfo(screenWidth, screenHeight);
		m_sysInfo->viewMngr = m_viewMngr;
		m_sysInfo->inputMngr = new InputManager(&m_keyboard, &m_mouse);
		m_sysInfo->collisionMngr = new CollisionSystem::CollisionManager(m_collisionCore);
		m_sysInfo->sceneMngr = new SceneSystem::SceneManager(*m_sysInfo);

		// TODO: Delete services on engine shutdown
		ServiceLocator::provideEntityService(new EntitySystem::EntityManager(*m_sysInfo));

		m_isRunning = true;
	}

	void Engine::addScene(SceneSystem::BaseScene* scene, bool isEntryScene)
	{
		auto idx = m_sysInfo->sceneMngr->addScene(scene);
		if (isEntryScene)
			m_sysInfo->sceneMngr->changeScene(idx);
	}

	void Engine::run()
	{
		m_viewMngr->resetCurrentView();

		
		const double TARGET_RATE = 1.0 / 60.0;
		// const double COLLISION_RATE = 0.5;

		double lastTime = 0.0;
		//double lastCollisionCheck = 0.0;
		double currentTime = 0.0;
		double accumulator = 0.0;
		sf::Clock clock;
		bool isClosing = false;

		while (m_isRunning)
		{
			if (!m_window->isOpen())
				continue;

			currentTime = clock.getElapsedTime().asSeconds();
			double deltaTime = std::min(currentTime - lastTime, 0.25);
			lastTime = currentTime;

			accumulator += deltaTime;

			while (accumulator >= TARGET_RATE)
			{
				if (!processEvents())
					isClosing = true;
				else if (!update(TARGET_RATE))
					isClosing = true;

				if (isClosing)
					break;

				m_viewMngr->update(TARGET_RATE);

				accumulator -= TARGET_RATE;
			}

			// TODO: Update AI
			m_collisionCore->check();


			if (isClosing)
			{
				m_window->close();
				m_isRunning = false;
			}
			
			
			render();
		}
	}
	
	void Engine::shutdown()
	{
		m_isRunning = false;

		delete m_sysInfo->collisionMngr;
		m_sysInfo->collisionMngr = nullptr;

		delete m_sysInfo->inputMngr;
		m_sysInfo->inputMngr = nullptr;

		delete m_sysInfo->sceneMngr;
		m_sysInfo->sceneMngr = nullptr;

		delete m_sysInfo->viewMngr;
		m_sysInfo->viewMngr = nullptr;

		delete m_sysInfo;
		m_sysInfo = nullptr;

		delete m_window;
		m_window = nullptr;
	}

	bool Engine::processEvents()
	{
		sf::Event ev;
		while (m_window->pollEvent(ev))
		{
			if (ev.type == sf::Event::KeyPressed)
				m_keyboard.onKeyEvent(ev.key.code, true, 0, 0);
			else if (ev.type == sf::Event::KeyReleased)
				m_keyboard.onKeyEvent(ev.key.code, false, 0, 0);
			else if (ev.type == sf::Event::MouseButtonPressed)
				m_mouse.onMouseButtonEvent(ev.mouseButton.button, true, ev.mouseButton.x, ev.mouseButton.y);
			else if (ev.type == sf::Event::MouseButtonReleased)
				m_mouse.onMouseButtonEvent(ev.mouseButton.button, false, ev.mouseButton.x, ev.mouseButton.y);
			else if (ev.type == sf::Event::MouseMoved)
				m_mouse.onMouseMoved(ev.mouseMove.x, ev.mouseMove.y);
			else if (ev.type == sf::Event::Resized)
			{
				// adjust the viewport when the window is resized
				glViewport(0, 0, ev.size.width, ev.size.height);
			}
			else if (ev.type == sf::Event::Closed)
				return false;
		}

		return true;
	}

	bool Engine::update(float deltaTime)
	{
		m_sysInfo->sceneMngr->update(deltaTime);
		if (m_sysInfo->sceneMngr->shouldClose())
			return false;

		m_keyboard.swap();
		m_mouse.swap();

		return true;
	}

	void Engine::render()
	{
		m_window->clear();
		m_sysInfo->sceneMngr->render(*m_window);
		ServiceLocator::getEntityService()->onRender(*m_window, sf::RenderStates::Default);
		m_window->display();
	}
}
