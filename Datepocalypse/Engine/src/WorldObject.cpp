#include <Engine\Objects\WorldObject.h>
#include <Engine\CollisionSystem\Body.h>
#include <Engine\Utils\Macros.h>

namespace dpEngine {
	void WorldObject::setPosition(float x, float y)
	{
		dp_ASSERT(body != nullptr);
		body->setPosition(x, y);
	}

	void WorldObject::setPosition(const sf::Vector2f pos)
	{
		dp_ASSERT(body != nullptr);
		body->setPosition(pos.x, pos.y);
	}

	void WorldObject::addPosition(float x, float y)
	{
		dp_ASSERT(body != nullptr);
		body->addPosition(x, y);
	}

	void WorldObject::addPosition(const sf::Vector2f pos)
	{
		dp_ASSERT(body != nullptr);
		body->addPosition(pos.x, pos.y);
	}

	const sf::Vector2f& WorldObject::getPosition() const { return body->getPosition(); }
}
