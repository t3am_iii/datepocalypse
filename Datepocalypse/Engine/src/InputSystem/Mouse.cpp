#include <Engine\InputSystem\Mouse.h>
#include <Engine\InputSystem\Enums.h>

namespace dpEngine { namespace InputSystem {
	Mouse::Mouse()
		: m_position(0.0f, 0.0f), m_previousFramePosition(0.0f, 0.0f)
	{
		for (int idx = 0; idx < 3; idx++)
			m_states[idx] = EInputState::Up;
	}

	Mouse::~Mouse() { /* EMPTY */ }

	void Mouse::onMouseButtonEvent(const sf::Mouse::Button& button, bool wasPressed, int x, int y)
	{
		if (wasPressed)
			m_states[static_cast<int>(button)] = EInputState::Pressed;
		else
			m_states[static_cast<int>(button)] = EInputState::Released;

		m_previousFramePosition = m_position;
		m_position = sf::Vector2f(x, y);
	}

	void Mouse::onMouseMoved(int x, int y)
	{
		m_previousFramePosition = m_position;
		m_position = sf::Vector2f(x, y);
	}

	const sf::Vector2f& Mouse::getPreviousFramePosition()
	{
		return m_previousFramePosition;
	}

	const sf::Vector2f& Mouse::getPosition()
	{
		return m_position;
	}

	const EInputState& Mouse::getState(const sf::Mouse::Button& button)
	{
		return m_states[static_cast<int>(button)];
	}

	void Mouse::swap()
	{
		for (int idx = 0; idx < 3; idx++)
		{
			if (m_states[idx] == EInputState::Pressed)
				m_states[idx] = EInputState::Down;
			else if (m_states[idx] == EInputState::Released)
				m_states[idx] = EInputState::Up;
		}
	}
} }
