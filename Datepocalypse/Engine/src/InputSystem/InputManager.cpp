#include <Engine\InputSystem\InputManager.h>
#include <Engine\InputSystem\Keyboard.h>
#include <Engine\InputSystem\Mouse.h>
#include <Engine\InputSystem\Enums.h>

namespace dpEngine {
	InputManager::InputManager(InputSystem::Keyboard* keyboard, InputSystem::Mouse* mouse)
		: m_keyboard(keyboard), m_mouse(mouse)
	{ /* EMPTY */ }

	InputManager::~InputManager() { /* EMPTY */ }

	bool InputManager::wasMouseButtonPressed(const sf::Mouse::Button& button)
	{
		return m_mouse->getState(button) == EInputState::Pressed;
	}

	bool InputManager::wasMouseButtonReleased(const sf::Mouse::Button& button)
	{
		return m_mouse->getState(button) == EInputState::Pressed;
	}

	bool InputManager::isMouseButtonDown(const sf::Mouse::Button& button)
	{
		return m_mouse->getState(button) == EInputState::Pressed || m_mouse->getState(button) == EInputState::Down;
	}

	bool InputManager::wasKeyPressed(const sf::Keyboard::Key& key)
	{
		return m_keyboard->getState(key) == EInputState::Pressed;
	}

	bool InputManager::wasKeyReleased(const sf::Keyboard::Key& key)
	{
		return m_keyboard->getState(key) == EInputState::Released;
	}

	bool InputManager::isKeyDown(const sf::Keyboard::Key& key)
	{
		return m_keyboard->getState(key) == EInputState::Pressed || m_keyboard->getState(key) == EInputState::Down;
	}

	int InputManager::getLatestKeyIDPressed()
	{
		return m_keyboard->getLatestKeyIDPressed();
	}

	char InputManager::getLatestKeyChar()
	{
		char keyChar = '?';

		switch (getLatestKeyIDPressed())
		{
		case 0: keyChar = 'A'; break;
		case 1: keyChar = 'B'; break;
		case 2: keyChar = 'C'; break;
		case 3: keyChar = 'D'; break;
		case 4: keyChar = 'E'; break;
		case 5: keyChar = 'F'; break;
		case 6: keyChar = 'G'; break;
		case 7: keyChar = 'H'; break;
		case 8: keyChar = 'I'; break;
		case 9: keyChar = 'J'; break;
		case 10: keyChar = 'K'; break;
		case 11: keyChar = 'L'; break;
		case 12: keyChar = 'M'; break;
		case 13: keyChar = 'N'; break;
		case 14: keyChar = 'O'; break;
		case 15: keyChar = 'P'; break;
		case 16: keyChar = 'Q'; break;
		case 17: keyChar = 'R'; break;
		case 18: keyChar = 'S'; break;
		case 19: keyChar = 'T'; break;
		case 20: keyChar = 'U'; break;
		case 21: keyChar = 'V'; break;
		case 22: keyChar = 'W'; break;
		case 23: keyChar = 'X'; break;
		case 24: keyChar = 'Y'; break;
		case 25: keyChar = 'Z'; break;
		case 26: keyChar = '0'; break;
		case 27: keyChar = '1'; break;
		case 28: keyChar = '2'; break;
		case 29: keyChar = '3'; break;
		case 30: keyChar = '4'; break;
		case 31: keyChar = '5'; break;
		case 32: keyChar = '6'; break;
		case 33: keyChar = '7'; break;
		case 34: keyChar = '8'; break;
		case 35: keyChar = '9'; break;
		//case 57: keyChar = ' '; break;
		}
		return keyChar;
	}

	bool InputManager::wasKeyPressed(bool consume)
	{
		if (getLatestKeyChar() == '?')
			return false;

		return m_keyboard->wasKeyPressed(consume);
	}
}
