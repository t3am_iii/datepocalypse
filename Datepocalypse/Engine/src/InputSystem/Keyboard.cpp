#include <Engine\InputSystem\Keyboard.h>
#include <Engine\InputSystem\Enums.h>

#include <iostream>

namespace dpEngine { namespace InputSystem {
	Keyboard::Keyboard()
	{
		for (int idx = 0; idx < sf::Keyboard::Key::KeyCount; idx++)
			m_states[idx] = EInputState::Up;
	}

	Keyboard::~Keyboard() { /* EMPTY */ }

	void Keyboard::onKeyEvent(const sf::Keyboard::Key& key, bool wasPressed, int mod, int repeat)
	{
		const int keyCode = static_cast<int>(key);
		const int maxKeyCode = static_cast<int>(sf::Keyboard::Key::KeyCount);
		if (keyCode > -1 && keyCode < maxKeyCode)
		{
			if (wasPressed)
			{
				if (repeat == 0)
				{
					m_states[static_cast<int>(key)] = EInputState::Pressed;
					m_latestKey = keyCode;
					m_wasKeyPressed = true;
				}
			}
			else
			{
				if (repeat == 0)
					m_states[static_cast<int>(key)] = EInputState::Released;
			}
		}
		else
		{
			std::cout << "[Keyboard] Invalid key code: " << keyCode << " (max: " << maxKeyCode << ")" << std::endl;
		}
	}

	EInputState Keyboard::getState(const sf::Keyboard::Key& key)
	{
		return m_states[static_cast<int>(key)];
	}

	int Keyboard::getLatestKeyIDPressed()
	{
		return m_latestKey;
	}

	bool Keyboard::wasKeyPressed(bool consume)
	{
		bool val = m_wasKeyPressed;
		if (consume)
			m_wasKeyPressed = false;
		return val;
	}

	void Keyboard::swap()
	{
		for (int idx = 0; idx < sf::Keyboard::Key::KeyCount; idx++)
		{
			if (m_states[idx] == EInputState::Pressed)
				m_states[idx] = EInputState::Down;
			else if (m_states[idx] == EInputState::Released)
				m_states[idx] = EInputState::Up;
		}
	}
} }
