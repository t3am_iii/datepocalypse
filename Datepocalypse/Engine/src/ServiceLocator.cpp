#include <Engine\Services\ServiceLocator.h>

namespace dpEngine {
	IEntityService* ServiceLocator::m_entityService = nullptr;

	void ServiceLocator::provideEntityService(IEntityService* service)
	{
		m_entityService = service;
	}

	IEntityService* ServiceLocator::getEntityService()
	{
		return m_entityService;
	}
}
