#include <Engine\ViewSystem\ViewManager.h>
#include <Engine\ViewSystem\IViewModifier.h>

namespace dpEngine
{
	ViewManager::ViewManager(std::function<void(sf::RenderWindow*)> callback, unsigned int width, unsigned int height, std::string title)
		: m_window(nullptr)
		, m_callback(callback)
		, m_title(title)
	{
		setupDefaultWindow();
	}
	
	void ViewManager::addModifier(IViewModifier* modifier)
	{
		auto it = std::find(m_modifiers.begin(), m_modifiers.end(), modifier);
		if (it == m_modifiers.end())
			m_modifiers.push_back(modifier);
	}

	void ViewManager::removeModifier(IViewModifier* modifier)
	{
		auto it = std::find(m_modifiers.begin(), m_modifiers.end(), modifier);
		if (it != m_modifiers.end())
			m_modifiers.erase(it);
	}

	void ViewManager::resetCurrentView()
	{
		m_defView = m_window->getDefaultView();
		m_curView = &m_defView;
		m_window->setView(*m_curView);
	}

	void ViewManager::setFullscreen(bool value)
	{
		if (m_isFullscreen != value)
		{
			auto size = m_window->getSize();
			createWindow(size.x, size.y, value, m_title);
		}
	}

	void ViewManager::createWindow(unsigned int width, unsigned int height, bool fullscreen, std::string title)
	{
		sf::ContextSettings settings;
		auto style = fullscreen ? sf::Style::Fullscreen : sf::Style::Default;
		auto mode = sf::VideoMode(width, height);
		
		if (m_isFullscreen && !mode.isValid())
			mode = sf::VideoMode::getFullscreenModes()[0];
		m_window->create(mode, title, style, settings);

		m_isFullscreen = fullscreen;
		
		m_callback(m_window);
		m_view = m_defView;
		m_curView = &m_view;
	}

	void ViewManager::setupDefaultWindow()
	{
		sf::VideoMode::getDesktopMode();


		m_window = new sf::RenderWindow(sf::VideoMode(1920, 1080), m_title, sf::Style::Fullscreen);
		m_isFullscreen = true;
		m_callback(m_window);
		
		m_defView = m_window->getDefaultView();
		m_curView = &m_defView;
	}

	void ViewManager::update(float deltaTime)
	{
		for (auto mod : m_modifiers)
			mod->onViewUpdate(*m_curView, deltaTime);

		m_window->setView(*m_curView);
	}
}
