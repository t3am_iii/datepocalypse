#include <Engine\EntitySystem\EntityManager.h>
#include <Engine\Objects\GameObject.h>
#include <Engine\SystemInfo.h>

namespace dpEngine { namespace EntitySystem {
	EntityManager::EntityManager(const SystemInfo& sysInfo)
	{
		kit.collisionMngr = sysInfo.collisionMngr;
	}
	
	void EntityManager::addEntity(GameObject* gameObject)
	{
		auto it = std::find(m_gameObjects.begin(), m_gameObjects.end(), gameObject);
		if (it == m_gameObjects.end())
		{
			gameObject->onConstruction(kit);
			m_gameObjects.push_back(gameObject);
		}
	}

	void EntityManager::removeEntity(GameObject* gameObject)
	{
		auto it = std::find(m_gameObjects.begin(), m_gameObjects.end(), gameObject);
		if (it != m_gameObjects.end())
		{
			gameObject->onDestruction(kit);
			delete (*it);
			m_gameObjects.erase(it);
		}
	}

	void EntityManager::onRender(sf::RenderTarget& target, sf::RenderStates states) const
	{
		/*for (auto go : m_gameObjects)
			target.draw(*go, states);*/
	}
} }
