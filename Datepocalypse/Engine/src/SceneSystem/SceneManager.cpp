#include <Engine\SceneSystem\SceneManager.h>
#include <Engine\SceneSystem\BaseScene.h>
#include <Engine\ViewSystem\ViewManager.h>
#include <Engine\SystemInfo.h>

namespace dpEngine { namespace SceneSystem {
	SceneManager::SceneManager(const SystemInfo& sysInfo)
		: m_sysInfo(sysInfo)
		, m_activeScene(-1)
	{ /* EMPTY */ }

	SceneManager::~SceneManager()
	{
		for (auto s : m_scenes)
			delete s;
		m_scenes.clear();
	}
	
	void SceneManager::update(float deltaTime)
	{
		if (m_activeScene != -1)
		{
			m_scenes[m_activeScene]->onUpdate(deltaTime, m_sysInfo);
		}

		int newIdx = -1;
		if (m_scenes[m_activeScene]->nextScene(newIdx))
		{
			changeScene(newIdx);
		}
	}

	bool SceneManager::shouldClose() const { return m_scenes[m_activeScene]->shouldClose(); }

	void SceneManager::render(sf::RenderWindow& window)
	{
		if (m_activeScene != -1)
		{
			m_scenes[m_activeScene]->onRender(window);
		}
	}

	void SceneManager::changeScene(int index)
	{
		if (index < 0 || index >= m_scenes.size())
			return; // Invalid index, do an early return.

		if (m_activeScene != -1)
			m_scenes[m_activeScene]->onExit(m_sysInfo);

		m_activeScene = index;
		m_scenes[m_activeScene]->onEnter(m_sysInfo);
		m_sysInfo.viewMngr->resetCurrentView();
	}

	int SceneManager::addScene(BaseScene* scene)
	{
		auto it = std::find(m_scenes.begin(), m_scenes.end(), scene);
		if (it != m_scenes.end())
			return -1; // Scene already added, -1 can be used as an error check.

		m_scenes.push_back(scene);
		return m_scenes.size() - 1; // Return index to the new scene, push_back adds the scene to the end of the vector
									// so we can safetly return the size - 1.
	}
} }
