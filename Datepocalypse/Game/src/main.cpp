#include <SFML\Graphics.hpp>

int main(int argc, char** argv)
{
	sf::RenderWindow window(sf::VideoMode(200, 200), "SFML works!");
	sf::CircleShape shape(20.f);
	shape.setFillColor(sf::Color::Green);

	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window.close();
		}

		sf::RenderStates s;
		s.transform = sf::Transform::Identity;
		s.transform.scale(1.0f, 1.0f, 0.5f, 0.5f).translate(50.0f, 50.0f);
		
		window.clear();
		window.draw(shape, s);
		window.display();
	}

	return 0;
}
